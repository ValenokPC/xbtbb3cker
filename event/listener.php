<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\event;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class listener implements EventSubscriberInterface
{
	/** @var \ppk\xbtbb3cker\core\xbtbb3cker */
	protected $xbt_functions;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var string table_prefix */
	protected $table_prefix;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var string phpEx */
	protected $php_ext;

	/** @var string phpBB root path */
	protected $root_path;

	/** @var \phpbb\cache\service */
	protected $cache;

	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\template\template */
	protected $template;

	protected $forum_tracker;

	protected $search_torrents;
	protected $forum_torrents;

	protected $topic_torrents;
	protected $topic_posters;
	protected $topic_screenshots;

	protected $torrents_cleanup;
	protected $torrents_hashes;
	protected $torrents_remote;
	protected $torrents_broken;
	protected $torrents_bookmark;

	protected $dt;

	protected $is_candownload;
	protected $is_candowntorr;
	protected $is_candownpostscr;
	protected $is_canviewvtstats;
	protected $is_cansetstatus;
	protected $is_cansetrequpratio;
	protected $is_canskiptrestricts;

	protected $is_admod;

	protected $user_ratio;
	protected $user_uploaded;
	protected $user_downloaded;
	protected $user_tracker_data;
	protected $user_tracker_options;
	protected $user_restricts;

	protected $tracker_css;
	protected $only_trackers;
	protected $post_torrent;
	protected $torrent_statuses;
	protected $torrents_size;
	protected $status_data;


	public function __construct(\ppk\xbtbb3cker\core\xbtbb3cker $functions, \phpbb\config\config $config, \phpbb\user $user, \phpbb\request\request_interface $request, $table_prefix, \phpbb\db\driver\driver_interface $db, $php_ext, $root_path, \phpbb\cache\service $cache, \phpbb\auth\auth $auth, \phpbb\template\template $template)
	{
		$this->xbt_functions = $functions;
		$this->config = $config;
		$this->user = $user;
		$this->request = $request;
		$this->table_prefix = $table_prefix;
		$this->db = $db;
		$this->php_ext = $php_ext;
		$this->root_path = $root_path;
		$this->cache = $cache;
		$this->auth = $auth;
		$this->template = $template;

		$this->torrents_remote = array();

		define('XBT_CONFIG', 'xbt_config');
		define('XBT_ANNOUNCE_LOG', $this->config['ppkbb_xctable_announce_log']);
		//define('XBT_DENY_FROM_HOSTS', $this->config['ppkbb_xctable_deny_from_hosts']);
		//define('XBT_DENY_FROM_CLIENTS', $this->config['ppkbb_xctable_deny_from_clients']);
		define('XBT_FILES', $this->config['ppkbb_xctable_files']);
		define('XBT_FILES_USERS', $this->config['ppkbb_xctable_files_users']);
		define('XBT_SCRAPE_LOG', $this->config['ppkbb_xctable_scrape_log']);
		define('XBT_USERS', $this->config['ppkbb_xctable_users']);

		define('TRACKER_FILES_TABLE', $table_prefix . 'tracker_files');
		define('TRACKER_DOWNLOADS_TABLE', $table_prefix . 'tracker_downloads');
		define('TRACKER_RTRACK_TABLE', $table_prefix . 'tracker_rtrack');
		define('TRACKER_RTRACKERS_TABLE', $table_prefix . 'tracker_rtrackers');
		define('TRACKER_RANNOUNCES_TABLE', $table_prefix . 'tracker_rannounces');
		define('TRACKER_CRON_TABLE', $table_prefix . 'tracker_cron');
		define('TRACKER_IMAGES_TABLE', $table_prefix . 'tracker_images');
		define('TRACKER_STATUSES_TABLE', $table_prefix . 'tracker_statuses');
		define('TRACKER_TRESTRICTS_TABLE', $table_prefix . 'tracker_trestricts');
		define('TRACKER_BOOKMARKS_TABLE', $table_prefix . 'tracker_bookmarks');
		define('TRACKER_PEERS_TABLE', $table_prefix . 'tracker_peers');

	}

	static public function getSubscribedEvents()
	{
		return array(
			'core.modify_uploaded_file' 							=> 'check_uploaded_file',
			'core.submit_post_end' 									=> 'submit_tracker_data',//posting_modify_submit_post_after, submit_post_end

			'core.posting_modify_submit_post_before' 									=> 'submit_tracker_data',
			'core.posting_modify_template_vars' 									=> 'posting_disablefpq_addvars_extpostscr',

			'core.user_setup'							=> 'xbtbb3cker_add_lang',
			'core.user_setup_after' 											=> 'xbtbb3cker_init',
			'core.page_header_after' 											=> 'xbtbb3cker_global_vars',
			'core.adm_page_header' 											=> 'xbtbb3cker_adm_vars',
			'core.index_modify_page_title' 											=> 'index_add_cron_tdt',

			'core.viewtopic_get_post_data' 	=> 'viewtopic_get_torrents',//viewtopic_get_post_data, viewtopic_assign_template_vars_before
			'core.viewtopic_modify_post_row' 					=> 'viewtopic_get_torrent_data',
			'core.viewtopic_post_row_after' 					=> 'viewtopic_add_torrent_template',
			'core.viewtopic_post_rowset_data' 					=> 'viewtopic_add_post_torrent',
			'core.viewtopic_modify_page_title' 					=> 'viewtopic_fix_broken_torrents',
			'core.viewtopic_cache_user_data' 					=> 'viewtopic_change_user_cache',
			'core.viewtopic_modify_post_data' 					=> 'viewtopic_get_users_tracker_data',

			'core.viewforum_get_topic_data' 					=> 'viewforum_get_forum_type_add_tdt',
			'core.viewforum_modify_topics_data' 					=> 'viewforum_get_torrents',
			'core.viewforum_modify_topicrow' 					=> 'viewforum_add_torrent_template',

			'core.search_native_author_count_query_before' 					=> 'search_sql_author_post_torrent',
			'core.search_native_keywords_count_query_before' 					=> 'search_sql_keywords_post_torrent',
			'core.search_native_by_keyword_modify_search_key' 					=> 'search_sql_keywords_add_search_key',
			'core.search_native_by_author_modify_search_key' 					=> 'search_sql_keywords_add_search_key',

			'core.search_mysql_author_query_before' 					=> 'search_sql_author_post_torrent',
			'core.search_mysql_keywords_main_query_before' 					=> 'search_sql_keywords_post_torrent',
			'core.search_mysql_by_keyword_modify_search_key' 					=> 'search_sql_keywords_add_search_key',
			'core.search_mysql_by_author_modify_search_key' 					=> 'search_sql_keywords_add_search_key',

			'core.search_postgres_author_count_query_before' 					=> 'search_sql_author_post_torrent',
			'core.search_postgres_keywords_main_query_before' 					=> 'search_sql_keywords_post_torrent',
			'core.search_postgres_by_keyword_modify_search_key' 					=> 'search_sql_keywords_add_search_key',
			'core.search_postgres_by_author_modify_search_key' 					=> 'search_sql_keywords_add_search_key',

			'core.search_sphinx_keywords_modify_options' 					=> 'search_sphinx_keywords_post_torrent_key',
			'core.search_sphinx_modify_config_data' 					=> 'search_sphinx_change_config',

			'core.search_get_topic_data' 					=> 'search_sql_get_forum_type',
			'core.search_modify_rowset' 					=> 'search_get_torrrents',
			'core.search_modify_tpl_ary' 					=> 'search_add_torrent_template',
			'core.search_modify_param_before' 					=> 'search_exclude_nontrackers',
			'core.search_modify_param_after' 					=> 'search_post_torrent',
			'core.search_modify_url_parameters' 					=> 'search_modify_link',

			'core.move_topics_before_query' 					=> 'move_topic_torrents',
			'core.move_posts_after' 					=> 'move_post_torrents',

			'core.delete_topics_before_query' 					=> 'delete_topic_torrents',
			'core.delete_posts_in_transaction_before' 					=> 'delete_post_torrents',//delete_posts_before_query, delete_posts_in_transaction, delete_posts_after
			'core.delete_attachments_collect_data_before' 					=> 'delete_attachment_torrents',

			'core.memberlist_view_profile' 					=> 'memberlist_usertt_info',
			'core.download_file_send_to_browser_before' 					=> 'check_downloaded_file',

			'core.mcp_view_forum_modify_sql' 					=> 'mcp_view_forum_get_torrent_data',
			'core.mcp_view_forum_modify_topicrow' 					=> 'mcp_view_forum_add_torrent_template',
// 			'core.mcp_topic_review_after_query' 					=> 'mcp_topic_review_get_torrent_data',
// 			'core.mcp_topic_review_modify_row' 					=> 'mcp_topic_review_add_torrent_template',

			'core.permissions'								=> 'add_permissions',


		);
	}


// 	public function mcp_topic_review_get_torrent_data($event)
// 	{
// 		$post_id_list=$event['post_id_list'];
// 	}
//
// 	public function mcp_topic_review_add_torrent_template($event)
// 	{
//
// 	}

	public function mcp_view_forum_get_torrent_data($event)
	{

		$forum_id=$event['forum_id'];

		$sql="SELECT forum_tracker FROM ".FORUMS_TABLE." WHERE forum_id='{$forum_id}'";
		$result=$this->db->sql_query($sql);
		$this->forum_tracker=$this->db->sql_fetchfield('forum_tracker') ? true : false;
		$this->db->sql_freeresult($result);

		if($this->forum_tracker)
		{
			$sql=$event['sql'];

			$result=$this->db->sql_query($sql);
			$topic_list=array();
			while($row=$this->db->sql_fetchrow($result))
			{
				$topic_list[]=$row['topic_id'];
			}
			$this->db->sql_freeresult($result);

			if($topic_list)
			{
				$this->forum_torrents=array();

				$sql='SELECT
					tr.fid torrent_id,
					tr.completed'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_times_completed' : '').' times_completed,
					tr.leechers'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_leechers' : '').' leechers,
					tr.seeders'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_seeders' : '').' seeders,
					tr.completed real_times_completed,
					tr.leechers real_leechers,
					tr.seeders real_seeders,
					'.($this->config['ppkbb_tcenable_rannounces'][0] ? '
					tr.rem_times_completed rem_times_completed,
					tr.rem_leechers rem_leechers,
					tr.rem_seeders rem_seeders,
					' : '').'
					tr.size,
					tr.ctime added,
					tr.private,
					tr.info_hash,
					tr.lastcleanup,
					tr.rem_leechers,
					tr.rem_seeders,
					tr.poster_id,
					tr.rem_times_completed,
					tr.lastremote,
					tr.numfiles,
					tr.topic_id
				FROM '.ATTACHMENTS_TABLE.' a, '.XBT_FILES." tr WHERE a.extension='torrent' AND a.in_message='0' AND ".$this->db->sql_in_set('a.topic_id', $topic_list)." AND a.attach_id=tr.fid";
				$result=$this->db->sql_query($sql);
				while($row=$this->db->sql_fetchrow($result))
				{
					$row['info_hash']=substr($row['info_hash'], 0, 20);
					$this->forum_torrents[$row['topic_id']][$row['torrent_id']]=$row;

				}
				$this->db->sql_freeresult($result);
			}
		}
	}

	public function mcp_view_forum_add_torrent_template($event)
	{
		$row=$event['row'];
		$topic_row=$event['topic_row'];

		if($this->forum_tracker)
		{
			if(isset($this->forum_torrents[$row['topic_id']]))
			{
				if(!$this->tracker_css)
				{
					$this->tracker_css=true;
					$this->template->assign_vars(array(
						'S_HAS_TOPICS_TORRENT'		=> true,
						)
					);
				}
				if($row['torrent_status'] < 1)
				{
					$seeders=0;
					$leechers=0;
					$times_completed=0;
					$size=0;
					$real_seeders=0;
					$real_leechers=0;
					$real_times_completed=0;
					$rem_seeders=0;
					$rem_leechers=0;
					$rem_times_completed=0;
					$torrents=0;
					foreach($this->forum_torrents[$row['topic_id']] as $id => $torrent)
					{
						$seeders+=$torrent['seeders'];
						$leechers+=$torrent['leechers'];
						$times_completed+=$torrent['times_completed'];
						$size+=$torrent['size'];
						$real_seeders+=$torrent['real_seeders'];
						$real_leechers+=$torrent['real_leechers'];
						$real_times_completed+=$torrent['real_times_completed'];
						$rem_seeders+=$torrent['rem_seeders'];
						$rem_leechers+=$torrent['rem_leechers'];
						$rem_times_completed+=$torrent['rem_times_completed'];
						$torrents+=1;
					}
					$torrent_row=array(
						'TORRENT_SEEDERS_VAL'		=> $seeders,
						'TORRENT_LEECHERS_VAL'		=> $leechers,
						'TORRENT_COMPLETED_VAL'		=> $times_completed,
						'TORRENT_SIZE_VAL'		=> get_formatted_filesize($size),
						'TORRENT_HEALTH_VAL'		=> $this->xbt_functions->get_torrent_health($seeders, $leechers),

						'TORRENT_REAL_SEEDERS_VAL'		=> $real_seeders,
						'TORRENT_REAL_LEECHERS_VAL'		=> $real_leechers,
						'TORRENT_REAL_COMPLETED_VAL'		=> $real_times_completed,
						'TORRENT_REM_SEEDERS_VAL'		=> $rem_seeders,
						'TORRENT_REM_LEECHERS_VAL'		=> $rem_leechers,
						'TORRENT_REM_COMPLETED_VAL'		=> $rem_times_completed,

						'TOPIC_TORRENTS_COUNT'		=> $torrents,
					);
					$topic_row['REPLIES'].="<br /><span class=\"my_tt seed\" title=\"".$this->user->lang['TORRENT_SEEDERS']."\">{$seeders}</span>|<span class=\"my_tt leech\" title=\"".$this->user->lang['TORRENT_LEECHERS']."\">{$leechers}</span>|<span class=\"my_tt complet\" title=\"".$this->user->lang['TORRENT_COMPLETED']."\">{$times_completed}</span>";

				}
// 				$topic_row['TOPIC_TITLE']='<strong>'.$this->user->lang['ATTACHMENTS_TORRENT'].'</strong> '.$topic_row['TOPIC_TITLE'];

				$torrent_row['S_HAS_TORRENT']=true;
				$topic_row=array_merge($topic_row, $torrent_row);

				$event['topic_row']=$topic_row;
			}
		}
	}

	public function search_sphinx_change_config($event)
	{
		$new_config_data=$config_data=$event['config_data'];

		foreach($config_data as $k => $v)
		{
			if(preg_match('#(^source source_phpbb_(?:[a-z0-9]+)_main$)#', $k, $key))
			{
				$new_config_data[$key[1]]=array();
				foreach($v as $k2 => $v2)
				{
					$add_key='';
					if($v2[0]=='sql_query')
					{
						$sql_query=str_replace('p.post_visibility,', "p.post_torrent,\n\t\t\t\t\t\tp.post_visibility,", $v2[1]);
						$v2=array('sql_query', $sql_query);
					}
					else if($v2[0]=='sql_attr_uint' && $v2[1]=='poster_id')
					{
						$add_key=array('sql_attr_uint', 'post_torrent');
					}
					$new_config_data[$key[1]][]=$v2;
					$add_key ? $new_config_data[$key[1]][]=$add_key : '';
				}
			}
			else if(preg_match('#(^source source_phpbb_(?:[a-z0-9]+)_delta : source_phpbb_(?:[a-z0-9]+)_main$)#', $k, $key))
			{
				$new_config_data[$key[1]]=array();
				foreach($v as $k2 => $v2)
				{
					if($v2[0]=='sql_query')
					{
						$sql_query=str_replace('p.post_visibility,', "p.post_torrent,\n\t\t\t\t\t\tp.post_visibility,", $v2[1]);
						$v2=array('sql_query', $sql_query);
					}
					$new_config_data[$key[1]][]=$v2;

				}

			}
		}

		$event['config_data']=$new_config_data;
	}

	public function viewtopic_get_users_tracker_data($event)
	{
		if($this->config['ppkbb_torr_blocks'][10])
		{
			$user_cache=$event['user_cache'];

			$user_ids=array_keys($user_cache);

			$sql = 'SELECT uid, downloaded user_downloaded, uploaded user_uploaded
				FROM '.XBT_USERS.'
				WHERE '.$this->db->sql_in_set('uid', $user_ids);
			$result = $this->db->sql_query($sql);
			while($row=$this->db->sql_fetchrow($result))
			{
				$user_cache[$row['uid']]=array_merge($user_cache[$row['uid']], array(
					'user_uploaded' => get_formatted_filesize($row['user_uploaded']),
					'user_downloaded' => get_formatted_filesize($row['user_downloaded']),
					'user_ratio' => $this->xbt_functions->get_ratio($row['user_uploaded'], $row['user_downloaded'], $this->config['ppkbb_tcratio_start']),

					)
				);
			}
			$this->db->sql_freeresult($result);

			$event['user_cache']=$user_cache;
		}

	}

	public function viewtopic_change_user_cache($event)
	{
		if($this->config['ppkbb_torr_blocks'][10])
		{
			$row=$event['row'];
			$user_cache_data=$event['user_cache_data'];
			$poster_id=$event['poster_id'];

			$user_cache_data['user_torrents']=$row['user_torrents'];
			$user_cache_data['user_comments']=$row['user_comments'];

			$event['user_cache_data']=$user_cache_data;
		}
	}

	public function viewtopic_add_post_torrent($event)
	{
		$rowset_data=$event['rowset_data'];
		$row=$event['row'];

		$rowset_data['post_torrent']=$row['post_torrent'];
		$rowset_data['post_poster']=$row['post_poster'];
//  	$rowset_data['torrent_status']=$row['torrent_status'];

		$event['rowset_data']=$rowset_data;
	}

	public function memberlist_usertt_info($event)
	{
		$member=$event['member'];

		$is_canviewmuastatr=$this->auth->acl_get('u_canviewmuastatr') || $member['user_id']==$this->user->data['user_id'] ? 1 : 0;
		$is_canviewmuastatorr=$this->auth->acl_get('u_canviewmuastatorr') || $member['user_id']==$this->user->data['user_id'] ? 1 : 0;

		if($is_canviewmuastatr || $is_canviewmuastatorr)
		{
			$this->user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_memberlist');

			if($is_canviewmuastatr)
			{
				$sql = 'SELECT downloaded user_downloaded, uploaded user_uploaded
					FROM '.XBT_USERS."
					WHERE uid={$member['user_id']}";
				$result = $this->db->sql_query($sql);
				$data = $this->db->sql_fetchrow($result);
				$this->db->sql_freeresult($result);

				if(!$data)
				{
					$data=array('user_downloaded' => 0, 'user_uploaded' => 0);
				}

				$member=array_merge($member, $data);
			}

			$this->xbt_functions->memberlist_tracker_info($member, $is_canviewmuastatr, $is_canviewmuastatorr, $this->is_admod);
		}
	}

	public function search_modify_link($event)
	{
		//$pt=$this->request->variable('pt', '');

		$u_search=$event['u_search'];

		$u_search.='&amp;ot='.$this->only_trackers.($this->post_torrent ? '&amp;pt='.$this->post_torrent : '');

		$event['u_search']=$u_search;
	}

	public function search_post_torrent($event)
	{
		$search_id=$event['search_id'];
		$sql=$event['sql'];
		$show_results=$event['show_results'];

		if($show_results=='posts' && in_array($search_id, array('newposts', 'unanswered')))
		{
			$sql=str_replace('ORDER BY', " AND p.post_torrent='".($search_id=='newposts' ? 0 : 1)."' ORDER BY", $sql);
			$event['sql']=$sql;

		}

		$l_search_title=$event['l_search_title'];

		if($this->only_trackers && $l_search_title)
		{
			$suffix=$this->post_torrent=='t' ? 'TORRENT' : 'COMMENT';

			switch($search_id)
			{
				case 'active_topics':
					$l_search_title='SEARCH_ACTIVE_'.$suffix;
				break;

				case 'unanswered':
					$l_search_title='SEARCH_UNANSWERED_'.$suffix;
				break;

				case 'unreadposts':
					$l_search_title='SEARCH_UNREAD_'.$suffix;
				break;

				case 'newposts':
					$l_search_title='SEARCH_NEW_'.$suffix;
				break;

				case 'egosearch':
					$l_search_title='SEARCH_SELF_'.$suffix;
				break;

			}
			isset($this->user->lang[$l_search_title]) ? $event['l_search_title']=$this->user->lang[$l_search_title] : '';

		}

	}

	public function search_exclude_nontrackers($event)
	{
		$this->only_trackers=$this->request->variable('ot', 0);
		$this->post_torrent=$this->request->variable('pt', '');

		if($this->only_trackers)
		{
			$ex_fid_ary=$event['ex_fid_ary'];

			$sql="SELECT forum_id FROM ".FORUMS_TABLE." WHERE forum_tracker!='1' AND forum_type=".FORUM_POST;
			$result=$this->db->sql_query($sql);
			while($row=$this->db->sql_fetchrow($result))
			{
				if(!in_array($row['forum_id'], $ex_fid_ary))
				{
					$ex_fid_ary[]=$row['forum_id'];
				}
			}
			$this->db->sql_freeresult($result);
			$event['ex_fid_ary']=$ex_fid_ary;
		}
	}

	public function search_add_torrent_template($event)
	{
		$tpl_ary=$event['tpl_ary'];
		$row=$event['row'];

		if(isset($this->search_torrents[$row['topic_id']]))
		{
			if(!$this->tracker_css)
			{
				$this->tracker_css=true;
				$this->template->assign_vars(array(
					'S_HAS_TOPICS_TORRENT'		=> true,
					)
				);
			}
			if($row['torrent_status'] < 1)
			{
				$seeders=0;
				$leechers=0;
				$times_completed=0;
				$size=0;
				$real_seeders=0;
				$real_leechers=0;
				$real_times_completed=0;
				$rem_seeders=0;
				$rem_leechers=0;
				$rem_times_completed=0;
				$torrents=0;
				foreach($this->search_torrents[$row['topic_id']] as $id => $torrent)
				{
					$seeders+=$torrent['seeders'];
					$leechers+=$torrent['leechers'];
					$times_completed+=$torrent['times_completed'];
					$size+=$torrent['size'];
					$real_seeders+=$torrent['real_seeders'];
					$real_leechers+=$torrent['real_leechers'];
					$real_times_completed+=$torrent['real_times_completed'];
					$rem_seeders+=$torrent['rem_seeders'];
					$rem_leechers+=$torrent['rem_leechers'];
					$rem_times_completed+=$torrent['rem_times_completed'];
					$torrents+=1;
				}
				$torrent_row=array(
					'TORRENT_SEEDERS_VAL'		=> $seeders,
					'TORRENT_LEECHERS_VAL'		=> $leechers,
					'TORRENT_COMPLETED_VAL'		=> $times_completed,
					'TORRENT_SIZE_VAL'		=> get_formatted_filesize($size),
					'TORRENT_HEALTH_VAL'		=> $this->xbt_functions->get_torrent_health($seeders, $leechers),

					'TORRENT_REAL_SEEDERS_VAL'		=> $real_seeders,
					'TORRENT_REAL_LEECHERS_VAL'		=> $real_leechers,
					'TORRENT_REAL_COMPLETED_VAL'		=> $real_times_completed,
					'TORRENT_REM_SEEDERS_VAL'		=> $rem_seeders,
					'TORRENT_REM_LEECHERS_VAL'		=> $rem_leechers,
					'TORRENT_REM_COMPLETED_VAL'		=> $rem_times_completed,

					'TOPIC_TORRENTS_COUNT'		=> $torrents,
				);
				$tpl_ary['TOPIC_REPLIES'].="<br /><span class=\"my_tt seed\" title=\"".$this->user->lang['TORRENT_SEEDERS']."\">{$seeders}</span>|<span class=\"my_tt leech\" title=\"".$this->user->lang['TORRENT_LEECHERS']."\">{$leechers}</span>";
				$tpl_ary['TOPIC_VIEWS'].="<br /><span class=\"my_tt complet\" title=\"".$this->user->lang['TORRENT_COMPLETED']."\">{$times_completed}</span>";
			}
			$torrent_row['S_HAS_TORRENT']=true;
			$tpl_ary=array_merge($tpl_ary, $torrent_row);

			$event['tpl_ary']=$tpl_ary;
		}
	}

	public function search_get_torrrents($event)
	{
		$rowset=$event['rowset'];
		$show_results=$event['show_results'];

		if($show_results=='topics')
		{
			$topic_ids=array();

			foreach($rowset as $topic_id => $row)
			{
				if($row['forum_tracker'] && $row['topic_attachment'])
				{
					$topic_ids[$topic_id]=$topic_id;
				}
			}

			if($topic_ids)
			{
				$this->forum_torrents=$this->torrents_cleanup=$this->torrents_hashes=array();

				$sql='SELECT
					tr.fid torrent_id,
					tr.completed'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_times_completed' : '').' times_completed,
					tr.leechers'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_leechers' : '').' leechers,
					tr.seeders'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_seeders' : '').' seeders,
					tr.completed real_times_completed,
					tr.leechers real_leechers,
					tr.seeders real_seeders,
					'.($this->config['ppkbb_tcenable_rannounces'][0] ? '
					tr.rem_times_completed rem_times_completed,
					tr.rem_leechers rem_leechers,
					tr.rem_seeders rem_seeders,
					' : '').'
					tr.size,
					tr.ctime added,
					tr.private,
					tr.info_hash,
					tr.lastcleanup,
					tr.rem_leechers,
					tr.rem_seeders,
					tr.poster_id,
					tr.rem_times_completed,
					tr.lastremote,
					tr.numfiles,
					tr.topic_id
				FROM '.ATTACHMENTS_TABLE.' a, '.XBT_FILES." tr WHERE a.extension='torrent' AND a.in_message='0' AND ".$this->db->sql_in_set('a.topic_id', $topic_ids)." AND a.attach_id=tr.fid";
				$result=$this->db->sql_query($sql);
				$valid_peers_time=intval($this->config['ppkbb_xcannounce_interval']*1.25);
				while($row=$this->db->sql_fetchrow($result))
				{
					$row['info_hash']=substr($row['info_hash'], 0, 20);

					$this->search_torrents[$row['topic_id']][$row['torrent_id']]=$row;

					$this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_tcclean_place'][1] && $this->dt - $row['lastcleanup'] > $valid_peers_time ? $this->torrents_cleanup[$row['torrent_id']]=$row['torrent_id'] : '';
					$rowset[$row['topic_id']]['torrent_status'] < 1 && $this->config['ppkbb_tcenable_rannounces'][0] && $this->config['ppkbb_tcenable_rannounces'][1] ? $this->torrents_hashes[$row['torrent_id']]=$row['info_hash'] : '';
				}
				$this->db->sql_freeresult($result);

				if($this->torrents_hashes)
				{
					$this->torrents_remote=$this->xbt_functions->torrents_remote($this->torrents_hashes);
				}

				$this->xbt_functions->search_torrent_cron(0, $this->torrents_cleanup, $this->torrents_remote);
			}
		}
	}

	public function search_sql_keywords_add_search_key($event)
	{
		$search_key_array=$event['search_key_array'];

		$search_key_array['ot']=$this->request->variable('ot', '');
		$search_key_array['pt']=$this->request->variable('pt', '');

		$event['search_key_array']=$search_key_array;
	}

	public function search_sphinx_keywords_post_torrent_key($event)
	{
		//$pt=$this->request->variable('pt', '');

		if($this->post_torrent)
		{
			//$type=$event['type'];
			$sphinx=$event['sphinx'];

			$sphinx->SetFilter('post_torrent', array($this->post_torrent=='t' ? 1 : 0));

// 			$additional_filters['post_torrent']=($this->post_torrent=='t' ? 1 : 0);

// 			$event['additional_filters']=$additional_filters;
		}

	}

	public function search_sql_keywords_post_torrent($event)
	{
		//$pt=$this->request->variable('pt', '');

		if($this->post_torrent)
		{
			if(isset($event['sql_where']))
			{
				$sql_where=$event['sql_where'];
				$sql_where[]="p.post_torrent='".($this->post_torrent=='t' ? 1 : 0)."'";

				$event['sql_where']=$sql_where;
			}
			$sql_match_where=$event['sql_match_where'];

			$sql_match_where.=" AND p.post_torrent='".($this->post_torrent=='t' ? 1 : 0)."'";

			$event['sql_match_where']=$sql_match_where;
		}
	}

	public function search_sql_author_post_torrent($event)
	{
		//$pt=$this->request->variable('pt', '');

		if($this->post_torrent)
		{
			$sql_author=$event['sql_author'];

			$sql_author.=" AND p.post_torrent='".($this->post_torrent=='t' ? 1 : 0)."'";

			$event['sql_author']=$sql_author;
		}
	}

	public function search_sql_get_forum_type($event)
	{
		$sql_select=$event['sql_select'];
		$sql_select.=', f.forum_tracker';

		$event['sql_select']=$sql_select;
	}

	public function viewforum_add_torrent_template($event)
	{
		$topic_row=$event['topic_row'];
		$row=$event['row'];

		if(isset($this->forum_torrents[$row['topic_id']]))
		{
			if(!$this->tracker_css)
			{
				$this->tracker_css=true;
				$this->template->assign_vars(array(
					'S_HAS_TOPICS_TORRENT'		=> true,
					)
				);
			}

			if($row['torrent_status'] < 1)
			{
				$seeders=0;
				$leechers=0;
				$times_completed=0;
				$size=0;
				$real_seeders=0;
				$real_leechers=0;
				$real_times_completed=0;
				$rem_seeders=0;
				$rem_leechers=0;
				$rem_times_completed=0;
				$torrents=0;
				foreach($this->forum_torrents[$row['topic_id']] as $id => $torrent)
				{
					$seeders+=$torrent['seeders'];
					$leechers+=$torrent['leechers'];
					$times_completed+=$torrent['times_completed'];
					$size+=$torrent['size'];
					$real_seeders+=$torrent['real_seeders'];
					$real_leechers+=$torrent['real_leechers'];
					$real_times_completed+=$torrent['real_times_completed'];
					$rem_seeders+=$torrent['rem_seeders'];
					$rem_leechers+=$torrent['rem_leechers'];
					$rem_times_completed+=$torrent['rem_times_completed'];
					$torrents+=1;
				}
				$torrent_row=array(
					'TORRENT_SEEDERS_VAL'		=> $seeders,
					'TORRENT_LEECHERS_VAL'		=> $leechers,
					'TORRENT_COMPLETED_VAL'		=> $times_completed,
					'TORRENT_SIZE_VAL'		=> get_formatted_filesize($size),
					'TORRENT_HEALTH_VAL'		=> $this->xbt_functions->get_torrent_health($seeders, $leechers),

					'TORRENT_REAL_SEEDERS_VAL'		=> $real_seeders,
					'TORRENT_REAL_LEECHERS_VAL'		=> $real_leechers,
					'TORRENT_REAL_COMPLETED_VAL'		=> $real_times_completed,
					'TORRENT_REM_SEEDERS_VAL'		=> $rem_seeders,
					'TORRENT_REM_LEECHERS_VAL'		=> $rem_leechers,
					'TORRENT_REM_COMPLETED_VAL'		=> $rem_times_completed,

					'TOPIC_TORRENTS_COUNT'		=> $torrents,
				);
				$topic_row['REPLIES'].="<br /><span class=\"my_tt seed\" title=\"".$this->user->lang['TORRENT_SEEDERS']."\">{$seeders}</span>|<span class=\"my_tt leech\" title=\"".$this->user->lang['TORRENT_LEECHERS']."\">{$leechers}</span>";
				$topic_row['VIEWS'].="<br /><span class=\"my_tt complet\" title=\"".$this->user->lang['TORRENT_COMPLETED']."\">{$times_completed}</span>";
			}
			$torrent_row['S_HAS_TORRENT']=true;
			$topic_row=array_merge($topic_row, $torrent_row);

			$event['topic_row']=$topic_row;
		}
	}

	public function viewforum_get_torrents($event)
	{
		$topic_list=$event['topic_list'];
		$rowset=$event['rowset'];
		$forum_id=$this->request->variable('f', 0);

		if($this->forum_tracker && $topic_list)
		{
			$this->forum_torrents=$this->torrents_cleanup=$this->torrents_hashes=array();

			$sql='SELECT
				tr.fid torrent_id,
				tr.completed'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_times_completed' : '').' times_completed,
				tr.leechers'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_leechers' : '').' leechers,
				tr.seeders'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_seeders' : '').' seeders,
				tr.completed real_times_completed,
				tr.leechers real_leechers,
				tr.seeders real_seeders,
				'.($this->config['ppkbb_tcenable_rannounces'][0] ? '
				tr.rem_times_completed rem_times_completed,
				tr.rem_leechers rem_leechers,
				tr.rem_seeders rem_seeders,
				' : '').'
				tr.size,
				tr.ctime added,
				tr.private,
				tr.info_hash,
				tr.lastcleanup,
				tr.rem_leechers,
				tr.rem_seeders,
				tr.poster_id,
				tr.rem_times_completed,
				tr.lastremote,
				tr.numfiles,
				tr.topic_id
			FROM '.ATTACHMENTS_TABLE.' a, '.XBT_FILES." tr WHERE a.extension='torrent' AND a.in_message='0' AND ".$this->db->sql_in_set('a.topic_id', $topic_list)." AND a.attach_id=tr.fid";
			$result=$this->db->sql_query($sql);
			$valid_peers_time=intval($this->config['ppkbb_xcannounce_interval']*1.25);
			while($row=$this->db->sql_fetchrow($result))
			{
				$row['info_hash']=substr($row['info_hash'], 0, 20);

				$this->forum_torrents[$row['topic_id']][$row['torrent_id']]=$row;

				$this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_tcclean_place'][2] && $this->dt - $row['lastcleanup'] > $valid_peers_time ? $this->torrents_cleanup[$row['torrent_id']]=$row['torrent_id'] : '';
				$rowset[$row['topic_id']]['torrent_status'] < 1 && $this->config['ppkbb_tcenable_rannounces'][0] && $this->config['ppkbb_tcenable_rannounces'][2] ? $this->torrents_hashes[$row['torrent_id']]=$row['info_hash'] : '';
			}
			$this->db->sql_freeresult($result);

			if($this->torrents_hashes)
			{
				$this->torrents_remote=$this->xbt_functions->torrents_remote($this->torrents_hashes);
			}

			$this->xbt_functions->viewforum_torrent_cron($forum_id, $this->torrents_cleanup, $this->torrents_remote);

		}
	}

	public function viewforum_get_forum_type_add_tdt($event)
	{
		$forum_data=$event['forum_data'];

		if($forum_data['forum_tracker'])
		{
			$this->forum_tracker=1;

			$this->template->assign_vars(array(
				'S_ENABLE_TRFEEDS_FORUM' => $this->config['ppkbb_feed_forum'] ? true : false,
				)
			);

			$this->xbt_functions->topdown_torrents('viewforum', $this->user_tracker_options, $forum_data['forum_id'], $forum_data['forum_name']);

		}


	}

	public function posting_disablefpq_addvars_extpostscr($event)
	{
		$post_data=$event['post_data'];
		$page_data=$event['page_data'];
		$mode=$event['mode'];

		if($post_data['forum_tracker'])
		{
			if($mode=='edit' || ($mode=='quote' && $this->config['ppkbb_disable_fpquote'] && $post_data['post_attachment']))
			{
				$sql='SELECT a.post_msg_id FROM '.ATTACHMENTS_TABLE.' a, '.XBT_FILES ." tr WHERE a.attach_id=tr.fid AND a.post_msg_id='{$post_data['post_id']}' AND a.extension='torrent' AND a.in_message='0' LIMIT 1";
				$result=$this->db->sql_query($sql);
				$is_tracker=$this->db->sql_fetchfield('post_msg_id');
				if($mode=='quote' && $is_tracker)
				{
					$page_data['MESSAGE']='[quote=&quot;' . $post_data['quote_username'] . '&quot;]' . $post_data['post_subject'] . "[/quote]\n";
					$event['page_data']=$page_data;
				}
			}

			if($mode=='post' || ($mode=='edit' && $post_data['forum_id'] && $post_data['post_id']==$post_data['topic_first_post_id']/* && $is_tracker*/))
			{
				$tracker_url=generate_board_url($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? false : true);
				if(!$this->config['ppkbb_announce_url'])
				{
					$this->config['ppkbb_announce_url']=$tracker_url.($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? $this->config['ppkbb_phpannounce_url'] : ':'.$this->config['ppkbb_xclisten_port'].'/announce');
				}
				else
				{
					$this->config['ppkbb_announce_url']=($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? $tracker_url.$this->config['ppkbb_phpannounce_url'] : $this->config['ppkbb_announce_url'].':'.$this->config['ppkbb_xclisten_port'].'/announce');
				}

				$this->template->assign_vars(array(
					'TRACKER_ANNOUNCE_URL'		=> $this->config['ppkbb_announce_url'],
					'S_SHOW_ANNWARN' => $this->config['ppkbb_tfile_annreplace'][0] ? true : false,
					'S_ANNWARN_CHECKED' => $this->config['ppkbb_tfile_annreplace'][0] && $this->config['ppkbb_tfile_annreplace'][2] ? true : false,

					)
				);

				if($this->config['ppkbb_max_extposters'][0] || $this->config['ppkbb_max_extscreenshots'][0])
				{
					$submit=$event['submit'];
					$preview=$event['preview'];
					$refresh=$event['refresh'];
					$this->xbt_functions->posting_ext_postsrc($submit, $preview, $refresh, $mode, ($mode=='edit' ? $post_data['post_id'] : 0), $post_data['forum_id']);

				}
			}
		}
	}

	public function index_add_cron_tdt()
	{
		$this->xbt_functions->index_stat_cron($this->auth->acl_get('u_canviewtrstat'));

		$this->xbt_functions->topdown_torrents('index', $this->user_tracker_options);
	}

	public function delete_attachment_torrents($event)
	{
		$mode=$event['mode'];
		$ids=$event['ids'];

		if($mode=='attach' && !$this->config['ppkbb_addit_options'][0])
		{
			$delete_post=$event['delete_post'];
			$is_ucp=$this->request->variable('i', '');

			if(!$delete_post && $is_ucp=='ucp_attachments')
			{
				$new_ids=array();
				$sql="SELECT attach_id FROM ".ATTACHMENTS_TABLE." a, ".POSTS_TABLE." p WHERE ".$this->db->sql_in_set('attach_id', $ids)." AND in_message='0' AND a.post_msg_id=p.post_id AND p.post_torrent='0'";
				$result=$this->db->sql_query($sql);
				while($row=$this->db->sql_fetchrow($result))
				{
					$new_ids[]=$row['attach_id'];
				}
				$this->db->sql_freeresult($result);
				$ids=$new_ids ? $new_ids : array(0);
			}
		}

		$modes=array(
			'attach' => array(
				XBT_FILES => 'fid',
				XBT_FILES_USERS => 'fid',
				TRACKER_FILES_TABLE => 'torrent',
				TRACKER_RANNOUNCES_TABLE => 'torrent',
				TRACKER_RTRACK_TABLE => 'user_torrent_zone',
				TRACKER_DOWNLOADS_TABLE => 'attach_id',
				TRACKER_BOOKMARKS_TABLE => 'attach_id',
			),
			'post' => array(
				XBT_FILES => 'post_msg_id',
				TRACKER_DOWNLOADS_TABLE => 'post_msg_id',
			),
			'topic' => array(
				XBT_FILES => 'topic_id',
			),
			'user' => array(
				XBT_FILES => 'poster_id',
				XBT_FILES_USERS => 'uid',
				TRACKER_DOWNLOADS_TABLE => 'downloader_id',
			),
		);

		$sql_where=array(
			TRACKER_RTRACK_TABLE => array(
				"rtracker_type='t'",
			),
		);

		$update_table=array(XBT_FILES);

		//UPDATE|DELETE WHERE field
		if(isset($modes[$mode]) && $ids)
		{
			foreach($modes[$mode] as $table => $field)
			{
				if(in_array($table, $update_table))
				{
					$sql = 'UPDATE ' . $table . ' SET flags=1
						WHERE ' . $this->db->sql_in_set($field, $ids)
							.(isset($sql_where[$table]) ? ' AND '.implode(' AND ', $sql_where[$table]) : '');
				}
				else
				{
					$sql = 'DELETE FROM ' . $table . '
						WHERE ' . $this->db->sql_in_set($field, $ids)
							.(isset($sql_where[$table]) ? ' AND '.implode(' AND ', $sql_where[$table]) : '');
				}
				$this->db->sql_query($sql);
			}
		}

		$event['ids']=$ids;
	}

	public function delete_post_torrents($event)
	{
		//$table_ary=$event['table_ary'];
		//$table_ary=array_merge($table_ary, array(TRACKER_IMAGES_TABLE);//post_id, post_msg_id need
		//$event['table_ary']=$table_ary;

		$post_ids=$event['post_ids'];

		//DELETE WHERE post_id, post_msg_id
		$table_ary=array(TRACKER_IMAGES_TABLE);
		foreach ($table_ary as $table)
		{
			$sql = "DELETE FROM  $table
				WHERE " . $this->db->sql_in_set('post_msg_id', $post_ids);
			$this->db->sql_query($sql);
		}

		$torrent_ids=array();
		$sql="SELECT attach_id FROM ".ATTACHMENTS_TABLE." WHERE extension='torrent' AND in_message='0' AND ".$this->db->sql_in_set('post_msg_id', $post_ids);
		$result=$this->db->sql_query($sql);
		while($row=$this->db->sql_fetchrow($result))
		{
			$torrent_ids[]=$row['attach_id'];
		}
		$this->db->sql_freeresult($result);

		if($torrent_ids)
		{
			$data=array('ids' => $torrent_ids, 'mode' => 'attach', 'delete_post' => true);
			$this->delete_attachment_torrents($data);
		}

	}

	public function delete_topic_torrents($event)
	{
		//UPDATE|DELETE WHERE topic_id
		$table_ary=$event['table_ary'];
		$topic_ids=$event['topic_ids'];
		if($topic_ids)
		{
			$sql = 'UPDATE '.XBT_FILES.' SET flags=1
				WHERE ' . $this->db->sql_in_set('topic_id', $topic_ids);
			$this->db->sql_query($sql);
		}
		$table_ary=array_merge($table_ary, array(/*XBT_FILES, */TRACKER_CRON_TABLE, TRACKER_IMAGES_TABLE));

		$event['table_ary']=$table_ary;
	}

	public function move_post_torrents($event)
	{
		$topic_id=$event['topic_id'];
		$post_ids=$event['post_ids'];
		$forum_row=$event['forum_row'];

		//UPDATE forum_id, topic_id WHERE post_id | post_msg_id
		$sql = 'UPDATE ' . XBT_FILES . '
			SET forum_id = ' . (int) $forum_row['forum_id'] . ", topic_id = $topic_id
			WHERE " . $this->db->sql_in_set('post_msg_id', $post_ids);
		$this->db->sql_query($sql);

		$sql = 'UPDATE ' . TRACKER_IMAGES_TABLE . "
			SET topic_id = $topic_id
			WHERE " . $this->db->sql_in_set('post_msg_id', $post_ids);
		$this->db->sql_query($sql);
	}

	public function move_topic_torrents($event)
	{
		//UPDATE forum_id WHERE topic_id
		$table_ary=$event['table_ary'];
		$table_ary=array_merge($table_ary, array(TRACKER_CRON_TABLE, XBT_FILES));

		$event['table_ary']=$table_ary;
	}

	public function xbtbb3cker_global_vars()
	{

		if($this->user->data['is_registered'])
		{

			$u_update=array();
			if($this->config['ppkbb_tccron_jobs'][0] && $this->dt - $this->user_tracker_data[0] > $this->config['ppkbb_tccron_jobs'][0])
			{
				$u_update['t']=1;
			}
			if($this->config['ppkbb_tccron_jobs'][1] && $this->dt - $this->user_tracker_data[1] > $this->config['ppkbb_tccron_jobs'][1])
			{
				$u_update['c']=1;
			}
			if(isset($this->user_restricts['update']))
			{
				$u_update['r']=$this->user_restricts;
			}
			$tracker_cron=array();
			if(count($u_update))
			{
				$u_update['user_id']=$this->user->data['user_id'];
				$u_update['tracker_data']=$this->user->data['user_tracker_data'];
				$sql="INSERT INTO ".TRACKER_CRON_TABLE."(type, data, added) VALUES('u_update', '".$this->db->sql_escape(serialize($u_update))."', '{$this->dt}')";
				$this->db->sql_query($sql);
				$cron_id=$this->db->sql_nextid();

				if($cron_id)
				{
					$tracker_cron[]=$cron_id;
				}
				if(count($tracker_cron))
				{
					$cron_id=implode('&amp;id[]=', $tracker_cron);
					$this->template->assign_block_vars('tracker_cron', array(
						'CRON_TASK' => '<img src="' . append_sid($this->root_path . 'ext/ppk/xbtbb3cker/include/cron.' . $this->php_ext, 'id[]='.$cron_id) . '" alt="tracker_cron" width="1" height="1" />'
						)
					);
				}
			}

		}

		if($this->user_restricts && count($this->user_restricts) > 1)
		{

			$restricts_explain=array();
			isset($this->user_restricts['ratio']) ? $restricts_explain[]=(in_array($this->user_restricts['ratio'], array('Inf.', 'Leech.', 'Seed.', 'None.')) ? sprintf($this->user->lang['USER_RESTRICTS_RATIO_TEXT'], $this->user_restricts['ratio']) : sprintf($this->user->lang['USER_RESTRICTS_RATIO'], $this->user_restricts['ratio'])) : '';
			isset($this->user_restricts['upload']) ? $restricts_explain[]=sprintf($this->user->lang['USER_RESTRICTS_UPLOAD'], get_formatted_filesize($this->user_restricts['upload'])) : '';
			isset($this->user_restricts['download']) ? $restricts_explain[]=sprintf($this->user->lang['USER_RESTRICTS_DOWNLOAD'], get_formatted_filesize($this->user_restricts['download'])) : '';

			if(!class_exists('timedelta'))
			{
				$this->user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
				include_once($this->root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$this->php_ext);
			}
			$td = new \timedelta();

			$user_restricts=sprintf($this->user->lang['USER_RESTRICTS_EXPLAIN'], implode(', ', $restricts_explain), $td->spelldelta(0, $this->config['ppkbb_trestricts_options'][2]));

			if(isset($this->user_restricts['can_leech']))
			{
				$user_restricts.=$this->user->lang['USER_RESTRICTS_LEECH'];
			}
			if(isset($this->user_restricts['wait_time']))
			{
				$user_restricts.=sprintf($this->user->lang['USER_RESTRICTS_WAIT'], $td->spelldelta(0, $this->user_restricts['wait_time']), ($this->user_restricts['include_torrent'] ? $this->user->lang['USER_RESTRICTS_INCLUDE'] : ''));
			}
			if(isset($this->user_restricts['peers_limit']))
			{
				$user_restricts.=sprintf($this->user->lang['USER_RESTRICTS_PEERS'], $this->user_restricts['peers_limit']);
			}
			if(isset($this->user_restricts['torrents_limit']))
			{
				$user_restricts.=sprintf($this->user->lang['USER_RESTRICTS_TORRENTS'], $this->user_restricts['torrents_limit']);
			}
			if(isset($this->user_restricts['days_limit']))
			{
				$user_restricts.=sprintf($this->user->lang['USER_RESTRICTS_DAYS'], $this->user_restricts['days_limit']);
			}

		}

		$xbtbb3cker_addons="{$this->root_path}ext/ppk/xbtbb3cker/";

		$this->template->assign_vars(array(
			'XBTBB3CKER_ADDONS'	=> $xbtbb3cker_addons,

			'TRACKER_ADDIT_CSS' => "{$this->root_path}ext/ppk/xbtbb3cker/include/cssjs.{$this->php_ext}?type=css".(!$this->config['ppkbb_cssjs_cache'] ? '&amp;no_cache=1' : ''),
			'TRACKER_ADDIT_JS' => "{$this->root_path}ext/ppk/xbtbb3cker/include/cssjs.{$this->php_ext}?type=js".(!$this->config['ppkbb_cssjs_cache'] ? '&amp;no_cache=1' : ''),

			'S_DISCLAIMER_BLOCK' => $this->config['ppkbb_noticedisclaimer_blocks'][0] && $this->user->lang['TRACKER_BOTTOM_DISCLAIMER'] ? $this->user->lang['TRACKER_BOTTOM_DISCLAIMER'] : false,
			'S_SEARCH_ASTRACKER' => $this->config['ppkbb_search_astracker'] ? true : false,
			'S_SEARCH_POST_TORRENT' => true,//$this->config['search_type']!='\phpbb\search\fulltext_sphinx' ? true : false,

			'S_IS_ADMOD' => $this->is_admod,

			'S_USER_TRACKER_INFO' => $this->config['ppkbb_torr_blocks'][10] && $this->user->data['is_registered'] ? true : false,
			'S_USER_RESTRICTS' => isset($user_restricts) ? $user_restricts : false,

			'U_TINFO_UP'			=> get_formatted_filesize($this->user_uploaded),
			'U_TINFO_DOWN'			=> get_formatted_filesize($this->user_downloaded),
			'U_TINFO_RATIO'			=> $this->xbt_functions->get_ratio_alias($this->user_ratio),

			'S_TINFO_TORRENTSSIZES' => $this->config['ppkbb_tccron_jobs'][0] ? true : false,
			'S_TINFO_COMMENTS' => $this->config['ppkbb_tccron_jobs'][1] ? true : false,

			'U_TRTOP' => $this->auth->acl_get('u_canviewtrtop') && $this->config['ppkbb_tracker_top'][0] ? $this->xbt_functions->get_top_url() : false,
			'LNG_TRTOP' => $this->config['ppkbb_tracker_top'][0] ? sprintf($this->user->lang['TRACKER_TOP'], $this->config['ppkbb_tracker_top'][0]) : '',

			'U_SEARCH_SELF_TORRENT'	=> append_sid("{$this->root_path}search.$this->php_ext", 'search_id=egosearch&amp;sr=topics&amp;ot=1&amp;pt=t'),
			'U_SEARCH_NEW_TORRENT'	=> append_sid("{$this->root_path}search.$this->php_ext", 'search_id=newposts&amp;sr=topics&amp;ot=1&amp;pt=t'),
			'U_SEARCH_ACTIVE_TORRENT'	=> append_sid("{$this->root_path}search.$this->php_ext", 'search_id=active_topics&amp;sr=topics&amp;ot=1&amp;pt=t'),
			'U_SEARCH_UNANSWERED_TORRENT'	=> append_sid("{$this->root_path}search.$this->php_ext", 'search_id=unanswered&amp;sr=topics&amp;ot=1&amp;pt=t'),
			'U_SEARCH_UNREAD_TORRENT'	=> append_sid("{$this->root_path}search.$this->php_ext", 'search_id=unreadposts&amp;sr=topics&amp;ot=1&amp;pt=t'),
			'U_SEARCH_SELF_COMMENT'	=> append_sid("{$this->root_path}search.$this->php_ext", 'search_id=egosearch&amp;sr=posts&amp;ot=1&amp;pt=c'),
			'U_SEARCH_NEW_COMMENT'	=> append_sid("{$this->root_path}search.$this->php_ext", 'search_id=newposts&amp;sr=posts&amp;ot=1&amp;pt=c'),

			'S_ENABLE_TRFEEDS' => $this->config['ppkbb_feed_enable'] ? true : false,

			'S_ENABLE_FEEDS_TRACKERS' => $this->config['ppkbb_feed_overall_trackers'] ? true : false,
			'S_ENABLE_TRFEEDS_OVERALL' => $this->config['ppkbb_feed_overall'] ? true : false,

			'S_ENABLE_FEEDS_TORRENTS_NEW' => $this->config['ppkbb_feed_torrents_new'] ? true : false,
			'S_ENABLE_FEEDS_TORRENTS_ACTIVE' => $this->config['ppkbb_feed_torrents_active'] ? true : false,

			'S_ENABLE_FEEDS_TORRENTS' => $this->config['ppkbb_feed_torrents'] ? true : false,
			'S_ENABLE_FEEDS_COMMENTS' => $this->config['ppkbb_feed_comments'] ? true : false,

			'S_ENABLE_TRFEEDS_TOPIC' => $this->config['ppkbb_feed_topic'] && $this->forum_tracker ? true : false,
			//'S_ENABLE_TRFEEDS_FORUM' => $this->config['ppkbb_feed_forum'] ? true : false,

			'U_TRFEED' => $this->xbt_functions->feed_tracker_url(),
			)
		);

		if(isset($this->config['ppkbb_manual_update']))
		{
			$this->update_warning();
		}
	}

	public function xbtbb3cker_adm_vars()
	{

		$xbtbb3cker_addons="{$this->root_path}ext/ppk/xbtbb3cker/";

		$this->template->assign_vars(array(
			'XBTBB3CKER_ADDONS'	=> $xbtbb3cker_addons,
// 			'ADMIN_PATH'				=> $phpbb_root_path,

			'TRACKER_ADDIT_CSS' => "{$this->root_path}ext/ppk/xbtbb3cker/include/cssjs.{$this->php_ext}?type=css&amp;css_set=1".(!$this->config['ppkbb_cssjs_cache'] ? '&amp;no_cache=1' : ''),
			'TRACKER_ADDIT_JS' => "{$this->root_path}ext/ppk/xbtbb3cker/include/cssjs.{$this->php_ext}?type=js&amp;js_set=1".(!$this->config['ppkbb_cssjs_cache'] ? '&amp;no_cache=1' : ''),

			)
		);
	}

	public function viewtopic_fix_broken_torrents($event)
	{

		$forum_id=$event['forum_id'];

		if($this->torrents_broken)
		{
			$this->xbt_functions->viewtopic_torrent_cron($forum_id, array(), array(), $this->torrents_broken);
		}

	}

	public function viewtopic_add_torrent_template($event)
	{
		if($this->forum_tracker)
		{
			$post_row=$event['post_row'];
			$row=$event['row'];

			if(isset($this->topic_torrents[$row['post_id']]))
			{
				$this->xbt_functions->viewtopic_torrent_template($this->is_candowntorr, $this->topic_torrents[$row['post_id']], $row['user_id'], $this->is_canviewvtstats, $row['forum_id'], $row['topic_id'], $row['post_id'], $this->is_admod, $this->topic_posters[$row['post_id']], $this->topic_screenshots[$row['post_id']], $this->is_candownpostscr, $this->torrents_bookmark);
			}
		}
	}

	public function viewtopic_get_torrent_data($event)
	{
		$post_row=$event['post_row'];
		$topic_data=$event['topic_data'];
		$user_poster_data=$event['user_poster_data'];
		$poster_id=$event['poster_id'];
		$row=$event['row'];

		if($this->forum_tracker)
		{
			if(isset($this->topic_torrents[$row['post_id']]))
			{
				isset($this->topic_posters[$row['post_id']]) ? '' : $this->topic_posters[$row['post_id']]=array();
				isset($this->topic_screenshots[$row['post_id']]) ? '' : $this->topic_screenshots[$row['post_id']]=array();
				if($this->topic_posters[$row['post_id']] && !$row['post_poster'])
				{
					$this->db->sql_query("UPDATE ".POSTS_TABLE." SET post_poster='110' WHERE post_id='{$row['post_id']}'");
					$this->db->sql_query("UPDATE ".ATTACHMENTS_TABLE." SET i_poster='1' WHERE post_msg_id='{$row['post_id']}' AND real_filename LIKE '%poster%'");
				}

				$is_candownsize=!$this->user->data['is_registered'] && $this->config['ppkbb_trestricts_options'][5] && $this->torrents_size < $this->config['ppkbb_trestricts_options'][5] ? true : false;

				$torrents_row=$this->xbt_functions->viewtopic_torrent_data(($is_candownsize ? 1 : $this->is_candowntorr), $this->topic_torrents[$row['post_id']], $row['user_id'], $this->topic_posters[$row['post_id']], $this->topic_screenshots[$row['post_id']], $this->is_candownpostscr, $this->is_canviewvtstats, $row['post_id'], $this->is_cansetstatus, $this->is_cansetrequpratio, $topic_data['torrent_status']);

				$post_row=array_merge($post_row, $torrents_row[0]);

				/*$this->is_admod ? '' : */$post_row['S_HAS_ATTACHMENTS']=false;
				if($torrents_row[1])
				{
					$post_row['MESSAGE']=$torrents_row[1].$post_row['MESSAGE'];
				}

				if($this->user->data['is_registered'] && $this->is_candowntorr)
				{
					if($this->config['ppkbb_trestricts_options'][0])
					{
						$is_canskiprequpratio = $this->user->data['user_id']!=$poster_id ? (($this->auth->acl_get('u_canskiprequpratio') && $this->auth->acl_get('f_canskiprequpratio', $topic_data['forum_id'])) ? 1 : 0) : 1;

						if(!$is_canskiprequpratio)
						{
							if($topic_data['torrent_requpload'] && $this->user_uploaded < $topic_data['torrent_requpload'])
							{
								$post_row['TORRENT_REQRATIO_ERROR']=sprintf($this->user->lang['TORRENT_REQUPLOAD_ERROR'], get_formatted_filesize($topic_data['torrent_requpload']), get_formatted_filesize($this->user_uploaded));
							}

							if($topic_data['torrent_reqratio']!=0.000 && $this->user_ratio!='None.' && ($this->user_ratio < $topic_data['torrent_reqratio'] || $this->user_ratio=='Leech.' || $this->user_ratio=='Inf.'))
							{
								$post_row['TORRENT_REQUPLOAD_ERROR']=sprintf($this->user->lang['TORRENT_REQRATIO_ERROR'], $topic_data['torrent_reqratio'], $this->xbt_functions->get_ratio_alias($this->user_ratio));
							}
						}

						$post_row['TORRENT_REQRATIO']=$topic_data['torrent_reqratio'];
						$post_row['TORRENT_REQUPLOAD']=get_formatted_filesize($topic_data['torrent_requpload']);
						$post_row['TORRENT_REQUPRATIO']=$topic_data['torrent_reqratio']!=0.000 || $topic_data['torrent_requpload'] || $this->is_cansetrequpratio ? sprintf($this->user->lang['TORRENT_REQUPRATIO'], $topic_data['torrent_reqratio'], get_formatted_filesize($topic_data['torrent_requpload'])) : false;
					}

// 					if($this->config['ppkbb_trestricts_options'][1]==2)
// 					{
// 						$is_canskiptrestricts = $this->user->data['user_id']!=$poster_id ? ($this->auth->acl_get('u_canskiptrestricts') ? 1 : 0) : 1;
//
// 					}
				}

				$post_row['TORRENT_STATUS']=isset($this->torrent_statuses['STATUS_MARK'][$topic_data['torrent_status']]) ? $this->torrent_statuses['STATUS_MARK'][$topic_data['torrent_status']] : $this->user->lang['TORRENT_UNKNOWN_STATUS'];
				$post_row['CANDOWN_TORRENT_STATUS']=$this->is_cansetstatus || $topic_data['torrent_status'] < 1 || ($this->user->data['user_id']==$poster_id && $topic_data['torrent_status'] > 0 &&  in_array($topic_data['torrent_status'], $this->config['ppkbb_tcauthor_candown'])) ? true : false;

				if($topic_data['status_dt'])
				{
					$post_row['TORRENT_STATUS_AUTHOR']=get_username_string('full', $topic_data['status_author'], $this->status_data['username'], $this->status_data['user_colour']);
					$post_row['TORRENT_STATUS_REASON']=$topic_data['status_reason'] ? $topic_data['status_reason'] : $this->user->lang['STATUS_NO_REASON'];
					$post_row['TORRENT_STATUS_DT']=$this->user->format_date($topic_data['status_dt']);
					$post_row['TORRENT_STATUS_EXPLAIN']=sprintf($this->user->lang['STATUS_EXPLAIN'], $this->user->format_date($topic_data['status_dt']), $this->status_data['username'], ($topic_data['status_reason'] ? $topic_data['status_reason'] : $this->user->lang['STATUS_NO_REASON']));
				}
			}
		}
		else if($row['post_torrent'])
		{
			$this->torrents_broken[$row['post_id']]=$row['topic_id'];
		}

		if($this->config['ppkbb_torr_blocks'][10] && $this->user->data['is_registered']/* && isset($user_poster_data['user_uploaded'])*/ && $user_poster_data['user_type']!=USER_IGNORE)
		{
			if(!isset($user_poster_data['user_ratio']))
			{
				$user_poster_data['user_uploaded']=$user_poster_data['user_downloaded']=0;
				$user_poster_data['user_ratio']='None.';
			}
			$post_row['USER_UPLOADED']=$user_poster_data['user_uploaded'];
			$post_row['USER_DOWNLOADED']=$user_poster_data['user_downloaded'];
			$post_row['USER_RATIO']=$user_poster_data['user_ratio'];
			$post_row['USER_TORRENTS']=$user_poster_data['user_torrents'];
			$post_row['USER_COMMENTS']=$user_poster_data['user_comments'];

			$post_row['U_USER_TORRENTS']=append_sid("{$this->root_path}search.{$this->php_ext}", "author_id={$poster_id}&amp;sr=topics&amp;ot=1&amp;pt=t");
			$post_row['U_USER_COMMENTS']=append_sid("{$this->root_path}search.{$this->php_ext}", "author_id={$poster_id}&amp;sr=posts&amp;ot=1&amp;pt=c");
		}

		$event['post_row']=$post_row;
	}

	public function viewtopic_get_torrents($event)
	{
		$this->user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_viewtopic');

		$topic_data=$event['topic_data'];
		$post_list=$event['post_list'];
		$start=$event['start'];
// 		$sql_ary=$event['sql_ary'];

		$this->topic_torrents=$this->topic_posters=$this->topic_screenshots=$this->torrents_cleanup=$this->torrents_hashes=array();
// 		$this->torrent_status=$topic_data['torrent_status'];
		if($topic_data['forum_tracker'])
		{
			$this->template->assign_vars(array(
				'S_ENABLE_TRFEEDS_FORUM' => $this->config['ppkbb_feed_forum'] ? true : false,
				)
			);
			$this->torrent_statuses=$this->xbt_functions->get_torrent_statuses();
		}

		if($topic_data['forum_tracker'] && $topic_data['topic_attachment']/* && !$start*/)
		{

			if(!$post_list)
			{
				return false;
			}

			$torrents_data=array();

			$post_ids=array();
			$image_ext=array('gif', 'jpeg', 'jpg', 'png', 'tga', 'tif', 'tiff');
			$sql='
			SELECT
				tr.fid torrent_id,
				tr.completed'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_times_completed' : '').' times_completed,
				tr.leechers'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_leechers' : '').' leechers,
				tr.seeders'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_seeders' : '').' seeders,
				tr.completed real_times_completed,
				tr.leechers real_leechers,
				tr.seeders real_seeders,
				'.($this->config['ppkbb_tcenable_rannounces'][0] ? '
				tr.rem_times_completed rem_times_completed,
				tr.rem_leechers rem_leechers,
				tr.rem_seeders rem_seeders,
				' : '').'
				tr.size,
				tr.ctime added,
				tr.private,
				tr.info_hash,
				tr.lastcleanup,
				tr.rem_leechers,
				tr.rem_seeders,
				tr.poster_id,
				tr.rem_times_completed,
				tr.lastremote,
				tr.numfiles,
				a.post_msg_id,
				a.extension,
				a.attach_id,
				a.download_count,
				a.is_orphan,
				a.filesize,
				a.real_filename,
				a.thumbnail,
				a.attach_comment,
				u.username,
				u.user_colour'.
				($this->config['ppkbb_tracker_bookmarks'][0] && $this->user->data['is_registered'] ? ', b.id bookmark' : '').'
				FROM
					'.ATTACHMENTS_TABLE.' a
				LEFT JOIN
					'.XBT_FILES .' tr ON(a.attach_id=tr.fid)
				LEFT JOIN
					'.USERS_TABLE." u ON(u.user_id='{$topic_data['status_author']}')"
					.($this->config['ppkbb_tracker_bookmarks'][0] && $this->user->data['is_registered'] ? 'LEFT JOIN '.TRACKER_BOOKMARKS_TABLE." b ON(a.attach_id=b.attach_id AND b.user_id='{$this->user->data['user_id']}')" : '')
				.'WHERE
					'.$this->db->sql_in_set('a.post_msg_id', $post_list)."
					AND a.topic_id='{$topic_data['topic_id']}'
					AND a.in_message='0'";
			$result=$this->db->sql_query($sql);
			$valid_peers_time=intval($this->config['ppkbb_xcannounce_interval']*1.25);
			while($row=$this->db->sql_fetchrow($result))
			{
				$torrents_data[$row['post_msg_id']][$row['torrent_id']]=array('seeders'=>$row['seeders'], 'leechers'=>$row['leechers']);
				$post_ids[$row['post_msg_id']]=$row['post_msg_id'];
				if(!is_array($this->status_data) && $topic_data['status_dt'])
				{
					$this->status_data=array('username'=>$row['username'], 'user_colour'=>$row['user_colour']);
				}
				if($row['torrent_id'] && $row['extension']=='torrent')
				{
					$row['info_hash']=substr($row['info_hash'], 0, 20);

					$this->topic_torrents[$row['post_msg_id']][$row['torrent_id']]=$row;
					$this->torrents_size+=$row['size'];

					$this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_tcclean_place'][3] && $this->dt - $row['lastcleanup'] > $valid_peers_time ? $this->torrents_cleanup[$row['torrent_id']]=$row['torrent_id'] : '';
					$topic_data['torrent_status'] < 1 && $this->config['ppkbb_tcenable_rannounces'][0] && $this->config['ppkbb_tcenable_rannounces'][3] ? $this->torrents_hashes[$row['torrent_id']]=$row['info_hash'] : '';
				}
				else if(in_array($row['extension'], $image_ext))
				{
					if(strpos($row['real_filename'], 'poster')!==false)
					{
						$this->topic_posters[$row['post_msg_id']][$row['attach_id']]=$row;

					}
					else
					{
						$this->topic_screenshots[$row['post_msg_id']][$row['attach_id']]=$row;

					}
				}
			}
			$this->db->sql_freeresult($result);

			$event['torrents_data']=$torrents_data;

			if($this->config['ppkbb_max_extposters'][0] || $this->config['ppkbb_max_extscreenshots'][0])
			{
				$sql="SELECT * FROM ".TRACKER_IMAGES_TABLE.' WHERE '.$this->db->sql_in_set('post_msg_id', $post_list);
				$result=$this->db->sql_query($sql);
				while($row=$this->db->sql_fetchrow($result))
				{
					$row['i_external']=1;
					if($row['i_poster'])
					{
						$this->topic_posters[$row['post_msg_id']][]=$row;

					}
					else
					{
						$this->topic_screenshots[$row['post_msg_id']][]=$row;

					}
				}
				$this->db->sql_freeresult($result);
			}

			if($this->topic_torrents)
			{
				$this->forum_tracker=1;

				if($this->config['ppkbb_tracker_bookmarks'][0] && $this->config['ppkbb_tracker_bookmarks'][1] && $this->user->data['is_registered'])
				{
					$sql='SELECT attach_id, COUNT(*) bookmarks FROM '.TRACKER_BOOKMARKS_TABLE.' WHERE '.$this->db->sql_in_set('post_msg_id', $post_ids).' GROUP BY attach_id';
					$result=$this->db->sql_query($sql);
					while($row=$this->db->sql_fetchrow($result))
					{
						$this->torrents_bookmark[$row['attach_id']]=intval($row['bookmarks']);
					}
					$this->db->sql_freeresult($result);
				}

				$this->is_candownload=$this->auth->acl_get('u_download') && $this->auth->acl_get('f_download', $topic_data['forum_id']) ? 1 : 0;

				if(!$this->user->data['is_registered'])
				{
					$this->is_cansetstatus=$this->is_cansetrequpratio=false;

					$this->is_candowntorr = /*$this->config['ppkbb_xcanonymous_announce'] && */$this->auth->acl_get('u_candowntorr') && $this->auth->acl_get('f_candowntorr', $topic_data['forum_id']) ? 1 : 0;
					$this->is_candownpostscr = $this->auth->acl_get('u_candownpostscr') && $this->auth->acl_get('f_candownpostscr', $topic_data['forum_id']) ? 1 : 0;

					if((!$this->config['ppkbb_torrent_downlink'][0] && !$this->config['ppkbb_torrent_downlink'][1] && !$this->config['ppkbb_torrent_downlink'][2])
						|| ($this->config['ppkbb_torrent_downlink'][0]==1 && $this->config['ppkbb_torrent_downlink'][1]==1 && $this->config['ppkbb_torrent_downlink'][2]==1))
					{
						$this->is_candowntorr=0;
					}
				}
				else
				{
					$this->is_cansetstatus = $this->auth->acl_get('u_cansetstatus') && $this->auth->acl_get('f_cansetstatus', $topic_data['forum_id']) ? 1 : 0;
					$this->is_cansetrequpratio = $this->config['ppkbb_trestricts_options'][0] && $this->auth->acl_get('u_cansetrequpratio') && $this->auth->acl_get('f_cansetrequpratio', $topic_data['forum_id']) ? 1 : 0;

					$this->is_candowntorr=$this->is_candownpostscr=1;

					if($this->user->data['user_id']!=$topic_data['topic_poster'])
					{
						$this->is_candowntorr=$this->auth->acl_get('u_candowntorr') && $this->auth->acl_get('f_candowntorr', $topic_data['forum_id']) ? 1 : 0;
						$this->is_candownpostscr=$this->auth->acl_get('u_candownpostscr') && $this->auth->acl_get('f_candownpostscr', $topic_data['forum_id']) ? 1 : 0;
					}

					if((!$this->config['ppkbb_torrent_downlink'][0] && !$this->config['ppkbb_torrent_downlink'][1] && !$this->config['ppkbb_torrent_downlink'][2])
						|| ($this->config['ppkbb_torrent_downlink'][0]==2 && $this->config['ppkbb_torrent_downlink'][1]==2 && $this->config['ppkbb_torrent_downlink'][2]==2))
					{
						$this->is_candowntorr=0;
					}
				}
				$this->is_canviewvtstats=$this->auth->acl_get('u_canviewvtstats') && $this->auth->acl_get('f_canviewvtstats', $topic_data['forum_id']) ? 1 : 0;

				if($this->torrents_hashes)
				{
					$this->torrents_remote=$this->xbt_functions->torrents_remote($this->torrents_hashes);
				}

				$this->xbt_functions->viewtopic_torrent_cron($topic_data['forum_id'], $this->torrents_cleanup, $this->torrents_remote, array());

			}

		}

	}

	public function submit_tracker_data($event)
	{
		$data=$event['data'];

		$forum_id=$data['forum_id'];
		$topic_id=$data['topic_id'];
		$post_id=$data['post_id'];

		$mode=$event['mode'];

		$pre_submit=false;
		if(!isset($event['url']))
		{
			$pre_submit=true;
// 			$error=$event['error'];
		}

		$is_canuploadtorr=$this->auth->acl_get('u_attachfonly') && $this->auth->acl_get('f_attachfonly', $forum_id) ? 1 : 0;
		$is_admod=$this->auth->acl_gets('a_', 'm_') || $this->auth->acl_get('m_', $forum_id) ? true : false;

		$post_torrents=0;
		$errors=array();

		if($is_canuploadtorr && ($mode=='post' || ($mode=='edit' && $forum_id && $post_id)))
		{
			$sql="SELECT forum_tracker FROM ".FORUMS_TABLE." WHERE forum_id='{$forum_id}'";
			$result=$this->db->sql_query($sql);
			$this->forum_tracker=$this->db->sql_fetchfield('forum_tracker') ? true : false;
			$this->db->sql_freeresult($result);

			if($this->forum_tracker)
			{
				$torrents = $posters =array();
				$post_posters = 0;
				$post_screenshots = 0;
				if(isset($data['attachment_data']) && count($data['attachment_data']))
				{
					$attachments=array();
					foreach($data['attachment_data'] as $k => $a)
					{
						$attachments[]=$a['attach_id'];
					}
					if($attachments)
					{
						$image_ext=array('gif', 'jpeg', 'jpg', 'png', 'tga', 'tif', 'tiff');
						$sql='SELECT a.attach_id, a.extension, a.physical_filename, a.real_filename, x.fid FROM '.ATTACHMENTS_TABLE.' a LEFT JOIN '.XBT_FILES.' x ON(a.attach_id=x.fid) WHERE '.$this->db->sql_in_set('a.attach_id', $attachments)." AND a.in_message='0' ORDER BY filetime ASC";
						$result=$this->db->sql_query($sql);
						while($row=$this->db->sql_fetchrow($result))
						{
							if($row['extension']=='torrent')
							{
								if(!$row['fid'])
								{
									$torrents[]=$row;

								}
								else
								{
									$post_torrents+=1;
								}
							}
							else if(in_array($row['extension'], $image_ext))
							{
								if(strpos($row['real_filename'], 'poster')!==false)
								{
									$posters[]=$row['attach_id'];
								}
								else
								{
									$post_screenshots+=1;

								}

							}
						}
						$this->db->sql_freeresult($result);

						if(!$pre_submit)
						{
							$torrent_status = ($this->auth->acl_get('u_canskiptcheck') && $this->auth->acl_get('f_canskiptcheck', $forum_id)) ? $this->config['ppkbb_tcdef_statuses'][0] : $this->config['ppkbb_tcdef_statuses'][1];
							if($torrents && $post_torrents < $this->config['ppkbb_max_torrents'][1])
							{

								foreach($torrents as $t)
								{
									if($post_torrents >= $this->config['ppkbb_max_torrents'][1])
									{
										break;
									}
									$recode=$this->xbt_functions->recode_torrent($t['physical_filename'], $t['attach_id'], $data, $torrent_status);
									if($recode!==true)
									{
										$errors[]=$recode;
									}
									else
									{
										$post_torrents+=1;
									}
								}
							}

							if($post_torrents)
							{
								$sql="UPDATE ".POSTS_TABLE." SET post_torrent='1' WHERE post_id='{$post_id}'";
								$result=$this->db->sql_query($sql);

								//if($mode=='post')
								//{
									$sql='UPDATE '.TOPICS_TABLE." SET topic_torrent=1, torrent_status='{$torrent_status}'".($mode=='edit' ? ", status_author='0', status_dt='0', status_reason=''" : '')." WHERE topic_id='{$topic_id}'";
									$result=$this->db->sql_query($sql);

								//}

							}
						}

						$ext_post_posters=0;
						if($this->config['ppkbb_max_extposters'][0] || $this->config['ppkbb_max_extscreenshots'][0])
						{
							$ext_postscr=$this->xbt_functions->posting_ext_postsrc(true, false, false, $mode, $post_id, $forum_id);
							!$pre_submit ? $this->xbt_functions->posting_add_ext_postsrc($ext_postscr[0], $ext_postscr[1], $topic_id, $post_id) : '';
							$ext_postscr[0] ? $ext_post_posters=count($ext_postscr[0]) : '';
						}

						$post_posters=count($posters)+$ext_post_posters;
						if(!$pre_submit && ($posters || $ext_post_posters))
						{
							if($posters)
							{
								$sql="UPDATE ".ATTACHMENTS_TABLE." SET i_poster='1' WHERE ".$this->db->sql_in_set('attach_id', $posters);
								$result=$this->db->sql_query($sql);
							}
							$sql="UPDATE ".POSTS_TABLE." SET post_poster='1".($posters ? 1 : 0).($ext_post_posters ? 1 : 0)."' WHERE post_id='{$post_id}'";
							$result=$this->db->sql_query($sql);
						}
					}
				}

				if($pre_submit)
				{
					$torrents=count($torrents)+$post_torrents;

					if(!$is_admod && $this->config['ppkbb_max_torrents'][0] && $torrents < $this->config['ppkbb_max_torrents'][0])
					{
						$errors[]=sprintf($this->user->lang['MESS_MIN_TORRENTS'], $this->config['ppkbb_max_torrents'][0]);

					}
					if($this->config['ppkbb_max_torrents'][1] && $torrents > $this->config['ppkbb_max_torrents'][1])
					{
						$errors[]=sprintf($this->user->lang['MESS_MAX_TORRENTS'], $this->config['ppkbb_max_torrents'][1]);

					}

					if(!$is_admod && $this->config['ppkbb_max_posters'][0] && $post_posters < $this->config['ppkbb_max_posters'][0])
					{
						$errors[]=sprintf($this->user->lang['MESS_MIN_POSTERS'], $this->config['ppkbb_max_posters'][0]);

					}
					if($this->config['ppkbb_max_posters'][1] && $post_posters > $this->config['ppkbb_max_posters'][1])
					{
						$errors[]=sprintf($this->user->lang['MESS_MAX_POSTERS'], $this->config['ppkbb_max_posters'][1]);

					}

					if(!$is_admod && $this->config['ppkbb_max_screenshots'][0] && $post_screenshots < $this->config['ppkbb_max_screenshots'][0])
					{
						$errors[]=sprintf($this->user->lang['MESS_MIN_SCREENSHOTS'], $this->config['ppkbb_max_screenshots'][0]);

					}
					if($this->config['ppkbb_max_screenshots'][1] && $post_screenshots > $this->config['ppkbb_max_screenshots'][1])
					{
						$errors[]=sprintf($this->user->lang['MESS_MAX_SCREENSHOTS'], $this->config['ppkbb_max_screenshots'][1]);

					}

					if(isset($ext_postscr[2]) && count($ext_postscr[2]))
					{
						$errors=array_merge($errors, $ext_postscr[2]);
					}

					if($errors)
					{
						trigger_error(implode('<br />', $errors).'<br /><br />'.sprintf($this->user->lang['RETURN_PAGE'], '<a onclick="history.back(); return false;" href="#">', '</a>'));
						//$error=array_merge($error, $errors);

						//$event['error']=$error;
					}
					else
					{
						if($mode=='post' && $this->config['ppkbb_poll_options'][0] && $this->config['ppkbb_poll_options'][1])
						{
							$poll=$event['poll'];

							$can_create_poll=$this->auth->acl_get('f_poll', $forum_id) ? true : false;
							$poll_exist=$poll['poll_options'] && $this->config['ppkbb_poll_options'][2] ? true : false;
							if(!$poll_exist && $can_create_poll)
							{
								$quest_answ=explode("\n", trim($this->config['ppkbb_poll_options'][1]));
								if(isset($quest_answ[0]) && isset($quest_answ[1]))
								{
									$answ_count=count($quest_answ);
									$poll=array(
										'poll_title' => isset($this->user->lang[$quest_answ[0]]) ? $this->user->lang[$quest_answ[0]] : $quest_answ[0],
										'poll_length' => 0,
										'poll_max_options' => 1,
										'poll_start' => 0,
										'poll_last_vote' => 0,
										'poll_vote_change' => 0,
										'enable_bbcode' => false,
										'enable_urls' => 0,
										'enable_smilies' => false,
										'img_status' => false,
										'poll_options_size' => $answ_count-1,
									);
									unset($quest_answ[0]);
									$answ=array();
									foreach($quest_answ as $k => $q)
									{
										$answ[]=isset($this->user->lang[$q]) ? $this->user->lang[$q] : $q;
									}
									$poll['poll_option_text']=implode("\n", $answ);
									$poll['poll_options']=$answ;

									$event['poll']=$poll;
								}
							}
						}
					}
				}

				if(!$pre_submit && count($errors))
				{
					$url=$event['url'];

					trigger_error(
						sprintf($this->user->lang['SUCCESS_BUT'], implode(', ', $errors)).'<br /><br />'.
						sprintf($this->user->lang['RETURN_TOPIC'], '<a href="'.$url.'">', '</a>').'<br /><br />'.
						sprintf($this->user->lang['RETURN_FORUM'], '<a href="'.append_sid("{$this->root_path}viewforum.{$this->php_ext}", 'f='.$forum_id).'">', '</a>')
					);

				}

			}
		}
	}

	public function check_uploaded_file($event)
	{
		$forum_id=$this->request->variable('f', 0);
		//$topic_id=$this->request->variable('t', 0);
		$post_id=$this->request->variable('p', 0);

		$mode=$this->request->variable('mode', '');

		if($mode=='post' || ($mode=='edit' && $forum_id && $post_id))
		{
			$sql="SELECT forum_tracker FROM ".FORUMS_TABLE." WHERE forum_id='{$forum_id}'";
			$result=$this->db->sql_query($sql);
			$this->forum_tracker=$this->db->sql_fetchfield('forum_tracker') ? true : false;
			$this->db->sql_freeresult($result);

			if($this->forum_tracker)
			{
				$filedata=$event['filedata'];
				$is_image=$event['is_image'];

				$is_canuploadtorr=$this->auth->acl_get('u_attachfonly') && $this->auth->acl_get('f_attachfonly', $forum_id) ? 1 : 0;

				if($is_canuploadtorr && !$is_image && $filedata['extension']=='torrent')
				{
					if($mode=='edit')
					{
						$sql="SELECT COUNT(*) torrents FROM ".ATTACHMENTS_TABLE." a LEFT JOIN ".XBT_FILES." x ON(a.attach_id=x.fid) WHERE a.post_msg_id='{$post_id}' AND a.extension='torrent' AND a.in_message='0'";
						$result=$this->db->sql_query($sql);
						$torrents=intval($this->db->sql_fetchfield('torrents'));
						$this->db->sql_freeresult($result);
						if($this->config['ppkbb_max_torrents'][1] && $torrents >= $this->config['ppkbb_max_torrents'][1])
						{
							$filedata['error'][]=sprintf($this->user->lang['MESS_MAX_TORRENTS'], $this->config['ppkbb_max_torrents'][1]);

							$event['filedata']=$filedata;
							return false;
						}
					}
					$check=$this->xbt_functions->check_torrent($filedata);

					if($check!==true)
					{
						$filedata['error'][]=$check;

						$event['filedata']=$filedata;
					}
				}
				else if($is_image)
				{
					if($mode=='edit')
					{
						$image_ext=array('gif', 'jpeg', 'jpg', 'png', 'tga', 'tif', 'tiff');

						$posters=$screenshots=0;
						$is_poster=strpos($filedata['real_filename'], 'poster')!==false ? true : false;

						$sql="SELECT real_filename FROM ".ATTACHMENTS_TABLE." WHERE post_msg_id='{$post_id}' AND extension IN('".implode("', '", $image_ext)."') AND in_message='0'";
						$result=$this->db->sql_query($sql);
						while($row=$this->db->sql_fetchrow($result))
						{
							if(strpos($row['real_filename'], 'poster')!==false)
							{
								$posters+=1;

							}
							else
							{
								$screenshots+=1;

							}
						}
						$this->db->sql_freeresult($result);

						if($is_poster && $this->config['ppkbb_max_posters'][1] && $posters >= $this->config['ppkbb_max_posters'][1])
						{
							$filedata['error'][]=sprintf($this->user->lang['MESS_MAX_POSTERS'], $this->config['ppkbb_max_posters'][1]);

						}
						if(!$is_poster && $this->config['ppkbb_max_screenshots'][1] && $screenshots >= $this->config['ppkbb_max_screenshots'][1])
						{
							$filedata['error'][]=sprintf($this->user->lang['MESS_MAX_SCREENSHOTS'], $this->config['ppkbb_max_screenshots'][1]);

						}

						$event['filedata']=$filedata;
					}
				}
			}
		}


	}

	public function xbtbb3cker_add_lang($event)
	{

		$lang_set_ext=$event['lang_set_ext'];

		$lang_set_ext[]=array(
			'ext_name' => 'ppk/xbtbb3cker',
			'lang_set' => 'xbtbb3cker_common',
		);

		$event['lang_set_ext']=$lang_set_ext;
	}

	public function xbtbb3cker_init()
	{

		$this->obtain_tracker_config();
		$this->dt=time();

		if($this->user->data['is_registered'])
		{
			$sql = 'SELECT uid, torrent_pass user_passkey, downloaded user_downloaded, uploaded user_uploaded
				FROM '.XBT_USERS."
				WHERE uid={$this->user->data['user_id']}";
			$result = $this->db->sql_query($sql);
			$data = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);

			if(isset($data['uid']))
			{
				$this->user_tracker_options=$this->xbt_functions->my_split_config($this->user->data['user_tracker_options'], 4, 'intval');
				$this->user->data['is_registered'] ? '' : $this->config['ppkbb_tcclean_place']=array_fill(0, 6, 0);
				$this->user_uploaded=$data['user_uploaded'];
				$this->user_downloaded=$data['user_downloaded'];
				$this->user_passkey=$data['user_passkey'] ? $data['user_passkey'] : $this->create_passkey($this->user->data['user_id']);
			}
			else
			{
				$user_passkey=$this->create_passkey($this->user->data['user_id']);

				$sql = 'INSERT INTO ' . XBT_USERS . "
					(uid, torrent_pass, uploaded) VALUES('{$this->user->data['user_id']}', '{$user_passkey}', '".($this->config['ppkbb_ipreg_countrestrict'][1] ? $this->config['ppkbb_ipreg_countrestrict'][1] : 0)."')";
				$result=$this->db->sql_query($sql);

				$this->user_uploaded=$this->user_downloaded=0;
				$this->user_passkey=$user_passkey;
			}

			$this->is_admod=$this->auth->acl_gets('a_', 'm_') || $this->auth->acl_getf_global('m_') ? 1 : 0;
			$this->user_ratio=$this->xbt_functions->get_ratio($this->user_uploaded, $this->user_downloaded, $this->config['ppkbb_tcratio_start']);

			$this->user_tracker_data=$this->xbt_functions->my_split_config($this->user->data['user_tracker_data'], 6, 'intval');

			$this->is_canskiptrestricts=!$this->config['ppkbb_trestricts_options'][1] || !$this->config['ppkbb_trestricts_options'][2] || $this->auth->acl_get('u_canskiptrestricts') ? 1 : 0;
			if(!$this->is_canskiptrestricts)
			{
				if($this->dt - $this->user_tracker_data[5] > $this->config['ppkbb_trestricts_options'][2])
				{
					$this->user_restricts=$this->xbt_functions->get_trestricts($this->user_uploaded, $this->user_downloaded, $this->user_ratio);
					$this->user_restricts['update']=$this->dt;
				}
				else
				{
					$this->user_restricts=unserialize($this->user->data['user_trestricts']);
					unset($this->user_restricts['update']);
				}
			}
		}
		else
		{
			$this->user_uploaded=$this->user_downloaded=$this->is_admod=0;
			$this->user_passkey='';
			$this->user_ratio='None.';

		}

	}

	public function update_warning()
	{
		if($this->user->data['user_type']==USER_FOUNDER)
		{
			trigger_error('MANUAL_UPDATE_'.$this->config['ppkbb_manual_update']);
		}
		else
		{
			trigger_error('BOARD_DISABLED');
		}

	}

	public function obtain_tracker_config()
	{

		$tracker_config=$this->cache->get('_ppkbb3cker_cache');

		if(!$tracker_config)
		{
			include("{$this->root_path}ext/ppk/xbtbb3cker/include/config_map.{$this->php_ext}");

			$tracker_config=array();

			foreach($config_map as $k => $v)
			{
				isset($this->config[$k]) && $config_map[$k][0]!=1 ? $this->config[$k]=$this->xbt_functions->my_split_config($this->config[$k], $config_map[$k][0], $config_map[$k][1], $config_map[$k][2]) : '';
				$tracker_config[$k] = $this->config[$k];
			}

			$this->cache->put('_ppkbb3cker_cache', $tracker_config);

			return false;
		}
		else
		{
			foreach($tracker_config as $k => $v)
			{
				$this->config[$k]=$v;
			}

			return true;
		}


	}

	public function create_passkey($user_id=false)
	{

		$user_id===false ? $user_id=$this->user->data['user_id'] : '';
		$user_passkey=strtolower(gen_rand_string(8).gen_rand_string(8).gen_rand_string(8).gen_rand_string(8));

		/*$sql="SELECT uid, torrent_pass FROM ".XBT_USERS." WHERE uid='{$user_id}' LIMIT 1";
		$result=$this->db->sql_query($sql);
		if(!$this->db->sql_fetchrow($result))
		{
			$sql = 'INSERT INTO ' . XBT_USERS . "
				(uid, torrent_pass) VALUES('{$user_id}', '{$user_passkey}')";
			$result=$this->db->sql_query($sql);
		}
		else
		{*/
			$sql = 'UPDATE ' . XBT_USERS . "
				SET torrent_pass='{$user_passkey}' WHERE uid='{$user_id}'";
			$result=$this->db->sql_query($sql);
		//}

		if(!$result)
		{
			return false;
		}

		return $user_passkey;
	}

	public function check_downloaded_file($event)
	{
		$attachment=$event['attachment'];

		if($attachment['extension']=='torrent' && !$attachment['in_message'])
		{
			$sql = 'SELECT f.forum_tracker, f.forum_id
				FROM ' . POSTS_TABLE . ' p, ' . FORUMS_TABLE . ' f
				WHERE p.post_id = ' . (int) $attachment['post_msg_id'] . '
					AND p.forum_id = f.forum_id LIMIT 1';
			$result = $this->db->sql_query($sql);
			$forum_row = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);
			$forum_tracker=$forum_row['forum_tracker']==1 ? 1 : 0;

			if($forum_tracker && $this->user->data['user_id']!=$attachment['poster_id'] && !$this->is_admod && !$this->auth->acl_get('m_', $forum_row['forum_id']))
			{
				trigger_error('DOWNLOAD_NOT_ALLOWED');
			}
		}

	}

	public function add_permissions($event)
	{
		$permissions = $event['permissions'];

		$permissions += array(
			'u_attachfonly' => array('lang'=>'ACL_U_ATTACHFONLY', 'cat'=>'ppkextensions'),
			'u_candowntorr' => array('lang'=>'ACL_U_CANDOWNTORR', 'cat'=>'ppkextensions'),
			'u_candownpostscr' => array('lang'=>'ACL_U_CANDOWNPOSTSCR', 'cat'=>'ppkextensions'),
			'u_cansearchtr' => array('lang'=>'ACL_U_CANSEARCHTR', 'cat'=>'ppkextensions'),
			'u_canskiptcheck' => array('lang'=>'ACL_U_CANSKIPTCHECK', 'cat'=>'ppkextensions'),
			'u_cansetstatus' => array('lang'=>'ACL_U_CANSETSTATUS', 'cat'=>'ppkextensions'),
			'u_canviewtrtop' => array('lang'=>'ACL_U_CANVIEWTRTOP', 'cat'=>'ppkextensions'),
			'u_canviewtopdowntorrents' => array('lang'=>'ACL_U_CANVIEWTOPDOWNTORRENTS', 'cat'=>'ppkextensions'),
			'u_canviewvtstats' => array('lang'=>'ACL_U_CANVIEWVTSTATS', 'cat'=>'ppkextensions'),
			'u_canviewmuastatr' => array('lang'=>'ACL_U_CANVIEWMUASTATR', 'cat'=>'ppkextensions'),
			'u_canviewmuastatorr' => array('lang'=>'ACL_U_CANVIEWMUASTATORR', 'cat'=>'ppkextensions'),
			'u_canviewtrstat' => array('lang'=>'ACL_U_CANVIEWTRSTAT', 'cat'=>'ppkextensions'),
			'u_canskiprequpratio' => array('lang'=>'ACL_U_CANSKIPREQUPRATIO', 'cat'=>'ppkextensions'),
			'u_canskiptrestricts' => array('lang'=>'ACL_U_CANSKIPTRESTRICTS', 'cat'=>'ppkextensions'),
			'u_cansetrequpratio' => array('lang'=>'ACL_U_CANSETREQUPRATIO', 'cat'=>'ppkextensions'),

			'f_attachfonly' => array('lang'=>'ACL_F_ATTACHFONLY', 'cat'=>'ppkextensions'),
			'f_candowntorr' => array('lang'=>'ACL_F_CANDOWNTORR', 'cat'=>'ppkextensions'),
			'f_candownpostscr' => array('lang'=>'ACL_F_CANDOWNPOSTSCR', 'cat'=>'ppkextensions'),
			'f_canskiptcheck' => array('lang'=>'ACL_F_CANSKIPTCHECK', 'cat'=>'ppkextensions'),
			'f_cansetstatus' => array('lang'=>'ACL_F_CANSETSTATUS', 'cat'=>'ppkextensions'),
			'f_canviewvtstats' => array('lang'=>'ACL_F_CANVIEWVTSTATS', 'cat'=>'ppkextensions'),
			'f_canskiprequpratio' => array('lang'=>'ACL_F_CANSKIPREQUPRATIO', 'cat'=>'ppkextensions'),
			'f_cansetrequpratio' => array('lang'=>'ACL_F_CANSETREQUPRATIO', 'cat'=>'ppkextensions'),

			'a_xbtbb3cker' => array('lang'=>'ACL_A_XBTBB3CKER', 'cat'=>'ppkextensions'),

		);

		$event['permissions'] = $permissions;

		$categories['ppkextensions'] = 'ACL_CAT_PPKEXT';

		$event['categories'] = array_merge($event['categories'], $categories);
	}



}
