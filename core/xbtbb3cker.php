<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\core;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

class xbtbb3cker
{
	/** @var \phpbb\config\config */
	protected $config;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\user */
	protected $user;

	/** @var string phpBB root path */
	protected $root_path;

	/** @var string PHP extension */
	protected $php_ext;

	/** @var \phpbb\controller\helper */
	protected $helper;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\cache\service */
	protected $cache;

	public function __construct(\phpbb\config\config $config, \phpbb\request\request_interface $request, \phpbb\template\template $template, \phpbb\user $user, $root_path, $php_ext, \phpbb\controller\helper $helper, \phpbb\db\driver\driver_interface $db, \phpbb\auth\auth $auth, \phpbb\cache\service $cache)
	{
		$this->config = $config;
		$this->request = $request;
		$this->template = $template;
		$this->user = $user;
		$this->root_path = $root_path;
		$this->php_ext = $php_ext;
		$this->helper = $helper;
		$this->db = $db;
		$this->auth = $auth;
		$this->cache = $cache;

	}

	public function feed_tracker_url()
	{
		return $this->helper->route('ppk_xbtbb3cker_controller_rss');

	}

	public function topdown_torrents($page, $user_tracker_options, $forum_id=0, $forum_name='')
	{
		if($page=='index')
		{
			$u_canviewtopdowntorrents=$this->auth->acl_get('u_canviewtopdowntorrents') && $this->config['ppkbb_topdown_torrents'][1] && !$user_tracker_options[2] ? true : false;
			$tdt_suffix='fid=0&id=_i';
			$tdt_id='_i';
			$tdt_title=$this->config['ppkbb_topdown_torrents'][11] ? sprintf($this->user->lang['TOPDOWN_TORRENTS_ASNEWTORRENTS']) : sprintf($this->user->lang['TOPDOWN_TORRENTS']);
		}
		else
		{
			$u_canviewtopdowntorrents=$this->auth->acl_get('u_canviewtopdowntorrents') && $this->config['ppkbb_topdown_torrents'][2] && !$user_tracker_options[2] ? true : false;
			if($this->config['ppkbb_topdown_torrents_exclude'] && $u_canviewtopdowntorrents)
			{
				if($this->config['ppkbb_topdown_torrents_exclude'] && (($this->config['ppkbb_topdown_torrents_trueexclude'] && in_array($forum_id, $this->config['ppkbb_topdown_torrents_exclude']))) || (!$this->config['ppkbb_topdown_torrents_trueexclude'] && !in_array($forum_id, $this->config['ppkbb_topdown_torrents_exclude'])))
				{
					$u_canviewtopdowntorrents=false;
				}
			}
			$tdt_suffix="fid={$forum_id}&id=_f";
			$tdt_id='_f';
			$tdt_title=$this->config['ppkbb_topdown_torrents'][11] ? sprintf($this->user->lang['TOPDOWN_TORRENTS_ASNEWTORRENTS_INFORUM'], $forum_name) : sprintf($this->user->lang['TOPDOWN_TORRENTS_INFORUM'], $forum_name);

		}

		if($u_canviewtopdowntorrents)
		{

			$topdown_torrents_url=str_replace('&amp;', '&', $this->helper->route('ppk_xbtbb3cker_controller_topdown_torrents'));

			$tdt_amp=strpos($topdown_torrents_url, '?')!==false ? '&' : '?';

			$topdown_torrents_url.=$tdt_amp.$tdt_suffix;

			$this->template->assign_vars(array(
				'TDT_URL'	=> $topdown_torrents_url,
				'TDT_ID'	=> $tdt_id,

				'TOPDOWN_TORRENTS_POSTERS' => true,

				'S_TOPDOWN_TORRENTS_WIDTH' => $this->config['ppkbb_topdown_torrents'][4],
				'S_TOPDOWN_TORRENTS_WIDTH2' => $this->config['ppkbb_topdown_torrents'][12]==1 ? $this->config['ppkbb_topdown_torrents'][5]*2 : false,
				'S_TOPDOWN_TORRENTS_HEIGHT' => $this->config['ppkbb_topdown_torrents'][5]+20,
				'S_TOPDOWN_TORRENTS_BUTTPOS' => $this->my_int_val($this->config['ppkbb_topdown_torrents'][5]/2),
				'S_TDT_TYPE' => $this->config['ppkbb_topdown_torrents'][12],
				'S_TOPDOWN_TORRENTS' => $tdt_title,
				'S_TOPDOWN_TORRENTS_AUTOSTEP' => $this->config['ppkbb_topdown_torrents_options'][0] ? 'true' : 'false',
				'S_TOPDOWN_TORRENTS_MOVEBY' => $this->config['ppkbb_topdown_torrents_options'][1] ? $this->config['ppkbb_topdown_torrents_options'][1] : 1,
				'S_TOPDOWN_TORRENTS_PAUSE' => $this->config['ppkbb_topdown_torrents_options'][2] ? $this->config['ppkbb_topdown_torrents_options'][2]*1000 : 1000,
				'S_TOPDOWN_TORRENTS_SPEED' => $this->config['ppkbb_topdown_torrents_options'][3] ? $this->config['ppkbb_topdown_torrents_options'][3]*1000 : 3000,
				'S_TOPDOWN_TORRENTS_WRAPAROUND' => $this->config['ppkbb_topdown_torrents_options'][4] ? 'true' : 'false',
				'S_TOPDOWN_TORRENTS_WRAPBEHAVIOR' => in_array($this->config['ppkbb_topdown_torrents_options'][5], array('pushpull', 'slide')) ? $this->config['ppkbb_topdown_torrents_options'][5] : 'slide',
				'S_TOPDOWN_TORRENTS_PERSIST' => $this->config['ppkbb_topdown_torrents_options'][6] ? 'true' : 'false',
				'S_TOPDOWN_TORRENTS_DEFAULTBUTTONS' => $this->config['ppkbb_topdown_torrents_options'][7] ? 'true' : 'false',
				'S_TOPDOWN_TORRENTS_MOVEBY2' => $this->config['ppkbb_topdown_torrents_options'][8] ? $this->config['ppkbb_topdown_torrents_options'][8] : 1,
				)
			);
		}

	}

	public function memberlist_tracker_info($member, $is_canviewmuastatr, $is_canviewmuastatorr, $is_admod)
	{
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/memberlist_add1.'.$this->php_ext);

	}

	public function get_top_url()
	{
		$top_url=$this->helper->route('ppk_xbtbb3cker_controller_top');

		return $top_url;
	}

	public function viewtopic_torrent_template($is_candowntorr, $post_torrents, $poster_id, $is_canviewvtstats, $forum_id, $topic_id, $post_id, $is_admod, $post_posters, $post_screenshots, $is_candownpostscr, $torrents_bookmark)
	{
		$torrent_url=$this->helper->route('ppk_xbtbb3cker_controller_download');
		$muavt_url=$this->helper->route('ppk_xbtbb3cker_controller_tracker_ajax');

		/*if(!$this->config['enable_mod_rewrite'] && strpos($torrent_url, 'app.'.$this->phpEx.'/download/torrent.')===false)
		{
			$torrent_url=str_replace('torrent.', 'app.'.$this->phpEx.'/download/torrent.', $torrent_url);
		}	*/
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/viewtopic_add2.'.$this->php_ext);

		torrent_template($is_candowntorr, $post_torrents, $poster_id, $is_canviewvtstats, $forum_id, $topic_id, $post_id, $is_admod, $post_posters, $post_screenshots, $is_candownpostscr, $torrent_url, $this, $torrents_bookmark, $muavt_url);
	}

	public function viewtopic_torrent_data($is_candowntorr, $post_torrents, $poster_id, $post_posters, $post_screenshots, $is_candownpostscr, $is_canviewvtstats, $post_id, $is_cansetstatus, $is_cansetrequpratio, $torrent_status)
	{
		$tracker_ajax_url=$this->helper->route('ppk_xbtbb3cker_controller_tracker_ajax');
		$tracker_ajax_amp=strpos($tracker_ajax_url, '?')!==false ? '&amp;' : '?';

		$set_status_url=$set_requpratio_url=$bookmarks_url=false;

		if($is_cansetstatus)
		{
			$set_status_url=$tracker_ajax_url.$tracker_ajax_amp.'action=set_status&amp;p='.$post_id;
		}

		if($is_cansetrequpratio)
		{
			$set_requpratio_url=$tracker_ajax_url.$tracker_ajax_amp.'action=set_requpratio&amp;p='.$post_id;
		}

		if($this->config['ppkbb_tracker_bookmarks'][0] && $this->user->data['is_registered'])
		{
			$bookmarks_url=$tracker_ajax_url.$tracker_ajax_amp.'action=bookmarks&amp;p='.$post_id;
		}

		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/viewtopic_add1.'.$this->php_ext);

		return torrent_data($is_candowntorr, $post_torrents, $poster_id, $post_posters, $post_screenshots, $is_candownpostscr, $is_canviewvtstats, $post_id, $set_status_url, $set_requpratio_url, $torrent_status, $bookmarks_url);

	}

	public function posting_add_ext_postsrc($exs_posters, $exs_screenshots, $topic_id, $post_id)
	{
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/posting_extimages_add2.'.$this->php_ext);

		add_ext_postscr($exs_posters, $exs_screenshots, $topic_id, $post_id);
	}

	public function posting_ext_postsrc($submit, $preview, $refresh, $mode, $post_id, $forum_id)
	{
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/posting_extimages_add1.'.$this->php_ext);

		return ext_postscr($submit, $preview, $refresh, $mode, $post_id, $forum_id);

	}

	public function search_torrent_cron($forum_id, $torrents_cleanup, $torrents_remote)
	{
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/search_add_cron.'.$this->php_ext);

	}

	public function viewforum_torrent_cron($forum_id, $torrents_cleanup, $torrents_remote)
	{
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/viewforum_add_cron.'.$this->php_ext);

	}

	public function viewtopic_torrent_cron($forum_id, $torrents_cleanup, $torrents_remote, $torrents_broken)
	{

		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/viewtopic_add_cron.'.$this->php_ext);

		viewtopic_cron($forum_id, $torrents_cleanup, $torrents_remote, $torrents_broken);

	}

	public function index_stat_cron($is_canviewtrstat)
	{
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/index_add_cron.'.$this->php_ext);

		if($this->config['ppkbb_display_trstat'][0] && $is_canviewtrstat)
		{
			include_once($this->root_path.'ext/ppk/xbtbb3cker/include/tracker_stat.'.$this->php_ext);
		}

	}

	public function recode_torrent($physical_filename, $attach_id, $data, $torrent_status)
	{
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/recode_torrent.'.$this->php_ext);
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/bencoding.'.$this->php_ext);

		return recode_torrent_data($physical_filename, $attach_id, $data['forum_id'], $data['topic_id'], $data['post_id'], $torrent_status, array(), $data['poster_id']);

	}

	public function check_torrent($filedata)
	{
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/check_torrent.'.$this->php_ext);
		include_once($this->root_path.'ext/ppk/xbtbb3cker/include/bencoding.'.$this->php_ext);

		$torrent_statuses=$this->get_torrent_statuses();

		return check_torrent_data($filedata, $torrent_statuses);

	}

	public function get_forb_rtrack()
	{

		$forb_rtracks=array();

		$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_forb rtrack_forb, rt.forb_type FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND rt.user_torrent_zone='0' AND rt.rtracker_remote!='0' AND rt.rtracker_type='s' AND rt.rtracker_forb!='0'";
		$result=$this->db->sql_query($sql, 86400);
		while($row=$this->db->sql_fetchrow($result))
		{
			$forb_rtracks[]=$row;
		}
		$this->db->sql_freeresult($result);

		return $forb_rtracks;
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);
		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : count($s_config);

		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				$s_config[$i]=isset($s_config[$i]) ? $s_config[$i] : '';
				if($type)
				{
					$use_function=false;
					if(is_array($type) && isset($type[$i]) && !empty($type[$i]))
					{
						$use_function=$type[$i];
					}
					else if(is_string($type) && !empty($type))
					{
						$use_function=$type;
					}

					if($use_function)
					{
						$s_config[$i]=@function_exists($use_function) ? call_user_func($use_function, $s_config[$i]) : call_user_func(array($this, $use_function), $s_config[$i]);
					}
				}
			}
		}

		return $s_config;
	}

	public function get_ratio($up, $down, $skip=0)
	{

		if($skip && $down < $skip)
		{
			$ratio='None.';
		}
		else if(!$up && !$down)
		{
			$ratio='Inf.';
		}
		else if(!$up && $down)
		{
			$ratio='Leech.';
		}
		else if(!$down && $up)
		{
			$ratio='Seed.';
		}
		else
		{
			$ratio=number_format($up / $down, 3, '.', '');
		}

		return $ratio;
	}

	public function get_ratio_alias($ratio='')
	{

		if(in_array($ratio, array('Leech.', 'Seed.', 'Inf.', 'None.')) && isset($this->user->lang['USER_RATIOS'][$ratio]))
		{
			$ratio=$this->user->lang['USER_RATIOS'][$ratio];
		}

		return $ratio;
	}

	public function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	public function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');

	}

	public function torrents_remote($torrents_hashes)
	{
		$torrents_remote=array();

		$dt=time();

		if(count($torrents_hashes))
		{
			$r_torr=array('all'=>array(), 'torr'=>array());
			$r_exs=array();
			$torrents_id=array_keys($torrents_hashes);

			$sql='SELECT rt.rtracker_id tracker, rt.user_torrent_zone torrent FROM '.TRACKER_RTRACK_TABLE." rt WHERE rt.rtracker_enabled='1' AND
			(
				(rt.rtracker_remote='1' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')
					OR
				(".$this->db->sql_in_set('rt.user_torrent_zone', $torrents_id)." AND rt.rtracker_type='t')
			)";
			$result=$this->db->sql_query($sql);
			$ra=array();
			while($row_remote=$this->db->sql_fetchrow($result))
			{
				$ra[$row_remote['tracker']]=$row_remote['torrent'];
				if(!$row_remote['torrent'])
				{
					$r_torr['all'][$row_remote['tracker']]=$torrents_hashes;
				}
				else
				{
					isset($torrents_hashes[$row_remote['torrent']]) ? $r_torr['torr'][$row_remote['tracker']][$row_remote['torrent']]=$torrents_hashes[$row_remote['torrent']] : '';
				}
			}
			$this->db->sql_freeresult($result);

			$sql='SELECT tracker, torrent, next_announce FROM '.TRACKER_RANNOUNCES_TABLE.' WHERE '.$this->db->sql_in_set('torrent', $torrents_id);
			$result=$this->db->sql_query($sql);
			while($row_remote=$this->db->sql_fetchrow($result))
			{
				if(isset($ra[$row_remote['tracker']]))
				{
					$r_exs[$ra[$row_remote['tracker']].'_'.$row_remote['torrent']][$row_remote['tracker']]=$row_remote;
				}
			}
			$this->db->sql_freeresult($result);

			if(isset($r_torr['all']))
			{
				foreach($r_torr['all'] as $tr_id => $a_data)
				{
					foreach($a_data as $t_id => $t_hash)
					{
						if(isset($r_exs['0_'.$t_id][$tr_id]))
						{
							if($dt > $r_exs['0_'.$t_id][$tr_id]['next_announce'])
							{
								$torrents_remote[$t_id]=$torrents_hashes[$t_id];
							}
						}
						else
						{
							$torrents_remote[$t_id]=$torrents_hashes[$t_id];
						}
					}
				}
				unset($r_torr['all']);
			}
			if(isset($r_torr['torr']))
			{
				foreach($r_torr['torr'] as $tr_id => $a_data)
				{
					foreach($a_data as $t_id => $t_hash)
					{
						if(isset($r_exs[$t_id.'_'.$t_id][$tr_id]))
						{
							if($dt > $r_exs[$t_id.'_'.$t_id][$tr_id]['next_announce'])
							{
								$torrents_remote[$t_id]=$torrents_hashes[$t_id];
							}
						}
						else
						{
							$torrents_remote[$t_id]=$torrents_hashes[$t_id];
						}
					}
				}
				unset($r_torr['torr']);
			}
			unset($torrents_hashes);
		}

		return $torrents_remote;
	}

	public function get_torrent_health($seed, $leech)
	{

		settype($seed, 'integer');
		settype($leech, 'integer');
		$health=0;
		if(!$leech)
		{
			if(!$seed)
			{
				$health = 0;
			}

			else
			{
				$health = 100;
			}
		}
		else if(!$seed && $leech)
		{
			return $this->user->lang['TORRENT_UNKNOWN_HEALTH'];
		}
		else
		{
			$health = $seed / $leech * 100;

			$health > 100 ? $health=100 : $health=intval($health);
		}

		return $health;

	}

	public function get_torrent_statuses()
	{

		$torrent_statuses=array();

		$this->user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_statuses');

		$torrent_statuses=$this->cache->get("_ppkbb3cker_statuses_{$this->user->data['user_lang']}_cache");

		if(!$torrent_statuses)
		{
			$sql='SELECT * FROM ' . TRACKER_STATUSES_TABLE." WHERE status_enabled='1'";
			$result=$this->db->sql_query($sql/*, 3600*/);
			while($row=$this->db->sql_fetchrow($result))
			{
				$torrent_statuses['STATUS_REASON'][$row['status_id']]=isset($this->user->lang[$row['status_reason']]) ? $this->user->lang[$row['status_reason']] : $row['status_reason'];
				$torrent_statuses['STATUS_MARK'][$row['status_id']]=htmlspecialchars_decode(isset($this->user->lang[$row['status_mark']]) ? $this->user->lang[$row['status_mark']] : $row['status_mark']);
			}
			$this->db->sql_freeresult($result);

			$this->cache->put("_ppkbb3cker_statuses_{$this->user->data['user_lang']}_cache", $torrent_statuses);
		}

		return $torrent_statuses;
	}

	public function get_trestricts($upload, $download, $ratio)
	{
		$trestricts=$this->cache->get('_ppkbb3cker_trestricts');

		if(!$trestricts)
		{
			$sql='SELECT * FROM '.TRACKER_TRESTRICTS_TABLE." WHERE trestrict_enabled='1' ORDER BY user_ratio, user_upload, user_download";
			$result=$this->db->sql_query($sql);
			while($row=$this->db->sql_fetchrow($result))
			{
				$trestricts[]=$row;
			}
			$this->db->sql_freeresult($result);

			$this->cache->put('_ppkbb3cker_trestricts', $trestricts);
		}

		if($trestricts)
		{
			$text_ratio=array('Inf.', 'Leech.', 'Seed.', 'None.');
			foreach($trestricts as $row)
			{
				$valid_restricts=$user_restricts=array();
				$row['user_ratio']!='0.000' ? $valid_restricts['ratio']=$row['user_ratio'] : '';
				$row['user_upload'] ? $valid_restricts['upload']=$row['user_upload'] : '';
				$row['user_download'] ? $valid_restricts['download']=$row['user_download'] : '';

				if(isset($valid_restricts['ratio']))
				{
					if(in_array($ratio, $text_ratio) && in_array($row['user_ratio'], $text_ratio))
					{
						if($ratio==$row['user_ratio'])
						{
							$user_restricts['ratio']=$row['user_ratio'];
						}

					}
					else if(!in_array($ratio, $text_ratio) && !in_array($row['user_ratio'], $text_ratio))
					{
						if($ratio < $row['user_ratio'])
						{
							$user_restricts['ratio']=$row['user_ratio'];
						}

					}
				}

				if(isset($valid_restricts['upload']))
				{
					if($upload < $row['user_upload'])
					{
						$user_restricts['upload']=$row['user_upload'];
					}
				}

				if(isset($valid_restricts['download']))
				{
					if($download > $row['user_download'])
					{
						$user_restricts['download']=$row['user_download'];
					}
				}

				if(count($valid_restricts) && count($user_restricts) && count($valid_restricts)==count($user_restricts))
				{

					!$row['can_leech'] ? $user_restricts['can_leech']=0 : '';
					$row['wait_time'] ? $user_restricts['wait_time']=$row['wait_time'] : '';
					$row['include_torrent'] ? $user_restricts['include_torrent']=1 : '';
					$row['peers_limit'] ? $user_restricts['peers_limit']=$row['peers_limit'] : '';
					$row['torrents_limit'] ? $user_restricts['torrents_limit']=$row['torrents_limit'] : '';
					$row['days_limit'] ? $user_restricts['days_limit']=$row['days_limit'] : '';

					return $user_restricts;
				}
			}
		}

		return array();
	}

	public function get_size_value($value='b', $size=0)
	{
		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		switch($value)
		{
			case 'b':
				return $size;
			break;

			case 'kb':
				return 1024*$size;
			break;

			case 'mb':
				return 1024*1024*$size;
			break;

			case 'gb':
				return 1024*1024*1024*$size;
			break;

			case 'tb':
				return 1024*1024*1024*1024*$size;
			break;

			case 'pb':
				return 1024*1024*1024*1024*1024*$size;
			break;

			case 'eb':
				return 1024*1024*1024*1024*1024*1024*$size;
			break;
		}

	}

	public function select_size_value($value='b', $speed=false, $override=true)
	{
		global $user;

		$values=array('b'=>'BYTES', 'kb'=>'KB', 'mb'=>'MB', 'gb'=>'GB', 'tb'=>'TB', 'pb'=>'PB', 'eb'=>'EB');

		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].($speed ? '/'.$user->lang['TSEC'] : '').'</option>';
		}

		return $form;
	}

	public function build_muavt_page($end, $current, $url, $anchor, $per_page, $type='select')
	{
		if($type=='select')
		{
			$url=str_replace('&amp;', '&', $url);

			$select='<select name="pg" id="pg" onchange="window.location.href=\''.$url.'&pg=\' + this.options[this.selectedIndex].value + \'#'.$anchor.'\';">';

			for($i=0;$i*$per_page<$end;$i++)
			{
				$page=$i+1;
				$select.='<option value="'.$page.'"'.($page==$current || ($page==1 && !$current) ? ' selected="selected"' : '').'>'.$page.'</option>';
			}

			$select.='</select>';
		}
		else
		{
			$select='';
			for($i=0;$i*$per_page<$end;$i++)
			{
				$page=$i+1;
				$select.=' <a class="vt_pg" href="javascript:;" data-href="'.$url.'&amp;pg='.$page.'">'.($page==$current || ($page==1 && !$current) ? "<strong>{$page}</strong>" : $page).'</a> ';
			}
		}

		return $select;

	}

	public function get_muavt_countlist($count)
	{
		global $user;

		$mua_countlist_keys=$mua_countlist_values=array();

		$mua_countlist=array(5=>5, 10=>10, 25=>25, 50=>50, 75=>75, 100=>100, 250=>250, 500=>500, 750=>750, 1000=>1000, 5000=>5000, 10000=>10000);
		foreach($mua_countlist as $k => $v)
		{
			if($count > $k)
			{
				$mua_countlist_keys[]=$k;
				$mua_countlist_values[]=$v;
			}
		}

		$mua_countlist_keys[]=-1;
		$mua_countlist_values[]="'{$this->user->lang['MUA_ALL']}'";

		return array($mua_countlist_keys, $mua_countlist_values);
	}
}
?>
