<?php
/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\ucp;

class ucp_xbtbb3cker_tracker_settings_info
{
	function module()
	{
		return array(
			'filename'	=> '\ppk\xbtbb3cker\ucp\ucp_xbtbb3cker_tracker_settings_module',
			'title'		=> 'UCP_XBTBB3CKER_TRACKER_SETTINGS',
			'modes'		=> array(
				'tracker_settings'	=> array(
					'title' => 'UCP_XBTBB3CKER_TRACKER_SETTINGS',
					'auth' => 'ext_ppk/xbtbb3cker',
					'cat' => array('UCP_XBTBB3CKER_TRACKER_SETTINGS')),
			),
		);
	}
}
