<?php
/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\ucp;

class ucp_xbtbb3cker_tracker_rtrackurl_module
{

	function main($id, $mode)
	{
		global $cache, $config, $db, $user, $auth, $template, $phpbb_root_path, $phpEx, $request;

		$user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_ucp');

		$submit = (isset($_POST['submit'])) ? true : false;
		$error = array();
		$user_id=$user->data['user_id'];

		add_form_key('ucp_tracker_rtrackurl');

		if ($submit)
		{
			if (!check_form_key('ucp_tracker_rtrackurl'))
			{
				$error[] = 'FORM_INVALID';
			}

			if (!count($error))
			{

				$rtracker_id=$request->variable('rtrack_id', array(0=>''));
				if ($config['ppkbb_rtrack_enable'][1])
				{
					$message='';

					if($request->variable('add_rtrack', 0) && $rtracker_id)
					{
						$forb_rtracks=$this->get_forb_rtrack();
						$rtracker_url=$request->variable('rtrack_url', array(0=>''), true);
						$rtracker_enabled=$request->variable('rtrack_enabled', array(0=>''));
						$rtracker_delete=$request->variable('rtrack_delete', array(0=>''));
						$exists_tracker=array();
						$sql="SELECT rs.id, rs.rtracker_md5, rt.rtracker_id rtrack FROM ".TRACKER_RTRACKERS_TABLE." rs LEFT JOIN ".TRACKER_RTRACK_TABLE." rt ON(rs.id=rt.rtracker_id AND rt.rtracker_type='u' AND rt.user_torrent_zone='{$user_id}') WHERE ".$db->sql_in_set('rs.rtracker_md5', array_map('md5', $rtracker_url));
						$result=$db->sql_query($sql);
						while($row=$db->sql_fetchrow($result))
						{
							$exists_tracker[$row['rtracker_md5']]=array('id'=>$row['id'], 'rtrack'=>$row['rtrack']);
						}
						$db->sql_freeresult($result);

						$user_trackers=0;
						if(count($rtracker_id)==1 && isset($rtracker_id[0]) && !$rtracker_id[0])
						{
							$sql="SELECT COUNT(*) user_trackers FROM ".TRACKER_RTRACK_TABLE." WHERE rtracker_type='u' and user_torrent_zone='{$user_id}'";
							$result=$db->sql_query($sql);
							$user_trackers=(int) $db->sql_fetchfield('user_trackers');
							$db->sql_freeresult($result);
						}

						$rtrack_count=1;
						$d_rtrack=array();
						foreach($rtracker_id as $k=>$v)
						{
							$rtrack_url=isset($rtracker_url[$k]) ? $rtracker_url[$k] : '';
							$rtrack_delete=isset($rtracker_delete[$k]) ? $rtracker_delete[$k] : '';
							$rtrack_enabled=isset($rtracker_enabled[$k]) ? $rtracker_enabled[$k] : '';
							$rtrack_id=$this->my_int_val($k);

							if($rtrack_url==='')
							{
								continue;
							}

							if($rtrack_delete)
							{
								$d_rtrack[]=$rtrack_id;

								$rtrack_count-=1;
							}
							else
							{
								$rtrack_url=utf8_normalize_nfc($rtrack_url);

								if((!preg_match('#^(https?|udp):\/\/(\w+|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})#', $rtrack_url) || strlen($rtrack_url) > 255))
								{
									$message=$user->lang['INVALID_RTRACK_URL'].': '.htmlspecialchars($rtrack_url);
									trigger_error($message.'<br /><br />'. sprintf($user->lang['RETURN_UCP'], '<a href="' . $this->u_action . '">', '</a>'));
								}

								$rtrack_forb=0;
								if(count($forb_rtracks))
								{
									foreach($forb_rtracks as $f)
									{
										if($f['forb_type']=='s' && strstr($rtrack_url, $f['rtrack_url']))
										{
											$rtrack_forb=1;
										}
										else if($f['forb_type']=='i' && stristr($rtrack_url, $f['rtrack_url']))
										{
											$rtrack_forb=1;
										}
										else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $rtrack_url))
										{
											$rtrack_forb=1;
										}
									}
								}
								if($rtrack_forb)
								{
									$message=$user->lang['FORB_RTRACK_URL'].': '.htmlspecialchars($rtrack_url);
									trigger_error($message.'<br /><br />'. sprintf($user->lang['RETURN_UCP'], '<a href="' . $this->u_action . '">', '</a>'));
								}
								if($rtrack_count > $config['ppkbb_rtrack_enable'][1] || $user_trackers+$rtrack_count > $config['ppkbb_rtrack_enable'][1])
								{
									$message=sprintf($user->lang['MAXUSERS_ANNOUNCES_LIMIT'], $config['ppkbb_rtrack_enable'][1]);
									trigger_error($message.'<br /><br />'. sprintf($user->lang['RETURN_UCP'], '<a href="' . $this->u_action . '">', '</a>'));
								}

								$rtrack_md5=md5($rtrack_url);
								if(isset($exists_tracker[$rtrack_md5]))
								{
									$rtrack_id=$exists_tracker[$rtrack_md5]['id'];

									$rtrack_new=false;
								}
								else
								{
									$sql="INSERT INTO ".TRACKER_RTRACKERS_TABLE."(rtracker_url, rtracker_md5) VALUES('".$db->sql_escape($rtrack_url)."', '{$rtrack_md5}')";
									$result=$db->sql_query($sql);
									$rtrack_id=$db->sql_nextid();

									$rtrack_new=true;
								}

								if($rtrack_new || !$exists_tracker[$rtrack_md5]['rtrack'])
								{
									$sql='INSERT INTO '.TRACKER_RTRACK_TABLE."
										(rtracker_id, rtracker_enabled, rtracker_type, user_torrent_zone)
											VALUES(
											'".$rtrack_id."',
											'".($rtrack_enabled ? 1 : 0)."',
											'u',
											'{$user_id}'
									)";
									$result=$db->sql_query($sql);
								}
								else
								{
									$sql='UPDATE '.TRACKER_RTRACK_TABLE." SET
										rtracker_enabled='".($rtrack_enabled ? 1 : 0)."'
										WHERE rtracker_id='".$rtrack_id."' AND rtracker_type='u' AND user_torrent_zone='{$user_id}'";
									$result=$db->sql_query($sql);
								}

								$rtrack_count+=1;
							}
						}

						if($d_rtrack)
						{
							$sql='DELETE FROM '.TRACKER_RTRACK_TABLE." WHERE rtracker_id IN('".implode("', '", $d_rtrack)."') AND user_torrent_zone='{$user_id}' AND rtracker_type='u'";
							$result=$db->sql_query($sql);
						}
						$message=$user->lang['RTRACK_SUCCESS'];
						trigger_error($message.'<br /><br />'. sprintf($user->lang['RETURN_UCP'], '<a href="' . $this->u_action . '">', '</a>'));
					}
					$message ? trigger_error($message.'<br /><br />'. sprintf($user->lang['RETURN_UCP'], '<a href="' . $this->u_action . '">', '</a>')) : '';
				}
			}
			// Replace "error" strings with their real, localised form
			$error = preg_replace('#^([A-Z_]+)$#e', "(!empty(\$user->lang['\\1'])) ? \$user->lang['\\1'] : '\\1'", $error);
		}

		if($config['ppkbb_rtrack_enable'][1])
		{
			if($request->variable('add_new', ''))
			{
				$template->assign_block_vars('rtracks', array(
					'COUNT'	=> 0,
					'URL'	=> '',
					'ENABLED'	=> '',
					)
				);
				$template->assign_vars(array(
					'S_NEW_RTRACK'	=> true,
					'S_HIDDEN_FIELD' => '<input type="hidden" name="add_rtrack" value="1" />',
					)
				);
			}
			else
			{
				$sql_where="rt.user_torrent_zone='{$user_id}' AND rt.rtracker_type='u'";
				$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_enabled rtrack_enabled FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND {$sql_where}";
				$result=$db->sql_query($sql);
				while($row=$db->sql_fetchrow($result))
				{
					$template->assign_block_vars('rtracks', array(
						'COUNT'	=> $row['id'],
						'URL'	=> htmlspecialchars($row['rtrack_url']),
						'ENABLED'	=> $row['rtrack_enabled'] ? ' checked="checked"' : '',
						)
					);
				}
				$db->sql_freeresult($result);

				$template->assign_vars(array(
					'S_VIEW_RTRACK'	=> true,
					'S_HIDDEN_FIELD' => '<input type="hidden" name="add_rtrack" value="1" />',
					)
				);
			}
		}
		else
		{
			$template->assign_vars(array(
				'S_VIEW_RTRACK'	=> false,
				)
			);
		}

		$template->assign_vars(array(
			'ERROR'			=> (count($error)) ? implode('<br />', $error) : '',

		));

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['UCP_XBTBB3CKER_TRACKER_RTRACKURL'],
			'S_UCP_ACTION'		=> $this->u_action
		));

		// Set desired template
		$this->tpl_name = 'ucp_xbtbb3cker_tracker_rtrackurl';
		$this->page_title = 'UCP_XBTBB3CKER_TRACKER_RTRACKURL';
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);
		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : count($s_config);

		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				$s_config[$i]=isset($s_config[$i]) ? $s_config[$i] : '';
				if($type)
				{
					$use_function=false;
					if(is_array($type) && isset($type[$i]) && !empty($type[$i]))
					{
						$use_function=$type[$i];
					}
					else if(is_string($type) && !empty($type))
					{
						$use_function=$type;
					}

					if($use_function)
					{
						$s_config[$i]=@function_exists($use_function) ? call_user_func($use_function, $s_config[$i]) : call_user_func(array($this, $use_function), $s_config[$i]);
					}
				}
			}
		}

		return $s_config;
	}

	public function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	public function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

	public function get_forb_rtrack()
	{
		global $db;

		$forb_rtracks=array();

		$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_forb rtrack_forb, rt.forb_type FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND rt.user_torrent_zone='0' AND rt.rtracker_remote!='0' AND rt.rtracker_type='s' AND rt.rtracker_forb!='0'";
		$result=$db->sql_query($sql, 86400);
		while($row=$db->sql_fetchrow($result))
		{
			$forb_rtracks[]=$row;
		}
		$db->sql_freeresult($result);

		return $forb_rtracks;
	}

}
