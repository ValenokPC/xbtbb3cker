<?php
/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\ucp;

class ucp_xbtbb3cker_tracker_data_module
{

	function main($id, $mode)
	{
		global $cache, $config, $db, $user, $auth, $template, $phpbb_root_path, $phpEx;

		$user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_ucp');

		$sql = 'SELECT downloaded user_downloaded, uploaded user_uploaded
			FROM '.XBT_USERS."
			WHERE uid={$user->data['user_id']}";
		$result = $db->sql_query($sql);
		$data = $db->sql_fetchrow($result);
		$db->sql_freeresult($result);

		$user->data['user_uploaded']=$data['user_uploaded'];
		$user->data['user_downloaded']=$data['user_downloaded'];

		$template->assign_vars(array(
			'TRACKER_USER_TORRENTS'		=> $user->data['user_torrents'],
			'TRACKER_USER_COMMENTS'		=> $user->data['user_comments'],
			'TRACKER_USER_UPLOAD'		=> get_formatted_filesize($user->data['user_uploaded']),
			'TRACKER_USER_DOWNLOAD'		=> get_formatted_filesize($user->data['user_downloaded']),
			'U_TRACKER_USER_TORRENTS'		=> append_sid("{$phpbb_root_path}search.$phpEx", "author_id={$user->data['user_id']}&amp;sr=topics&amp;ot=1&amp;pt=t"),
			'U_TRACKER_USER_COMMENTS'		=> append_sid("{$phpbb_root_path}search.$phpEx", "author_id={$user->data['user_id']}&amp;sr=posts&amp;ot=1&amp;pt=c"),

			'TRACKER_USER_RATIO'		=> $this->get_ratio_alias($this->get_ratio($user->data['user_uploaded'], $user->data['user_downloaded'], $config['ppkbb_tcratio_start'])),


		));

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['UCP_XBTBB3CKER_TRACKER_DATA'],
			'S_UCP_ACTION'		=> $this->u_action
		));

		// Set desired template
		$this->tpl_name = 'ucp_xbtbb3cker_tracker_data';
		$this->page_title = 'UCP_XBTBB3CKER_TRACKER_DATA';
	}


	function get_ratio_alias($ratio='')
	{

		if(in_array($ratio, array('Leech.', 'Seed.', 'Inf.', 'None.')) && isset($this->user->lang['USER_RATIOS'][$ratio]))
		{
			$ratio=$this->user->lang['USER_RATIOS'][$ratio];
		}

		return $ratio;
	}

	function get_ratio($up, $down, $skip=0)
	{

		if($skip && $down < $skip)
		{
			$ratio='None.';
		}
		else if(!$up && !$down)
		{
			$ratio='Inf.';
		}
		else if(!$up && $down)
		{
			$ratio='Leech.';
		}
		else if(!$down && $up)
		{
			$ratio='Seed.';
		}
		else
		{
			$ratio=number_format($up / $down, 3, '.', '');
		}

		return $ratio;
	}
}
