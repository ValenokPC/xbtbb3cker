<?php
/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\ucp;

class ucp_xbtbb3cker_tracker_rtrackurl_info
{
	function module()
	{
		return array(
			'filename'	=> '\ppk\xbtbb3cker\ucp\ucp_xbtbb3cker_tracker_rtrackurl_module',
			'title'		=> 'UCP_XBTBB3CKER_TRACKER_RTRACKURL',
			'modes'		=> array(
				'tracker_rtrackurl'	=> array(
					'title' => 'UCP_XBTBB3CKER_TRACKER_RTRACKURL',
					'auth' => 'ext_ppk/xbtbb3cker',
					'cat' => array('UCP_XBTBB3CKER_TRACKER_RTRACKURL')),
			),
		);
	}
}
