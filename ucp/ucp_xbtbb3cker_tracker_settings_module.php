<?php
/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\ucp;

class ucp_xbtbb3cker_tracker_settings_module
{

	function main($id, $mode)
	{
		global $cache, $config, $db, $user, $auth, $template, $phpbb_root_path, $phpEx, $request;

		$user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_ucp');

		$submit = (isset($_POST['submit'])) ? true : false;
		$error = array();


		add_form_key('ucp_tracker_settings');

		$user->data['user_tracker_options']=$this->my_split_config($user->data['user_tracker_options'], 4, 'my_int_val');

		$template->assign_vars(array(

			'S_UTO_TDT' => $user->data['user_tracker_options'][2] ? true : false,
			'S_TDT_TYPE' => $config['ppkbb_topdown_torrents'][11] ? false : true,

			'S_BOOKMARKS' => $config['ppkbb_tracker_bookmarks'][0] ? true : false,
			'S_SHOW_BOOKMARKS' => $user->data['user_tracker_bookmarks'] ? true : false,
			'S_AUTODEL_BOOKMARKS' => $user->data['user_tracker_options'][3] ? true : false,
			)
		);

		if ($submit)
		{
			if (!check_form_key('ucp_tracker_settings'))
			{
				$error[] = 'FORM_INVALID';
			}

			if (!count($error))
			{
				$message='';

				if($request->variable('recreate_passkey', 0) && $this->create_passkey())
				{
					meta_refresh(3, $this->u_action);
					$message = $user->lang['USER_PASSKEY_CREATED'] . '<br />';
				}

				$del_bookmarks=$request->variable('del_bookmarks', 0);
				if($del_bookmarks && $config['ppkbb_tracker_bookmarks'][0])
				{
					if($del_bookmarks==1)
					{
						$sql='DELETE FROM '.TRACKER_BOOKMARKS_TABLE." WHERE user_id='{$user->data['user_id']}'";
						$db->sql_query($sql);
					}
					else if($del_bookmarks==2)
					{
						$delete_bookmarks=array();
						$sql='SELECT id FROM '.TRACKER_BOOKMARKS_TABLE." b, ".XBT_FILES_USERS." x WHERE b.user_id='{$user->data['user_id']}' AND b.attach_id=x.fid AND b.user_id=x.uid AND x.left='0'";
						$result=$db->sql_query($sql);
						while($row=$db->sql_fetchrow($result))
						{
							$delete_bookmarks[]=$row['id'];
						}
						$db->sql_freeresult($result);

						if($delete_bookmarks)
						{
							$sql='DELETE FROM '.TRACKER_BOOKMARKS_TABLE.' WHERE '.$db->sql_in_set('id', $delete_bookmarks);
							$db->sql_query($sql);
						}
					}

				}

				$uto_tdt=$request->variable('uto_tdt', 0) ? 1 : 0;
				$auto_delete_bookmarks=$request->variable('autodel_bookmarks', 0) ? 1 : 0;
				$user_tracker_bookmarks=$request->variable('show_bookmarks', 0) ? 1 : 0;
				$sql = 'UPDATE '.USERS_TABLE." SET user_tracker_options='0 0 {$uto_tdt} {$auto_delete_bookmarks}', user_tracker_bookmarks='{$user_tracker_bookmarks}' WHERE user_id='{$user->data['user_id']}'";
				$db->sql_query($sql);

				$message .= $user->lang['SETTINGS_UPDATED'] . '<br /><br />';
				$message ? trigger_error($message. sprintf($user->lang['RETURN_UCP'], '<a href="' . $this->u_action . '">', '</a>')) : '';

			}

			// Replace "error" strings with their real, localised form
			$error = preg_replace('#^([A-Z_]+)$#e', "(!empty(\$user->lang['\\1'])) ? \$user->lang['\\1'] : '\\1'", $error);
		}


		$template->assign_vars(array(
			'ERROR'			=> (count($error)) ? implode('<br />', $error) : '',

		));

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['UCP_XBTBB3CKER_TRACKER_SETTINGS'],
			'S_UCP_ACTION'		=> $this->u_action
		));

		// Set desired template
		$this->tpl_name = 'ucp_xbtbb3cker_tracker_settings';
		$this->page_title = 'UCP_XBTBB3CKER_TRACKER_SETTINGS';
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);
		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : count($s_config);

		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				$s_config[$i]=isset($s_config[$i]) ? $s_config[$i] : '';
				if($type)
				{
					$use_function=false;
					if(is_array($type) && isset($type[$i]) && !empty($type[$i]))
					{
						$use_function=$type[$i];
					}
					else if(is_string($type) && !empty($type))
					{
						$use_function=$type;
					}

					if($use_function)
					{
						$s_config[$i]=@function_exists($use_function) ? call_user_func($use_function, $s_config[$i]) : call_user_func(array($this, $use_function), $s_config[$i]);
					}
				}
			}
		}

		return $s_config;
	}

	public function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	public function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

	public function create_passkey($user_id=false)
	{
		global $user, $db;

		$user_id===false ? $user_id=$user->data['user_id'] : '';
		$user_passkey=strtolower(gen_rand_string(8).gen_rand_string(8).gen_rand_string(8).gen_rand_string(8));

		/*$sql="SELECT uid, torrent_pass FROM ".XBT_USERS." WHERE uid='{$user_id}' LIMIT 1";
		$result=$db->sql_query($sql);
		if(!$db->sql_fetchrow($result))
		{
			$sql = 'INSERT INTO ' . XBT_USERS . "
				(uid, torrent_pass) VALUES('{$user_id}', '{$user_passkey}')";
			$result=$db->sql_query($sql);
		}
		else
		{*/
			$sql = 'UPDATE ' . XBT_USERS . "
				SET torrent_pass='{$user_passkey}' WHERE uid='{$user_id}'";
			$result=$db->sql_query($sql);
		//}

		if(!$result)
		{
			return false;
		}

		return $user_passkey;
	}

}
