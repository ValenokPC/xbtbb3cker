<?php
/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\ucp;

class ucp_xbtbb3cker_torrent_data_module
{

	function main($id, $mode)
	{
		global $cache, $config, $db, $user, $auth, $template, $phpbb_root_path, $phpEx, $request;

		$user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_ucp');

		$dt=time();

		$user_id=$user->data['user_id'];

		$torrent_explain=false;

		$torrent_opt=$request->variable('opt', '');

		$torrents_info=array('torrent', 'finished', 'seed', 'leech', '', '', 'history', 'leave', '', 'downloads', 'bookmarks');
		in_array($torrent_opt, $torrents_info) ? '' : $torrent_opt='';

		$torrent_info=array();

		foreach($torrents_info as $k=>$v)
		{
			if($v)
			{
				if(!$config['ppkbb_torrent_statml'][$k] || ($v=='bookmarks' && !$config['ppkbb_tracker_bookmarks'][0]))
				{
					$torrent_opt==$v/* && !$config['ppkbb_torrent_statml'][$k]*/ ? $torrent_opt='' : '';
					continue;
				}
				$ucp_url=$this->u_action."&amp;opt={$v}";
				$torrent_info[$v]='<a href="'.$ucp_url.'#torrent_stat">'.($torrent_opt==$v ? '<b>' : '').$user->lang['TORRENT_INFO_HEADER_'.strtoupper($v)].($torrent_opt==$v ? '</b>' : '').'</a>';
			}
		}

		$sort_opt=array(
			'downloads' => array('author', 'topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'dl_time'),
			'finished' => array('topic_title', 'uploaded', 'downloaded', 'active', 'ratio', 'mtime', 'announced', '`left`'),
			'history' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
			'leave' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
			'leech' => array('topic_title', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
			'seed' => array('topic_title', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
			'torrent' => array('topic_title', 'uploaded', 'downloaded', 'ratio', '`left`', 'active', 'mtime', 'announced'),
			'bookmarks' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'add_date'),

		);

		if(!class_exists('timedelta'))
		{
			$user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
			include_once($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$phpEx);

		}
		$td = new \timedelta('D_MINUTES');

		$assign_vars=$postrow_headers=$postrow_footers=array();

		$i=$mua_count=0;

		if($torrent_opt)
		{
			$torrent_explain=true;
		}

		$mua_add1inc=$phpbb_root_path.'ext/ppk/xbtbb3cker/include/';

		$page=$request->variable('pg', 0);
		$mua_limit=($page ? $page-1 : 0)*$config['ppkbb_mua_countlist'].', '.$config['ppkbb_mua_countlist'];

		if($torrent_explain)
		{
			$ex_fid_ary=array_keys($auth->acl_getf('!f_read', true));
			switch($torrent_opt)
			{
				case 'leave':
					include($mua_add1inc.'mua_add1_leave.'.$phpEx);
					break;

				case 'history':
					include($mua_add1inc.'mua_add1_history.'.$phpEx);
					break;

				case 'finished':
					include($mua_add1inc.'mua_add1_finished.'.$phpEx);
					break;

				case 'seed':
					include($mua_add1inc.'mua_add1_seed.'.$phpEx);
					break;

				case 'leech':
					include($mua_add1inc.'mua_add1_leech.'.$phpEx);
					break;

				case 'torrent':
					include($mua_add1inc.'mua_add1_torrent.'.$phpEx);
					break;

				case 'downloads':
					include($mua_add1inc.'mua_add1_downloads.'.$phpEx);
					break;

				case 'bookmarks':
					include($mua_add1inc.'mua_add1_bookmarks.'.$phpEx);
					break;

			}

			$assigned_vars=count($assign_vars);

			if($mua_count > $i)
			{
				$mua_page=$this->build_muavt_page($mua_count, $page, $this->u_action."&amp;opt={$torrent_opt}", 'torrent_stat', $config['ppkbb_mua_countlist']);
				$torrent_info[$torrent_opt].='&nbsp;'.$mua_page;
			}
			$mua_countlist=$this->get_muavt_countlist(min($i, $config['ppkbb_mua_countlist']));

			foreach($sort_opt[$torrent_opt] as $k => $v)
			{
				$v=strtoupper(str_replace('`', '', $v));
				$postrow_headers[]='<th class="my_th"><a class="my_ath" href="javascript:;">'.(isset($user->lang['TORRENT_INFO_HEADER_'.$v]) ? $user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a></th>';
				$postrow_footers[]='<a href="javascript:;" onclick="fnShowHide('.$k.');">'.(isset($user->lang['TORRENT_INFO_HEADER_'.$v]) ? $user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a>';
			}

			if($assign_vars)
			{
				foreach($assign_vars as $k => $v)
				{
					$template->assign_block_vars($torrent_opt.'_option', $v);
				}
			}

			if($postrow_headers)
			{
				foreach($postrow_headers as $k => $v)
				{
					$template->assign_block_vars('torrent_headers', array('VALUE' => $v));
				}
			}

			$template->assign_vars(array(
				'S_MUA_COUNTLIST_DEFAULT' => count($mua_countlist[0]) > 1 ? 5 : -1,
				'S_MUA_COUNTLIST_KEYS' => implode(', ', $mua_countlist[0]),
				'S_MUA_COUNTLIST_VALUES' => implode(', ', $mua_countlist[1]),

				'S_HAS_TORRENT_EXPLAIN'	=> $torrent_opt ? true : false,

				'S_HAS_TORRENT_EXPLAIN_'.strtoupper($torrent_opt)	=> $torrent_explain ? true : false,

				'S_TORRENT_HEADER' => $postrow_headers ? implode('', $postrow_headers) :false,
				'S_TORRENT_FOOTER' => $postrow_footers ? implode(' : ', $postrow_footers) :false,

			));
		}

		$template->assign_vars(array(
			'TORRENT_INFO_OPT'	=> true,
			'S_FROM_UCP' => true,
			'TORRENT_INFO_STAT_HEADERS' => count($torrent_info) ? implode(' : ', $torrent_info) : false,

			)
		);

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['UCP_XBTBB3CKER_TORRENT_DATA'],
			'S_UCP_ACTION'		=> $this->u_action
		));

		// Set desired template
		$this->tpl_name = 'ucp_xbtbb3cker_torrent_data';
		$this->page_title = 'UCP_XBTBB3CKER_TORRENT_DATA';
	}


	public function get_ratio_alias($ratio='')
	{

		if(in_array($ratio, array('Leech.', 'Seed.', 'Inf.', 'None.')) && isset($this->user->lang['USER_RATIOS'][$ratio]))
		{
			$ratio=$this->user->lang['USER_RATIOS'][$ratio];
		}

		return $ratio;
	}

	public function get_ratio($up, $down, $skip=0)
	{

		if($skip && $down < $skip)
		{
			$ratio='None.';
		}
		else if(!$up && !$down)
		{
			$ratio='Inf.';
		}
		else if(!$up && $down)
		{
			$ratio='Leech.';
		}
		else if(!$down && $up)
		{
			$ratio='Seed.';
		}
		else
		{
			$ratio=number_format($up / $down, 3, '.', '');
		}

		return $ratio;
	}

	public function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	public function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');

	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);
		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : count($s_config);

		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				$s_config[$i]=isset($s_config[$i]) ? $s_config[$i] : '';
				if($type)
				{
					$use_function=false;
					if(is_array($type) && isset($type[$i]) && !empty($type[$i]))
					{
						$use_function=$type[$i];
					}
					else if(is_string($type) && !empty($type))
					{
						$use_function=$type;
					}

					if($use_function)
					{
						$s_config[$i]=@function_exists($use_function) ? call_user_func($use_function, $s_config[$i]) : call_user_func(array($this, $use_function), $s_config[$i]);
					}
				}
			}
		}

		return $s_config;
	}

	public function build_muavt_page($end, $current, $url, $anchor, $per_page, $type='select')
	{
		if($type=='select')
		{
			$url=str_replace('&amp;', '&', $url);

			$select='<select name="pg" id="pg" onchange="window.location.href=\''.$url.'&pg=\' + this.options[this.selectedIndex].value + \'#'.$anchor.'\';">';

			for($i=0;$i*$per_page<$end;$i++)
			{
				$page=$i+1;
				$select.='<option value="'.$page.'"'.($page==$current || ($page==1 && !$current) ? ' selected="selected"' : '').'>'.$page.'</option>';
			}

			$select.='</select>';
		}
		else
		{
			$select='';
			for($i=0;$i*$per_page<$end;$i++)
			{
				$page=$i+1;
				$select.=' <a class="vt_pg" href="javascript:;" data-href="'.$url.'&amp;pg='.$page.'">'.($page==$current || ($page==1 && !$current) ? "<strong>{$page}</strong>" : $page).'</a> ';
			}
		}

		return $select;

	}

	public function get_muavt_countlist($count)
	{
		global $user;

		$mua_countlist_keys=$mua_countlist_values=array();

		$mua_countlist=array(5=>5, 10=>10, 25=>25, 50=>50, 75=>75, 100=>100, 250=>250, 500=>500, 750=>750, 1000=>1000, 5000=>5000, 10000=>10000);
		foreach($mua_countlist as $k => $v)
		{
			if($count > $k)
			{
				$mua_countlist_keys[]=$k;
				$mua_countlist_values[]=$v;
			}
		}

		$mua_countlist_keys[]=-1;
		$mua_countlist_values[]="'{$user->lang['MUA_ALL']}'";


		return array($mua_countlist_keys, $mua_countlist_values);
	}

}
