<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\controller;

class topdown_torrents
{
	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var \phpbb\user */
	protected $user;

	/** @var \ppk\xbtbb3cker\core\xbtbb3cker */
	protected $xbt_functions;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var string phpbb_root_path */
	protected $phpbb_root_path;

	/** @var string phpEx */
	protected $php_ext;

	/** @var string table_prefix */
	protected $table_prefix;

	/** @var \phpbb\cache\service */
	protected $cache;

	public function __construct(\phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\template\template $template, \phpbb\request\request_interface $request, \phpbb\user $user, \ppk\xbtbb3cker\core\xbtbb3cker $functions, \phpbb\config\config $config, $phpbb_root_path, $php_ext, $table_prefix, \phpbb\cache\service $cache)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->template = $template;
		$this->request = $request;
		$this->user = $user;
		$this->xbt_functions = $functions;
		$this->config = $config;
		$this->phpbb_root_path = $phpbb_root_path;
		$this->php_ext = $php_ext;
		$this->cache = $cache;

	}

	public function main()
	{
 		if(!$this->request->server('HTTP_X_REQUESTED_WITH'))
 		{
 			exit();
 		}

		$dt=time();
		$topdown_torrents_fid=$this->request->variable('fid', 0);
		$topdown_torrents_id=$this->request->variable('id', '');
		$topdown_torrents='';

		if(!$this->auth->acl_get('u_canviewtopdowntorrents') || (!$this->config['ppkbb_topdown_torrents'][1] && !$topdown_torrents_fid) || (!$this->config['ppkbb_topdown_torrents'][2] && $topdown_torrents_fid) || !in_array($topdown_torrents_id, array('_p', '_i', '_f')))
		{
			$topdown_torrents=false;
		}
		else
		{
			$disallow_access=array_unique(array_keys($this->auth->acl_getf('!f_read', true)));
			$post_time=($this->config['ppkbb_topdown_torrents'][10] ? "AND p.post_time > ".($dt-($this->config['ppkbb_topdown_torrents'][10])) : '');

			$sql='
				SELECT
					SUM(x.completed'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+x.rem_times_completed' : '').') sum_times_completed,
					SUM(x.leechers'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+x.rem_leechers' : '').') leechers,
					SUM(x.seeders'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+x.rem_seeders' : '').') seeders,
					p.post_subject, p.post_id
				FROM
					'.POSTS_TABLE.' p,
					'.XBT_FILES.' x
				WHERE
					p.post_poster IN(101, 110, 111)
					AND p.post_id = x.post_msg_id
					'.($this->config['ppkbb_topdown_torrents'][7] ? 'AND seeders'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+x.rem_seeders' : '')." >= {$this->config['ppkbb_topdown_torrents'][7]} " : '').'
					'.(count($this->config['ppkbb_topdown_torrents_exclude']) ? 'AND '.$this->db->sql_in_set('p.forum_id', $this->config['ppkbb_topdown_torrents_exclude'], ($this->config['ppkbb_topdown_torrents_trueexclude'] ? true : false)) : '').'
					'.(count($disallow_access) ? 'AND '.$this->db->sql_in_set('p.forum_id', $disallow_access, true) : '').'
					'.($topdown_torrents_fid ? "AND p.forum_id='{$topdown_torrents_fid}'" : '').'
					%1$s
				GROUP BY
					p.post_id
				ORDER BY
					'.($this->config['ppkbb_topdown_torrents'][11] ? 'p.post_time' : 'sum_times_completed').' DESC
			 LIMIT 0, '.$this->config['ppkbb_topdown_torrents'][3];

			$topdown_torrents=$this->config['ppkbb_topdown_torrents'][6] ? $this->cache->get('_ppkbb3cker_tdt_'.md5($sql)) : false;
			if(!$topdown_torrents)
			{
				$result=$this->db->sql_query(sprintf($sql, $post_time), $this->config['ppkbb_topdown_torrents'][6]);
				$post_ids=array();
				while($row=$this->db->sql_fetchrow($result))
				{
					$post_ids[$row['post_id']]=$row;
				}
				$this->db->sql_freeresult($result);

				if(!$post_ids || ($this->config['ppkbb_topdown_torrents'][8] && count($post_ids) < $this->config['ppkbb_topdown_torrents'][8]))
				{
					$topdown_torrents=false;
				}
				else
				{
					$torrents_posters=array();
					$sql_='
						SELECT
							x.post_msg_id, x.topic_id, x.forum_id, i.i_width, i.i_height, i.real_filename i_external, a.real_filename, a.thumbnail, a.attach_id
						FROM
							'.XBT_FILES.' x
						LEFT JOIN
							'.ATTACHMENTS_TABLE." a ON(x.post_msg_id=a.post_msg_id AND a.i_poster='1')
						LEFT JOIN
							".TRACKER_IMAGES_TABLE." i ON(x.post_msg_id=i.post_msg_id AND i.i_poster='1')
						WHERE
							".$this->db->sql_in_set('x.post_msg_id', array_keys($post_ids)
					);
					$result=$this->db->sql_query($sql_);
					while($row=$this->db->sql_fetchrow($result))
					{
						$torrents_posters[$row['post_msg_id']]=$row;
					}
					$this->db->sql_freeresult($result);

					$i=0;
					$topdown_torrents='';
					foreach($post_ids as $k => $v)
					{
						if(!isset($torrents_posters[$k]))
						{
							continue;
						}
						$v=$torrents_posters[$k];

						$i_factor=$v['i_height'] ? $this->xbt_functions->my_float_val($v['i_width']/$v['i_height']) : 0.675;
						$i_width=$this->xbt_functions->my_int_val($this->config['ppkbb_topdown_torrents'][5]*$i_factor);

						$t_title=censor_text($post_ids[$k]['post_subject']).(!$this->config['ppkbb_topdown_torrents'][11] ? " ({$this->user->lang['TORRENT_COMPLETED']}: {$post_ids[$k]['sum_times_completed']})" : '');

						$no_image_url=$this->phpbb_root_path.'ext/ppk/xbtbb3cker/images/no_poster.gif';
						if($v['attach_id'])
						{
							$image_url=append_sid("{$this->phpbb_root_path}download/file.{$this->php_ext}", 'id='.$v['attach_id'].($this->config['ppkbb_topdown_torrents'][9] && $v['thumbnail'] ? '&amp;t=1' : ''));
						}
						else if($v['i_external'])
						{
							$image_url=$v['i_external'];
						}
						else
						{
							$image_url=$no_image_url;
						}

						$post_url=append_sid("{$this->phpbb_root_path}viewtopic.{$this->php_ext}", 'f='.$v['forum_id'].'&amp;t='.$v['topic_id'].'&amp;p='.$k.'#p'.$k);

						if(!$this->config['enable_mod_rewrite'])
						{
							if($topdown_torrents_id=='_p')
							{
// 								$replace_str="../";
// 								$insert_str='';
// 								$image_url=str_replace($replace_str, $insert_str, $image_url);
// 								$post_url=str_replace($replace_str, $insert_str, $post_url);
							}
							else
							{
								$replace_str='../';
								$insert_str='';
								$image_url=str_replace($replace_str, $insert_str, $image_url);
								$post_url=str_replace($replace_str, $insert_str, $post_url);
							}

						}
						$tdt_image='<img class="tdt_image" rel="tdt'.$i.'" src="'.$image_url.'" alt="'.$t_title.'" height="'.$this->config['ppkbb_topdown_torrents'][5].'" width="'.$i_width.'" onError="this.onerror=null;this.src=\'/'.$no_image_url.'\';" />';

						$topdown_torrents.='<div class="panel" style="padding:5px;"><a href="'.$post_url.'" title="'.$t_title.'">'.$tdt_image.'</a></div>';

						$i+=1;
					}


				}
				$this->config['ppkbb_topdown_torrents'][6] ? $this->cache->put('_ppkbb3cker_tdt_'.md5($sql), array($topdown_torrents), $this->config['ppkbb_topdown_torrents'][6]) : '';
			}
			else
			{
				$topdown_torrents=$topdown_torrents[0];
			}

		}

		header('Content-type: text/html; charset=UTF-8');

		if($topdown_torrents)
		{
			echo $topdown_torrents;
		}
		else
		{
			echo '
				<div id="panel" style="margin-left:10px;white-space:nowrap;">'.$this->user->lang['NO_TDT'].'</div>
				<script type="text/javascript">
				// <![CDATA[
				jQuery(document).ready(
					function($)
					{
						//$(".tdtnav_buttons").remove();
						$("#topdown_torrents").animate({\'height\': \'15px\'});
						$("#gallerya'.htmlspecialchars($topdown_torrents_id).'").animate({\'height\': \'15px\'});
					}
				);
				stepcarousel.resetsettings();
				// ]]>
				</script>
			';
		}

		exit();
	}


}
