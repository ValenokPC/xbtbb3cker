<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\controller;

class top
{
	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var \phpbb\user */
	protected $user;

	/** @var \ppk\xbtbb3cker\core\xbtbb3cker */
	protected $xbt_functions;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var string phpbb_root_path */
	protected $phpbb_root_path;

	/** @var string phpEx */
	protected $php_ext;

	/** @var string table_prefix */
	protected $table_prefix;

	/** @var \phpbb\cache\service */
	protected $cache;

	/** @var \phpbb\controller\helper */
	protected $helper;

	public function __construct(\phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\template\template $template, \phpbb\request\request_interface $request, \phpbb\user $user, \ppk\xbtbb3cker\core\xbtbb3cker $functions, \phpbb\config\config $config, $phpbb_root_path, $php_ext, $table_prefix, \phpbb\cache\service $cache, \phpbb\controller\helper $helper)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->template = $template;
		$this->request = $request;
		$this->user = $user;
		$this->xbt_functions = $functions;
		$this->config = $config;
		$this->phpbb_root_path = $phpbb_root_path;
		$this->php_ext = $php_ext;
		$this->cache = $cache;
		$this->helper = $helper;

	}

	public function main()
	{
		$this->user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_top');

		if(!$this->config['ppkbb_tracker_top'][0])
		{
			trigger_error($this->user->lang['TOP_DISABLED']);
		}

		if(!$this->auth->acl_get('u_canviewtrtop'))
		{
			trigger_error($this->user->lang['TOP_NOAUTH']);
		}

		$start=$top_limit=0;
		$top_limit=$this->config['ppkbb_tracker_top'][0];
		$top_inc=$this->phpbb_root_path.'ext/ppk/xbtbb3cker/include/top/';

		$dt=time();

		$ex_fid_ary=array_keys($this->auth->acl_getf('!f_read', true));

		$top_by=$this->request->variable('t', '');
		$top_times=array('w' => $dt-(86400*7), 'm' => $dt-(86400*30), 'a' => 0);
		$top_langs=array('w' => 'TOP_WEEK', 'm' => 'TOP_MONTH', 'a' => 'TOP_ALL');
		!isset($top_times[$top_by]) ? $top_by='a' : '';
		$top_time=$top_times[$top_by];

		foreach($top_langs as $k=>$v)
		{
			$this->template->assign_vars(array(
				'LNG_'.$v => isset($this->user->lang[$top_langs[$top_by]]) && $k==$top_by ? "<strong>{$this->user->lang[$v]}</strong>" : $this->user->lang[$v],
				)
			);
		}

		$top_url=$this->helper->route('ppk_xbtbb3cker_controller_top');
		$top_amp=strpos($top_url, '?')!==false ? '&amp;' : '?';

		$this->template->assign_vars(array(
			'LNG_TOP_BY_AUTHOR'=>sprintf($this->user->lang['TOP_BY_AUTHOR'], $top_limit),
			'LNG_TOP_BY_DOWNSUM'=>sprintf($this->user->lang['TOP_BY_DOWNSUM'], $top_limit),
			'LNG_TOP_BY_RATIO'=>sprintf($this->user->lang['TOP_BY_RATIO'], $top_limit),
			'LNG_TOP_BY_TORRENTS'=>sprintf($this->user->lang['TOP_BY_TORRENTS'], $top_limit),
			'LNG_TOP_BY_UPLOAD'=>sprintf($this->user->lang['TOP_BY_UPLOAD'], $top_limit),
			'U_TOP_WEEK' => $top_url.$top_amp.'t=w',
			'U_TOP_MONTH' => $top_url.$top_amp.'t=m',
			'U_TOP_ALL' => $top_url.$top_amp.'t=a',
			'TOP_CURR' => $top_by,
			'TOP_BY_AUTHOR' => $this->config['ppkbb_tccron_jobs'][0] ? true : false,
			'TRACKER_EXT_PATH' => $this->config['enable_mod_rewrite'] ? '' : '../',
			)
		);

		include("{$top_inc}top_by_ratio.{$this->php_ext}");
		include("{$top_inc}top_by_upload.{$this->php_ext}");
		include("{$top_inc}top_by_torrents.{$this->php_ext}");

		include("{$top_inc}top_by_downsum.{$this->php_ext}");

		if($this->config['ppkbb_tccron_jobs'][0])
		{
			include("{$top_inc}top_by_author.{$this->php_ext}");
		}

		// Output page ...
		page_header(sprintf($this->user->lang['TRACKER_TOP'], $this->config['ppkbb_tracker_top'][0]));

		$this->template->set_filenames(array(
			'body' => '@ppk_xbtbb3cker/top_body.html')
		);

		page_footer();
	}


}
