<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\controller;

class download
{
	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\controller\helper */
	protected $helper;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var string phpbb_root_path */
	protected $phpbb_root_path;

	/** @var string phpEx */
	protected $php_ext;

	/** @var string table_prefix */
	protected $table_prefix;

	/** @var \phpbb\cache\service */
	protected $cache;

	/** @var \ppk\xbtbb3cker\core\xbtbb3cker */
	protected $xbt_functions;

	protected $torrent;

	public function __construct(\phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\template\template $template, \phpbb\request\request_interface $request, \phpbb\user $user, \phpbb\controller\helper $helper, \phpbb\config\config $config, $phpbb_root_path, $php_ext, $table_prefix, \phpbb\cache\service $cache, \ppk\xbtbb3cker\core\xbtbb3cker $functions)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->template = $template;
		$this->request = $request;
		$this->user = $user;
		$this->helper = $helper;
		$this->config = $config;
		$this->phpbb_root_path = $phpbb_root_path;
		$this->php_ext = $php_ext;
		$this->cache = $cache;
		$this->xbt_functions = $functions;

	}

	public function main()
	{
		$this->user->add_lang('viewtopic');

		// Thank you sun.
		if (isset($_SERVER['CONTENT_TYPE']))
		{
			if ($this->request->server('CONTENT_TYPE') === 'application/x-java-archive')
			{
				exit;
			}
		}
		else if ($this->request->server('HTTP_USER_AGENT') && strpos($this->request->server('HTTP_USER_AGENT'), 'Java') !== false)
		{
			exit;
		}

		require($this->phpbb_root_path . 'includes/functions_download' . '.' . $this->php_ext);

		$attach_id = $this->request->variable('id', 0);
		$mode = $this->request->variable('mode', '');
		$type=$this->request->variable('type', '');
		in_array($type, array('magnet', 'hash')) ? '' : $type='';

		if($this->user->data['is_bot'])
		{
			trigger_error('BOTS_NOT_ALLOWED');
		}

		if (!$this->config['allow_attachments'] && !$this->config['allow_pm_attach'])
		{
			send_status_line(404, 'Not Found');
			trigger_error('ATTACHMENT_FUNCTIONALITY_DISABLED');
		}

		if (!$attach_id)
		{
			send_status_line(404, 'Not Found');
			trigger_error('NO_ATTACHMENT_SELECTED');
		}

		$sql = 'SELECT attach_id, post_msg_id, topic_id, in_message, poster_id, is_orphan, physical_filename, real_filename, extension, mimetype, filesize, filetime
			FROM ' . ATTACHMENTS_TABLE . "
			WHERE attach_id = $attach_id";
		$result = $this->db->sql_query($sql);
		$attachment = $this->db->sql_fetchrow($result);
		$this->db->sql_freeresult($result);

		$forum_tracker=0;
		$dt=time();

		if (!$attachment)
		{
			send_status_line(404, 'Not Found');
			trigger_error('ERROR_NO_ATTACHMENT');
		}
		else if (!download_allowed())
		{
			send_status_line(403, 'Forbidden');
			trigger_error($this->user->lang['LINKAGE_FORBIDDEN']);
		}
		else
		{
			$attachment['physical_filename'] = utf8_basename($attachment['physical_filename']);

			if (!$attachment['in_message'] && !$this->config['allow_attachments'] || $attachment['in_message'] && !$this->config['allow_pm_attach'])
			{
				send_status_line(404, 'Not Found');
				trigger_error('ATTACHMENT_FUNCTIONALITY_DISABLED');
			}

			if ($attachment['is_orphan'])
			{
				// We allow admins having attachment permissions to see orphan attachments...
				$own_attachment = ($this->auth->acl_get('a_attach') || $attachment['poster_id'] == $this->user->data['user_id']) ? true : false;

				if (!$own_attachment || ($attachment['in_message'] && !$this->auth->acl_get('u_pm_download')) || (!$attachment['in_message'] && !$this->auth->acl_get('u_download')))
				{
					send_status_line(404, 'Not Found');
					trigger_error('ERROR_NO_ATTACHMENT');
				}

				// Obtain all extensions...
				$extensions = $this->cache->obtain_attach_extensions(true);
			}
			else
			{
				if (!$attachment['in_message'])
				{
					phpbb_download_handle_forum_auth($this->db, $this->auth, $attachment['topic_id']);

					$sql = 'SELECT p.forum_id, p.post_visibility, t.torrent_status, t.torrent_reqratio, t.torrent_requpload, t.topic_poster, f.forum_tracker
						FROM ' . POSTS_TABLE . ' p, ' . TOPICS_TABLE . ' t, ' . FORUMS_TABLE . ' f
						WHERE p.post_id = ' . (int) $attachment['post_msg_id'] . '
							AND p.topic_id=t.topic_id AND p.forum_id = f.forum_id';
					$result = $this->db->sql_query($sql);
					$post_row = $this->db->sql_fetchrow($result);
					$this->db->sql_freeresult($result);

					if (!$post_row || ($post_row['post_visibility'] != ITEM_APPROVED && !$this->auth->acl_get('m_approve', $post_row['forum_id'])))
					{
						// Attachment of a soft deleted post and the user is not allowed to see the post
						send_status_line(404, 'Not Found');
						trigger_error('ERROR_NO_ATTACHMENT');
					}

					$forum_tracker=$post_row['forum_tracker']==1 ? 1 : 0;
					if(!$forum_tracker)
					{
						trigger_error('NOT_IN_TRACKER');
					}

					if($attachment['extension']!='torrent' || $attachment['mimetype']!='application/x-bittorrent')
					{
						trigger_error('NOT_TORRENT_EXTENSION');
					}

					$sql="SELECT x.fid id, x.info_hash, x.size, x.ctime added, u.username FROM ".XBT_FILES." x LEFT JOIN ".USERS_TABLE." u ON(x.poster_id=u.user_id) WHERE x.fid='{$attachment['attach_id']}' AND x.post_msg_id='{$attachment['post_msg_id']}' LIMIT 1";
					$result = $this->db->sql_query($sql);
					$this->torrent = $this->db->sql_fetchrow($result);
					$this->db->sql_freeresult($result);

					if(!$this->torrent)
					{
						trigger_error('TORRENT_NOT_REGISTERED');
					}

					$attachment['username']=$this->torrent['username'];
					$attachment['info_hash']=$this->torrent['info_hash'];
					$attachment['added']=$this->torrent['added'];
					$attachment['info_hash']=substr($attachment['info_hash'], 0, 20);
					$attachment['forum_id']=$post_row['forum_id'];

					if(!$this->user->data['is_registered'])
					{

						$is_candowntorr = $this->auth->acl_get('u_candowntorr') && $this->auth->acl_get('f_candowntorr', $post_row['forum_id']) ? 1 : 0;
						$this->config['ppkbb_trestricts_options'][5] && $this->torrent['size'] < $this->config['ppkbb_trestricts_options'][5] ? $is_candowntorr=1 : '';
					}
					else
					{
						$is_candowntorr=1;
						if($this->user->data['user_id']!=$post_row['topic_poster'])
						{
							$is_candowntorr=$this->auth->acl_get('u_candowntorr') && $this->auth->acl_get('f_candowntorr', $post_row['forum_id']) ? 1 : 0;
						}
					}

					if(!$is_candowntorr)
					{
						trigger_error('CANT_DOWN_TORRENT');
					}

					$register_link=append_sid("{$this->phpbb_root_path}ucp.{$this->php_ext}", "mode=register", false);
					$login_link=append_sid("{$this->phpbb_root_path}ucp.{$this->php_ext}", "mode=login", false);
					if(!$this->config['enable_mod_rewrite'])
					{
						/*$this->config['enable_mod_rewrite'] ? '' : */$register_link='./../..'.$register_link;
						/*$this->config['enable_mod_rewrite'] ? '' : */$login_link='./../..'.$login_link;
					}

					$torrent_statuses=$this->xbt_functions->get_torrent_statuses();
					if(isset($torrent_statuses['STATUS_MARK'][$post_row['torrent_status']]))
					{
						$is_cansetstatus = $this->user->data['is_registered'] && $this->auth->acl_get('u_cansetstatus') && $this->auth->acl_get('f_cansetstatus', $post_row['forum_id']) ? 1 : 0;

						if($is_cansetstatus || $post_row['torrent_status'] < 1 || ($this->user->data['user_id']==$attachment['poster_id'] && $post_row['torrent_status'] > 0 && in_array($post_row['torrent_status'], $this->config['ppkbb_tcauthor_candown'])))
						{

						}
						else
						{
							trigger_error(sprintf($this->user->lang['CANTDOWN_TORRENT_STATUS'], $torrent_statuses['STATUS_REASON'][$post_row['torrent_status']]));
						}

						if(!$this->user->data['is_registered'] && $post_row['torrent_status'] < 1 && in_array($post_row['torrent_status'], $this->config['ppkbb_tcguest_cantdown']))
						{
							trigger_error(sprintf($this->user->lang['LOGORREG_DOWNLOAD'], $register_link, $login_link));

						}
					}

// 					$post_row['torrent_status'] > 0 ? $attachment['info_hash']=substr($attachment['info_hash'], 0, 20) : '';

					if($this->user->data['is_registered'])
					{
						if($this->config['ppkbb_trestricts_options'][0])
						{

							$sql = 'SELECT downloaded user_downloaded, uploaded user_uploaded
								FROM '.XBT_USERS."
								WHERE uid={$this->user->data['user_id']}";
							$result = $this->db->sql_query($sql);
							$user_data = $this->db->sql_fetchrow($result);
							$this->db->sql_freeresult($result);

							$is_canskiprequpratio = $this->user->data['user_id']!=$attachment['poster_id'] ? (($this->auth->acl_get('u_canskiprequpratio') && $this->auth->acl_get('f_canskiprequpratio', $post_row['forum_id'])) ? 1 : 0) : 1;

							if(!$is_canskiprequpratio)
							{
								if($post_row['torrent_requpload'] && $user_data['user_uploaded'] < $post_row['torrent_requpload'])
								{
									trigger_error(sprintf($this->user->lang['TORRENT_REQUPLOAD_ERROR'], get_formatted_filesize($post_row['torrent_requpload']), get_formatted_filesize($user_data['user_uploaded'])));
								}

								$user_ratio=$this->xbt_functions->get_ratio($user_data['user_uploaded'], $user_data['user_downloaded'], $this->config['ppkbb_tcratio_start']);
								if($post_row['torrent_reqratio']!=0.000 && $user_ratio!='None.' && ($user_ratio < $post_row['torrent_reqratio'] || $user_ratio=='Leech.' || $user_ratio=='Inf.'))
								{
									trigger_error(sprintf($this->user->lang['TORRENT_REQRATIO_ERROR'], $post_row['torrent_reqratio'], $this->xbt_functions->get_ratio_alias($user_ratio)));
								}
							}
						}

						if($this->config['ppkbb_trestricts_options'][1] && $this->config['ppkbb_trestricts_options'][2])
						{
							$is_canskiptrestricts = $this->user->data['user_id']!=$attachment['poster_id'] ? ($this->auth->acl_get('u_canskiptrestricts') ? 1 : 0) : 1;

							if(!$is_canskiptrestricts)
							{
								$user_restricts=unserialize($this->user->data['user_trestricts']);

								if(isset($user_restricts['wait_time']) && isset($user_restricts['include_torrent']))
								{
									if($dt - $attachment['added'] < $user_restricts['wait_time'])
									{
										if(!class_exists('timedelta'))
										{
											$this->user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
											include_once($this->phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$this->php_ext);
										}
										$td = new \timedelta();

										trigger_error(sprintf($this->user->lang['USER_WAIT_TIME'], $td->spelldelta(0, $user_restricts['wait_time'])));
									}
								}

								if(isset($user_restricts['days_limit']))
								{
									$sql='SELECT COUNT(*) dl_count FROM '.TRACKER_DOWNLOADS_TABLE." WHERE downloader_id='{$this->user->data['user_id']}' AND dl_date='".date('Ymd', $dt)."'";
									$result=$this->db->sql_query($sql);
									$dl_count=$this->db->sql_fetchfield('dl_count');
									$this->db->sql_freeresult($result);
									if($dl_count >= $user_restricts['days_limit'])
									{
										trigger_error(sprintf($this->user->lang['USER_RESTRICT_DOWNLOADS_LIMIT'], $user_restricts['days_limit']));
									}
								}
							}
						}

						if($this->user->data['user_type']!=USER_FOUNDER)
						{
							if($this->config['ppkbb_trestricts_options'][4])
							{
								$sql='SELECT COUNT(*) dl_count FROM '.TRACKER_DOWNLOADS_TABLE." WHERE downloader_id='{$this->user->data['user_id']}' AND dl_date='".date('Ymd', $dt)."'";
								$result=$this->db->sql_query($sql);
								$dl_count=$this->db->sql_fetchfield('dl_count');
								$this->db->sql_freeresult($result);
								if($dl_count >= $this->config['ppkbb_trestricts_options'][4])
								{
									trigger_error($this->user->lang['USER_DOWNLOADS_LIMIT']);
								}
							}
							if($this->config['ppkbb_trestricts_options'][7])
							{
								$sql='SELECT COUNT(*) dl_count FROM '.TRACKER_DOWNLOADS_TABLE." WHERE downloader_id!='1' AND dl_date='".date('Ymd', $dt)."' AND dl_ip='{$this->user->ip}'";
								$result=$this->db->sql_query($sql);
								$dl_count=$this->db->sql_fetchfield('dl_count');
								$this->db->sql_freeresult($result);
								if($dl_count >= $this->config['ppkbb_trestricts_options'][7])
								{
									trigger_error($this->user->lang['USER_DOWNLOADS_IPLIMIT']);
								}
							}
						}
					}
					else
					{
						if($this->config['ppkbb_trestricts_options'][3])
						{
							$sql='SELECT COUNT(*) dl_count FROM '.TRACKER_DOWNLOADS_TABLE." WHERE downloader_id='1' AND dl_date='".date('Ymd', $dt)."'";
							$result=$this->db->sql_query($sql);
							$dl_count=$this->db->sql_fetchfield('dl_count');
							$this->db->sql_freeresult($result);
							if($dl_count >= $this->config['ppkbb_trestricts_options'][3])
							{
								trigger_error(sprintf($this->user->lang['GUEST_DOWNLOADS_LIMIT'], $register_link, $login_link));
							}
						}
						if($this->config['ppkbb_trestricts_options'][6])
						{
							$sql='SELECT COUNT(*) dl_count FROM '.TRACKER_DOWNLOADS_TABLE." WHERE downloader_id='1' AND dl_date='".date('Ymd', $dt)."' AND dl_ip='{$this->user->data['session_ip']}'";
							$result=$this->db->sql_query($sql);
							$dl_count=$this->db->sql_fetchfield('dl_count');
							$this->db->sql_freeresult($result);
							if($dl_count >= $this->config['ppkbb_trestricts_options'][6])
							{
								trigger_error($this->user->lang['GUEST_DOWNLOADS_IPLIMIT']);
							}
						}
					}

				}
				else
				{
					trigger_error('NO_TORRENTS_IN_PM');

					// Attachment is in a private message.
					$post_row = array('forum_id' => false);
					phpbb_download_handle_pm_auth($this->db, $this->auth, $this->user->data['user_id'], $attachment['post_msg_id']);
				}

				$extensions = array();
				if (!extension_allowed($post_row['forum_id'], $attachment['extension'], $extensions))
				{
					send_status_line(403, 'Forbidden');
					trigger_error(sprintf($this->user->lang['EXTENSION_DISABLED_AFTER_POSTING'], $attachment['extension']));
				}
			}

			$download_mode = (int) $extensions[$attachment['extension']]['download_mode'];
			$display_cat = $extensions[$attachment['extension']]['display_cat'];

			$this->send_torrent_to_browser($attachment, $this->config['upload_path'], $display_cat, $type, $dt);
			file_gc();

		}


	}

	# /includes/functions_download.php
	public function send_torrent_to_browser($attachment, $upload_dir, $category, $type, $dt)
	{
		$confirm=$this->request->variable('confirm', '');

		if($this->user->data['is_registered'])
		{
			$this->torrent_link=$this->config['ppkbb_torrent_downlink'][0]==1 || $this->config['ppkbb_torrent_downlink'][0]==3 ? true : false;
			$magnet_link=$this->config['ppkbb_torrent_downlink'][1]==1 || $this->config['ppkbb_torrent_downlink'][1]==3 ? true : false;
			$hash_link=$this->config['ppkbb_torrent_downlink'][2]==1 || $this->config['ppkbb_torrent_downlink'][2]==3 ? true : false;
		}
		else
		{
			$this->torrent_link=$this->config['ppkbb_torrent_downlink'][0]==2 || $this->config['ppkbb_torrent_downlink'][0]==3 ? true : false;
			$magnet_link=$this->config['ppkbb_torrent_downlink'][1]==2 || $this->config['ppkbb_torrent_downlink'][1]==3 ? true : false;
			$hash_link=$this->config['ppkbb_torrent_downlink'][2]==2 || $this->config['ppkbb_torrent_downlink'][2]==3 ? true : false;
		}

		if($type=='magnet')
		{
			!$magnet_link ? trigger_error('MAGNET_DOWNLOADS_DISABLED') : '';
		}
		else if($type=='hash')
		{
			!$hash_link ? trigger_error('HASH_DOWNLOADS_DISABLED') : '';
		}
		else
		{
			!$this->torrent_link ? trigger_error('TORRENT_DOWNLOADS_DISABLED') : '';
		}

		$filename = $this->phpbb_root_path . $upload_dir . '/' . $attachment['physical_filename'];

		if (!@file_exists($filename))
		{
			send_status_line(404, 'Not Found');
			trigger_error('ERROR_NO_ATTACHMENT');
		}

// 		if(!$this->user->data['is_registered'] && !$this->config['ppkbb_xcanonymous_announce'])
// 		{
// 			trigger_error('TRGUESTS_DISABLED');
// 		}

		if($this->user->data['is_registered'])
		{
			$sql = 'SELECT uid, torrent_pass user_passkey, downloaded user_downloaded, uploaded user_uploaded
				FROM '.XBT_USERS."
				WHERE uid={$this->user->data['user_id']}";
			$result = $this->db->sql_query($sql);
			$this->user_data = $this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);

			$this->user->data['user_passkey']=$this->user_data['user_passkey'];

		}
		else
		{
			$this->user->data['user_passkey']='';
		}

		$this->config['ppkbb_xclisten_port'] ? '' : $this->config['ppkbb_xclisten_port']=2710;

		$tracker_url=generate_board_url($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? false : true);
		if(!$this->config['ppkbb_announce_url'])
		{
			$this->config['ppkbb_announce_url']=$tracker_url.($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? '' : ':'.$this->config['ppkbb_xclisten_port']);
		}
		else
		{
			$this->config['ppkbb_announce_url']=($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? $tracker_url : $this->config['ppkbb_announce_url'].':'.$this->config['ppkbb_xclisten_port']);
		}

		if($type)
		{
			$this->template->assign_vars(array(
				'TRACKER_EXT_PATH' => $this->config['enable_mod_rewrite'] ? '../' : '../../',
				)
			);
		}

		if((!$this->user->data['is_registered'] && $this->config['ppkbb_addit_options'][1]==2) || $type=='hash' || ($type=='magnet' && !$confirm) || !$this->config['ppkbb_addit_options'][1] || $this->user->data['user_id']==$attachment['poster_id'])
		{

		}
		else
		{
			$sql='SELECT id, dl_ip FROM '.TRACKER_DOWNLOADS_TABLE." WHERE dl_date='".date('Ymd', $dt)."' AND downloader_id='{$this->user->data['user_id']}' AND attach_id='{$attachment['attach_id']}' LIMIT 1";
			$result=$this->db->sql_query($sql);
			$row=$this->db->sql_fetchrow($result);
			$this->db->sql_freeresult($result);

			if(!$row || (!$this->user->data['is_registered'] && $row['dl_ip']!=$this->user->ip))
			{
				$sql="INSERT INTO ".TRACKER_DOWNLOADS_TABLE."(downloader_id, dl_time, dl_ip, attach_id, post_msg_id, dl_date) VALUES('{$this->user->data['user_id']}', '{$dt}', '".$this->db->sql_escape($this->user->ip)."', '{$attachment['attach_id']}', '{$attachment['post_msg_id']}', '".date('Ymd', $dt)."')";
				$result=$this->db->sql_query($sql);
			}

		}

		if($type=='hash')
		{
			trigger_error(sprintf($this->user->lang['TORRENT_HASH_DLINK'], bin2hex($attachment['info_hash'])).$this->user->lang['TRACKER_RETURN_BACK']);
		}
		else if($type=='magnet')
		{
			$this->torrent_basename=utf8_basename(urldecode($attachment['real_filename']));

			if($this->user->data['is_registered'])
			{
				$rtracks=$this->config['ppkbb_announce_url'].($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? $this->config['ppkbb_phpannounce_url'].'?passkey='.$this->user->data['user_passkey'] : '/'.$this->user->data['user_passkey'].'/announce');
			}
			else
			{
				$ppkbb_rtrack_enable=$this->config['ppkbb_rtrack_enable'];
				$ppkbb_rtrack_enable[1]=0;
				$this->config['ppkbb_rtrack_enable']=$ppkbb_rtrack_enable;

				$rtracks=$this->config['ppkbb_announce_url'].($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? $this->config['ppkbb_phpannounce_url'].'?passkey=' : '/announce');
			}
			$rtracks=urlencode($rtracks);

			if($this->config['ppkbb_rtrack_enable'][0] || $this->config['ppkbb_rtrack_enable'][1])
			{
				include("{$this->phpbb_root_path}ext/ppk/xbtbb3cker/include/rtrackfunc.{$this->php_ext}");
				$forb_rtracks=$this->get_forb_rtrack();
				$rtrack=get_rtrack($this->user->ip, $this->config['ppkbb_rtrack_enable'][0], $this->config['ppkbb_rtrack_enable'][1], $this->torrent['id'], $forb_rtracks);
				$rtracks=benc_rtrack_url($rtrack, $this->config['ppkbb_rtrack_enable'][2], true);
			}
			$magnet_src_link="magnet:?xt=urn:btih:".bin2hex($this->torrent['info_hash'])."&dn=".urlencode($this->torrent_basename)."&xl={$this->torrent['size']}&tr={$rtracks}";

			if($confirm)
			{
				header("Location: {$magnet_src_link}");
			}
			else
			{
				$download_url=$this->helper->route('ppk_xbtbb3cker_controller_download');
				$download_amp=strpos($download_url, '?')!==false ? '&amp;' : '?';
				$magnet_src_link=$download_url.$download_amp."id={$attachment['attach_id']}&amp;type={$type}&amp;confirm=1";
			}
			trigger_error(sprintf($this->user->lang['TORRENT_MAGNET_DLINK'], $magnet_src_link).($confirm ? sprintf($this->user->lang['RETURN_BACK'], append_sid("{$this->phpbb_root_path}viewtopic.{$this->php_ext}", 'p='.$attachment['post_msg_id']).'#p'.$attachment['post_msg_id']) : $this->user->lang['TRACKER_RETURN_BACK']));
		}

		if($this->config['ppkbb_append_tfile']/* && stristr($attachment['real_filename'], "[{$this->config['server_name']}]")===false*/)
		{
			$p_array=explode('.', $attachment['real_filename']);
			unset($p_array[count($p_array)-1]);
			$attachment['real_filename']=implode('.', $p_array);

			$this->config['ppkbb_append_tfile']=preg_replace("#[\\/:*?\"<>|]#i", '', $this->config['ppkbb_append_tfile']);

			$attachment['real_filename']=urldecode(str_replace(array('{DOMAIN}', '{TOPICID}', '{FILENAME}'), array($this->config['server_name'], $attachment['topic_id'], $attachment['real_filename']), $this->config['ppkbb_append_tfile'])).'.'.$attachment['extension'];
		}

		// Correct the mime type - we force application/octetstream for all files, except images
		// Please do not change this, it is a security precaution
		if ($category != ATTACHMENT_CATEGORY_IMAGE || strpos($attachment['mimetype'], 'image') !== 0)
		{
			$attachment['mimetype'] = (strpos(strtolower($this->user->browser), 'msie') !== false || strpos(strtolower($this->user->browser), 'opera') !== false) ? 'application/octetstream' : 'application/octet-stream';
		}

		if (@ob_get_length())
		{
			@ob_end_clean();
		}

		// Now send the File Contents to the Browser
		$size = @filesize($filename);

		// To correctly display further errors we need to make sure we are using the correct headers for both (unsetting content-length may not work)

		// Check if headers already sent or not able to get the file contents.
		if (headers_sent() || !@file_exists($filename) || !@is_readable($filename))
		{
			// PHP track_errors setting On?
			if (!empty($php_errormsg))
			{
				send_status_line(500, 'Internal Server Error');
				trigger_error($this->user->lang['UNABLE_TO_DELIVER_FILE'] . '<br />' . sprintf($this->user->lang['TRACKED_PHP_ERROR'], $php_errormsg));
			}

			send_status_line(500, 'Internal Server Error');
			trigger_error('UNABLE_TO_DELIVER_FILE');
		}

		// Make sure the database record for the filesize is correct
		if ($size > 0 && $size != $attachment['filesize'])
		{
			// Update database record
			$sql = 'UPDATE ' . ATTACHMENTS_TABLE . '
				SET filesize = ' . (int) $size . '
				WHERE attach_id = ' . (int) $attachment['attach_id'];
			$this->db->sql_query($sql);
		}

		// Now the tricky part... let's dance
		header('Cache-Control: public');

		// Send out the Headers. Do not set Content-Disposition to inline please, it is a security measure for users using the Internet Explorer.
		header('Content-Type: ' . $attachment['mimetype']);

		if (phpbb_is_greater_ie_version($this->user->browser, 7))
		{
			header('X-Content-Type-Options: nosniff');
		}

		if (empty($this->user->browser) || ((strpos(strtolower($this->user->browser), 'msie') !== false) && !phpbb_is_greater_ie_version($this->user->browser, 7)))
		{
			header('Content-Disposition: attachment; ' . header_filename(htmlspecialchars_decode($attachment['real_filename'])));
			if (empty($this->user->browser) || (strpos(strtolower($this->user->browser), 'msie 6.0') !== false))
			{
				header('Expires: ' . gmdate('D, d M Y H:i:s', $dt) . ' GMT');
			}
		}
		else
		{
			header('Content-Disposition: ' . ((strpos($attachment['mimetype'], 'image') === 0) ? 'inline' : 'attachment') . '; ' . header_filename(htmlspecialchars_decode($attachment['real_filename'])));
			if (phpbb_is_greater_ie_version($this->user->browser, 7) && (strpos($attachment['mimetype'], 'image') !== 0))
			{
				header('X-Download-Options: noopen');
			}
		}

		include("{$this->phpbb_root_path}ext/ppk/xbtbb3cker/include/bencoding.{$this->php_ext}");

		$dict = bdecode_f($filename, $size);

		$dict['comment']=$dict['comment.utf-8']="{$this->user->lang['TORRENT_CREATED_FOR']}: {$this->config['server_name']}\r\n"
			."{$this->user->lang['TORRENT_SUBJECT']}: {$tracker_url}/viewtopic.{$this->php_ext}?f={$attachment['forum_id']}&t={$attachment['topic_id']}&p={$attachment['post_msg_id']}#p{$attachment['post_msg_id']}\r\n"
			."{$this->user->lang['AUTHOR']}: {$attachment['username']} {$tracker_url}/memberlist.$this->php_ext?mode=viewprofile&u={$attachment['poster_id']}\r\n"
			."{$this->user->lang['DOWNLOAD_FOR']}: {$this->user->data['username']} {$tracker_url}/memberlist.$this->php_ext?mode=viewprofile&u={$this->user->data['user_id']}";
		$dict['publisher-url']=$dict['publisher-url.utf-8']="{$tracker_url}/viewtopic.{$this->php_ext}?f={$attachment['forum_id']}&t={$attachment['topic_id']}&p={$attachment['post_msg_id']}#p{$attachment['post_msg_id']}";

		if($this->user->data['is_registered'])
		{
			$announce_url=$this->config['ppkbb_announce_url'].($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? $this->config['ppkbb_phpannounce_url'].'?passkey='.$this->user->data['user_passkey'] : '/'.$this->user->data['user_passkey'].'/announce');
		}
		else
		{
			$announce_url=$this->config['ppkbb_announce_url'].($this->config['ppkbb_phpannounce_enabled'] && $this->config['ppkbb_phpannounce_url'] ? $this->config['ppkbb_phpannounce_url'].'?passkey=' : '/announce');
		}
		$dict['announce'] = $announce_url;

		if($this->config['ppkbb_rtrack_enable'][0] || $this->config['ppkbb_rtrack_enable'][1])
		{
			//unset($dict['announce']);
			require($this->phpbb_root_path.'ext/ppk/xbtbb3cker/include/rtrackfunc.'.$this->php_ext);
			$forb_rtracks=$this->get_forb_rtrack();
			$rtrack=get_rtrack($this->user->ip, $this->config['ppkbb_rtrack_enable'][0], $this->config['ppkbb_rtrack_enable'][1], $attachment['attach_id'], $forb_rtracks);
			$dict['announce-list']=benc_rtrack_url($rtrack, $this->config['ppkbb_rtrack_enable'][2]);
			/*if($this->config['ppkbb_rtrack_enable'][2])
			{
				unset($dict['announce']);
				$dict['announce']='';
			}*/
		}

		// Close the db connection before sending the file
		$this->db->sql_close();

		$enc_file=bencode($dict);
		$size=strlen($enc_file);
		if (!set_modified_headers($attachment['filetime'], $this->user->browser))
		{

			if ($size)
			{
				header("Content-Length: $size");
			}

			// Try to deliver in chunks
			@set_time_limit(0);

			echo $enc_file;

			flush();
		}
		file_gc(false);

		exit;
	}

	public function get_forb_rtrack()
	{

		$forb_rtracks=array();

		$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_forb rtrack_forb, rt.forb_type FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND rt.user_torrent_zone='0' AND rt.rtracker_remote!='0' AND rt.rtracker_type='s' AND rt.rtracker_forb!='0'";
		$result=$this->db->sql_query($sql, 86400);
		while($row=$this->db->sql_fetchrow($result))
		{
			$forb_rtracks[]=$row;
		}
		$this->db->sql_freeresult($result);

		return $forb_rtracks;
	}

}
