<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\controller;

class rss
{
	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var \phpbb\user */
	protected $user;

	/** @var \phpbb\controller\helper */
	protected $helper;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var string phpbb_root_path */
	protected $phpbb_root_path;

	/** @var string phpEx */
	protected $php_ext;

	/** @var string table_prefix */
	protected $table_prefix;

	/** @var \phpbb\cache\service */
	protected $cache;

	protected $feed_options;
	protected $attachments;
	protected $torrents;
	protected $torrent_url;
	protected $tracker_forums;
	protected $topic_data;
	protected $forum_data;

	public function __construct(\phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\template\template $template, \phpbb\request\request_interface $request, \phpbb\user $user, \phpbb\controller\helper $helper, \phpbb\config\config $config, $phpbb_root_path, $php_ext, $table_prefix, \phpbb\cache\service $cache)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->template = $template;
		$this->request = $request;
		$this->user = $user;
		$this->helper = $helper;
		$this->config = $config;
		$this->phpbb_root_path = $phpbb_root_path;
		$this->php_ext = $php_ext;
		$this->cache = $cache;

		$this->separator_stats = "\xE2\x80\x94"; // &mdash;
		$this->separator = "\xE2\x80\xA2"; // &bull;

		$this->num_items = 0;
		$this->sort_days = 7;

	}

	public function main()
	{


		if (/*!$this->config['feed_enable'] || */!$this->config['ppkbb_feed_enable'])
		{
			trigger_error('NO_FEED_ENABLED');
		}

		// Start session
		//$this->user->session_begin();

		if (!empty($this->config['feed_http_auth']) && $this->request->variable('auth', '') == 'http')
		{
			phpbb_http_login(array(
				'auth_message'	=> 'Feed',
				'viewonline'	=> $this->request->variable('viewonline', true),
			));
		}

		//$this->auth->acl($this->user->data);
		//$this->user->setup('viewtopic');

		// Initial var setup
		$forum_id	= $this->request->variable('f', 0);
		$topic_id	= $this->request->variable('t', 0);
		$mode		= $this->request->variable('mode', '');

		// We do not use a template, therefore we simply define the global template variables here
		$global_vars = $item_vars = array();
		$feed_updated_time = 0;

		// Generate params array for use in append_sid() to correctly link back to this page
		$params = false;
		if ($forum_id || $topic_id || $mode)
		{
			$params = array(
				'f'		=> ($forum_id) ? $forum_id : NULL,
				't'		=> ($topic_id) ? $topic_id : NULL,
				'mode'	=> ($mode) ? $mode : NULL,
			);
		}

		// This boards URL
		$board_url = $this->get_board_url();
		$this->tracker_forums=$this->get_tracker_forums();

		// Get correct feed object
		$feed = $this->get_feed($mode, $forum_id, $topic_id);

		// No feed found
		if ($feed === false)
		{
			trigger_error('NO_FEED');
		}

		//$this->attachments=$this->get_attachments($forum_id, $topic_id);
		$this->torrent_url=$this->get_board_url(true).$this->helper->route('ppk_xbtbb3cker_controller_download');
		$torrent_amp=strpos($this->torrent_url, '?')!==false ? '&amp;' : '?';
		$this->torrent_url.=$torrent_amp;

		// Iterate through items
		$result=$this->db->sql_query($feed);
		while ($row = $this->db->sql_fetchrow($result))
		{
			if($this->topic_data)
			{
				$row=array_merge($row, $this->topic_data);
			}
			if($this->forum_data)
			{
				$row=array_merge($row, $this->forum_data);
			}
			// BBCode options to correctly disable urls, smilies, bbcode...
			if (!isset($this->feed_options['options']))
			{
				// Allow all combinations
				$options = 7;

				if (isset($this->feed_options['enable_bbcode']) && isset($this->feed_options['enable_smilies']) && isset($this->feed_options['enable_magic_url']))
				{
					$options = (($row[$this->feed_options['enable_bbcode']]) ? OPTION_FLAG_BBCODE : 0) + (($row[$this->feed_options['enable_smilies']]) ? OPTION_FLAG_SMILIES : 0) + (($row[$this->feed_options['enable_magic_url']]) ? OPTION_FLAG_LINKS : 0);
				}
			}
			else
			{
				$options = $row[$this->feed_options['options']];
			}

			$title = (isset($row[$this->feed_options['title']]) && $row[$this->feed_options['title']] !== '') ? $row[$this->feed_options['title']] : ((isset($row[$this->feed_options['title2']])) ? $row[$this->feed_options['title2']] : '');

			$published = isset($this->feed_options['published']) ? (int) $row[$this->feed_options['published']] : 0;
			$updated = isset($this->feed_options['updated']) ? (int) $row[$this->feed_options['updated']] : 0;

			$display_attachments = ($this->auth->acl_get('u_download') && $this->auth->acl_get('f_download', $row['forum_id']) && isset($row['post_attachment']) && $row['post_attachment']) ? true : false;
			$display_attachments ? $this->get_attachments($forum_id, $topic_id, $row['post_id']) : '';

			$item_row = array(
				'author'		=> isset($this->feed_options['creator']) ? $row[$this->feed_options['creator']] : '',
				'published'		=> ($published > 0) ? $this->format_date($published) : '',
				'updated'		=> ($updated > 0) ? $this->format_date($updated) : '',
				'link'			=> '',
				'title'			=> censor_text($title),
				'category'		=> ($this->config['feed_item_statistics'] && !empty($row['forum_id'])) ? $board_url . '/viewforum.' . $this->php_ext . '?f=' . $row['forum_id'] : '',
				'category_name'	=> ($this->config['feed_item_statistics'] && isset($row['forum_name'])) ? $row['forum_name'] : '',
				'description'	=> censor_text($this->generate_content($row[$this->feed_options['text']], $row[$this->feed_options['bbcode_uid']], $row[$this->feed_options['bitfield']], $options, $row['forum_id'], ($display_attachments ? $this->attachments[$row['post_id']] : array()))),
				'statistics'	=> '',
			);

			// Adjust items, fill link, etc.
			$this->adjust_item($mode, $item_row, $row, $forum_id, $topic_id);

			$item_vars[] = $item_row;

			$feed_updated_time = max($feed_updated_time, $published, $updated);
		}
		$this->db->sql_freeresult($result);

		// If we do not have any items at all, sending the current time is better than sending no time.
		if (!$feed_updated_time)
		{
			$feed_updated_time = time();
		}

		$rss_url=$this->helper->route('ppk_xbtbb3cker_controller_rss');
		$rss_amp=strpos($rss_url, '?')!==false ? '&amp;' : '?';

		// Some default assignments
		// FEED_IMAGE is not used (atom)
		$global_vars = array_merge($global_vars, array(
			'FEED_IMAGE'			=> '',
			'SELF_LINK'				=> $this->get_board_url(true).$rss_url.($params ? $rss_amp.$this->build_feed_url_params($params) : ''),
			'FEED_LINK'				=> $board_url . '/index.' . $this->php_ext,
			'FEED_TITLE'			=> $this->config['sitename'],
			'FEED_SUBTITLE'			=> $this->config['site_desc'],
			'FEED_UPDATED'			=> $this->format_date($feed_updated_time),
			'FEED_LANG'				=> $this->user->lang['USER_LANG'],
			'FEED_AUTHOR'			=> $this->config['sitename'],
		));

		// Output page

		// gzip_compression
		if ($this->config['gzip_compress'])
		{
			if (@extension_loaded('zlib') && !headers_sent())
			{
				ob_start('ob_gzhandler');
			}
		}

		// IF debug extra is enabled and admin want to "explain" the page we need to set other headers...
		if (defined('DEBUG') && $this->request->variable('explain', 0) && $this->auth->acl_get('a_'))
		{
			header('Content-type: text/html; charset=UTF-8');
			header('Cache-Control: private, no-cache="set-cookie"');
			header('Expires: ' . gmdate('D, d M Y H:i:s', time()) . ' GMT');

			$mtime = explode(' ', microtime());
			$totaltime = $mtime[0] + $mtime[1] - $starttime;

			if (method_exists($db, 'sql_report'))
			{
				$db->sql_report('display');
			}

			garbage_collection();
			exit_handler();
		}

		header("Content-Type: application/atom+xml; charset=UTF-8");
		header("Last-Modified: " . gmdate('D, d M Y H:i:s', $feed_updated_time) . ' GMT');

		if (!empty($this->user->data['is_bot']))
		{
			// Let reverse proxies know we detected a bot.
			header('X-PHPBB-IS-BOT: yes');
		}

		echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
		echo '<feed xmlns="http://www.w3.org/2005/Atom" xml:lang="' . $global_vars['FEED_LANG'] . '">' . "\n";
		echo '<link rel="self" type="application/atom+xml" href="' . $global_vars['SELF_LINK'] . '" />' . "\n\n";

		echo (!empty($global_vars['FEED_TITLE'])) ? '<title>' . $global_vars['FEED_TITLE'] . '</title>' . "\n" : '';
		echo (!empty($global_vars['FEED_SUBTITLE'])) ? '<subtitle>' . $global_vars['FEED_SUBTITLE'] . '</subtitle>' . "\n" : '';
		echo (!empty($global_vars['FEED_LINK'])) ? '<link href="' . $global_vars['FEED_LINK'] .'" />' . "\n" : '';
		echo '<updated>' . $global_vars['FEED_UPDATED'] . '</updated>' . "\n\n";

		echo '<author><name><![CDATA[' . $global_vars['FEED_AUTHOR'] . ']]></name></author>' . "\n";
		echo '<id>' . $global_vars['SELF_LINK'] . '</id>' . "\n";

		foreach ($item_vars as $row)
		{
			echo '<entry>' . "\n";

			if (!empty($row['author']))
			{
				echo '<author><name><![CDATA[' . $row['author'] . ']]></name></author>' . "\n";
			}

			echo '<updated>' . ((!empty($row['updated'])) ? $row['updated'] : $row['published']) . '</updated>' . "\n";

			if (!empty($row['published']))
			{
				echo '<published>' . $row['published'] . '</published>' . "\n";
			}

			echo '<id>' . $row['link'] . '</id>' . "\n";
			echo '<link href="' . $row['link'] . '"/>' . "\n";
			echo '<title type="html"><![CDATA[' . $row['title'] . ']]></title>' . "\n\n";

			if (!empty($row['category']) && isset($row['category_name']) && $row['category_name'] !== '')
			{
				echo '<category term="' . $row['category_name'] . '" scheme="' . $row['category'] . '" label="' . $row['category_name'] . '"/>' . "\n";
			}

			echo '<content type="html" xml:base="' . $row['link'] . '"><![CDATA[' . "\n";
			echo $row['description'];

			if (!empty($row['statistics']))
			{
				echo '<p>' . $this->user->lang['STATISTICS'] . ': ' . $row['statistics'] . '</p>';
			}

			echo '<hr />' . "\n" . ']]></content>' . "\n";
			echo '</entry>' . "\n";
		}

		echo '</feed>';

		garbage_collection();
		exit_handler();
	}

	public function set_feed_options($mode, $forum_id=0, $topic_id=0)
	{
		switch($mode)
		{
			case 'trackers':
				$this->feed_options['title']='forum_name';
				$this->feed_options['text']='forum_desc';
				$this->feed_options['bitfield']='forum_desc_bitfield';
				$this->feed_options['bbcode_uid']='forum_desc_uid';
				$this->feed_options['updated']='forum_last_post_time';
				$this->feed_options['options']='forum_desc_options';
			break;

			case 'torrents_new':
				$this->feed_options['title']='topic_title';
				$this->feed_options['title2']='forum_name';

				$this->feed_options['author_id']='topic_poster';
				$this->feed_options['creator']='topic_first_poster_name';
				$this->feed_options['published']='post_time';
				$this->feed_options['updated']='post_edit_time';
				$this->feed_options['text']='post_text';

				$this->feed_options['bitfield']='bbcode_bitfield';
				$this->feed_options['bbcode_uid']='bbcode_uid';

				$this->feed_options['enable_bbcode']='enable_bbcode';
				$this->feed_options['enable_smilies']='enable_smilies';
				$this->feed_options['enable_magic_url']='enable_magic_url';
			break;

			case 'torrents_active':
				$this->feed_options['author_id']='topic_last_poster_id';
				$this->feed_options['creator']='topic_last_poster_name';
			break;

			default:
				if ($topic_id && $this->config['ppkbb_feed_topic'])
				{
					$this->feed_options['title']='post_subject';
					$this->feed_options['title2']='topic_title';

					$this->feed_options['author_id']='user_id';
					$this->feed_options['creator']='username';
					$this->feed_options['published']='post_time';
					$this->feed_options['updated']='post_edit_time';
					$this->feed_options['text']='post_text';

					$this->feed_options['bitfield']='bbcode_bitfield';
					$this->feed_options['bbcode_uid']='bbcode_uid';

					$this->feed_options['enable_bbcode']='enable_bbcode';
					$this->feed_options['enable_smilies']='enable_smilies';
					$this->feed_options['enable_magic_url']='enable_magic_url';
				}
				else if ($forum_id && $this->config['ppkbb_feed_forum'])
				{
					$this->feed_options['title']='post_subject';
					$this->feed_options['title2']='topic_title';

					$this->feed_options['author_id']='user_id';
					$this->feed_options['creator']='username';
					$this->feed_options['published']='post_time';
					$this->feed_options['updated']='post_edit_time';
					$this->feed_options['text']='post_text';

					$this->feed_options['bitfield']='bbcode_bitfield';
					$this->feed_options['bbcode_uid']='bbcode_uid';

					$this->feed_options['enable_bbcode']='enable_bbcode';
					$this->feed_options['enable_smilies']='enable_smilies';
					$this->feed_options['enable_magic_url']='enable_magic_url';
				}
				else if ($this->config['ppkbb_feed_overall'])
				{
					$this->feed_options['title']='post_subject';
					$this->feed_options['title2']='topic_title';

					$this->feed_options['author_id']='user_id';
					$this->feed_options['creator']='username';
					$this->feed_options['published']='post_time';
					$this->feed_options['updated']='post_edit_time';
					$this->feed_options['text']='post_text';

					$this->feed_options['bitfield']='bbcode_bitfield';
					$this->feed_options['bbcode_uid']='bbcode_uid';

					$this->feed_options['enable_bbcode']='enable_bbcode';
					$this->feed_options['enable_smilies']='enable_smilies';
					$this->feed_options['enable_magic_url']='enable_magic_url';
				}
			break;
		}
	}

	public function get_feed($mode, $forum_id, $topic_id)
	{
		switch ($mode)
		{
			case 'trackers':
				if (!$this->config['ppkbb_feed_overall_trackers'])
				{
					return false;
				}

				$this->set_feed_options($mode);

				$in_fid_ary = array_diff($this->get_readable_forums(), $this->get_excluded_forums());
				if (empty($in_fid_ary))
				{
					return false;
				}

				$sql = array(
					'SELECT'	=> 'f.forum_id, f.left_id, f.forum_name, f.forum_last_post_time,
									f.forum_desc, f.forum_desc_bitfield, f.forum_desc_uid, f.forum_desc_options,
									f.forum_topics_approved, f.forum_posts_approved',
					'FROM'		=> array(FORUMS_TABLE => 'f'),
					'WHERE'		=> 'f.forum_type = ' . FORUM_POST . '
									AND ' . $this->db->sql_in_set('f.forum_id', $in_fid_ary),
					'ORDER_BY'	=> 'f.left_id ASC',
				);
				$sql=$this->db->sql_build_query('SELECT', $sql);

				return $sql;

			break;

			case 'torrents_new':
				if (!$this->config['ppkbb_feed_torrents_new']/* || !$this->config['ppkbb_feed_torrents']*/)
				{
					return false;
				}

				$this->num_items = $this->config['ppkbb_feed_torrents_limit'];

				$this->set_feed_options($mode);

				$forum_ids_read = $this->get_readable_forums();
				if (empty($forum_ids_read))
				{
					return false;
				}

				$in_fid_ary = array_diff($forum_ids_read, $this->get_excluded_forums(), $this->get_passworded_forums());
				if (empty($in_fid_ary))
				{
					return false;
				}

				// We really have to get the post ids first!
				$sql = 'SELECT topic_first_post_id, topic_time
					FROM ' . TOPICS_TABLE . '
					WHERE  topic_moved_id = 0
						AND ' . $this->get_forums_visibility_sql('topic', $in_fid_ary)
					. 'ORDER BY topic_time DESC';
				$result = $this->db->sql_query_limit($sql, $this->num_items);

				$post_ids = array();
				while ($row = $this->db->sql_fetchrow($result))
				{
					$post_ids[] = (int) $row['topic_first_post_id'];
				}
				$this->db->sql_freeresult($result);

				if (empty($post_ids))
				{
					return false;
				}

				$sql = array(
					'SELECT'	=> 'f.forum_id, f.forum_name,
									t.topic_id, t.topic_title, t.topic_poster, t.topic_first_poster_name, t.topic_posts_approved, t.topic_posts_unapproved, t.topic_posts_softdeleted, t.topic_views, t.topic_time, t.topic_last_post_time,
									p.post_id, p.post_time, p.post_edit_time, p.post_text, p.bbcode_bitfield, p.bbcode_uid, p.enable_bbcode, p.enable_smilies, p.enable_magic_url, p.post_attachment, t.topic_visibility',
					'FROM'		=> array(
						TOPICS_TABLE	=> 't',
						POSTS_TABLE		=> 'p',
					),
					'LEFT_JOIN'	=> array(
						array(
							'FROM'	=> array(FORUMS_TABLE => 'f'),
							'ON'	=> 'p.forum_id = f.forum_id',
						),
					),
					'WHERE'		=> 'p.topic_id = t.topic_id AND p.post_torrent=1
									AND ' . $this->db->sql_in_set('p.post_id', $post_ids),
					'ORDER_BY'	=> 'p.post_time DESC, p.post_id DESC',
				);
				$sql=$this->db->sql_build_query('SELECT', $sql);

				return $sql;

			break;

			case 'torrents_active':
				if (!$this->config['ppkbb_feed_torrents_active']/* || !$this->config['ppkbb_feed_torrents']*/)
				{
					return false;
				}

				$this->num_items = $this->config['ppkbb_feed_torrents_limit'];

				$this->set_feed_options('torrents');
				$this->set_feed_options($mode);

				$forum_ids_read = $this->get_readable_forums();
				if (empty($forum_ids_read))
				{
					return false;
				}

				$in_fid_ary = array_intersect($forum_ids_read, $this->get_forum_ids());
				//$in_fid_ary = array_diff($in_fid_ary, $this->get_passworded_forums());
				$in_fid_ary = array_diff($in_fid_ary, $this->get_excluded_forums(), $this->get_passworded_forums());

				if (empty($in_fid_ary))
				{
					return false;
				}

				// Search for topics in last X days
				$last_post_time_sql = ($this->sort_days) ? ' AND topic_last_post_time > ' . (time() - ($this->sort_days * 24 * 3600)) : '';

				// We really have to get the post ids first!
				$sql = 'SELECT topic_last_post_id, topic_last_post_time
					FROM ' . TOPICS_TABLE . '
					WHERE topic_moved_id = 0
						AND ' . $this->get_forums_visibility_sql('topic', $in_fid_ary) . '
						' . $last_post_time_sql . '
					ORDER BY topic_last_post_time DESC, topic_last_post_id DESC';
				$result = $this->db->sql_query_limit($sql, $this->num_items);

				$post_ids = array();
				while ($row = $this->db->sql_fetchrow($result))
				{
					$post_ids[] = (int) $row['topic_last_post_id'];
				}
				$this->db->sql_freeresult($result);

				if (empty($post_ids))
				{
					return false;
				}

				$sql = array(
					'SELECT'	=> 'f.forum_id, f.forum_name,
									t.topic_id, t.topic_title, t.topic_posts_approved, t.topic_posts_unapproved, t.topic_posts_softdeleted, t.topic_views,
									t.topic_last_poster_id, t.topic_last_poster_name, t.topic_last_post_time,
									p.post_id, p.post_time, p.post_edit_time, p.post_text, p.bbcode_bitfield, p.bbcode_uid, p.enable_bbcode, p.enable_smilies, p.enable_magic_url, p.post_attachment, t.topic_visibility',
					'FROM'		=> array(
						TOPICS_TABLE	=> 't',
						POSTS_TABLE		=> 'p',
					),
					'LEFT_JOIN'	=> array(
						array(
							'FROM'	=> array(FORUMS_TABLE => 'f'),
							'ON'	=> 'p.forum_id = f.forum_id',
						),
					),
					'WHERE'		=> 'p.topic_id = t.topic_id AND p.post_torrent = 1
									AND ' . $this->db->sql_in_set('p.post_id', $post_ids),
					'ORDER_BY'	=> 'p.post_time DESC, p.post_id DESC',
				);
				$sql=$this->db->sql_build_query('SELECT', $sql);

				return $sql;

			break;

			default:

				if ($topic_id && $this->config['ppkbb_feed_topic'])
				{
					if($mode=='comments' && !$this->config['ppkbb_feed_comments'])
					{
						return false;
					}
					else if(($mode=='torrents' || $mode=='') && !$this->config['ppkbb_feed_torrents'])
					{
						return false;
					}
					$this->set_feed_options('', $forum_id, $topic_id);

					$sql = 'SELECT f.forum_options, f.forum_password, t.topic_id, t.forum_id, t.topic_visibility, t.topic_title, t.topic_time, t.topic_views, t.topic_posts_approved, t.topic_type
						FROM ' . TOPICS_TABLE . ' t
						LEFT JOIN ' . FORUMS_TABLE . ' f
							ON (f.forum_id = t.forum_id)
						WHERE t.topic_id = ' . $topic_id;
					$result = $this->db->sql_query($sql);
					$this->topic_data = $this->db->sql_fetchrow($result);
					$this->db->sql_freeresult($result);

					if (empty($this->topic_data))
					{
						trigger_error('NO_TOPIC');
					}

					$forum_id = (int) $this->topic_data['forum_id'];

					// Make sure topic is either approved or user authed
					if ($this->topic_data['topic_visibility'] != ITEM_APPROVED && !$this->auth->acl_get('m_approve', $forum_id))
					{
						trigger_error('SORRY_AUTH_READ');
					}

					// Make sure forum is not excluded from feed
					$ex_fid=$this->get_excluded_forums();
					if (isset($ex_fid[$this->topic_data['forum_id']]))
					{
						trigger_error('NO_FEED');
					}

					// Make sure we can read this forum
					if (!$this->auth->acl_get('f_read', $forum_id))
					{
						trigger_error('SORRY_AUTH_READ');
					}

					// Make sure forum is not passworded or user is authed
					if ($this->topic_data['forum_password'])
					{
						$forum_ids_passworded = $this->get_passworded_forums();

						if (isset($forum_ids_passworded[$forum_id]))
						{
							trigger_error('SORRY_AUTH_READ');
						}

						unset($forum_ids_passworded);
					}

					$sql = array(
						'SELECT'	=>	'p.post_id, p.post_time, p.post_edit_time, p.post_visibility, p.post_subject, p.post_text, p.bbcode_bitfield, p.bbcode_uid, p.enable_bbcode, p.enable_smilies, p.enable_magic_url, p.post_attachment, ' .
										'u.username, u.user_id',
						'FROM'		=> array(
							POSTS_TABLE		=> 'p',
							USERS_TABLE		=> 'u',
						),
						'WHERE'		=> 'p.topic_id = ' . $topic_id . '
											AND ' . $this->get_visibility_sql('post', $forum_id, 'p.') . '
											AND p.poster_id = u.user_id AND p.post_torrent = ' . ($mode=='comments' ? 0 : 1),
						'ORDER_BY'	=> 'p.post_time DESC, p.post_id DESC
							LIMIT ' . ($mode=='comments' ? $this->config['ppkbb_feed_comments_limit'] : $this->config['ppkbb_feed_torrents_limit']),
					);

					$sql=$this->db->sql_build_query('SELECT', $sql);

					return $sql;
				}
				else if ($forum_id && $this->config['ppkbb_feed_forum'])
				{
					if($mode=='comments' && !$this->config['ppkbb_feed_comments'])
					{
						return false;
					}
					else if(($mode=='torrents' || $mode=='') && !$this->config['ppkbb_feed_torrents'])
					{
						return false;
					}
					$this->set_feed_options('', $forum_id, $topic_id);

					// Check if forum exists
					$sql = 'SELECT forum_id, forum_name, forum_password, forum_type, forum_options
						FROM ' . FORUMS_TABLE . '
						WHERE forum_id = ' . $forum_id;
					$result = $this->db->sql_query($sql);
					$this->forum_data = $this->db->sql_fetchrow($result);
					$this->db->sql_freeresult($result);

					if (empty($this->forum_data))
					{
						trigger_error('NO_FORUM');
					}

					// Forum needs to be postable
					if ($this->forum_data['forum_type'] != FORUM_POST)
					{
						trigger_error('NO_FEED');
					}

					// Make sure forum is not excluded from feed
					$ex_fid=$this->get_excluded_forums();
					if (isset($ex_fid[$this->forum_data['forum_id']]))
					{
						trigger_error('NO_FEED');
					}

					// Make sure we can read this forum
					if (!$this->auth->acl_get('f_read', $forum_id))
					{
						trigger_error('SORRY_AUTH_READ');
					}

					// Make sure forum is not passworded or user is authed
					if ($this->forum_data['forum_password'])
					{
						$forum_ids_passworded = $this->get_passworded_forums();

						if (isset($forum_ids_passworded[$forum_id]))
						{
							trigger_error('SORRY_AUTH_READ');
						}

						unset($forum_ids_passworded);
					}

					// Determine topics with recent activity
					$sql = 'SELECT topic_id, topic_last_post_time
						FROM ' . TOPICS_TABLE . '
						WHERE forum_id = ' . $forum_id . '
							AND topic_moved_id = 0
							AND ' . $this->get_visibility_sql('topic', $forum_id) . '
						ORDER BY topic_last_post_time DESC, topic_last_post_id DESC';
					$result = $this->db->sql_query_limit($sql, $this->num_items);

					$topic_ids = array();
					$min_post_time = 0;
					while ($row = $this->db->sql_fetchrow())
					{
						$topic_ids[] = (int) $row['topic_id'];

						$min_post_time = (int) $row['topic_last_post_time'];
					}
					$this->db->sql_freeresult($result);

					if (empty($topic_ids))
					{
						return false;
					}

					$sql = array(
						'SELECT'	=>	'p.post_id, p.topic_id, p.post_time, p.post_edit_time, p.post_visibility, p.post_subject, p.post_text, p.bbcode_bitfield, p.bbcode_uid, p.enable_bbcode, p.enable_smilies, p.enable_magic_url, p.post_attachment, ' .
										'u.username, u.user_id',
						'FROM'		=> array(
							POSTS_TABLE		=> 'p',
							USERS_TABLE		=> 'u',
						),
						'WHERE'		=> $this->db->sql_in_set('p.topic_id', $topic_ids) . '
										AND ' . $this->get_visibility_sql('post', $forum_id, 'p.') . '
										AND p.post_time >= ' . $min_post_time . '
										AND p.poster_id = u.user_id AND p.post_torrent = ' . ($mode=='comments' ? 0 : 1),
						'ORDER_BY'	=> 'p.post_time DESC, p.post_id DESC
							LIMIT ' . ($mode=='comments' ? $this->config['ppkbb_feed_comments_limit'] : $this->config['ppkbb_feed_torrents_limit']),
					);

					$sql=$this->db->sql_build_query('SELECT', $sql);

					return $sql;
				}
				else if ($this->config['ppkbb_feed_overall'])
				{
					if(($mode=='comments' && !$this->config['ppkbb_feed_comments']) || ($mode=='torrents' && !$this->config['ppkbb_feed_torrents']))
					{
						return false;
					}

					$this->num_items = ($mode=='torrents' ? $this->config['ppkbb_feed_torrents_limit'] : $this->config['ppkbb_feed_comments_limit']);
					$this->set_feed_options('');

					$forum_ids = array_diff($this->get_readable_forums(), $this->get_excluded_forums(), $this->get_passworded_forums());
					if (empty($forum_ids))
					{
						return false;
					}

					// Determine topics with recent activity
					$sql = 'SELECT topic_id, topic_last_post_time
						FROM ' . TOPICS_TABLE . '
						WHERE topic_moved_id = 0
							AND ' . $this->get_forums_visibility_sql('topic', $forum_ids) . '
						ORDER BY topic_last_post_time DESC, topic_last_post_id DESC';
					$result = $this->db->sql_query_limit($sql, $this->num_items);

					$topic_ids = array();
					$min_post_time = 0;
					while ($row = $this->db->sql_fetchrow())
					{
						$topic_ids[] = (int) $row['topic_id'];

						$min_post_time = (int) $row['topic_last_post_time'];
					}
					$this->db->sql_freeresult($result);

					if (empty($topic_ids))
					{
						return false;
					}

					// Get the actual data
					$sql = array(
						'SELECT'	=>	'f.forum_id, f.forum_name, ' .
										'p.post_id, p.topic_id, p.post_time, p.post_edit_time, p.post_visibility, p.post_subject, p.post_text, p.bbcode_bitfield, p.bbcode_uid, p.enable_bbcode, p.enable_smilies, p.enable_magic_url, p.post_attachment, ' .
										'u.username, u.user_id',
						'FROM'		=> array(
							USERS_TABLE		=> 'u',
							POSTS_TABLE		=> 'p',
						),
						'LEFT_JOIN'	=> array(
							array(
								'FROM'	=> array(FORUMS_TABLE	=> 'f'),
								'ON'	=> 'f.forum_id = p.forum_id',
							),
						),
						'WHERE'		=> $this->db->sql_in_set('p.topic_id', $topic_ids) . '
										AND ' . $this->get_forums_visibility_sql('post', $forum_ids, 'p.') . '
										AND p.post_time >= ' . $min_post_time . '
										AND u.user_id = p.poster_id' . ($mode ? ' AND p.post_torrent = ' . ($mode=='comments' ? 0 : 1) : ''),
						'ORDER_BY'	=> 'p.post_time DESC, p.post_id DESC',
					);

					$sql=$this->db->sql_build_query('SELECT', $sql);

					return $sql;
				}

				return false;
			break;
		}

	}

	function adjust_item($mode, &$item_row, &$row, $forum_id=0, $topic_id=0)
	{

		switch ($mode)
		{
			case 'trackers':
				$item_row['link'] = $this->append_sid('viewforum.' . $this->php_ext, 'f=' . $row['forum_id']);

				if ($this->config['feed_item_statistics'])
				{
					$item_row['statistics'] = $this->user->lang('TOTAL_TOPICS', (int) $row['forum_topics_approved'])
						. ' ' . $this->separator_stats . ' ' . $this->user->lang('TOTAL_POSTS_COUNT', (int) $row['forum_posts_approved']);
				}
			break;

			case 'torrents_new':
			case 'torrents_active':

				$item_row['link'] = $this->append_sid('viewtopic.' . $this->php_ext, 't=' . $row['topic_id'] . '&amp;p=' . $row['post_id'] . '#p' . $row['post_id']);

				if ($this->config['feed_item_statistics'])
				{
					$item_row['statistics'] = $this->user->lang['POSTED'] . ' ' . $this->user->lang['POST_BY_AUTHOR'] . ' ' . $this->user_viewprofile($row)
						. ' ' . $this->separator_stats . ' ' . $this->user->format_date($row[$this->feed_options['published']])
						. ' ' . $this->separator_stats . ' ' . $this->user->lang['REPLIES'] . ' ' . ($this->get_count('topic_posts', $row, $row['forum_id']) - 1)
						. ' ' . $this->separator_stats . ' ' . $this->user->lang['VIEWS'] . ' ' . $row['topic_views'];

					if ($this->is_moderator_approve_forum($row['forum_id']))
					{
						if ((int) $row['topic_visibility'] === ITEM_DELETED)
						{
							$item_row['statistics'] .= ' ' . $this->separator_stats . ' ' . $this->user->lang['TOPIC_DELETED'];
						}
						else if ((int) $row['topic_visibility'] === ITEM_UNAPPROVED)
						{
							$item_row['statistics'] .= ' ' . $this->separator_stats . ' ' . $this->user->lang['TOPIC_UNAPPROVED'];
						}
						else if ($row['topic_posts_unapproved'])
						{
							$item_row['statistics'] .= ' ' . $this->separator_stats . ' ' . $this->user->lang['POSTS_UNAPPROVED'];
						}
					}
				}

				$item_row['title'] = (isset($row['forum_name']) && $row['forum_name'] !== '') ? $row['forum_name'] . ' ' . $this->separator . ' ' . $item_row['title'] : $item_row['title'];

			break;

			default:
				if ($topic_id && $this->config['ppkbb_feed_topic'])
				{
					$item_row['link'] = $this->append_sid('viewtopic.' . $this->php_ext, "t={$row['topic_id']}&amp;p={$row['post_id']}#p{$row['post_id']}");

					if ($this->config['feed_item_statistics'])
					{
						$item_row['statistics'] = $this->user->lang['POSTED'] . ' ' . $this->user->lang['POST_BY_AUTHOR'] . ' ' . $this->user_viewprofile($row)
							. ' ' . $this->separator_stats . ' ' . $this->user->format_date($row[$this->feed_options['published']])
							. (($this->is_moderator_approve_forum($row['forum_id']) && (int) $row['post_visibility'] === ITEM_UNAPPROVED) ? ' ' . $this->separator_stats . ' ' . $this->user->lang['POST_UNAPPROVED'] : '')
							. (($this->is_moderator_approve_forum($row['forum_id']) && (int) $row['post_visibility'] === ITEM_DELETED) ? ' ' . $this->separator_stats . ' ' . $this->user->lang['POST_DELETED'] : '');
					}

					$item_row['forum_id'] = $forum_id;

				}
				else if ($forum_id && $this->config['ppkbb_feed_forum'])
				{
					$item_row['link'] = $this->append_sid('viewtopic.' . $this->php_ext, "t={$row['topic_id']}&amp;p={$row['post_id']}#p{$row['post_id']}");

					if ($this->config['feed_item_statistics'])
					{
						$item_row['statistics'] = $this->user->lang['POSTED'] . ' ' . $this->user->lang['POST_BY_AUTHOR'] . ' ' . $this->user_viewprofile($row)
							. ' ' . $this->separator_stats . ' ' . $this->user->format_date($row[$this->feed_options['published']])
							. (($this->is_moderator_approve_forum($row['forum_id']) && (int) $row['post_visibility'] === ITEM_UNAPPROVED) ? ' ' . $this->separator_stats . ' ' . $this->user->lang['POST_UNAPPROVED'] : '')
							. (($this->is_moderator_approve_forum($row['forum_id']) && (int) $row['post_visibility'] === ITEM_DELETED) ? ' ' . $this->separator_stats . ' ' . $this->user->lang['POST_DELETED'] : '');
					}

					$item_row['title'] = (isset($row['forum_name']) && $row['forum_name'] !== '') ? $row['forum_name'] . ' ' . $this->separator . ' ' . $item_row['title'] : $item_row['title'];
					$item_row['forum_id'] = $forum_id;
				}
				else if ($this->config['ppkbb_feed_overall'])
				{
					$item_row['link'] = $this->append_sid('viewtopic.' . $this->php_ext, "t={$row['topic_id']}&amp;p={$row['post_id']}#p{$row['post_id']}");

					if ($this->config['feed_item_statistics'])
					{
						$item_row['statistics'] = $this->user->lang['POSTED'] . ' ' . $this->user->lang['POST_BY_AUTHOR'] . ' ' . $this->user_viewprofile($row)
							. ' ' . $this->separator_stats . ' ' . $this->user->format_date($row[$this->feed_options['published']])
							. (($this->is_moderator_approve_forum($row['forum_id']) && (int) $row['post_visibility'] === ITEM_UNAPPROVED) ? ' ' . $this->separator_stats . ' ' . $this->user->lang['POST_UNAPPROVED'] : '')
							. (($this->is_moderator_approve_forum($row['forum_id']) && (int) $row['post_visibility'] === ITEM_DELETED) ? ' ' . $this->separator_stats . ' ' . $this->user->lang['POST_DELETED'] : '');
					}

					$item_row['title'] = (isset($row['forum_name']) && $row['forum_name'] !== '') ? $row['forum_name'] . ' ' . $this->separator . ' ' . $item_row['title'] : $item_row['title'];

				}
			break;
		}
	}

	public function get_tracker_forums()
	{
		static $tracker_ids;

		$cache_name	= 'feed_tracker_forum_ids';

		if (!isset($tracker_ids) && ($tracker_ids = $this->cache->get('_' . $cache_name)) === false)
		{
			$sql = 'SELECT forum_id
				FROM ' . FORUMS_TABLE . '
				WHERE forum_tracker = 1';
			$result = $this->db->sql_query($sql);

			$tracker_ids = array();
			while ($forum_id = (int) $this->db->sql_fetchfield('forum_id'))
			{
				$tracker_ids[$forum_id] = $forum_id;
			}
			$this->db->sql_freeresult($result);

			$this->cache->put('_' . $cache_name, $tracker_ids);
		}

		return $tracker_ids;
	}

	public function build_feed_url_params($params)
	{
		$str='';
		if($params)
		{
			foreach($params as $k => $v)
			{
				$v ? $str.=($str ? '&amp;': '')."{$k}={$v}" : '';
			}
		}

		return $str;
	}

	//phpbb/feed/base.php
	public function get_readable_forums()
	{
		static $forum_ids;

		if (!isset($forum_ids))
		{
			$forum_ids = array_keys($this->auth->acl_getf('f_read', true));
		}

		return $forum_ids;
	}

	public function get_moderator_approve_forums()
	{
		static $forum_ids;

		if (!isset($forum_ids))
		{
			$forum_ids = array_keys($this->auth->acl_getf('m_approve', true));
		}

		return $forum_ids;
	}

	public function is_moderator_approve_forum($forum_id)
	{
		static $forum_ids;

		if (!isset($forum_ids))
		{
			$forum_ids = array_flip($this->get_moderator_approve_forums());
		}

		return (isset($forum_ids[$forum_id])) ? true : false;
	}

	public function get_excluded_forums()
	{
		static $forum_ids;

		$cache_name	= 'tracker_feed_excluded_forum_ids';

		if (!isset($forum_ids) && ($forum_ids = $this->cache->get('_' . $cache_name)) === false)
		{

			$sql = 'SELECT forum_id
				FROM ' . FORUMS_TABLE . '
				WHERE forum_tracker = 0 ' . ($this->config['ppkbb_feed_enblist'] ? ' OR '.$this->db->sql_in_set('forum_id', $this->config['ppkbb_feed_enblist'], ($this->config['ppkbb_feed_trueenblist'] ? false : true)) : '');
			$result = $this->db->sql_query($sql);

			$forum_ids = array();
			while ($forum_id = (int) $this->db->sql_fetchfield('forum_id'))
			{
				$forum_ids[$forum_id] = $forum_id;
			}
			$this->db->sql_freeresult($result);

			$this->cache->put('_' . $cache_name, $forum_ids);
		}

		return $forum_ids;
	}

	public function is_excluded_forum($forum_id)
	{
		$forum_ids = $this->get_excluded_forums();

		return isset($forum_ids[$forum_id]) ? true : false;
	}

	public function get_passworded_forums()
	{
		return $this->user->get_passworded_forums();
	}

	public function user_viewprofile($row)
	{
		$author_id = (int) $row[$this->feed_options['author_id']];

		if ($author_id == ANONYMOUS)
		{
			// Since we cannot link to a profile, we just return GUEST
			// instead of $row['username']
			return $this->user->lang['GUEST'];
		}

		return '<a href="' . $this->append_sid('memberlist.' . $this->php_ext, 'mode=viewprofile&amp;u=' . $author_id) . '">' . $row[$this->feed_options['creator']] . '</a>';
	}

	//phpbb/feed/helper.php
	public function get_board_url($without_script_path = false)
	{
		//static $board_url;

		if (empty($board_url))
		{
			$board_url = generate_board_url($without_script_path);
		}

		return $board_url;
	}

	/**
	* Run links through append_sid(), prepend generate_board_url() and remove session id
	*/
	public function append_sid($url, $params)
	{
		return append_sid($this->get_board_url() . '/' . $url, $params, true, '');
	}

	/**
	* Generate ISO 8601 date string (RFC 3339)
	*/
	public function format_date($time)
	{
		static $zone_offset;
		static $offset_string;

		if (empty($offset_string))
		{
			$zone_offset = $this->user->create_datetime()->getOffset();
			$offset_string = phpbb_format_timezone_offset($zone_offset);
		}

		return gmdate("Y-m-d\TH:i:s", $time + $zone_offset) . $offset_string;
	}

	/**
	* Generate text content
	*
	* @param string $content is feed text content
	* @param string $uid is bbcode_uid
	* @param string $bitfield is bbcode bitfield
	* @param int $options bbcode flag options
	* @param int $forum_id is the forum id
	* @param array $post_attachments is an array containing the attachments and their respective info
	* @return string the html content to be printed for the feed
	*/
	public function generate_content($content, $uid, $bitfield, $options, $forum_id, $post_attachments)
	{
		if (empty($content))
		{
			return '';
		}

		// Prepare some bbcodes for better parsing
		$content	= preg_replace("#\[quote(=&quot;.*?&quot;)?:$uid\]\s*(.*?)\s*\[/quote:$uid\]#si", "[quote$1:$uid]<br />$2<br />[/quote:$uid]", $content);

		$content = generate_text_for_display($content, $uid, $bitfield, $options);

		// Add newlines
		$content = str_replace('<br />', '<br />' . "\n", $content);

		// Convert smiley Relative paths to Absolute path, Windows style
		$content = str_replace($this->phpbb_root_path . $this->config['smilies_path'], $this->get_board_url() . '/' . $this->config['smilies_path'], $content);

		// Remove "Select all" link and mouse events
		$content = str_replace('<a href="#" onclick="selectCode(this); return false;">' . $this->user->lang['SELECT_ALL_CODE'] . '</a>', '', $content);
		$content = preg_replace('#(onkeypress|onclick)="(.*?)"#si', '', $content);

		// Firefox does not support CSS for feeds, though

		// Remove font sizes
	//	$content = preg_replace('#<span style="font-size: [0-9]+%; line-height: [0-9]+%;">([^>]+)</span>#iU', '\1', $content);

		// Make text strong :P
	//	$content = preg_replace('#<span style="font-weight: bold?">(.*?)</span>#iU', '<strong>\1</strong>', $content);

		// Italic
	//	$content = preg_replace('#<span style="font-style: italic?">([^<]+)</span>#iU', '<em>\1</em>', $content);

		// Underline
	//	$content = preg_replace('#<span style="text-decoration: underline?">([^<]+)</span>#iU', '<u>\1</u>', $content);

		// Remove embed Windows Media Streams
		$content	= preg_replace( '#<\!--\[if \!IE\]>-->([^[]+)<\!--<!\[endif\]-->#si', '', $content);

		// Do not use &lt; and &gt;, because we want to retain code contained in [code][/code]

		// Remove embed and objects
		$content	= preg_replace( '#<(object|embed)(.*?) (value|src)=(.*?) ([^[]+)(object|embed)>#si',' <a href=$4 target="_blank"><strong>$1</strong></a> ',$content);

		// Remove some specials html tag, because somewhere there are a mod to allow html tags ;)
		$content	= preg_replace( '#<(script|iframe)([^[]+)\1>#siU', ' <strong>$1</strong> ', $content);

		// Parse inline images to display with the feed
		if (!empty($post_attachments))
		{
			$update_count = array();
			parse_attachments($forum_id, $content, $post_attachments, $update_count);
			$post_attachments = implode('<br />', $post_attachments);

			// Convert attachments' relative path to absolute path
			$post_attachments = str_replace($this->phpbb_root_path . ($this->config['enable_mod_rewrite'] ? '' : '../') . 'download/file.' . $this->php_ext, $this->get_board_url() . '/download/file.' . $this->php_ext, $post_attachments);
			if($this->config['ppkbb_feed_downloads'] && $this->torrents)
			{
				foreach($this->torrents as $k)
				{
					$post_attachments = str_replace($this->get_board_url() . '/download/file.' . $this->php_ext . '?id=' . $k, $this->torrent_url . 'id=' . $k, $post_attachments);
				}
			}

			$content .= $post_attachments;
		}

		// Remove Comments from inline attachments [ia]
		$content = preg_replace('#<dd>(.*?)</dd>#','',$content);

		// Replace some entities with their unicode counterpart
		$entities = array(
			'&nbsp;'	=> "\xC2\xA0",
			'&bull;'	=> "\xE2\x80\xA2",
			'&middot;'	=> "\xC2\xB7",
			'&copy;'	=> "\xC2\xA9",
		);

		$content = str_replace(array_keys($entities), array_values($entities), $content);

		// Remove CDATA blocks. ;)
		$content = preg_replace('#\<\!\[CDATA\[(.*?)\]\]\>#s', '', $content);

		// Other control characters
		$content = preg_replace('#(?:[\x00-\x1F\x7F]+|(?:\xC2[\x80-\x9F])+)#', '', $content);

		return $content;
	}

	//phpbb/content_visibility.php
	public function get_forums_visibility_sql($mode, $forum_ids = array(), $table_alias = '')
	{
		$where_sql = '(';

		$approve_forums = array_intersect($forum_ids, array_keys($this->auth->acl_getf('m_approve', true)));

		$get_forums_visibility_sql_overwrite = false;

		if ($get_forums_visibility_sql_overwrite !== false)
		{
			return $get_forums_visibility_sql_overwrite;
		}

		if (count($approve_forums))
		{
			// Remove moderator forums from the rest
			$forum_ids = array_diff($forum_ids, $approve_forums);

			if (!count($forum_ids))
			{
				// The user can see all posts/topics in all specified forums
				return $where_sql . $this->db->sql_in_set($table_alias . 'forum_id', $approve_forums) . ')';
			}
			else
			{
				// Moderator can view all posts/topics in some forums
				$where_sql .= $this->db->sql_in_set($table_alias . 'forum_id', $approve_forums) . ' OR ';
			}
		}
		else
		{
			// The user is just a normal user
			return $where_sql . $table_alias . $mode . '_visibility = ' . ITEM_APPROVED . '
				AND ' . $this->db->sql_in_set($table_alias . 'forum_id', $forum_ids, false, true) . ')';
		}

		$where_sql .= '(' . $table_alias . $mode . '_visibility = ' . ITEM_APPROVED . '
			AND ' . $this->db->sql_in_set($table_alias . 'forum_id', $forum_ids) . '))';

		return $where_sql;
	}

	public function get_visibility_sql($mode, $forum_id, $table_alias = '')
	{
		$where_sql = '';

		$get_visibility_sql_overwrite = false;

		if ($get_visibility_sql_overwrite !== false)
		{
			return $get_visibility_sql_overwrite;
		}

		if ($this->auth->acl_get('m_approve', $forum_id))
		{
			return $where_sql . '1 = 1';
		}

		return $where_sql . $table_alias . $mode . '_visibility = ' . ITEM_APPROVED;
	}

	public function get_count($mode, $data, $forum_id)
	{
		if (!$this->auth->acl_get('m_approve', $forum_id))
		{
			return (int) $data[$mode . '_approved'];
		}

		return (int) $data[$mode . '_approved'] + (int) $data[$mode . '_unapproved'] + (int) $data[$mode . '_softdeleted'];
	}

	//phpbb/feed/attachments_base.php
	public function get_attachments($forum_id=0, $topic_id=0, $post_id=0)
	{
		if(isset($this->attachments[$post_id]))
		{
			return true;
		}

		$sql_array = array(
			'SELECT'   => 'a.*',
			'FROM'     => array(
				ATTACHMENTS_TABLE => 'a'
			),
			'WHERE'    => 'a.in_message = 0 ',
			'ORDER_BY' => 'a.filetime DESC, a.post_msg_id ASC',
		);

		if ($topic_id)
		{
			$sql_array['WHERE'] .= 'AND a.topic_id = ' . (int) $topic_id;
		}
		else if ($forum_id)
		{
			$sql_array['LEFT_JOIN'] = array(
				array(
					'FROM' => array(TOPICS_TABLE => 't'),
					'ON'   => 'a.topic_id = t.topic_id',
				)
			);
			$sql_array['WHERE'] .= 'AND t.forum_id = ' . (int) $forum_id;
		}
		$this->config['ppkbb_feed_downloads'] ? '' : $sql_array['WHERE'] .= " AND a.extension != 'torrent'";

		$sql = $this->db->sql_build_query('SELECT', $sql_array);
		$result = $this->db->sql_query($sql);

		// Set attachments in feed items
		while ($row = $this->db->sql_fetchrow($result))
		{
			$this->attachments[$row['post_msg_id']][] = $row;
			if($this->config['ppkbb_feed_downloads'] && $row['extension']=='torrent')
			{
				$this->torrents[$row['attach_id']] = $row['attach_id'];
			}
		}
		$this->db->sql_freeresult($result);
	}

	//phpbb/feed/topics_active.php
	public function get_forum_ids()
	{
		static $forum_ids;

		$cache_name	= 'feed_torrent_active_forum_ids';

		if (!isset($forum_ids) && ($forum_ids = $this->cache->get('_' . $cache_name)) === false)
		{
			$ex_fid = $this->get_excluded_forums();

			$sql = 'SELECT forum_id
				FROM ' . FORUMS_TABLE . '
				WHERE forum_type = ' . FORUM_POST . '
					' . ($ex_fid ? 'AND '.$this->db->sql_in_set('forum_id', $ex_fid, true) : '') . '
					AND ' . $this->db->sql_bit_and('forum_flags', log(FORUM_FLAG_ACTIVE_TOPICS, 2), '<> 0');
			$result = $this->db->sql_query($sql);

			$forum_ids = array();
			while ($forum_id = (int) $this->db->sql_fetchfield('forum_id'))
			{
				$forum_ids[$forum_id] = $forum_id;
			}
			$this->db->sql_freeresult($result);

			$this->cache->put('_' . $cache_name, $forum_ids, 180);
		}

		return $forum_ids;
	}
}
