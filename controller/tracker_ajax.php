<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\controller;

class tracker_ajax
{
	/** @var \phpbb\auth\auth */
	protected $auth;

	/** @var \phpbb\db\driver\driver_interface */
	protected $db;

	/** @var \phpbb\template\template */
	protected $template;

	/** @var \phpbb\request\request_interface */
	protected $request;

	/** @var \phpbb\user */
	protected $user;

	/** @var \ppk\xbtbb3cker\core\xbtbb3cker */
	protected $xbt_functions;

	/** @var \phpbb\config\config */
	protected $config;

	/** @var string phpbb_root_path */
	protected $phpbb_root_path;

	/** @var string phpEx */
	protected $php_ext;

	/** @var string table_prefix */
	protected $table_prefix;

	/** @var \phpbb\cache\service */
	protected $cache;

	/** @var \phpbb\controller\helper */
	protected $helper;

	public function __construct(\phpbb\auth\auth $auth, \phpbb\db\driver\driver_interface $db, \phpbb\template\template $template, \phpbb\request\request_interface $request, \phpbb\user $user, \ppk\xbtbb3cker\core\xbtbb3cker $functions, \phpbb\config\config $config, $phpbb_root_path, $php_ext, $table_prefix, \phpbb\cache\service $cache, \phpbb\controller\helper $helper)
	{
		$this->auth = $auth;
		$this->db = $db;
		$this->template = $template;
		$this->request = $request;
		$this->user = $user;
		$this->xbt_functions = $functions;
		$this->config = $config;
		$this->phpbb_root_path = $phpbb_root_path;
		$this->php_ext = $php_ext;
		$this->cache = $cache;
		$this->helper = $helper;

	}

	public function main()
	{

		$this->user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_ajax');

		$action=$this->request->variable('action', '');

		$valid_action=array('set_status', 'set_requpratio', 'bookmarks', 'vt_stat');
		if(!in_array($action, $valid_action))
		{
			trigger_error('AJAX_INVALID_ACTION');
		}

		$this->template->assign_vars(array(
			'S_'.strtoupper($action) => true,
		));

		$dt=time();
		$message=$title='';
		$s_hidden_fields=array();

		$action_options=array(
			'set_status' => array('get_post' => true),
			'set_requpratio' => array('get_post' => true),
			'bookmarks' => array('get_post' => true),
			'vt_stat' => array('get_post' => true),
		);

		if(isset($action_options[$action]['get_post']))
		{
// 			$forum_id=$this->request->variable('f', 0);
// 			$topic_id=$this->request->variable('t', 0);
			$post_id=$this->request->variable('p', 0);

			if(!$post_id)
			{
				trigger_error('AJAX_NO_POST');
			}

			$sql='SELECT p.post_edit_locked, p.topic_id, p.post_id, p.poster_id, p.post_subject, t.torrent_status, t.status_reason, t.topic_status, t.topic_title, t.torrent_requpload, t.torrent_reqratio, f.forum_id, f.forum_tracker FROM '.POSTS_TABLE.' p, '. TOPICS_TABLE .' t, '. FORUMS_TABLE." f WHERE p.post_id={$post_id} AND p.topic_id=t.topic_id AND p.forum_id=f.forum_id LIMIT 1";

			$result=$this->db->sql_query($sql);
			$data=$this->db->sql_fetchrow($result);
			if(!$data['post_id'])
			{
				trigger_error('AJAX_POST_NOTFOUND');
			}
			if(!$data['forum_tracker'])
			{
				trigger_error('AJAX_NOT_TRACKER');
			}
			$forum_id=$data['forum_id'];
			$topic_id=$data['topic_id'];
			$status_id=$data['torrent_status'];
			$poster_id=$data['poster_id'];
			$status_reason=$data['status_reason'];
			$topic_closed=$data['topic_status']==1 ? true : false;
			$post_edit_locked=$data['post_edit_locked']==1 ? true : false;
			$this->db->sql_freeresult($result);

			$redirect=append_sid("{$this->phpbb_root_path}viewtopic.{$this->php_ext}", "f={$forum_id}&t={$topic_id}&p={$post_id}&_=".time()."#p{$post_id}", false);
			$this->config['enable_mod_rewrite'] ? '' : $redirect=str_replace('../', './', $redirect);
		}

		$submit=$this->request->variable($action, 0);

		$tracker_ajax_url=$this->helper->route('ppk_xbtbb3cker_controller_tracker_ajax');
		$tracker_ajax_amp=strpos($tracker_ajax_url, '?')!==false ? '&amp;' : '?';

		switch($action)
		{
			case 'set_status':

				$is_cansetstatus = $this->user->data['is_registered'] && $this->auth->acl_get('u_cansetstatus') && $this->auth->acl_get('f_cansetstatus', $forum_id) ? 1 : 0;
				if(!$is_cansetstatus)
				{
					trigger_error('AJAX_NO_RIGHTS');
				}

				$torrent_statuses=$this->xbt_functions->get_torrent_statuses();
				$title=$this->user->lang['SET_STATUS_TITLE'];

				//if($is_cansetstatus)
				//{
					$set_status=$this->request->variable('set_status', '');
					$status_value=$this->request->variable('status_value', 0);
					!isset($torrent_statuses['STATUS_MARK'][$status_value]) || $status_value > 99 || $status_value < -99 ? $status_value=0 : '';
					!isset($torrent_statuses['STATUS_MARK'][$status_id]) || $status_id > 99 || $status_id < -99 ? $status_id=0 : '';

					if($submit)
					{
						$post_edit_locked=$this->request->variable('post_edit_locked', 0) ? 1 : 0;
						$topic_closed=$this->request->variable('topic_closed', 0) ? ITEM_LOCKED : ITEM_UNLOCKED;
						$status_reason = $this->request->variable('status_reason', '', true);
						$result=$this->db->sql_query('UPDATE '. POSTS_TABLE ." SET post_edit_locked='{$post_edit_locked}' WHERE post_id='{$post_id}'");
						$result=$this->db->sql_query('
							UPDATE
								'. TOPICS_TABLE ."
							SET
								topic_status='{$topic_closed}',
								torrent_status='{$status_value}',
								status_reason='".$this->db->sql_escape(truncate_string($status_reason, 255, 255, false))."',
								status_dt='{$dt}',
								status_author='{$this->user->data['user_id']}'
						WHERE
							topic_id='{$topic_id}'
						");

// 						if(($status_value < 1 && $status_id > 0) || ($status_value  > 0 && $status_id < 1))
// 						{
							if(!$this->config['ppkbb_tstatus_salt'])
							{
								$this->config['ppkbb_tstatus_salt']=substr(sha1(strtolower(gen_rand_string(8))), 0, 8);
								$this->config->set('ppkbb_tstatus_salt', $this->config['ppkbb_tstatus_salt'], true);
							}
							$bin_salt=pack('H*', $this->config['ppkbb_tstatus_salt']);
							$salt_add=$status_value < 1/* && $status_id > 0*/ ? false : true;

							$sql='SELECT fid, info_hash FROM '.XBT_FILES." WHERE topic_id='{$topic_id}'";
							$result=$this->db->sql_query($sql);
							while($row=$this->db->sql_fetchrow($result))
							{
								$row['info_hash']=substr($row['info_hash'], 0, 20);
								if(!$salt_add)
								{
									$sql3='DELETE FROM '.XBT_FILES." WHERE info_hash='".$this->db->sql_escape($row['info_hash'])."' AND fid='0'";
									$this->db->sql_query($sql3);
								}
								$sql2='UPDATE '.XBT_FILES." SET info_hash='".$this->db->sql_escape(($salt_add ? $row['info_hash'].$bin_salt : $row['info_hash']))."' WHERE fid='{$row['fid']}'";
								$result2=$this->db->sql_query($sql2);

							}
							$this->db->sql_freeresult($result);
// 						}

						if($this->user->data['user_id']!=$poster_id && (($this->config['ppkbb_tstatus_notify'][0] && $status_value < 1 && $status_id > 0) || ($this->config['ppkbb_tstatus_notify'][1] && $status_value  > 0 && $status_id < 1)))
						{

							$status_inform=array(
								'from_user_id' => $this->user->data['user_id'],
								'icon_id' => 0,
								'from_user_ip' => $this->user->ip,
								'from_username' => $this->user->data['username'],
								'enable_bbcode' => 0,
								'enable_smilies' => 0,
								'enable_urls' => 0,
								'enable_sig' => 0,
								'message' => sprintf($this->user->lang['STATUS_NOTIFY_TEXT'], $this->phpbb_root_path."viewtopic.{$this->php_ext}?f={$forum_id}&amp;t={$topic_id}&amp;p={$post_id}#p{$post_id}", $data['topic_title'], $torrent_statuses['STATUS_REASON'][$status_value], $torrent_statuses['STATUS_REASON'][$status_id], get_username_string('full', $this->user->data['user_id'], $this->user->data['username'], $this->user->data['user_colour']), ($status_reason ? htmlspecialchars($status_reason) : $this->user->lang['STATUS_NO_REASON']), $this->user->format_date($dt, 'Y-m-d H:i:s')),
								'bbcode_bitfield' => 0,
								'bbcode_uid' => 0,
							);
							$status_inform['address_list']['u'][$poster_id]='to';

							include_once($this->phpbb_root_path.'includes/functions_privmsgs.'.$this->php_ext);
							submit_pm('post', $this->user->lang['STATUS_NOTIFY_SUBJECT'], $status_inform, false);

						}

						$message=sprintf($this->user->lang['SET_STATUS_SUCCESS'], $torrent_statuses['STATUS_REASON'][$status_id], $torrent_statuses['STATUS_REASON'][$status_value]);
					}
					else
					{
						$select_form='';
						ksort($torrent_statuses['STATUS_REASON']);
						$forb_sel=array();
						foreach($torrent_statuses['STATUS_REASON'] as $rk => $rv)
						{
							if($rk < 0 && $rk > -50 && !isset($forb_sel[-1]))
							{
								$select_form.='<option disabled="disabled">'.$this->user->lang['STATUS_MREASON'].'</option>';
								$forb_sel[-1]=1;
							}
							if($rk > 0 && !isset($forb_sel[1]))
							{
								$select_form.='<option disabled="disabled">'.$this->user->lang['STATUS_PREASON'].'</option>';
								$forb_sel[1]=1;
							}
							if($rk == 0 && !isset($forb_sel[0]))
							{
								$select_form.='<option disabled="disabled">'.$this->user->lang['STATUS_UREASON'].'</option>';
								$forb_sel[0]=1;
							}
							$select_form.='<option value="'.$rk.'"'.($status_id==$rk ? ' selected="selected"' : '').'>'.$rv.'</option>';
						}
						$this->template->assign_vars(array(
							'STATUSES_SELECT' => $select_form,
							'STATUS_REASON' => $status_reason,
							'STATUS_EDIT_LOCKED' => $post_edit_locked ? ' checked="checked"' : '',
							'STATUS_CLOSED' => $topic_closed ? ' checked="checked"' : '',
						));

					}
				//}
			break;

			case 'set_requpratio':
				$is_cansetrequpratio = $this->user->data['is_registered'] && $this->config['ppkbb_trestricts_options'][0] && $this->auth->acl_get('u_cansetrequpratio') && $this->auth->acl_get('f_cansetrequpratio', $forum_id) ? 1 : 0;
				if(!$is_cansetrequpratio)
				{
					trigger_error('AJAX_NO_RIGHTS');
				}

				$title=$this->user->lang['SET_REQUPRATIO_TITLE'];

				//if($is_cansetrequpratio)
				//{

					if($submit)
					{

						$torrent_reqratio = floatval($this->request->variable('torrent_reqratio', 0.000));
						$torrent_reqratio > 999.999 || $torrent_reqratio < 0.000 ? $torrent_reqratio=0.000 : '';

						$torrent_requpload	 = $this->xbt_functions->get_size_value($this->request->variable('torrent_requploadv', 'b'), substr($this->request->variable('torrent_requpload', 0), 0, 20));
						$torrent_requpload < 0 ? $torrent_requpload=0 : '';

						$result=$this->db->sql_query('
							UPDATE
								'. TOPICS_TABLE ."
							SET
								torrent_reqratio='{$torrent_reqratio}',
								torrent_requpload='{$torrent_requpload}'
						WHERE
							topic_id='{$topic_id}'
						");

						$message=$this->user->lang['SET_REQUPRATIO_SUCCESS'];

					}
					else
					{
						$this->template->assign_vars(array(
							'FORM_SIZE_VALUE' => $this->xbt_functions->select_size_value(),
							'TORRENT_REQUPLOAD_VALUE' => get_formatted_filesize($data['torrent_requpload']),
							'TORRENT_REQUPLOAD_VAL' => $data['torrent_requpload'],
							'TORRENT_REQRATIO_VAL' => $data['torrent_reqratio'],

						));
					}

				//}

			break;

			case 'bookmarks':
				if(!$this->config['ppkbb_tracker_bookmarks'][0] || !$this->user->data['is_registered'])
				{
					trigger_error('FUNCTION_DISABLED');
				}

				$title=$this->user->lang['BOOKMARKS_TITLE'];

				$bookmarks=array();
				$sql='SELECT x.fid, a.real_filename, b.id FROM '.ATTACHMENTS_TABLE.' a, '.XBT_FILES.' x LEFT JOIN '.TRACKER_BOOKMARKS_TABLE." b ON(x.fid=b.attach_id AND b.user_id='{$this->user->data['user_id']}') WHERE x.post_msg_id='{$post_id}' AND x.fid=a.attach_id ORDER BY a.filetime DESC";
				$result=$this->db->sql_query($sql);

				while($row=$this->db->sql_fetchrow($result))
				{
					$bookmarks[]=$row;
				}
				$this->db->sql_freeresult($result);

				if($submit)
				{

					$add_bookmark=$this->request->variable('add_bookmark', array(''=>0));
					$new_bookmarks=$bookmarks_sql=array();
					foreach($add_bookmark as $attach_id)
					{
						$new_bookmarks[]=$attach_id;
						$bookmarks_sql[]="('{$this->user->data['user_id']}', '{$attach_id}', '{$post_id}', '{$dt}')";
					}
					$sql='DELETE FROM '.TRACKER_BOOKMARKS_TABLE." WHERE post_msg_id='{$post_id}' AND user_id='{$this->user->data['user_id']}'".($new_bookmarks ? ' AND '.$this->db->sql_in_set('id', $new_bookmarks, true) : '');
					$result=$this->db->sql_query($sql);

					if($new_bookmarks)
					{
						$sql='INSERT IGNORE INTO '.TRACKER_BOOKMARKS_TABLE.'(user_id, attach_id, post_msg_id, add_date) VALUES '.implode(', ', $bookmarks_sql);
						$result=$this->db->sql_query($sql);
					}

					$message.=$this->user->lang['BOOKMARKS_SUCCESS'];

				}
				else
				{
					foreach($bookmarks as $row)
					{
						$this->template->assign_block_vars('bookmarks', array(
							'FILENAME' => urldecode($row['real_filename']),
							'CHECKED' => $row['id'] ? true : false,
							'ID' => $row['fid'],
						));
					}
				}
			break;

			case 'vt_stat':

				if(!$this->config['ppkbb_torr_blocks'][0] || !$this->config['ppkbb_torr_blocks'][1] || $this->config['ppkbb_torr_blocks'][1]!=3)
				{
					trigger_error('FUNCTION_DISABLED');
				}

				$torrent_opt=$this->request->variable('opt', '');
				$torrent=$this->request->variable('torrent', 0);
				$page_mode=$this->request->variable('page', '');

				$is_candowntorr = $this->auth->acl_get('u_candowntorr') && $this->auth->acl_get('f_candowntorr', $data['forum_id']) ? 1 : 0;
				$is_canviewvtstats=$this->auth->acl_get('u_canviewvtstats') && $this->auth->acl_get('f_canviewvtstats', $data['forum_id']) ? 1 : 0;
				$is_cansetstatus = $this->auth->acl_get('u_cansetstatus') && $this->auth->acl_get('f_cansetstatus', $data['forum_id']) ? 1 : 0;

				$is_admod=$this->auth->acl_gets('a_', 'm_') || $this->auth->acl_get('m_', $data['forum_id']) ? true : false;

				$candown_torrent_status=$is_cansetstatus || $data['torrent_status'] < 1 || ($this->user->data['user_id']==$poster_id && $data['torrent_status'] > 0 &&  in_array($data['torrent_status'], $this->config['ppkbb_tcauthor_candown'])) ? true : false;

				if((!$is_candowntorr && !$candown_torrent_status) || !$is_canviewvtstats)
				{
					trigger_error('AJAX_NO_RIGHTS');
				}

				$valid_page_mode=array('adm', 'vt', 'mua');
				if(!in_array($page_mode, $valid_page_mode))
				{
					trigger_error('FUNCTION_DISABLED');
				}

				$torrent_data=array();
				$sql='SELECT
					tr.fid torrent_id,
					tr.completed'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_times_completed' : '').' times_completed,
					tr.leechers'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_leechers' : '').' leechers,
					tr.seeders'.($this->config['ppkbb_tcenable_rannounces'][0] ? '+tr.rem_seeders' : '').' seeders,
					tr.completed real_times_completed,
					tr.leechers real_leechers,
					tr.seeders real_seeders,
					'.($this->config['ppkbb_tcenable_rannounces'][0] ? '
					tr.rem_times_completed rem_times_completed,
					tr.rem_leechers rem_leechers,
					tr.rem_seeders rem_seeders,
					' : '').'
					tr.size,
					tr.ctime added,
					tr.private,
					tr.info_hash,
					tr.lastcleanup,
					tr.rem_leechers,
					tr.rem_seeders,
					tr.poster_id,
					tr.rem_times_completed,
					tr.lastremote,
					tr.numfiles,
					tr.topic_id,
					a.*
				FROM '.ATTACHMENTS_TABLE.' a, '.XBT_FILES." tr WHERE a.extension='torrent' AND a.in_message='0' AND a.attach_id='{$torrent}' AND a.attach_id=tr.fid";
				$result=$this->db->sql_query($sql);
				while($row=$this->db->sql_fetchrow($result))
				{
					$torrent_data=$row;
				}
				$this->db->sql_freeresult($result);

				if(!$torrent_data)
				{
					trigger_error('TORRENT_NOT_REGISTERED');
				}

				$this->user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_viewtopic');

				$torrents_info=array('filelist', 'finished', 'seed', 'leech', '', 'history', 'leave', '', 'remote', 'downloads', 'bookmarks');
				in_array($torrent_opt, $torrents_info) ? '' : $torrent_opt='';

				$torrent_info=array();

				foreach($torrents_info as $k=>$v)
				{
					if($v)
					{

						$torrent_opt==$v && !$this->config['ppkbb_torrent_statml'][$k] ? $torrent_opt='' : '';

					}
				}

				if(!$torrent_opt)
				{
					trigger_error('FUNCTION_DISABLED');
				}

				$sort_opt=array(
					'downloads' => array('username', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'dl_time'),
					'filelist' => array('filename', 'size'),
					'finished' => array('username', 'uploaded', 'downloaded', 'active', 'ratio', 'mtime', 'announced', '`left`'),
					'history' => array('username', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
					'leave' => array('username', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
					'leech' => array('username', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
					'remote' => array('rtrack_url', 'next_announce', 'a_message', 'a_interval', 'err_count', 'seeders', 'leechers', 'times_completed', 'peers'),
					'seed' => array('username', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
					'bookmarks' => array('username', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'add_date'),

				);

				if(!$is_admod)
				{
					$sort_opt['downloads']=array('username', 'uploaded', 'downloaded', '`left`', 'active',  'ratio', 'dl_time');
					$sort_opt['finished']=array('username', 'uploaded', 'downloaded', 'active', 'ratio',  '`left`');
					$sort_opt['history']=array('username', 'uploaded', 'downloaded', '`left`', 'active',  'ratio');
					$sort_opt['leave']=array('username', 'uploaded', 'downloaded', '`left`', 'active',  'ratio');
					$sort_opt['leech']=array('username', 'uploaded', 'downloaded', 'ratio', '`left`');
					$sort_opt['remote']=array('rtrack_url', 'next_announce', 'seeders', 'leechers', 'times_completed', 'peers');
					$sort_opt['seed']=array('username', 'uploaded', 'downloaded', 'ratio', '`left`');
					$sort_opt['bookmarks']=array('username', 'uploaded', 'downloaded', '`left`', 'active',  'ratio', 'add_date');

				}

				if(!class_exists('timedelta'))
				{
					$this->user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
					include_once($this->phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$this->php_ext);

				}
				$td = new \timedelta('D_MINUTES');

				$assign_vars=$postrow_headers=$postrow_footers=array();

				$i=$mua_count=0;
				$vt_page=false;
				$vt_page_url='';

				$viewtopic_add1inc=$this->phpbb_root_path.'ext/ppk/xbtbb3cker/include/';

				$torrent_times_completed=$torrent_data['times_completed']-$torrent_data['rem_times_completed'];

				$torrent_seeders=$torrent_data['seeders']-$torrent_data['rem_seeders'];
				if(!$torrent_seeders)
				{
					$seed_percent=0;
				}
				else if($torrent_times_completed && $torrent_seeders < $torrent_times_completed)
				{
					$seed_percent=$this->xbt_functions->my_int_val($torrent_seeders * 100 / $torrent_times_completed);
				}
				else
				{
					$seed_percent=100;
				}

				$page=$this->request->variable('pg', 0);
				$mua_limit=($page ? $page-1 : 0)*$this->config['ppkbb_mua_countlist'].', '.$this->config['ppkbb_mua_countlist'];

				switch($torrent_opt)
				{
					case 'leave':
						include($viewtopic_add1inc.'viewtopic_add1_leave.'.$this->php_ext);
						break;

					case 'history':
						include($viewtopic_add1inc.'viewtopic_add1_history.'.$this->php_ext);
						break;

					case 'filelist':
						include($viewtopic_add1inc.'viewtopic_add1_filelist.'.$this->php_ext);
						break;

					case 'finished':
						include($viewtopic_add1inc.'viewtopic_add1_finished.'.$this->php_ext);
						break;

					case 'seed':
						include($viewtopic_add1inc.'viewtopic_add1_seed.'.$this->php_ext);
						break;

					case 'leech':
						include($viewtopic_add1inc.'viewtopic_add1_leech.'.$this->php_ext);
						break;

					case 'remote':
						include($viewtopic_add1inc.'viewtopic_add1_remote.'.$this->php_ext);
						break;

					case 'downloads':
						include($viewtopic_add1inc.'viewtopic_add1_downloads.'.$this->php_ext);
						break;

					case 'bookmarks':
						include($viewtopic_add1inc.'viewtopic_add1_bookmarks.'.$this->php_ext);
						break;
				}

				$assigned_vars=count($assign_vars);

				if($vt_count > $i)
				{
					$vt_page=$this->xbt_functions->build_muavt_page($vt_count, $page, "{$tracker_ajax_url}{$tracker_ajax_amp}action=vt_stat&amp;opt={$torrent_opt}&amp;page=vt&amp;p={$post_id}&amp;torrent={$torrent}", 'torrent_stat'.$torrent, $this->config['ppkbb_mua_countlist'], 'links');
					$vt_page_url=str_replace('&amp;', '&', "{$tracker_ajax_url}{$tracker_ajax_amp}action=vt_stat&amp;opt={$torrent_opt}&amp;page=vt&amp;p={$post_id}&amp;torrent={$torrent}");
				}
				$mua_countlist=$this->xbt_functions->get_muavt_countlist(min($i, $this->config['ppkbb_mua_countlist']));

				foreach($sort_opt[$torrent_opt] as $k => $v)
				{
					$v=strtoupper(str_replace('`', '', $v));
					$postrow_headers[]='<th style="text-align:left;">'.(isset($this->user->lang['TORRENT_INFO_HEADER_'.$v]) ? $this->user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</th>';
					$postrow_footers[]='<a href="javascript:;" onclick="fnShowHide('.$k.');">'.(isset($this->user->lang['TORRENT_INFO_HEADER_'.$v]) ? $this->user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a>';
				}

				if($assign_vars)
				{
					foreach($assign_vars as $k => $v)
					{
						$this->template->assign_block_vars($torrent_opt.'_option', $v);
					}
				}

				if($postrow_headers)
				{
					foreach($postrow_headers as $k => $v)
					{
						$this->template->assign_block_vars('torrent_headers', array('VALUE' => $v));
					}
				}

				$this->template->assign_vars(array(
					'S_MUA_COUNTLIST_DEFAULT' => count($mua_countlist[0]) > 1 ? 5 : -1,
					'S_MUA_COUNTLIST_KEYS' => implode(', ', $mua_countlist[0]),
					'S_MUA_COUNTLIST_VALUES' => implode(', ', $mua_countlist[1]),

					'S_HAS_TORRENT_EXPLAIN' => $torrent ? true : false,

					'S_HAS_TORRENT_EXPLAIN_'.strtoupper($torrent_opt)	=> $torrent ? true : false,
					)
				);

				$title=$user->lang['TORRENT_INFO_HEADER_'.strtoupper($torrent_opt)];

				$this->template->assign_vars(array(
					'S_TORRENT_HEADER' => $postrow_headers ? implode('', $postrow_headers) :false,
					'S_TORRENT_FOOTER' => $postrow_footers ? implode(' : ', $postrow_footers) :false,
					'S_ANON_ADDON' => in_array($torrent_opt, array('leave', 'history', 'finished', 'seed', 'leech')) && $this->config['ppkbb_xcanonymous_announce'] ? true : false,

					'S_VT_PAGE' => $vt_page,
					'S_VT_STAT_URL' => $vt_page_url,

					'MESSAGE_TEXT'		=> $title,

					)
				);

				$s_hidden_fields=array('torrent'=>$torrent, 'opt'=>$torrent_opt);

				if($page)
				{
					page_header();

					$this->template->set_filenames(array(
						'body' => '@ppk_xbtbb3cker/ajax_body.html')
					);

					page_footer();
				}
			break;
		}

 		if (confirm_box(true))
 		{

			meta_refresh(3, $redirect);

			if ($this->request->is_ajax())
			{

				$json_response = new \phpbb\json_response;
				$json_response->send(array(
					'MESSAGE_TITLE'		=> $title,
					'MESSAGE_TEXT'		=> $message,
					'REFRESH_DATA'		=> array(
						'time' => 3,
						'url' => $redirect,
					),
					'visible'			=> true,
				));


			}
			$message .= '<br /><br />' . $this->user->lang('RETURN_PAGE', '<a href="' . $redirect . '">', '</a>');
			trigger_error($message);
		}
		else
		{


			$s_hidden_fields=array_merge($s_hidden_fields, array($action => 1, 'action' => $action, 'p' => $post_id));
			$u_action=$this->helper->route('ppk_xbtbb3cker_controller_tracker_ajax');

			/*$this->config['enable_mod_rewrite'] ? '' : */$u_action='./../..'.$u_action;

			confirm_box(false, $title, build_hidden_fields($s_hidden_fields), 'ajax_body.html', $u_action);
		}
		redirect($redirect);

	}


}
