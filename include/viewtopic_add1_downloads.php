<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

global $db, $config, $template, $user;

$sql="SELECT COUNT(*) vt_count FROM ".TRACKER_DOWNLOADS_TABLE." d  WHERE d.attach_id='{$torrent}'";//LEFT JOIN ".USERS_TABLE." u ON (d.downloader_id=u.user_id) LEFT JOIN ".XBT_FILES_USERS." s ON (d.downloader_id=s.uid AND d.attach_id=s.fid) GROUP BY downloader_id
$result=$db->sql_query($sql);
$vt_count=$db->sql_fetchfield('vt_count');
$db->sql_freeresult($result);

$sql="SELECT d.dl_time, d.downloader_id, d.guests is_guest, u.username, u.user_id, u.user_colour, s.* FROM ".TRACKER_DOWNLOADS_TABLE." d LEFT JOIN ".USERS_TABLE." u ON (d.downloader_id=u.user_id) LEFT JOIN ".XBT_FILES_USERS." s ON (d.downloader_id=s.uid AND d.attach_id=s.fid) WHERE d.attach_id='{$torrent}'".($mua_limit ? " LIMIT {$mua_limit}" : '');// GROUP BY downloader_id";// ORDER BY $sql_addon";

$result=$db->sql_query($sql);
while($userlist=$db->sql_fetchrow($result))
{
// 	if($torrent_id!=$userlist['torrent'])
// 	{
// 		continue;
// 	}
	if($userlist['is_guest'])
	{
		$userlist['username']=$user->lang['GUEST'];
	}

	if(empty($userlist['username']) && $userlist['user_id']!=1)
	{
		$torrent_user=$user->lang['USER_DELETED'];
	}
	else
	{

		$torrent_user=($userlist['user_id']!=1 ? str_replace('../', './', get_username_string('full', $userlist['user_id'], $userlist['username'], $userlist['user_colour'], $userlist['username'])) : $userlist['username']);

	}
	$i+=1;
	if($userlist['is_guest'])
	{
		$assign_vars[$i]['TORRENT_USER'] = $torrent_user;
		$assign_vars[$i]['TORRENT_CUSER'] = $userlist['username'];

		$assign_vars[$i]['TORRENT_UP'] = '';
		$assign_vars[$i]['TORRENT_BUP'] = 0;

		$assign_vars[$i]['TORRENT_DOWN'] = '';
		$assign_vars[$i]['TORRENT_BDOWN'] = 0;

		$assign_vars[$i]['TORRENT_COMPLETED'] = '';

		$assign_vars[$i]['TORRENT_ACTIVE'] = '';

		$assign_vars[$i]['TORRENT_LAST'] = '';
		$assign_vars[$i]['TORRENT_SLAST'] = 0;

		$assign_vars[$i]['TORRENT_ANNCOUNT'] = '';

		$assign_vars[$i]['TORRENT_RATIO'] = '';

		$assign_vars[$i]['TORRENT_DOWNLOADED'] = $userlist['dl_time'] ? $user->format_date($userlist['dl_time'], 'Y-m-d H:i:s') : '';
		$assign_vars[$i]['TORRENT_SDOWNLOADED'] = $userlist['dl_time'];
	}
	else
	{
		$assign_vars[$i]['TORRENT_USER'] = $torrent_user;
		$assign_vars[$i]['TORRENT_CUSER'] = $userlist['username'];

		$assign_vars[$i]['TORRENT_UP'] = get_formatted_filesize($userlist['uploaded']);
		$assign_vars[$i]['TORRENT_BUP'] = $userlist['uploaded'];

		$assign_vars[$i]['TORRENT_DOWN'] = get_formatted_filesize($userlist['downloaded']);
		$assign_vars[$i]['TORRENT_BDOWN'] = $userlist['downloaded'];

		$assign_vars[$i]['TORRENT_COMPLETED'] = $userlist['mtime'] && $userlist['left']!==null ? $this->xbt_functions->my_float_val(100 - (100 * $userlist['left'] / $torrent_data['size'])) : '';

		$assign_vars[$i]['TORRENT_ACTIVE'] = $userlist['active'] ? $user->lang['YES'] : $user->lang['NO'];

		$assign_vars[$i]['TORRENT_LAST'] = $userlist['mtime'] ? $user->format_date($userlist['mtime'], 'Y-m-d H:i:s') : '';
		$assign_vars[$i]['TORRENT_SLAST'] = $userlist['mtime'];

		$assign_vars[$i]['TORRENT_ANNCOUNT'] = $userlist['announced'];

		$assign_vars[$i]['TORRENT_RATIO'] = $this->xbt_functions->get_ratio_alias($this->xbt_functions->get_ratio($userlist['uploaded'], $userlist['downloaded']));

		$assign_vars[$i]['TORRENT_DOWNLOADED'] = $userlist['dl_time'] ? $user->format_date($userlist['dl_time'], 'Y-m-d H:i:s') : '';
		$assign_vars[$i]['TORRENT_SDOWNLOADED'] = $userlist['dl_time'];
	}
}
$db->sql_freeresult($result);




?>
