<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

global $db, $template, $config, $phpbb_root_path, $phpEx;

$dt=time();

$tracker_cron=array();

if(count($torrents_cleanup))
{
	$sql="INSERT INTO ".TRACKER_CRON_TABLE."(type, data, forum_id, added) VALUES('t_clean', '".$db->sql_escape(serialize($torrents_cleanup))."', '{$forum_id}', '{$dt}')";
	$db->sql_query($sql);
	$cron_id=$db->sql_nextid();
	if($cron_id)
	{
		$tracker_cron[]=$cron_id;
	}
}

if(count($torrents_remote))
{
	$sql="INSERT INTO ".TRACKER_CRON_TABLE."(type, data, forum_id, added) VALUES('t_announce', '".$db->sql_escape(serialize(array('torrents_id' => array_map('bin2hex', $torrents_remote))))."', '{$forum_id}', '{$dt}')";
	$db->sql_query($sql);
	$cron_id=$db->sql_nextid();
	if($cron_id)
	{
		$tracker_cron[]=$cron_id;
	}

}

if(count($tracker_cron))
{
	if(!$config['ppkbb_cron_options'][2])
	{
		$cron_id=implode('&amp;id[]=', $tracker_cron);
		$template->assign_block_vars('tracker_cron', array(
			'CRON_TASK' => '<img src="' . append_sid($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/cron.' . $phpEx, 'id[]='.$cron_id) . '" alt="tracker_cron" width="1" height="1" />'
			)
		);
	}
	else
	{
		foreach($tracker_cron as $cron_id)
		{
			$template->assign_block_vars('tracker_cron', array(
				'CRON_TASK' => '<img src="' . append_sid($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/cron.' . $phpEx, 'id[]='.$cron_id) . '" alt="tracker_cron" width="1" height="1" />'
				)
			);
		}
	}
}
/*else if($config['ppkbb_cron_options'][4])
{
	if(rand(1, $config['ppkbb_cron_options'][4])==rand(1, $config['ppkbb_cron_options'][4]))
	{
		$template->assign_block_vars('tracker_cron', array(
			'CRON_TASK' => '<img src="' . append_sid($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/cron.' . $phpEx) . '" alt="tracker_cron" width="1" height="1" />'
			)
		);
	}
}*/
?>
