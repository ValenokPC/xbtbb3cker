<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

function add_ext_postscr($exs_posters, $exs_screenshots, $topic_id, $post_id)
{
	global $user, $db;

	$sql="DELETE FROM ".TRACKER_IMAGES_TABLE." WHERE post_msg_id='{$post_id}'";
	$db->sql_query($sql);

	if($exs_posters)
	{
		foreach($exs_posters as $v)
		{
			$v['post_msg_id']=$post_id;
			$v['topic_id']=$topic_id;
			$v['poster_id']=$user->data['user_id'];
			$db->sql_query("INSERT INTO ".TRACKER_IMAGES_TABLE." (real_filename, i_width, i_height, extension, mimetype, i_poster, filetime, filesize, post_msg_id, topic_id, poster_id) VALUES('".(implode("', '", array_map('addslashes', $v)))."')");
		}
	}

	if($exs_screenshots)
	{
		foreach($exs_screenshots as $v)
		{
			$v['post_msg_id']=$post_id;
			$v['topic_id']=$topic_id;
			$v['poster_id']=$user->data['user_id'];
			$db->sql_query("INSERT INTO ".TRACKER_IMAGES_TABLE." (real_filename, i_width, i_height, extension, mimetype, i_poster, filetime, filesize, post_msg_id, topic_id, poster_id) VALUES('".(implode("', '", array_map('addslashes', $v)))."')");
		}
	}

	// if($external_posters)
	// {
	// 	$sql="UPDATE ".POSTS_TABLE." SET post_poster='1' WHERE post_id='{$post_id}'";
	// 	$db->sql_query($sql);
	// }
}

?>
