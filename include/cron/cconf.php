<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

include("{$phpbb_root_path}ext/ppk/xbtbb3cker/include/config_map.{$phpEx}");

$crcache_config=array();

$sql = 'SELECT config_name, config_value FROM '.CONFIG_TABLE." WHERE config_name IN('ppkbb_cron_options', 'ppkbb_tcrannounces_options', 'ppkbb_xcannounce_interval', 'ppkbb_tccron_jobs', 'ppkbb_tcenable_rannounces', 'ppkbb_display_trstat', 'ppkbb_cron_last_cleanup', 'ppkbb_logs_cleanup', 'ppkbb_xctable_announce_log', 'ppkbb_xctable_scrape_log', 'ppkbb_xctable_files', 'ppkbb_xctable_files_users', 'ppkbb_xctable_users')";
$result = my_sql_query($sql, $c);
$config_row=my_sql_fetch_array($result);
foreach($config_row as $row)
{
	isset($config_map[$row['config_name']]) ? $row['config_value']=$config_map[$row['config_name']][0]==1 ? $row['config_value'] : my_split_config($row['config_value'], $config_map[$row['config_name']][0], $config_map[$row['config_name']][1], $config_map[$row['config_name']][2]) : '';

	$config[$row['config_name']]=$row['config_value'];
	$crcache_config[$row['config_name']]=$row['config_value'];
}
my_sql_free_result($result);

include_once("{$tincludedir}tcache.{$phpEx}");

t_recache('cron_config', $crcache_config);
?>
