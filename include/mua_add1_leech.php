<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$sql='SELECT COUNT(*) mua_count FROM '. XBT_FILES_USERS .' p LEFT JOIN '.XBT_FILES." tt ON (p.fid=tt.fid) WHERE p.uid='{$user_id}' AND p.left!='0' AND p.active!='0'".(count($ex_fid_ary) ? " AND tt.forum_id NOT IN('".implode("', '", $ex_fid_ary)."')" : '');//LEFT JOIN '.ATTACHMENTS_TABLE." a ON (p.fid=a.attach_id) LEFT JOIN ".TOPICS_TABLE." t ON (a.post_msg_id=t.topic_first_post_id)
$result=$db->sql_query($sql);
$mua_count=$db->sql_fetchfield('mua_count');
$db->sql_freeresult($result);

$sql='SELECT p.*, t.topic_title, tt.size, tt.forum_id, tt.topic_id, a.post_msg_id FROM '. XBT_FILES_USERS .' p LEFT JOIN '.ATTACHMENTS_TABLE." a ON (p.fid=a.attach_id) LEFT JOIN ".TOPICS_TABLE." t ON (a.post_msg_id=t.topic_first_post_id) LEFT JOIN ".XBT_FILES." tt ON (p.fid=tt.fid) WHERE p.uid='{$user_id}' AND p.left!='0' AND p.active!='0'".(count($ex_fid_ary) ? " AND tt.forum_id NOT IN('".implode("', '", $ex_fid_ary)."')" : '').($mua_limit ? " LIMIT {$mua_limit}" : '');// ORDER BY $sql_addon";
$result=$db->sql_query($sql);
while($userlist=$db->sql_fetchrow($result))
{
	$i+=1;

	$assign_vars[$i]['TORRENT_URL'] = $userlist['post_msg_id'] ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", "f={$userlist['forum_id']}&amp;t={$userlist['topic_id']}&amp;p={$userlist['post_msg_id']}#p{$userlist['post_msg_id']}") : '';

	$assign_vars[$i]['TORRENT_NAME'] = $userlist['topic_title'] ? censor_text($userlist['topic_title']) : ($user->lang['TORRENT_DELETED']);
	$assign_vars[$i]['TORRENT_CNAME'] = $userlist['topic_title'];

	$assign_vars[$i]['TORRENT_UP'] = get_formatted_filesize($userlist['uploaded']);
	$assign_vars[$i]['TORRENT_BUP'] = $userlist['uploaded'];

	$assign_vars[$i]['TORRENT_DOWN'] = get_formatted_filesize($userlist['downloaded']);
	$assign_vars[$i]['TORRENT_BDOWN'] = $userlist['downloaded'];

	$assign_vars[$i]['TORRENT_RATIO'] = $this->get_ratio_alias($this->get_ratio($userlist['uploaded'], $userlist['downloaded']));

	$assign_vars[$i]['TORRENT_COMPLETED'] = $userlist['size'] && $userlist['left']!==null ? $this->my_float_val(100 - (100 * $userlist['left'] / $userlist['size'])) : '';

	$assign_vars[$i]['TORRENT_LAST'] = $userlist['mtime'] ? $user->format_date($userlist['mtime'], 'Y-m-d H:i:s') : '';
	$assign_vars[$i]['TORRENT_SLAST'] = $userlist['mtime'];

	$assign_vars[$i]['TORRENT_ANNCOUNT'] = $userlist['announced'];
	$assign_vars[$i]['ID'] = $userlist['fid'];
}
$db->sql_freeresult($result);



?>
