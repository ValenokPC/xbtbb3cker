<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$delete_bookmarks=array();

if($user_id!=$user->data['user_id'])
{
	$user_bookmarks_options=array();
}
else
{
	is_array($user->data['user_tracker_options']) ? '' : $user->data['user_tracker_options']=$this->my_split_config($user->data['user_tracker_options'], 4, 'my_int_val');
	$user_bookmarks_options[$user->data['user_id']]=$user->data['user_tracker_options'][3];
}

$sql='SELECT COUNT(*) mua_count FROM '. TRACKER_BOOKMARKS_TABLE.' b LEFT JOIN '.XBT_FILES." tt ON (b.attach_id=tt.fid) WHERE b.user_id='{$user_id}'".(count($ex_fid_ary) ? " AND tt.forum_id NOT IN('".implode("', '", $ex_fid_ary)."')" : '');// LEFT JOIN '.USERS_TABLE." u ON(b.user_id=u.user_id) LEFT JOIN '.POSTS_TABLE." p ON (b.post_msg_id=p.post_id) LEFT JOIN '.ATTACHMENTS_TABLE." a ON (b.attach_id=a.attach_id) LEFT JOIN '.XBT_FILES_USERS .' x ON(b.attach_id=x.fid)
$result=$db->sql_query($sql);
$mua_count=$db->sql_fetchfield('mua_count');
$db->sql_freeresult($result);

$sql='SELECT b.id bookmark_id, b.add_date, b.attach_id, b.user_id, x.*, p.post_subject, tt.size, tt.forum_id, tt.topic_id, a.post_msg_id, a.real_filename, u.user_tracker_options FROM '. TRACKER_BOOKMARKS_TABLE.' b LEFT JOIN '.XBT_FILES_USERS .' x ON(b.attach_id=x.fid) LEFT JOIN '.ATTACHMENTS_TABLE." a ON (b.attach_id=a.attach_id) LEFT JOIN ".POSTS_TABLE." p ON (b.post_msg_id=p.post_id) LEFT JOIN ".XBT_FILES.' tt ON (b.attach_id=tt.fid) LEFT JOIN '.USERS_TABLE." u ON(b.user_id=u.user_id) WHERE b.user_id='{$user_id}'".(count($ex_fid_ary) ? " AND tt.forum_id NOT IN('".implode("', '", $ex_fid_ary)."')" : '').($mua_limit ? " LIMIT {$mua_limit}" : '');// ORDER BY $sql_addon";
$result=$db->sql_query($sql);
while($userlist=$db->sql_fetchrow($result))
{
	$i+=1;
	if(!isset($user_bookmarks_options[$userlist['user_id']]))
	{
		$user_tracker_options=$this->my_split_config($userlist['user_tracker_options'], 4, 'my_int_val');
		$user_bookmarks_options[$userlist['user_id']]=$user_tracker_options[3];
	}

	if(($user_bookmarks_options[$userlist['user_id']] && $userlist['left']!==null && !$userlist['left']) || !$userlist['size'])
	{
		$delete_bookmarks[]=$userlist['bookmark_id'];
		$user_id==$user->data['user_id'] ? $assign_vars[$i]['TORRENT_BOOKMARKDEL'] = true : '';
// 		continue;
	}

	$assign_vars[$i]['TORRENT_URL'] = $userlist['post_msg_id'] ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", "f={$userlist['forum_id']}&amp;t={$userlist['topic_id']}&amp;p={$userlist['post_msg_id']}#p{$userlist['post_msg_id']}") : '';

	$assign_vars[$i]['TORRENT_NAME'] = $userlist['post_subject'] ? $userlist['post_subject'] : ($user->lang['TORRENT_DELETED']);
	$assign_vars[$i]['TORRENT_CNAME'] = $userlist['post_subject'];

// 	$assign_vars[$i]['TORRENT_FNAME'] = $userlist['real_filename'] ? urldecode($userlist['real_filename']) : false;
// 	$assign_vars[$i]['TORRENT_FURL'] = append_sid("{$phpbb_root_path}download/file.{$phpEx}", 'id='.$userlist['attach_id']);
// 	$assign_vars[$i]['TORRENT_FURL'] = append_sid("{$phpbb_root_path}".(!$config['enable_mod_rewrite'] ? "app.{$phpEx}/" : '')."download/torrent.{$phpEx}", 'id='.$userlist['attach_id']);

	$assign_vars[$i]['TORRENT_UP'] = get_formatted_filesize($userlist['uploaded']);
	$assign_vars[$i]['TORRENT_BUP'] = $userlist['uploaded'];

	$assign_vars[$i]['TORRENT_DOWN'] = get_formatted_filesize($userlist['downloaded']);
	$assign_vars[$i]['TORRENT_BDOWN'] = $userlist['downloaded'];

	$assign_vars[$i]['TORRENT_COMPLETED'] = $userlist['size'] && $userlist['left']!==null ? $this->my_float_val(100 - (100 * $userlist['left'] / $userlist['size'])) : '';

	$assign_vars[$i]['TORRENT_ACTIVE'] = $userlist['active'] ? $user->lang['YES'] : $user->lang['NO'];

	$assign_vars[$i]['TORRENT_LAST'] = $userlist['mtime'] ? $user->format_date($userlist['mtime'], 'Y-m-d H:i:s') : '';
	$assign_vars[$i]['TORRENT_SLAST'] = $userlist['mtime'];

	$assign_vars[$i]['TORRENT_ANNCOUNT'] = $userlist['announced'];

	$assign_vars[$i]['TORRENT_RATIO'] = $this->get_ratio_alias($this->get_ratio($userlist['uploaded'], $userlist['downloaded']));

	$assign_vars[$i]['TORRENT_ADDDATE'] = $userlist['add_date'] ? $user->format_date($userlist['add_date'], 'Y-m-d H:i:s') : '';
	$assign_vars[$i]['TORRENT_SADDDATE'] = $userlist['add_date'];

	$assign_vars[$i]['ID'] = $userlist['bookmark_id'];
}
$db->sql_freeresult($result);

if($delete_bookmarks)
{
	$sql='DELETE FROM '.TRACKER_BOOKMARKS_TABLE." WHERE user_id='{$user_id}' AND ".$db->sql_in_set('id', $delete_bookmarks);
	$db->sql_query($sql);
}

?>
