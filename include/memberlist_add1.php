<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

global $auth, $user, $db, $phpEx, $phpbb_root_path, $config, $template, $request;

$dt=time();

$user_id=$member['user_id'];

if($user->data['is_registered'])
{
	$user_tracker_data=$this->my_split_config($member['user_tracker_data'], 6, 'my_int_val');
	$u_update=array();
	if($config['ppkbb_tccron_jobs'][0] && $dt-$user_tracker_data[0] > $config['ppkbb_tccron_jobs'][0])
	{
		$u_update['t']=1;
	}
	if($config['ppkbb_tccron_jobs'][1] && $dt-$user_tracker_data[1] > $config['ppkbb_tccron_jobs'][1])
	{
		$u_update['c']=1;
	}
	$tracker_cron=array();
	if(count($u_update))
	{
		$u_update['user_id']=$member['user_id'];
		$u_update['tracker_data']=$member['user_tracker_data'];
		$sql="INSERT INTO ".TRACKER_CRON_TABLE."(type, data, forum_id, added) VALUES('u_update', '".$db->sql_escape(serialize($u_update))."', '0', '{$dt}')";
		$db->sql_query($sql);
		$cron_id=$db->sql_nextid();
		if($cron_id)
		{
			$tracker_cron[]=$cron_id;
		}
		if(count($tracker_cron))
		{
			$cron_id=implode('&amp;id[]=', $tracker_cron);
			$template->assign_block_vars('tracker_cron', array(
				'CRON_TASK' => '<img src="' . append_sid($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/cron.' . $phpEx, 'id[]='.$cron_id) . '" alt="tracker_cron" width="1" height="1" />'
				)
			);
		}
	}
}

if($is_canviewmuastatr)
{
	$template->assign_vars(array(
		'TRACKER_INFO_OPT' => true,

		'TRACKER_USER_TORRENTS'		=> $member['user_torrents'],
		'TRACKER_USER_COMMENTS'		=> $member['user_comments'],
		'U_TRACKER_USER_TORRENTS'		=> append_sid("{$phpbb_root_path}search.$phpEx", "author_id={$user_id}&amp;sr=topics&amp;ot=1&amp;pt=t"),
		'U_TRACKER_USER_COMMENTS'		=> append_sid("{$phpbb_root_path}search.$phpEx", "author_id={$user_id}&amp;sr=posts&amp;ot=1&amp;pt=c"),
		'TRACKER_USER_UPLOAD'		=> get_formatted_filesize($member['user_uploaded']),
		'TRACKER_USER_DOWNLOAD'		=> get_formatted_filesize($member['user_downloaded']),

		'TRACKER_USER_RATIO'		=> $this->get_ratio_alias($this->get_ratio($member['user_uploaded'], $member['user_downloaded'], $config['ppkbb_tcratio_start'])),

	));
}


if($is_canviewmuastatorr)
{
	$torrent_explain=false;

	$torrent_opt=$request->variable('opt', '');

	$torrents_info=array('torrent', 'finished', 'seed', 'leech', '', '', 'history', 'leave', '', 'downloads', 'bookmarks');

	in_array($torrent_opt, $torrents_info) ? '' : $torrent_opt='';

	$torrent_info=array();

	foreach($torrents_info as $k=>$v)
	{
		if($v)
		{
			if(!$config['ppkbb_torrent_statml'][$k] || ($v=='bookmarks' && !$config['ppkbb_tracker_bookmarks'][0]) || ($v=='bookmarks' && !$member['user_tracker_bookmarks']/* && $user_id!=$user->data['user_id']*/))
			{
				$torrent_opt==$v/* && !$config['ppkbb_torrent_statml'][$k]*/ ? $torrent_opt='' : '';
				continue;
			}
			$memberlist_url=append_sid("{$phpbb_root_path}memberlist.{$phpEx}", "mode=viewprofile&amp;u={$user_id}&amp;opt={$v}");
			$torrent_info[$v]='<a href="'.$memberlist_url.'#torrent_stat">'.($torrent_opt==$v ? '<b>' : '').$user->lang['TORRENT_INFO_HEADER_'.strtoupper($v)].($torrent_opt==$v ? '</b>' : '').'</a>';
		}
	}

	$sort_opt=array(
		'downloads' => array('author', 'topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'dl_time'),
		'finished' => array('topic_title', 'uploaded', 'downloaded', 'active', 'ratio', 'mtime', 'announced', '`left`'),
		'history' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
		'leave' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
		'leech' => array('topic_title', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
		'seed' => array('topic_title', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
		'torrent' => array('topic_title', 'uploaded', 'downloaded', 'ratio', '`left`', 'active', 'mtime', 'announced'),
		'bookmarks' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'add_date'),

	);

	if(!$is_admod)
	{
		$sort_opt['downloads']=array('author', 'topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'ratio', 'dl_time');
		$sort_opt['finished']=array('topic_title', 'uploaded', 'downloaded', 'active', 'ratio', '`left`');
		$sort_opt['history']=array('topic_title', 'uploaded', 'downloaded', '`left`', 'active',  'ratio');
		$sort_opt['leave']=array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'ratio');
		$sort_opt['leech']=array('topic_title', 'uploaded', 'downloaded', 'ratio', '`left`');
		$sort_opt['seed']=array('topic_title', 'uploaded', 'downloaded', 'ratio', '`left`');
		$sort_opt['torrent']=array('topic_title', 'uploaded', 'downloaded', 'ratio', '`left`', 'active');
		$sort_opt['bookmarks']=array('topic_title', 'uploaded', 'downloaded', '`left`', 'active',  'ratio', 'add_date');

	}


	if(!class_exists('timedelta'))
	{
		$user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
		include_once($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$phpEx);

	}
	$td = new \timedelta('D_MINUTES');

	$assign_vars=$postrow_headers=$postrow_footers=array();

	$i=$mua_count=0;

	if($torrent_opt)
	{
		$torrent_explain=true;
	}

	$mua_add1inc=$phpbb_root_path.'ext/ppk/xbtbb3cker/include/';

	$page=$request->variable('pg', 0);
	$mua_limit=($page ? $page-1 : 0)*$config['ppkbb_mua_countlist'].', '.$config['ppkbb_mua_countlist'];

	if($torrent_explain)
	{
		$ex_fid_ary=array_keys($auth->acl_getf('!f_read', true));
		switch($torrent_opt)
		{
			case 'leave':
				include($mua_add1inc.'mua_add1_leave.'.$phpEx);
				break;

			case 'history':
				include($mua_add1inc.'mua_add1_history.'.$phpEx);
				break;

			case 'finished':
				include($mua_add1inc.'mua_add1_finished.'.$phpEx);
				break;

			case 'seed':
				include($mua_add1inc.'mua_add1_seed.'.$phpEx);
				break;

			case 'leech':
				include($mua_add1inc.'mua_add1_leech.'.$phpEx);
				break;

			case 'torrent':
				include($mua_add1inc.'mua_add1_torrent.'.$phpEx);
				break;

			case 'downloads':
				include($mua_add1inc.'mua_add1_downloads.'.$phpEx);
				break;

			case 'bookmarks':
				include($mua_add1inc.'mua_add1_bookmarks.'.$phpEx);
				break;
		}

		$assigned_vars=count($assign_vars);

		if($torrent_explain && $mua_count > $i)
		{
			$mua_page=$this->build_muavt_page($mua_count, $page, append_sid("{$phpbb_root_path}memberlist.{$phpEx}", "mode=viewprofile&amp;u={$user_id}&amp;opt={$torrent_opt}"), 'torrent_stat', $config['ppkbb_mua_countlist']);
			$torrent_info[$torrent_opt].='&nbsp;'.$mua_page;
		}
		$mua_countlist=$this->get_muavt_countlist(min($i, $config['ppkbb_mua_countlist']));

		foreach($sort_opt[$torrent_opt] as $k => $v)
		{
			$v=strtoupper(str_replace('`', '', $v));
			$postrow_headers[]='<th class="my_th"><a class="my_ath" href="javascript:;">'.(isset($user->lang['TORRENT_INFO_HEADER_'.$v]) ? $user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a></th>';
			$postrow_footers[]='<a href="javascript:;" onclick="fnShowHide('.$k.');">'.(isset($user->lang['TORRENT_INFO_HEADER_'.$v]) ? $user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a>';
		}

		if($assign_vars)
		{
			foreach($assign_vars as $k => $v)
			{
				$template->assign_block_vars($torrent_opt.'_option', $v);
			}
		}

		if($postrow_headers)
		{
			foreach($postrow_headers as $k => $v)
			{
				$template->assign_block_vars('torrent_headers', array('VALUE' => $v));
			}
		}

		$template->assign_vars(array(
			'S_MUA_COUNTLIST_DEFAULT' => count($mua_countlist[0]) > 1 ? 5 : -1,
			'S_MUA_COUNTLIST_KEYS' => implode(', ', $mua_countlist[0]),
			'S_MUA_COUNTLIST_VALUES' => implode(', ', $mua_countlist[1]),

			'S_HAS_TORRENT_EXPLAIN'	=> $torrent_opt ? true : false,

			'S_HAS_TORRENT_EXPLAIN_'.strtoupper($torrent_opt)	=> $torrent_explain ? true : false,

			'S_TORRENT_HEADER' => $postrow_headers ? implode('', $postrow_headers) :false,
			'S_TORRENT_FOOTER' => $postrow_footers ? implode(' : ', $postrow_footers) :false,

		));
	}

	$template->assign_vars(array(
		'TORRENT_INFO_OPT'	=> true,

		'TORRENT_INFO_STAT_HEADERS' => count($torrent_info) ? implode(' : ', $torrent_info) : false,

		)
	);



}
?>
