<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}


function ext_postscr($submit, $preview, $refresh, $mode, $post_id, $forum_id)
{
	global $auth, $db, $user, $phpEx, $phpbb_root_path, $config, $template, $request;

	$dt=time();

	$exs_posters=$exs_screenshots=array();

	$exs_postscr=array();

	$error=array();

	$ppkbb_max_extposters=$config['ppkbb_max_extposters'];
	$ppkbb_max_extscreenshots=$config['ppkbb_max_extscreenshots'];

	if($config['ppkbb_extposters_exclude'] && (($config['ppkbb_extposters_trueexclude'] && in_array($forum_id, $config['ppkbb_extposters_exclude']))) || (!$config['ppkbb_extposters_trueexclude'] && !in_array($forum_id, $config['ppkbb_extposters_exclude'])))
	{
		$ppkbb_max_extposters[0]=0;
	}

	if($config['ppkbb_extscreenshots_exclude'] && (($config['ppkbb_extscreenshots_trueexclude'] && in_array($forum_id, $config['ppkbb_extscreenshots_exclude']))) || (!$config['ppkbb_extscreenshots_trueexclude'] && !in_array($forum_id, $config['ppkbb_extscreenshots_exclude'])))
	{
		$ppkbb_max_extscreenshots[0]=0;
	}


	if ($submit || $preview || $refresh)
	{
		$r_timeout = @ini_get('default_socket_timeout');

		$forb_extpostrscr=$config['ppkbb_forb_extpostscr'] ? explode("\n", trim($config['ppkbb_forb_extpostscr'])) : array();

		$extposter=$request->variable('extposter', array(0=>''));

		if($ppkbb_max_extposters[0] && count($extposter))
		{
			@ini_set('default_socket_timeout', $ppkbb_max_extposters[7]);

			$ppkbb_max_extposters[1] > 1 ? $ppkbb_max_extposters[1]=1 : '';
			$ppkbb_max_extposters[2]=1;

			$valid_extposters=0;

			$exp=$request->variable('exp', array(0=>''));
			$is_manual_p=$request->variable('is_manual_p', array(0=>''));
			$is_width_p=$request->variable('is_width_p', array(0=>''));

			$data=array();
			$data['postscr_min_width']=$ppkbb_max_extposters[3];
			$data['postscr_min_height']=$ppkbb_max_extposters[4];
			$data['postscr_max_width']=$ppkbb_max_extposters[5];
			$data['postscr_max_height']=$ppkbb_max_extposters[6];
			$data['postscr_max_filesize']=$ppkbb_max_extposters[8];
			foreach($extposter as $k => $v)
			{
				$p_check=md5($v);
				if(!isset($exs_postscr[$p_check]) && !empty($v) && utf8_strlen($v) < 256 && $k && $k <= $ppkbb_max_extposters[2])
				{
					$iforb_check=true;
					if(count($forb_extpostrscr))
					{
						foreach($forb_extpostrscr as $i)
						{
							$i=preg_quote($i);
							if(($config['ppkbb_forb_extpostscr_trueexclude'] && preg_match("#https?://(www\.)?{$i}#i", $v)) || (!$config['ppkbb_forb_extpostscr_trueexclude'] && !preg_match("#https?://(www\.)?{$i}#i", $v)))
							{
								$error['FORB_EXTPOSTSCR']=sprintf(($config['ppkbb_forb_extpostscr_trueexclude'] ? $user->lang['FORB_EXTPOSTSCR'] : $user->lang['FORB_EXTPOSTSCR_TRUEEXCLUDE']), implode(', ', $forb_extpostrscr));
								$iforb_check=false;
							}
						}
					}
					$data['remotelink']=$v;
					$data['width']=isset($is_manual_p[$k]) ? intval($is_width_p[$k]) : 0;
					$data['height']=isset($is_manual_p[$k]) ? intval($is_height_p[$k]) : 0;
					if($iforb_check && ($submit || !$exp[$k] || $exp[$k]!=$p_check))
					{
						$img_check=postscr_remote($data, $error, " {$user->lang['EXT_POSTERS']}: {$user->lang['EXT_POSTER']}", $k, $ppkbb_max_extposters[7]);
	// 					$error ? trigger_error(implode('<br />', $error)) : '';
	// 					exit();
					}
					else
					{
						$img_check=array();
						$img_check[0]=$data['width'];
						$img_check[1]=$data['height'];
						$img_check[2]=$img_check[3]='';
						$img_check[4]=0;
					}
					if($img_check)
					{
						$exs_posters[$k]['real_filename']=$v;
						$exs_posters[$k]['i_width']=$img_check[0];
						$exs_posters[$k]['i_height']=$img_check[1];
						$exs_posters[$k]['extension']=$img_check[2];
						$exs_posters[$k]['mimetype']=$img_check[3];
						$exs_posters[$k]['i_poster']=1;
						$exs_posters[$k]['filetime']=$dt;
						$exs_posters[$k]['filesize']=$img_check[4];
						$valid_extposters+=1;
					}
					$exs_postscr[$p_check]=1;
				}
			}
		}



		$extscreenshot=$request->variable('extscreenshot', array(0=>''));

		$exs=$request->variable('exs', array(0=>''));
		$is_manual_s=$request->variable('is_manual_s', array(0=>''));
		$is_width_s=$request->variable('is_width_s', array(0=>''));

		if($ppkbb_max_extscreenshots[0] && count($extscreenshot))
		{
			@ini_set('default_socket_timeout', $ppkbb_max_extscreenshots[7]);

			$ppkbb_max_extscreenshots[2] ? '' : $ppkbb_max_extscreenshots[2]=5;

			$valid_extscreenshots=0;

			$data=array();
			$data['postscr_min_width']=$ppkbb_max_extscreenshots[3];
			$data['postscr_min_height']=$ppkbb_max_extscreenshots[4];
			$data['postscr_max_width']=$ppkbb_max_extscreenshots[5];
			$data['postscr_max_height']=$ppkbb_max_extscreenshots[6];
			$data['postscr_max_filesize']=$ppkbb_max_extscreenshots[8];
			foreach($extscreenshot as $k => $v)
			{
				$s_check=md5($v);
				if(!isset($exs_postscr[$s_check]) && !empty($v) && utf8_strlen($v) < 256 && $k && $k <= $ppkbb_max_extscreenshots[2])
				{
					$iforb_check=true;
					if(count($forb_extpostrscr))
					{
						foreach($forb_extpostrscr as $i)
						{
							$i=preg_quote($i);
							if(($config['ppkbb_forb_extpostscr_trueexclude'] && preg_match("#https?://(www\.)?{$i}#i", $v)) || (!$config['ppkbb_forb_extpostscr_trueexclude'] && !preg_match("#https?://(www\.)?{$i}#i", $v)))
							{
								$error['FORB_EXTPOSTSCR']=sprintf(($config['ppkbb_forb_extpostscr_trueexclude'] ? $user->lang['FORB_EXTPOSTSCR'] : $user->lang['FORB_EXTPOSTSCR_TRUEEXCLUDE']), implode(', ', $forb_extpostrscr));
								$iforb_check=false;
							}
						}
					}
					$data['remotelink']=$v;
					$data['width']=isset($is_manual_s[$k]) ? intval($is_width_s[$k]) : 0;
					$data['height']=isset($is_manual_s[$k]) ? intval($is_height_s[$k]) : 0;
					if($iforb_check && ($submit || !$exs[$k] || $exs[$k]!=$s_check))
					{
						$img_check=postscr_remote($data, $error, " {$user->lang['EXT_SCREENSHOTS']}: {$user->lang['EXT_SCREENSHOT']}", $k, $ppkbb_max_extscreenshots[7]);
	// 					$error ? trigger_error(implode('<br />', $error)) : '';
	//					exit();
					}
					else
					{
						$img_check=array();
						$img_check[0]=$data['width'];
						$img_check[1]=$data['height'];
						$img_check[2]=$img_check[3]='';
						$img_check[4]=0;
					}
					if($img_check)
					{
						$exs_screenshots[$k]['real_filename']=$v;
						$exs_screenshots[$k]['i_width']=$img_check[0];
						$exs_screenshots[$k]['i_height']=$img_check[1];
						$exs_screenshots[$k]['extension']=$img_check[2];
						$exs_screenshots[$k]['mimetype']=$img_check[3];
						$exs_screenshots[$k]['i_poster']=0;
						$exs_screenshots[$k]['filetime']=$dt;
						$exs_screenshots[$k]['filesize']=$img_check[4];
						$valid_extscreenshots+=1;
					}
					$exs_postscr[$s_check]=1;
				}
			}
		}

		if($submit && !$auth->acl_gets('a_', 'm_') && !$auth->acl_get('m_', $forum_id))
		{
			if($ppkbb_max_extposters[0] && $ppkbb_max_extposters[1] && $valid_extposters < $ppkbb_max_extposters[1])
			{
				$error[]=sprintf($user->lang['EXTPOSTERS_REQUIRED'], $ppkbb_max_extposters[1]);
			}
			if($ppkbb_max_extscreenshots[0] && $ppkbb_max_extscreenshots[1] && $valid_extscreenshots < $ppkbb_max_extscreenshots[1])
			{
				$error[]=sprintf($user->lang['EXTSCREENSHOTS_REQUIRED'], $ppkbb_max_extscreenshots[1]);
			}
		}
		@ini_set('default_socket_timeout', $r_timeout);

	}

	if($mode=='edit' && !$preview && !$submit && $post_id)
	{
		$p=$s=1;
		$sql="SELECT * FROM ".TRACKER_IMAGES_TABLE." WHERE post_msg_id='{$post_id}'";
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			if($row['i_poster'])
			{
				$exs_posters[$p]=$row;
				$p+=1;
			}
			else
			{
				$exs_screenshots[$s]=$row;
				$s+=1;
			}
		}
		$db->sql_freeresult($result);

	}

	if($ppkbb_max_extposters[0])
	{
		$template->assign_vars(array(
			'S_EXTPOSTERS' => true,
			'LNG_MAX_EXT_POSTERS' => sprintf($user->lang['MAX_EXT_POSTERS'], $ppkbb_max_extposters[2], $ppkbb_max_extposters[1]),
			)
		);
		for($i=1;$i<$ppkbb_max_extposters[2]+1;$i++)
		{
			$poster=isset($exs_posters[$i]) ? $exs_posters[$i]['real_filename'] : '';
			$width=isset($exs_posters[$i]) ? $exs_posters[$i]['i_width'] : 0;
			$height=isset($exs_posters[$i]) ? $exs_posters[$i]['i_height'] : 0;
			$template->assign_block_vars('extposters', array(
				'EXTPOSTERS_I' => $i,
				'EXTPOSTERS_MD5' => $poster ? md5($poster) : 0,
				'EXTPOSTERS_POSTER' => htmlspecialchars($poster),
				'EXTPOSTERS_WIDTH' => $width,
				'EXTPOSTERS_HEIGHT' => $height,
				)
			);
		}
	}

	if($ppkbb_max_extscreenshots[0])
	{
		$template->assign_vars(array(
			'S_EXTSCREENSHOTS' => true,
			'LNG_MAX_EXT_SCREENSHOTS' => sprintf($user->lang['MAX_EXT_SCREENSHOTS'], $ppkbb_max_extscreenshots[2], $ppkbb_max_extscreenshots[1]),
			)
		);
		for($i=1;$i<$ppkbb_max_extscreenshots[2]+1;$i++)
		{
			$screenshot=isset($exs_screenshots[$i]) ? $exs_screenshots[$i]['real_filename'] : '';
			$width=isset($exs_screenshots[$i]) ? $exs_screenshots[$i]['i_width'] : 0;
			$height=isset($exs_screenshots[$i]) ? $exs_screenshots[$i]['i_height'] : 0;
			$template->assign_block_vars('extscreenshots', array(
				'EXTSCREENSHOTS_I' => $i,
				'EXTSCREENSHOTS_MD5' => $screenshot ? md5($screenshot) : 0,
				'EXTSCREENSHOTS_SCREENSHOT' => htmlspecialchars($screenshot),
				'EXTSCREENSHOTS_WIDTH' => $width,
				'EXTSCREENSHOTS_HEIGHT' => $height,
				)
			);
		}
	}

	return array($exs_posters, $exs_screenshots, $error);
}

function postscr_remote($data, &$error, $field, $i=0, $timeout=3)
{
	global $user, $phpbb_root_path, $phpEx;

	$error_append=$field.($i ? " [{$i}]" : '');

	if (!preg_match('#^(http|https)://#i', $data['remotelink']))
	{
		$data['remotelink'] = 'http://' . $data['remotelink'];
	}
	if (!preg_match('#^(http|https)://(?:(.*?\.)*?[a-z0-9\-]+?\.[a-z]{2,4}|(?:\d{1,3}\.){3,5}\d{1,3}):?([0-9]*?).*?\.(gif|jpg|jpeg|png)$#i', $data['remotelink']))
	{
		$error[] = $user->lang['POSTSCR_URL_INVALID'].$error_append;
		return false;
	}
	$image_data=array();

	// We're just trying to reach the server to avoid timeouts
	$p_url=parse_url($data['remotelink']);
	!isset($p_url['port']) ? $p_url['port']=80 : '';
	$fp = @fsockopen($p_url['host'], $p_url['port'], $errno, $errstr, $timeout);
	if ($fp)
	{
		// Make sure getimagesize works...
		if (($image_data = @getimagesize($data['remotelink'])) === false && (empty($data['width']) || empty($data['height'])))
		{
			$error[] = $user->lang['UNABLE_GET_IMAGE_SIZE'].$error_append;
			return false;
		}
		fclose($fp);
	}

	if (!empty($image_data) && ($image_data[0] < 2 || $image_data[1] < 2))
	{
		$error[] = $user->lang['POSTSCR_NO_SIZE'];
		return false;
	}

	isset($image_data[0]) ? '' : $image_data[0]=0;
	isset($image_data[1]) ? '' : $image_data[1]=0;
	isset($image_data[2]) ? '' : $image_data[2]='';

	$width = ($data['width'] && $data['height']) ? $data['width'] : $image_data[0];
	$height = ($data['width'] && $data['height']) ? $data['height'] : $image_data[1];

	if ($width < 2 || $height < 2)
	{
		$error[] = $user->lang['POSTSCR_NO_SIZE'].$error_append;
		return false;
	}

	// Check image type
	$types = image_types();
	$extension = strtolower(get_extension($data['remotelink']));

	if (!empty($image_data) && (!isset($types[$image_data[2]]) || !in_array($extension, $types[$image_data[2]])))
	{
		if (!isset($types[$image_data[2]]))
		{
			$error[] = $user->lang['UNABLE_GET_IMAGE_SIZE'].$error_append;
		}
		else
		{
			$error[] = sprintf($user->lang['IMAGE_FILETYPE_MISMATCH'], $types[$image_data[2]][0], $extension).$error_append;
		}
		return false;
	}

	if (isset($data['postscr_max_width']) || isset($data['postscr_max_height']))
	{
		if ($width > $data['postscr_max_width'] || $height > $data['postscr_max_height'])
		{
			$error[] = sprintf($user->lang['POSTSCR_WRONG_SIZE'], $data['postscr_min_width'], $data['postscr_min_height'], $data['postscr_max_width'], $data['postscr_max_height'], $width, $height).$error_append;
			return false;
		}
	}

	if (isset($data['postscr_min_width']) || isset($data['postscr_min_height']))
	{
		if ($width < $data['postscr_min_width'] || $height < $data['postscr_min_height'])
		{
			$error[] = sprintf($user->lang['POSTSCR_WRONG_SIZE'], $data['postscr_min_width'], $data['postscr_min_height'], $data['postscr_max_width'], $data['postscr_max_height'], $width, $height).$error_append;
			return false;
		}
	}

	//if($data['postscr_max_filesize'])
	//{
		$postscr_file_size=@filesize($data['remotelink']);
		if (!$postscr_file_size)
		{
			if (substr($data['remotelink'], 0, 4)=='http')
			{
				$x = array_change_key_case(get_headers($data['remotelink'], 1), CASE_LOWER);
				if ( preg_match('#HTTP/1\.(?:0|1) 200 OK#', $x[0]) && isset($x['content-length']))
				{
					$postscr_file_size = $x['content-length'];
				}

			}
		}
		$postscr_file_size=intval($postscr_file_size);
		if($data['postscr_max_filesize'] && (!$postscr_file_size || $postscr_file_size > $data['postscr_max_filesize']))
		{
			$error[] = sprintf($user->lang['POSTSCR_WRONG_FILESIZE'], $postscr_file_size, get_formatted_filesize($postscr_file_size), $data['postscr_max_filesize'], get_formatted_filesize($data['postscr_max_filesize'])).$error_append;
			return false;
		}
	//}

	return array($width, $height, $extension, image_type_to_mime_type($image_data[2]), $postscr_file_size);
}

function image_types()
{
	$result = array(
		IMAGETYPE_GIF		=> array('gif'),
		IMAGETYPE_JPEG		=> array('jpg', 'jpeg'),
		IMAGETYPE_PNG		=> array('png'),
		IMAGETYPE_SWF		=> array('swf'),
		IMAGETYPE_PSD		=> array('psd'),
		IMAGETYPE_BMP		=> array('bmp'),
		IMAGETYPE_TIFF_II	=> array('tif', 'tiff'),
		IMAGETYPE_TIFF_MM	=> array('tif', 'tiff'),
		IMAGETYPE_JPC		=> array('jpg', 'jpeg'),
		IMAGETYPE_JP2		=> array('jpg', 'jpeg'),
		IMAGETYPE_JPX		=> array('jpg', 'jpeg'),
		IMAGETYPE_JB2		=> array('jpg', 'jpeg'),
		IMAGETYPE_IFF		=> array('iff'),
		IMAGETYPE_WBMP		=> array('wbmp'),
		IMAGETYPE_XBM		=> array('xbm'),
	);

	if (defined('IMAGETYPE_SWC'))
	{
		$result[IMAGETYPE_SWC] = array('swc');
	}

	return $result;
}

function get_extension($filename)
{
	$filename = utf8_basename($filename);

	if (strpos($filename, '.') === false)
	{
		return '';
	}

	$filename = explode('.', $filename);
	return array_pop($filename);
}


?>
