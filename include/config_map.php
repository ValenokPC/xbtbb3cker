<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

/* phpbb_config dynamic

ppkbb_cron_last_cleanup,
ppkbb_last_stattime,
ppkbb_total_peers_size,
ppkbb_total_seed_leech,
ppkbb_total_speedup_speeddown,
ppkbb_total_sup_sdown,
ppkbb_total_tdown_tup,
ppkbb_total_udown_uup,
ppkbb_total_up_down,
ppkbb_total_remseed_remleech
ppkbb_total_complet_remcomplet
ppkbb_logs_last_cleanup
ppkbb_peers_last_cleanup
*/

$config_map=array(

	'ppkbb_poll_options' => array(3, array('my_int_val', 'strval'), '', 'field'=>array('select:0=OFF:1=AUTO', 'textarea:10:5', 'radio:1=YES:0=NO'), 'default'=>array('0', "TRACKER_POLL_QUEST\nTRACKER_POLL_ANSW5\nTRACKER_POLL_ANSW4\nTRACKER_POLL_ANSW3\nTRACKER_POLL_ANSW2\nTRACKER_POLL_ANSW1", '1')),
	'ppkbb_postscr_opentype' => array(2, 'my_int_val', '', 'field'=>array('select:0=CURR:1=NEW:2=PPHOTO', 'select:0=CURR:1=NEW:2=PPHOTO'), 'default'=>array('0', '0')),

	'ppkbb_tcrewr_updown' => array(4, 'my_int_val', '', 'field'=>array('text:3:3', 'text:3:3', 'radio:1=YES:0=NO', 'text:3:3'), 'default'=>array('0', '0', '0', '0')),
	'ppkbb_torr_blocks' => array(11, 'my_int_val', '', 'field'=>array('select:1=YES:0=NO:2=WLINKS', 'select:0=NO:3=MENU', 'select:1=YES:0=NO:2=HIDE:3=MENU', 'select:0=OFF:1=THUMB:2=FULL', 'select:0=OFF:1=THUMB:2=FULL', 'radio:1=YES:0=NO', 'text:4:4', 'text:4:4', 'text:4:4', 'select:1=CLOSE:0=OPEN:2=SPOILER', 'radio:1=YES:0=NO'), 'default'=>array('1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1')),
	'ppkbb_tcenable_rannounces' => array(9, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('1', '1', '1', '1', '1', '1', '1', '1', '1')),
	'ppkbb_tcrannounces_options' => array(11, array('my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_float_val', 'my_int_val', 'my_int_val', 'strval', 'strval'), '', 'field'=>array('time:6:12', 'time:6:12', 'text:4:4', 'text:4:4', 'time:6:12', 'text:4:4', 'text:7:7', 'radio:1=YES:0=NO', 'text:4:4', 'text:32:128', 'text:8:8'), 'default'=>array('30', '30', '50', '75', '5', '1', '0.1', '0', '20', 'uTorrent/1820', '-UT1820-')),
	'ppkbb_tcclean_place' => array(6, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0', '0', '0', '0', '0')),
	'ppkbb_tcauthor_candown' => array(0 , 'intval', ''),
	'ppkbb_tcguest_cantdown' => array(0 , 'intval', ''),
	'ppkbb_topdown_torrents' => array(15, array('my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'strval', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val'), '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'text:4:4', 'text:6:6', 'text:4:4', 'time:6:12', 'text:4:4', 'text:4:4', 'radio:1=YES:0=NO', 'time:6:12', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'select:0=STANDART', 'text:4:4'), 'default'=>array('0', '0', '0', '15', '100%', '120', '10', '0', '0', '0'), 'append'=>array('', '', '', '', '', 'PIXEL', '')),
	'ppkbb_topdown_torrents_options' => array(9, array('my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'my_int_val', 'strval', 'my_int_val', 'my_int_val', 'my_int_val'), '', 'field'=>array('radio:1=YES:0=NO', 'text:4:4', 'time:6:12', 'time:6:12', 'radio:1=YES:0=NO', 'select:pushpull=pushpull:slide=slide', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'text:4:4'), 'default'=>array('1', '1', '1', '1', '2500', 'slide', '0', '1', '2')),
	'ppkbb_topdown_torrents_exclude' => array(0, 'my_int_val', ','),
	'ppkbb_tfile_annreplace' => array(3, 'my_int_val', '', 'field'=>array('select:0=DEL:1=EXTERNAL:2=ADDIT', 'text:3:3', 'radio:1=YES:0=NO'), 'default'=>array('1', '0', '0')),
	'ppkbb_torrent_statvt' => array(11, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1')),
	'ppkbb_torrent_downlink' => array(3, 'my_int_val', '', 'field'=>array('select:1=REG:2=GUEST:3=REGGUEST:0=OFF', 'select:1=REG:2=GUEST:3=REGGUEST:0=OFF','select:1=REG:2=GUEST:3=REGGUEST:0=OFF'), 'default'=>array('1', '0', '0')),
	'ppkbb_torrent_statml' => array(11, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1')),
	'ppkbb_tccron_jobs' => array(5, 'my_int_val', '', 'field'=>array('time:6:12', 'time:6:12', 'time:6:12', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('12', '12', '60', '0', '0')),
	'ppkbb_tprivate_flag' => array(1, 'intval', '', 'field'=>array('select:0=ASIS:1=PRIVATE:2=NONPRIVATE:-1=UPLOADPRIVATE:-2=UPLOADNONPRIVATE'), 'default'=>array('0')),
	'ppkbb_tcratio_start' => array(1, 'my_int_val', '', 'field'=>array('bytes:8:16'), 'default'=>array('0')),
	'ppkbb_tmin_thumbsize' => array(1, 'my_int_val', '', 'field'=>array('bytes:8:16'), 'default'=>array('0')),
	'ppkbb_trclear_snatched' => array(1, 'my_int_val', '', 'field'=>array('select:0=NO:1=YES:2=NULL:3=NOTNULL'), 'default'=>array('0')),
	'ppkbb_tccleanup_interval' => array(1, 'my_int_val', '', 'field'=>array('time:6:12'), 'default'=>array('3600')),
	'ppkbb_tracker_top' => array(2, 'my_int_val', '', 'field'=>array('text:3:3', 'time:6:12'), 'default'=>array('0', '3600')),
	'ppkbb_tcdef_statuses' => array(2, 'intval', ''),
	'ppkbb_tstatus_notify' => array(2, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0')),
	'ppkbb_trestricts_options' => array(8, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'time:6:12', 'text:5:10', 'text:5:10', 'bytes:8:16', 'text:5:10', 'text:5:10'), 'default'=>array('0', '0', '3600', '0', '0', '0', '0', '0')),
	'ppkbb_tracker_bookmarks' => array(2, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO'), 'default'=>array('0', '0')),

// 	'ppkbb_dead_time' => array(1, 'my_int_val', '', 'field'=>array('time:6:12'), 'default'=>array('2700')),
	'ppkbb_display_trstat' => array(2, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'time:6:12'), 'default'=>array('1', '600')),

	'ppkbb_cron_options' => array(5, 'my_int_val', '', 'field'=>array('time:6:12', 'time:6:12', 'radio:1=YES:0=NO', 'time:6:12', 'text:2:2'), 'default'=>array('3', '1', '0', '60', '5')),
	'ppkbb_clear_logs' => array(1, 'my_int_val', '', 'field'=>array('select:no=OFF:time=TIME:all=ALL'), 'default'=>array('0')),

	'ppkbb_ipreg_countrestrict' => array(2, 'my_int_val', '', 'field'=>array('text:5:5', 'bytes:8:16'), 'default'=>array('0', '0')),

	'ppkbb_noticedisclaimer_blocks' => array(6, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'radio:1=YES:0=NO', 'select:0=OFF:1=REG:2=GUEST:3=ALL'), 'default'=>array('1', '1', '1', '1', '1', '1')),

	'ppkbb_max_torrents' => array(2, 'my_int_val', ''),
	'ppkbb_max_posters' => array(2, 'my_int_val', ''),
	'ppkbb_max_screenshots' => array(2, 'my_int_val', ''),
	'ppkbb_max_extposters' => array(10, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'text:4:4', 'text:4:4', 'text:4:4', 'text:4:4', 'text:4:4', 'text:4:4', 'time:6:12', 'bytes:8:16', 'radio:1=YES:0=NO'), 'default'=>array('1', '0', '1', '10', '10', '1920', '1080', '3', '0', '0'), 'append'=>array('', '', '', 'PIXEL', 'PIXEL', 'PIXEL', 'PIXEL')),
	'ppkbb_max_extscreenshots' => array(10, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'text:4:4', 'text:4:4', 'text:4:4', 'text:4:4', 'text:4:4', 'text:4:4', 'time:6:12', 'bytes:8:16', 'radio:1=YES:0=NO'), 'default'=>array('1', '0', '3', '10', '10', '1920', '1080', '3', '0', '0'), 'append'=>array('', '', '', 'PIXEL', 'PIXEL', 'PIXEL', 'PIXEL')),
	//'ppkbb_mua_countlist' => array(0, 'my_int_val', ''),

	'ppkbb_addit_options' => array(3, 'my_int_val', '', 'field'=>array('radio:1=YES:0=NO', 'select:1=YES:0=NO:2=WOGUEST', 'radio:1=YES:0=NO'), 'default'=>array('1', '2', '1')),
// 	'ppkbb_append_tfile' => array(1, 'my_int_val', '', 'field'=>array('select:0=OFF:2=AFTER'), 'default'=>array('2')),

	'ppkbb_rtrack_enable' => array(3, 'my_int_val', '', 'field'=>array('select:0=OFF:3=ALL', 'text:3:3', 'select:0=FOROFF:1=FORALL:2=FORREG:3=FORGUEST'), 'default'=>array('1', '0', '0')),

	'ppkbb_extposters_exclude' => array(0, 'my_int_val', ','),
	'ppkbb_extscreenshots_exclude' => array(0, 'my_int_val', ','),

	'ppkbb_feed_enblist' => array(0, 'my_int_val', ','),

	'ppkbb_last_dtad' => array(2, 'my_int_val', ''),

	'ppkbb_xcannounce_interval' => array(1, 'my_int_val', '', 'field'=>array('time:6:12'), 'default'=>array('1800')),
	'ppkbb_xcscrape_interval' => array(1, 'my_int_val', '', 'field'=>array('time:6:12'), 'default'=>array('1800')),
	'ppkbb_xcclean_up_interval' => array(1, 'my_int_val', '', 'field'=>array('time:6:12'), 'default'=>array('1800')),
	'ppkbb_xcread_config_interval' => array(1, 'my_int_val', '', 'field'=>array('time:6:12'), 'default'=>array('1800')),
	'ppkbb_xcread_db_interval' => array(1, 'my_int_val', '', 'field'=>array('time:6:12'), 'default'=>array('1800')),
	'ppkbb_xcwrite_db_interval' => array(1, 'my_int_val', '', 'field'=>array('time:6:12'), 'default'=>array('1800')),

	'ppkbb_tcgz_rewrite' => array(1, 'my_int_val', '', 'field'=>array('select:0=AUTO:1=GZ:2=NONGZ'), 'default'=>array('0')),
	'ppkbb_tciptype' => array(1, 'my_int_val', '', 'field'=>array('select:0=STANDART:1=HEADER:2=CLIENT'), 'default'=>array('0')),
	'ppkbb_tcignore_connectable' => array(1, 'my_int_val', '', 'field'=>array('select:0=NOIGNORE:1=IGNORENOCHECK:2=IGNOREYESCHECK'), 'default'=>array('1')),
	'ppkbb_tcignored_upload' => array(1, 'my_int_val', '', 'field'=>array('bytes:8:16'), 'default'=>array('0')),
	'ppkbb_logs_cleanup' => array(2, 'my_int_val', '', 'field'=>array('time:6:12', 'time:6:12'), 'default'=>array('0', '0')),
	'ppkbb_phpclear_peers' => array(1, 'my_int_val', '', 'field'=>array('select:no=OFF:time=TIME:all=ALL'), 'default'=>array('0')),

);

?>
