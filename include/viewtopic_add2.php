<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

function torrent_template($is_candowntorr, $post_torrents, $poster_id, $is_canviewvtstats, $forum_id, $topic_id, $post_id, $is_admod, $post_posters, $post_screenshots, $is_candownpostscr, $torrent_url, $core_functions, $torrents_bookmark, $muavt_url)
{

	global $auth, $user, $db, $phpEx, $phpbb_root_path, $config, $template, $request;

	if($config['ppkbb_torr_blocks'][0])
	{

		$dt=time();

		$torrent_opt='';//$request->variable('opt', '');
		$torrent=$request->variable('torrent', 0);

		$torrents_info=array('filelist', 'finished', 'seed', 'leech', '', 'history', 'leave', '', 'remote', 'downloads', 'bookmarks');
		in_array($torrent_opt, $torrents_info) ? '' : $torrent_opt='';

		!$is_candowntorr ? $torrent_opt='' : '';

		$torrent_info=$torrent_info_menu=array();
		if($is_candowntorr)
		{
			$muavt_amp=strpos($muavt_url, '?')!==false ? '&amp;' : '?';

			if($is_canviewvtstats)
			{
				foreach($torrents_info as $k=>$v)
				{
					if($v)
					{
						if(($v=='remote' && !$config['ppkbb_tcenable_rannounces'][0]) || ($v=='bookmarks' && !$config['ppkbb_tracker_bookmarks'][0]) || !$config['ppkbb_torrent_statvt'][$k])
						{
							$torrent_opt==$v/* && !$config['ppkbb_torrent_statml'][$k]*/ ? $torrent_opt='' : '';
							continue;
						}
						$tracker_viewtopic_url="{$muavt_url}{$muavt_amp}action=vt_stat&amp;opt={$v}&amp;page=vt&amp;p={$post_id}";
						$torrent_info[$v]='<a href="'.append_sid("{$phpbb_root_path}viewtopic.{$phpEx}", "f={$forum_id}&amp;t={$topic_id}&amp;opt={$v}&amp;page=vt").'">'.($torrent_opt==$v ? '<b>' : '').$user->lang['TORRENT_INFO_HEADER_'.strtoupper($v)].($torrent_opt==$v ? '</b>' : '').'</a>';
						$torrent_info_menu[$v]='<li><a data-ajax="true" data-refresh="true" href="'.$tracker_viewtopic_url.'"><i class="icon fa-file-o fa-fw icon-gray" aria-hidden="true"></i><span>'.($torrent_opt==$v ? '<b>' : '').$user->lang['TORRENT_INFO_HEADER_'.strtoupper($v)].($torrent_opt==$v ? '</b>' : '').'</span></a></li>';
					}
				}

				$sort_opt=array(
					'downloads' => array('username', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'dl_time'),
					'filelist' => array('filename', 'size'),
					'finished' => array('username', 'uploaded', 'downloaded', 'active', 'ratio', 'mtime', 'announced', '`left`'),
					'history' => array('username', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
					'leave' => array('username', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
					'leech' => array('username', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
					'remote' => array('rtrack_url', 'next_announce', 'a_message', 'a_interval', 'err_count', 'seeders', 'leechers', 'times_completed', 'peers'),
					'seed' => array('username', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
					'bookmarks' => array('username', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'add_date'),

				);

				if(!$is_admod)
				{
					$sort_opt['downloads']=array('username', 'uploaded', 'downloaded', '`left`', 'active',  'ratio', 'dl_time');
					$sort_opt['finished']=array('username', 'uploaded', 'downloaded', 'active', 'ratio',  '`left`');
					$sort_opt['history']=array('username', 'uploaded', 'downloaded', '`left`', 'active',  'ratio');
					$sort_opt['leave']=array('username', 'uploaded', 'downloaded', '`left`', 'active',  'ratio');
					$sort_opt['leech']=array('username', 'uploaded', 'downloaded', 'ratio', '`left`');
					$sort_opt['remote']=array('rtrack_url', 'next_announce', 'seeders', 'leechers', 'times_completed', 'peers');
					$sort_opt['seed']=array('username', 'uploaded', 'downloaded', 'ratio', '`left`');
					$sort_opt['bookmarks']=array('username', 'uploaded', 'downloaded', '`left`', 'active',  'ratio', 'add_date');

				}
			}
		}

		if(!class_exists('timedelta'))
		{
			$user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
			include_once($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$phpEx);

		}
		$td = new \timedelta('D_MINUTES');

		$image_path=$phpbb_root_path.'ext/ppk/xbtbb3cker/images/';

		if($user->data['is_registered'])
		{
			$torrent_link=$config['ppkbb_torrent_downlink'][0]==1 || $config['ppkbb_torrent_downlink'][0]==3 ? true : false;
			$magnet_link=$config['ppkbb_torrent_downlink'][1]==1 || $config['ppkbb_torrent_downlink'][1]==3 ? true : false;
			$hash_link=$config['ppkbb_torrent_downlink'][2]==1 || $config['ppkbb_torrent_downlink'][2]==3 ? true : false;
		}
		else
		{
			$torrent_link=$config['ppkbb_torrent_downlink'][0]==2 || $config['ppkbb_torrent_downlink'][0]==3 ? true : false;
			$magnet_link=$config['ppkbb_torrent_downlink'][1]==2 || $config['ppkbb_torrent_downlink'][1]==3 ? true : false;
			$hash_link=$config['ppkbb_torrent_downlink'][2]==2 || $config['ppkbb_torrent_downlink'][2]==3 ? true : false;

		}

		$assign_vars=$postrow_headers=$postrow_footers=array();

		$i=$mua_count=0;

		$viewtopic_add1inc=$phpbb_root_path.'ext/ppk/xbtbb3cker/include/';

		foreach($post_torrents as $torrent_id => $torrent_data)
		{
			$torrent_explain=false;

			$torrent_times_completed=$torrent_data['times_completed']-$torrent_data['rem_times_completed'];

			$torrent_seeders=$torrent_data['seeders']-$torrent_data['rem_seeders'];
			if(!$torrent_seeders)
			{
				$seed_percent=0;
			}
			else if($torrent_times_completed && $torrent_seeders < $torrent_times_completed)
			{
				$seed_percent=$core_functions->my_int_val($torrent_seeders * 100 / $torrent_times_completed);
			}
			else
			{
				$seed_percent=100;
			}

			$torrent_basename=utf8_basename(urldecode($torrent_data['real_filename']));

			$torrent_amp=strpos($torrent_url, '?')!==false ? '&amp;' : '?';
			$torrent_src_link=$torrent_url.$torrent_amp.'id='.$torrent_data['torrent_id'];

			$magnet_src_link=false;
			if($magnet_link)
			{
				$magnet_src_link="{$torrent_src_link}&amp;type=magnet";
			}

			$hash_src_link=false;
			if($hash_link)
			{
				$hash_src_link="{$torrent_src_link}&amp;type=hash";
			}

			$torrent_shortname=$config['ppkbb_torr_blocks'][6] && utf8_strlen($torrent_basename)>$config['ppkbb_torr_blocks'][6] ? utf8_substr($torrent_basename, 0, $config['ppkbb_torr_blocks'][6]).'...' : false;

			if($torrent_opt && $torrent_id==$torrent)
			{
				$torrent_explain=true;
			}

			$page=$request->variable('pg', 0);
			$mua_limit=($page ? $page-1 : 0)*$config['ppkbb_mua_countlist'].', '.$config['ppkbb_mua_countlist'];

			if($torrent_explain)
			{

				switch($torrent_opt)
				{
					case 'leave':
						include($viewtopic_add1inc.'viewtopic_add1_leave.'.$phpEx);
						break;

					case 'history':
						include($viewtopic_add1inc.'viewtopic_add1_history.'.$phpEx);
						break;

					case 'filelist':
						include($viewtopic_add1inc.'viewtopic_add1_filelist.'.$phpEx);
						break;

					case 'finished':
						include($viewtopic_add1inc.'viewtopic_add1_finished.'.$phpEx);
						break;

					case 'seed':
						include($viewtopic_add1inc.'viewtopic_add1_seed.'.$phpEx);
						break;

					case 'leech':
						include($viewtopic_add1inc.'viewtopic_add1_leech.'.$phpEx);
						break;

					case 'remote':
						include($viewtopic_add1inc.'viewtopic_add1_remote.'.$phpEx);
						break;

					case 'downloads':
						include($viewtopic_add1inc.'viewtopic_add1_downloads.'.$phpEx);
						break;

					case 'bookmarks':
						include($viewtopic_add1inc.'viewtopic_add1_bookmarks.'.$phpEx);
						break;
				}

				$assigned_vars=count($assign_vars);

				if($vt_count > $i)
				{
					$vt_page=$core_functions->build_muavt_page($vt_count, $page, append_sid("{$phpbb_root_path}viewtopic.{$phpEx}", "f={$forum_id}&amp;t={$topic_id}&amp;opt={$torrent_opt}&amp;page=vt&amp;torrent={$torrent}"), 'torrent_stat'.$torrent, $config['ppkbb_mua_countlist']);
					$torrent_info[$torrent_opt].='&nbsp;'.$vt_page;
				}
				$mua_countlist=$core_functions->get_muavt_countlist(min($i, $config['ppkbb_mua_countlist']));
			}

			$template->assign_block_vars('postrow.torrent_fields', array(
				'TORRENT_ID' => $torrent_data['torrent_id'],

				'TORRENT_FILENAME' => $torrent_basename,

				'TORRENT_SRC_LINK' => $torrent_src_link,
				'MAGNET_SRC_LINK' => $magnet_src_link,
				'HASH_SRC_LINK' => $hash_src_link,

				'TORRENT_FILESIZE' => get_formatted_filesize($torrent_data['filesize']),
				'TORRENT_BFILESIZE' => number_format($torrent_data['filesize'], 0, '.', ' '),

				'TORRENT_SHORTNAME' => $torrent_shortname,
				'TORRENT_COMMENT' => $torrent_data['attach_comment'] ? $torrent_data['attach_comment'] : false,
// 				'TORRENTFILE_DOWNLOADED' => $torrent_data['download_count'],

				'TORRENT_ADDED' => $user->format_date($torrent_data['added']),
				'TORRENT_ADDED_LEFT' => $td->spelldelta($torrent_data['added'], $dt),

				'TORRENT_SEEDERS' => $torrent_data['seeders'],
				'TORRENT_LEECHERS' => $torrent_data['leechers'],
				'TORRENT_COMPLETED' => $torrent_data['times_completed'],

				'TORRENT_REAL_SEEDERS' => $torrent_data['real_seeders'],
				'TORRENT_REAL_LEECHERS' => $torrent_data['real_leechers'],
				'TORRENT_REAL_COMPLETED' => $torrent_data['real_times_completed'],
				'TORRENT_REM_SEEDERS' => (isset($torrent_data['rem_seeders']) ? $torrent_data['rem_seeders'] : 0),
				'TORRENT_REM_LEECHERS' => (isset($torrent_data['rem_leechers']) ? $torrent_data['rem_leechers'] : 0),
				'TORRENT_REM_COMPLETED' => (isset($torrent_data['rem_times_completed']) ? $torrent_data['rem_times_completed'] : 0),

				'TORRENT_HEALTH' => $core_functions->get_torrent_health($torrent_data['seeders'], $torrent_data['leechers']),

				'TORRENT_SPERCENT' => $seed_percent,
				'TORRENT_BOOKMARK' => isset($torrent_data['bookmark']) && $torrent_data['bookmark'] ? true : false,
				'TORRENT_BOOKMARK_COUNT' => isset($torrents_bookmark[$torrent_id]) ? $torrents_bookmark[$torrent_id] : 0,

				'TORRENT_SIZE' => get_formatted_filesize($torrent_data['size']),
				'TORRENT_BSIZE' => number_format($torrent_data['size'], 0, '.', ' '),

				'TORRENT_PRIVATE' => $torrent_data['private'] ? true : false,
				'TORRENT_FILES' => $torrent_data['numfiles'],

				'TORRENT_HASH' => bin2hex($torrent_data['info_hash']),

				'TORRENT_INFO_STAT_HEADERS' => count($torrent_info) ? str_replace('&amp;page=vt', '&amp;page=vt&amp;torrent='.$torrent_id.'#torrent_stat'.$torrent_id, ($torrent_explain ? implode(' : ', $torrent_info) : str_replace(array('<b>', '</b>'), '', implode(' : ', $torrent_info)))) : false,
				'TORRENT_INFO_STAT_HEADERS_MENU' => count($torrent_info) ? str_replace('&amp;page=vt', '&amp;page=vt&amp;torrent='.$torrent_id, ($torrent_explain ? implode('', $torrent_info_menu) : str_replace(array('<b>', '</b>'), '', implode('', $torrent_info_menu)))) : false,

				'S_HAS_TORRENT_EXPLAIN' => $torrent_explain ? true : false,

				'S_HAS_TORRENT_EXPLAIN_HIDE' => $torrent_explain ? false : true,
				)
			);

			if($torrent_explain)
			{
				foreach($sort_opt[$torrent_opt] as $k => $v)
				{
					$v=strtoupper(str_replace('`', '', $v));
					$postrow_headers[]='<th class="my_th" style="text-align:left;"><a class="my_ath" href="javascript:;">'.(isset($user->lang['TORRENT_INFO_HEADER_'.$v]) ? $user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a></th>';
					$postrow_footers[]='<a href="javascript:;" onclick="fnShowHide('.$k.');">'.(isset($user->lang['TORRENT_INFO_HEADER_'.$v]) ? $user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a>';
				}

				if($assign_vars)
				{
					foreach($assign_vars as $k => $v)
					{
						$template->assign_block_vars($torrent_opt.'_option', $v);
					}
				}

				if($postrow_headers)
				{
					foreach($postrow_headers as $k => $v)
					{
						$template->assign_block_vars('torrent_headers', array('VALUE' => $v));
					}
				}

				$template->assign_vars(array(
					'S_MUA_COUNTLIST_DEFAULT' => count($mua_countlist[0]) > 1 ? 5 : -1,
					'S_MUA_COUNTLIST_KEYS' => implode(', ', $mua_countlist[0]),
					'S_MUA_COUNTLIST_VALUES' => implode(', ', $mua_countlist[1]),

					'S_HAS_TORRENT_EXPLAIN' => $torrent ? true : false,

					'S_HAS_TORRENT_EXPLAIN_'.strtoupper($torrent_opt)	=> $torrent ? true : false,
					)
				);

			}
		}

		$template->assign_vars(array(
			'S_TORRENT_HEADER' => $postrow_headers ? implode('', $postrow_headers) :false,
			'S_TORRENT_FOOTER' => $postrow_footers ? implode(' : ', $postrow_footers) :false,
			'S_ANON_ADDON' => in_array($torrent_opt, array('leave', 'history', 'finished', 'seed', 'leech')) && $config['ppkbb_xcanonymous_announce'] ? true : false,

			'S_HASH_ONLY' => $hash_link && !$torrent_link && !$magnet_link ? true : false,

			'TORRENT_MAGNET_SRC_IMG'	=> $image_path.'kt-magnet.png',
			'TORRENT_HASH_SRC_IMG'	=> $image_path.'overview.png',
			'TORRENT_ADDIT_SRC_IMG'	=> $image_path.'news_subscribe.png',
			'TORRENT_STAT_SRC_IMG'	=> $image_path.'excellent.png',
			'TORRENT_DOWNLOAD_SRC_IMG' => $image_path . 'filesave.png',
			'TORRENT_REQUPRATIO_ERROR_SRC_LINK' => $image_path . 'status_unknown.png',
			'TORRENT_ADD_BOOKMARKS_SRC_LINK' => $image_path . 'bookmark_add.png',
			'TORRENT_IN_BOOKMARKS_SRC_LINK' => $image_path . 'bookmark.png',
			'TORRENT_NOTIN_BOOKMARKS_SRC_LINK' => $image_path . 'fileclose.png',

			'TORRENT_LINK' => $torrent_link,
			'TORRENT_MAGNET_LINK' => $magnet_link,
			'TORRENT_HASH_LINK' => $hash_link,
			)
		);

	}

	if($is_candownpostscr && $post_screenshots && $config['ppkbb_torr_blocks'][4])
	{
		$screenshot_height=$config['ppkbb_torr_blocks'][8] ? $config['ppkbb_torr_blocks'][8] : 200;
		foreach($post_screenshots as $screenshot_data)
		{
			$screenshot_addon='';

			$external=isset($screenshot_data['i_external']) ? true : false;

			if(!$external && $screenshot_data['thumbnail'] && $config['ppkbb_torr_blocks'][4]==1)
			{
				$screenshot_addon='&amp;t=1';
			}

			$screenshot_filesize = $screenshot_data['filesize'] ? get_formatted_filesize($screenshot_data['filesize']) : '';
			$screenshot_basename=utf8_basename($screenshot_data['real_filename']);

			$template->assign_block_vars('postrow.screenshot_fields', array(
				'SCREENSHOT_LINK' => !$external ? append_sid("{$phpbb_root_path}download/file.$phpEx", 'id=' . $screenshot_data['attach_id']) : $screenshot_data['real_filename'],
				'SCREENSHOT_SRC' => !$external ? append_sid("{$phpbb_root_path}download/file.$phpEx", 'id=' . $screenshot_data['attach_id'] . $screenshot_addon) : $screenshot_data['real_filename'],
				'SCREENSHOT_FILESIZE' => $screenshot_filesize,
				'SCREENSHOT_WH_HEIGHT' => $screenshot_height,
				'SCREENSHOT_FILENAME'	=> $screenshot_basename,
				'SCREENSHOT_COMMENT'	=> !$external && $screenshot_data['attach_comment'] ? $screenshot_data['attach_comment'] : false,
				'SCREENSHOT_DOWNLOADED' => !$external ? $screenshot_data['download_count'] : 0,
				'SCREENSHOT_FORUM' => 1,
				)
			);

		}
	}
}

?>
