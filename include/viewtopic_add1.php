<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

function torrent_data($is_candowntorr, $post_torrents, $poster_id, $post_posters, $post_screenshots, $is_candownpostscr, $is_canviewvtstats, $post_id, $set_status_url, $set_requpratio_url, $torrent_status, $bookmarks_url)
{
	global $auth, $user, $db, $phpEx, $phpbb_root_path, $config, $request;

	$torrent_top_notice=$torrent_bottom_notice=false;
	if($user->data['is_registered'])
	{

		$config['ppkbb_noticedisclaimer_blocks'][1] && $user->lang['TORRENT_TOP_NOTICE'] ? $torrent_top_notice=$user->lang['TORRENT_TOP_NOTICE'] : '';
		$config['ppkbb_noticedisclaimer_blocks'][2] && $user->lang['TORRENT_BOTTOM_NOTICE'] ? $torrent_bottom_notice=$user->lang['TORRENT_BOTTOM_NOTICE'] : '';
	}
	else
	{

		$config['ppkbb_noticedisclaimer_blocks'][3] && $user->lang['TORRENT_TOP_NOTICE_GUEST'] ? $torrent_top_notice=$user->lang['TORRENT_TOP_NOTICE_GUEST'] : '';
		$config['ppkbb_noticedisclaimer_blocks'][4] && $user->lang['TORRENT_BOTTOM_NOTICE_GUEST'] ? $torrent_bottom_notice=$user->lang['TORRENT_BOTTOM_NOTICE_GUEST'] : '';
	}

	$torrents_count=count($post_torrents);
	$posters_count=count($post_posters);
	$screenshots_count=count($post_screenshots);

	$postrow=array(

		'S_HAS_TRACKER_TORRENT' => $torrents_count ? true : false,
		'S_HAS_TRACKER_POSTER' => $posters_count ? true : false,
		'S_HAS_TRACKER_SCREENSHOT' => $screenshots_count ? true : false,

		'TRACKER_TORRENTS_COUNT' => $torrents_count,
		'TRACKER_POSTERS_COUNT' => $posters_count,
		'TRACKER_SCREENSHOTS_COUNT' => $screenshots_count,

		'S_POSTERS_OPENTYPE' => $config['ppkbb_postscr_opentype'][0],
		'S_SCREENSHOTS_OPENTYPE' => $config['ppkbb_postscr_opentype'][1],

		'S_TORRENT_TOP_NOTICE' => $torrent_top_notice,
		'S_TORRENT_BOTTOM_NOTICE' => $torrent_bottom_notice,

		'S_IS_CANDOWNTORR' => $is_candowntorr ? true : false,
		'S_IS_CANDOWNPOSTSCR' => $is_candownpostscr ? true : false,

		'U_SET_STATUS' => $set_status_url,
		'U_SET_REQUPRATIO' => $set_requpratio_url,

		'U_BOOKMARKS' => $bookmarks_url,
		'TORRENT_BOOKMARKS' => $config['ppkbb_tracker_bookmarks'][1] ? true : false,

		'TORRENT_BLOCK' => $config['ppkbb_torr_blocks'][0] ? true : false,
		'POSTER_BLOCK' => $is_candownpostscr && $config['ppkbb_torr_blocks'][3] ? true : false,

		'SCREENSHOT_BLOCK' => $is_candownpostscr && $config['ppkbb_torr_blocks'][4] ? true : false,
		'SCREENSHOT_BLOCK_CLOSE' => $config['ppkbb_torr_blocks'][9]==1 ? true : false,
		'SCREENSHOT_BLOCK_SPOILER' => $config['ppkbb_torr_blocks'][9]==2 ? true : false,

		'TORRENT_INFO_STAT' => $is_candowntorr && $is_canviewvtstats && $config['ppkbb_torr_blocks'][1] ? true : false,
		'TORRENT_INFO_STAT_HIDE' => $config['ppkbb_torr_blocks'][1]==2 && !$request->variable('opt', '') ? true : false,
		'TORRENT_INFO_STAT_MENU' => $config['ppkbb_torr_blocks'][1]==3 ? true : false,

		'TORRENT_INFO_ADDIT' => $is_candowntorr && $config['ppkbb_torr_blocks'][2] ? true : false,
		'TORRENT_INFO_ADDIT_HIDE' => $config['ppkbb_torr_blocks'][2]==2 ? true : false,
		'TORRENT_INFO_ADDIT_MENU' => $config['ppkbb_torr_blocks'][2]==3 ? true : false,

		'TORRENT_LOGORREG' => !$user->data['is_registered'] && ($config['ppkbb_torr_blocks'][0]==2 || ($torrent_status < 1 && in_array($torrent_status, $config['ppkbb_tcguest_cantdown']))) ? sprintf($user->lang['LOGORREG_DOWNLOAD'], append_sid("{$phpbb_root_path}ucp.{$phpEx}", 'mode=register'), append_sid("{$phpbb_root_path}ucp.{$phpEx}", 'mode=login&amp;redirect='.build_url())) : false,

		'TORRENT_INFO_SHORT' => $config['ppkbb_torr_blocks'][5] ? true : false,

		//'S_TORRENT_HASH' => $is_candowntorr && $config['ppkbb_torr_blocks'][9] ? true : false,

	);

	$poster=false;
	if($is_candownpostscr && $post_posters && $config['ppkbb_torr_blocks'][3])
	{
		$poster_height=$config['ppkbb_torr_blocks'][7] ? $config['ppkbb_torr_blocks'][7] : 200;
		foreach($post_posters as $poster_data)
		{
			if($poster)
			{
				break;
			}
			$poster_addon='';

			$external=isset($poster_data['i_external']) ? true : false;

			if(!$external && $poster_data['thumbnail'] && $config['ppkbb_torr_blocks'][3]==1)
			{
				$poster_addon='&amp;t=1';
			}

			$poster_filesize = $poster_data['filesize'] ? get_formatted_filesize($poster_data['filesize']) : '';
			$poster_basename=utf8_basename($poster_data['real_filename']);

			$image_height=($external || !$poster_addon ? ' height="'.($external && $poster_height > $poster_data['i_height'] ? $poster_data['i_height'] : $poster_height).'"' : '');

			$poster='<a style="float:right;"'.($config['ppkbb_postscr_opentype'][0]==1 ? ' target="_blank"' : '').'
				title="'.$poster_filesize.'"
				href="'.(!$external ? append_sid("{$phpbb_root_path}download/file.$phpEx", 'id=' . $poster_data['attach_id']) : $poster_data['real_filename']).'"'
				.($config['ppkbb_postscr_opentype'][0]==2 ? ' rel="prettyPhotoPosters'.($posters_count > 1 ? "[{$post_id}]" : '').'"' : '').'>
					<img style="float:right;margin:0px 0px 5px 5px;"
						src="'.(!$external ? append_sid("{$phpbb_root_path}download/file.$phpEx", 'id=' . $poster_data['attach_id'] . $poster_addon) : $poster_data['real_filename']).'"'.
						$image_height .
						' alt="" /></a>';

		}
	}

	return array($postrow, $poster);
}
?>
