<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

function get_rem_rtrack($t)
{
	$sql_where='';
	$rem_rtracks_array=array();

	$sql_where.="(rt.rtracker_remote='1' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')";
	$t ? $sql_where.=" OR (rt.user_torrent_zone='{$t}' AND rt.rtracker_type='t')" : '';

	$sql='SELECT rs.id, rs.rtracker_url rtrack_url FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND ({$sql_where})";
	$result=$db->sql_query($sql);
// 	$e_urtracks=0;
	while($row=$db->sql_fetchrow($result))
	{
		$rem_rtracks_array[$row['id']]=$row;
	}
	$db->sql_freeresult($result);

	return $rem_rtracks_array;
}

function get_rtrack($ip, $type=1, $users=0, $torrent=0, $forb_rtracks=array())
{
	global $db, $user;

	$rtracks_array=$sql_where=array();
	$user_zone=0;

	if($type)
	{
		$sql_where[]="(rt.rtracker_remote!='0' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')";
	}
	if($users)
	{
		$sql_where[]="(rt.user_torrent_zone='{$user->data['user_id']}' AND rt.rtracker_type='u')";
	}
	if($torrent)
	{
		$sql_where[]="(rt.user_torrent_zone='{$torrent}' AND rt.rtracker_type='t')";
	}

	if($sql_where)
	{
		$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rtracker_type rtrack_type, user_torrent_zone torrent FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND (".(implode(' OR ', $sql_where)).") AND rt.rtracker_forb='0'";
		$result=$db->sql_query($sql);
		$e_urtracks=0;
		while($row=$db->sql_fetchrow($result))
		{
			$rtrack_forb=0;
			if(count($forb_rtracks))
			{
				foreach($forb_rtracks as $f)
				{
					if($row['rtrack_type']=='u' && in_array($f['rtrack_forb'], array(2, 3)))
					{
						if($f['forb_type']=='s' && strstr($row['rtrack_url'], $f['rtrack_url']))
						{
							$rtrack_forb=1;
						}
						else if($f['forb_type']=='i' && stristr($row['rtrack_url'], $f['rtrack_url']))
						{
							$rtrack_forb=1;
						}
						else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $row['rtrack_url']))
						{
							$rtrack_forb=1;
						}
					}
					else if($row['rtrack_type']=='t' && in_array($f['rtrack_forb'], array(1, 3)))
					{
						if($f['forb_type']=='s' && strstr($row['rtrack_url'], $f['rtrack_url']))
						{
							$rtrack_forb=1;
						}
						else if($f['forb_type']=='i' && stristr($row['rtrack_url'], $f['rtrack_url']))
						{
							$rtrack_forb=1;
						}
						else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $row['rtrack_url']))
						{
							$rtrack_forb=1;
						}
					}
				}
			}
			if(!$rtrack_forb)
			{
				if($row['rtrack_type']=='u')
				{
					$e_urtracks+=1;
					if(!$users || $e_urtracks > $users)
					{
						continue;
					}
				}
				$rtracks_array[$row['id']]=$row;
			}
		}
		$db->sql_freeresult($result);
	}

	return $rtracks_array;
}

function benc_rtrack_url($a, $a_ex=0, $magnet=false)
{
	global $user, $config;

	$a_announce=$rtracks_url=array();

	if($user->data['is_registered'])
	{
		$announce_url=$config['ppkbb_announce_url'].($config['ppkbb_phpannounce_enabled'] && $config['ppkbb_phpannounce_url'] ? $config['ppkbb_phpannounce_url'].'?passkey='.$user->data['user_passkey'] : '/'.$user->data['user_passkey'].'/announce');
	}
	else
	{
		$announce_url=$config['ppkbb_announce_url'].($config['ppkbb_phpannounce_enabled'] && $config['ppkbb_phpannounce_url'] ? $config['ppkbb_phpannounce_url'].'?passkey=' : '/announce');
	}

	$a[0]['rtrack_url'] = $announce_url;

	if($a)
	{
		$a=array_reverse($a);

		foreach($a as $i => $a_url)
		{
			$rtrack_url=$a_url['rtrack_url'];
			if(!in_array($rtrack_url, $rtracks_url))
			{
				$rtrack_url=str_replace('{YOUR_PASSKEY}', $user->data['user_passkey'], $rtrack_url);
				$rtracks_url[]=$rtrack_url;
				$magnet ? $a_announce[$i] = $rtrack_url : $a_announce[][$i] = $rtrack_url;
			}
		}
	}
	if((($a_ex==1) || ($a_ex==2 && $user->data['is_registered']) || ($a_ex==3 && !$user->data['is_registered'])) && count($a_announce) > 1)
	{
		unset($a_announce[0]);
	}

	return $magnet ? implode('&tr=', array_map('urlencode', $a_announce)) : $a_announce;
}


?>
