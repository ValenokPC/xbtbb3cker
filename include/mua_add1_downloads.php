<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$sql="SELECT COUNT(*) mua_count FROM ".TRACKER_DOWNLOADS_TABLE." d LEFT JOIN ".XBT_FILES." ttt ON (d.attach_id=ttt.fid) WHERE d.downloader_id='{$user_id}'".(count($ex_fid_ary) ? " AND ttt.forum_id NOT IN('".implode("', '", $ex_fid_ary)."')" : '');//LEFT JOIN ".ATTACHMENTS_TABLE." a ON (d.attach_id=a.attach_id) LEFT JOIN ".TOPICS_TABLE." tt ON (d.post_msg_id=tt.topic_first_post_id) LEFT JOIN ".XBT_FILES_USERS." s ON (d.downloader_id=s.uid AND d.attach_id=s.fid) LEFT JOIN ".USERS_TABLE." u ON (tt.topic_poster=u.user_id)
$result=$db->sql_query($sql);
$mua_count=$db->sql_fetchfield('mua_count');
$db->sql_freeresult($result);

$sql="SELECT d.id dl_id, d.dl_time, d.post_msg_id, u.username, u.user_id, u.user_colour, tt.forum_id, tt.topic_id, tt.topic_title, s.*, ttt.size FROM ".TRACKER_DOWNLOADS_TABLE." d LEFT JOIN ".ATTACHMENTS_TABLE." a ON (d.attach_id=a.attach_id) LEFT JOIN ".TOPICS_TABLE." tt ON (d.post_msg_id=tt.topic_first_post_id) LEFT JOIN ".XBT_FILES_USERS." s ON (d.downloader_id=s.uid AND d.attach_id=s.fid) LEFT JOIN ".XBT_FILES." ttt ON (d.attach_id=ttt.fid) LEFT JOIN ".USERS_TABLE." u ON (tt.topic_poster=u.user_id) WHERE d.downloader_id='{$user_id}'".(count($ex_fid_ary) ? " AND ttt.forum_id NOT IN('".implode("', '", $ex_fid_ary)."')" : '').($mua_limit ? " LIMIT {$mua_limit}" : '');//." GROUP BY d.post_msg_id";// ORDER BY $sql_addon";
$result=$db->sql_query($sql);
while($userlist=$db->sql_fetchrow($result))
{
	if($userlist['user_id']==1)
	{
		$userlist['username']=$user->lang['GUEST'];
	}
	$i+=1;

	$assign_vars[$i]['TORRENT_USER'] = !empty($userlist['username']) ? str_replace('../', './', get_username_string('full', $userlist['user_id'], $userlist['username'], $userlist['user_colour'], $userlist['username'])) : $user->lang['USER_DELETED'];
	$assign_vars[$i]['TORRENT_CUSER'] = $userlist['username'];

	$assign_vars[$i]['TORRENT_URL'] = $userlist['post_msg_id'] ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", "f={$userlist['forum_id']}&amp;t={$userlist['topic_id']}&amp;p={$userlist['post_msg_id']}#p{$userlist['post_msg_id']}") : '';

	$assign_vars[$i]['TORRENT_NAME'] = $userlist['topic_title'] ? censor_text($userlist['topic_title']) : ($user->lang['TORRENT_DELETED']);
	$assign_vars[$i]['TORRENT_CNAME'] = $userlist['topic_title'];

	$assign_vars[$i]['TORRENT_UP'] = get_formatted_filesize($userlist['uploaded']);
	$assign_vars[$i]['TORRENT_BUP'] = $userlist['uploaded'];

	$assign_vars[$i]['TORRENT_DOWN'] = get_formatted_filesize($userlist['downloaded']);
	$assign_vars[$i]['TORRENT_BDOWN'] = $userlist['downloaded'];

	$assign_vars[$i]['TORRENT_COMPLETED'] = $userlist['size'] && $userlist['left']!==null ? $this->my_float_val(100 - (100 * $userlist['left'] / $userlist['size'])) : '';

	$assign_vars[$i]['TORRENT_ACTIVE'] = $userlist['active'] ? $user->lang['YES'] : $user->lang['NO'];

	$assign_vars[$i]['TORRENT_LAST'] = $userlist['mtime'] ? $user->format_date($userlist['mtime'], 'Y-m-d H:i:s') : '';
	$assign_vars[$i]['TORRENT_SLAST'] = $userlist['mtime'];

	$assign_vars[$i]['TORRENT_ANNCOUNT'] = $userlist['announced'];

	$assign_vars[$i]['TORRENT_RATIO'] = $this->get_ratio_alias($this->get_ratio($userlist['uploaded'], $userlist['downloaded']));

	$assign_vars[$i]['TORRENT_DOWNLOADED'] = $userlist['dl_time'] ? $user->format_date($userlist['dl_time'], 'Y-m-d H:i:s') : '';
	$assign_vars[$i]['TORRENT_SDOWNLOADED'] = $userlist['dl_time'];
	$assign_vars[$i]['ID'] = $userlist['dl_id'];
}
$db->sql_freeresult($result);



?>
