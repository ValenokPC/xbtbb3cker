<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

global $user, $db, $template, $config;

//TOP_BY_DOWNSUM
$sql = "SELECT u.user_id, u.username, u.user_colour, u.user_regdate, u.user_torrents, SUM(completed) AS rc, COUNT(*) AS user_torrents
	FROM ". XBT_FILES ." t JOIN ". USERS_TABLE ." u ON (t.poster_id=user_id)
	WHERE u.user_type IN (" . USER_NORMAL . ', ' . USER_FOUNDER . ")
	GROUP BY t.poster_id
	ORDER BY rc DESC, u.user_torrents
	LIMIT {$top_limit}";

$result = $db->sql_query($sql, $config['ppkbb_tracker_top'][1]);

$i = 0;
while ( $row = $db->sql_fetchrow($result) )
{
	$username = $row['username'];
	$user_id = $row['user_id'];
	$poster_id = $row['user_id'];
	$i++;
	$template->assign_block_vars('top_downsum', array(
		'ROW_NUMBER' => $i,
		'USERNAME' => get_username_string('full', $row['user_id'], $row['username'], $row['user_colour']),
		'JOINED' => $user->format_date($row['user_regdate']),
		'DL_COUNT' => $row['rc'],
		'RELEASES' => $row['user_torrents'],
		'DL_AVG' => round($row['rc'] / $row['user_torrents']),
		)
	);
}
$db->sql_freeresult($result);
?>
