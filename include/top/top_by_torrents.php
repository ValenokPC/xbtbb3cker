<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

global $user, $db, $template, $config, $phpEx, $phpbb_root_path;

//TOP_BY_TORRENTS
$sql = 'SELECT tor.*, t.*, u.username, u.user_id, u.user_colour, f.forum_id, f.forum_name, t.topic_title, t.topic_id, u.user_regdate, tor.completed times_completed
	FROM '. XBT_FILES .' tor, ' . TOPICS_TABLE . ' t , ' . FORUMS_TABLE. ' f , ' . USERS_TABLE . ' u
	WHERE tor.topic_id = t.topic_id
	AND tor.poster_id = u.user_id
	AND t.forum_id = f.forum_id
	AND tor.ctime > '.'%1$s'."
	AND tor.completed!='0'
	AND u.user_type IN (" . USER_NORMAL . ', ' . USER_FOUNDER . ')
	'.(count($ex_fid_ary) ? ' AND '.$db->sql_in_set('f.forum_id', $ex_fid_ary, true) : '')."
	ORDER BY tor.completed DESC
	LIMIT {$top_limit}";

$result = $db->sql_query(sprintf($sql, $top_time), $config['ppkbb_tracker_top'][1], md5($sql.$top_time));

$i = 0;
while ( $row = $db->sql_fetchrow($result) )
{
	$username = $row['username'];
	$user_id = $row['user_id'];
	$forum_name = $row ['forum_name'];
	$forum_id = $row['forum_id'];
	$poster_id = $row['user_id'];
	$topic_title = $row['topic_title'];
	$topic_id = $row['topic_id'];
	$complete = $row['times_completed'];
	$reg_time = $user->format_date($row['ctime']);
	$i++;
	$template->assign_block_vars('top_torrents', array(
		'ROW_NUMBER' => $i,
		'USERNAME' => get_username_string('full', $row['user_id'], $row['username'], $row['user_colour']),
		'FORUM_NAME' => $forum_name,
		'FORUM_HREF' => append_sid("{$phpbb_root_path}viewforum.{$phpEx}?f={$row['forum_id']}"),
		'REG_TIME' => $reg_time,
		'COMPLETE_COUNT' => $complete,
		'TOPIC_TITLE' => censor_text($topic_title),
		'TOPIC_HREF' => append_sid("{$phpbb_root_path}viewtopic.{$phpEx}?f={$row['forum_id']}&amp;t={$row['topic_id']}"),
		)
	);

}
$db->sql_freeresult($result);
?>
