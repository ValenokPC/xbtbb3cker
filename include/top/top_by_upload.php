<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

global $user, $db, $template, $config;

//TOP_BY_UPLOAD
$sql = "SELECT u.username, u.user_colour, u.user_id, xu.uploaded user_uploaded, xu.downloaded user_downloaded, u.user_torrents
	FROM " . USERS_TABLE . " u LEFT JOIN ".XBT_USERS." xu ON (u.user_id=xu.uid)
	WHERE xu.uploaded>0 AND u.user_type IN (" . USER_NORMAL . ', ' . USER_FOUNDER . ")
	ORDER BY xu.uploaded DESC
	LIMIT {$top_limit}";

$result = $db->sql_query($sql, $config['ppkbb_tracker_top'][1]);

$i = 0;
while ( $row = $db->sql_fetchrow($result) )
{
	$username = $row['username'];
	$user_id = $row['user_id'];
	$download = $row['user_downloaded'];
	$upload = $row['user_uploaded'];
	$poster_id = $row['user_id'];
	$ratio = $this->xbt_functions->get_ratio_alias($this->xbt_functions->get_ratio($row['user_uploaded'], $row['user_downloaded'], $config['ppkbb_tcratio_start']));
	$i++;
	$template->assign_block_vars('top_upload', array(
		'ROW_NUMBER' => $i,
		'USERNAME' => get_username_string('full', $row['user_id'], $row['username'], $row['user_colour']),
		'RELEASES' => $row['user_torrents'],
		'UP_DOWN_RATIO' => $ratio,
		'UP' => get_formatted_filesize($upload),
		'DOWN' => get_formatted_filesize($download),
		)
	);
}
$db->sql_freeresult($result);
?>
