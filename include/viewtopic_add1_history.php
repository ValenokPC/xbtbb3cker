<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

global $db, $config, $template, $user;

$sql='SELECT COUNT(*) vt_count FROM '. XBT_FILES_USERS ." s WHERE s.fid='{$torrent}'";//LEFT JOIN ". USERS_TABLE ." u ON (s.uid=u.user_id)
$result=$db->sql_query($sql);
$vt_count=$db->sql_fetchfield('vt_count');
$db->sql_freeresult($result);

$sql='SELECT s.*, u.username, u.user_id, u.user_colour FROM '. XBT_FILES_USERS ." s LEFT JOIN ". USERS_TABLE ." u ON (s.uid=u.user_id) WHERE s.fid='{$torrent}'".($mua_limit ? " LIMIT {$mua_limit}" : '');// ORDER BY $sql_addon";
$result=$db->sql_query($sql);
while($userlist=$db->sql_fetchrow($result))
{
	if($userlist['user_id']==1)
	{
		$userlist['username']=$user->lang['GUEST'];
	}
	$i+=1;
	$assign_vars[$i]['TORRENT_USER'] = empty($userlist['username']) ? $user->lang['USER_DELETED'] : str_replace('../', './', get_username_string('full', $userlist['user_id'], $userlist['username'], $userlist['user_colour'], $userlist['username']));
	$assign_vars[$i]['TORRENT_CUSER'] = $userlist['username'];

	$assign_vars[$i]['TORRENT_UP'] = get_formatted_filesize($userlist['uploaded']);
	$assign_vars[$i]['TORRENT_BUP'] = $userlist['uploaded'];

	$assign_vars[$i]['TORRENT_DOWN'] = get_formatted_filesize($userlist['downloaded']);
	$assign_vars[$i]['TORRENT_BDOWN'] = $userlist['downloaded'];

	$assign_vars[$i]['TORRENT_COMPLETED'] = $userlist['mtime'] && $userlist['left']!==null ? $this->xbt_functions->my_float_val(100 - (100 * $userlist['left'] / $torrent_data['size'])) : '';

	$assign_vars[$i]['TORRENT_ACTIVE'] = $userlist['active'] ? $user->lang['YES'] : $user->lang['NO'];

	$assign_vars[$i]['TORRENT_LAST'] = $userlist['mtime'] ? $user->format_date($userlist['mtime'], 'Y-m-d H:i:s') : '';
	$assign_vars[$i]['TORRENT_SLAST'] = $userlist['mtime'];

	$assign_vars[$i]['TORRENT_ANNCOUNT'] = $userlist['announced'];

	$assign_vars[$i]['TORRENT_RATIO'] = $this->xbt_functions->get_ratio_alias($this->xbt_functions->get_ratio($userlist['uploaded'], $userlist['downloaded']));
}
$db->sql_freeresult($result);

?>
