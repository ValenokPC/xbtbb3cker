<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$total_peers_size=$this->my_split_config($config['ppkbb_total_peers_size'], 2, 'my_int_val');
$total_up_down=$this->my_split_config($config['ppkbb_total_up_down'], 2, 'my_int_val');
$total_sup_sdown=$this->my_split_config($config['ppkbb_total_sup_sdown'], 2, 'my_int_val');
$total_seed_leech=$this->my_split_config($config['ppkbb_total_seed_leech'], 2, 'my_int_val');
$total_tdown_tup=$this->my_split_config($config['ppkbb_total_tdown_tup'], 2, 'my_int_val');
$total_udown_uup=$this->my_split_config($config['ppkbb_total_udown_uup'], 2, 'my_int_val');
$total_remseed_remleech=$this->my_split_config($config['ppkbb_total_remseed_remleech'], 2, 'my_int_val');
$total_complet_remcomplet=$this->my_split_config($config['ppkbb_total_complet_remcomplet'], 2, 'my_int_val');

$stat_update_time=intval($config['ppkbb_display_trstat'][1]/60);

$template->assign_vars(array(
	'S_TRACKER_STAT' => true,

	'STAT_UPDATE_TIME' => sprintf($user->lang['STAT_UPDATE_INTERVAL'], ($stat_update_time  ? $stat_update_time : 1)),

	'TOTAL_TORRENTS'	=> sprintf($user->lang['TOTAL_TORRENT'], $config['num_torrents']),
	'TOTAL_COMMENTS'	=> sprintf($user->lang['TOTAL_COMMENT'], $config['num_comments']),

	'TOTAL_PEERS'	=> sprintf($user->lang['TOTAL_PEER'], $total_peers_size[0]),
	'TOTAL_SIZE'	=> sprintf($user->lang['TOTAL_SIZE'], get_formatted_filesize($total_peers_size[1])),

	'TOTAL_UP'	=> sprintf($user->lang['TOTAL_UP'], get_formatted_filesize($total_up_down[0])),
	'TOTAL_DOWN'	=> sprintf($user->lang['TOTAL_DOWN'], get_formatted_filesize($total_up_down[1])),

	'TOTAL_SUP'	=> sprintf($user->lang['TOTAL_SUP'], get_formatted_filesize($total_sup_sdown[0])),
	'TOTAL_SDOWN'	=> sprintf($user->lang['TOTAL_SDOWN'], get_formatted_filesize($total_sup_sdown[1])),

	'TOTAL_COMPLET'	=> $config['ppkbb_tcenable_rannounces'][0] ? sprintf($user->lang['TOTAL_COMPLET_WR'], $total_complet_remcomplet[0], $total_complet_remcomplet[1]) : sprintf($user->lang['TOTAL_COMPLET'], $total_complet_remcomplet[0]),
	'TOTAL_REMCOMPLET'	=> sprintf($user->lang['TOTAL_REMCOMPLET'], $total_complet_remcomplet[1]),

	'TOTAL_SEED'	=> $config['ppkbb_tcenable_rannounces'][0] ? sprintf($user->lang['TOTAL_SEED_WR'], $total_seed_leech[0], $total_remseed_remleech[0]) : sprintf($user->lang['TOTAL_SEED'], $total_seed_leech[0]),
	'TOTAL_LEECH'	=> $config['ppkbb_tcenable_rannounces'][0] ? sprintf($user->lang['TOTAL_LEECH_WR'], $total_seed_leech[1], $total_remseed_remleech[1]) : sprintf($user->lang['TOTAL_LEECH'], $total_seed_leech[1]),

	'TOTAL_REMSEED'	=> sprintf($user->lang['TOTAL_REMSEED'], $total_remseed_remleech[0]),
	'TOTAL_REMLEECH'	=> sprintf($user->lang['TOTAL_REMLEECH'], $total_remseed_remleech[1]),

	'TOTAL_TDOWN'	=> sprintf($user->lang['TOTAL_TDOWN'], $total_tdown_tup[0]),
	'TOTAL_TUP'	=> sprintf($user->lang['TOTAL_TUP'], $total_tdown_tup[1]),

	'TOTAL_UDOWN'	=> sprintf($user->lang['TOTAL_UDOWN'], $total_udown_uup[0]),
	'TOTAL_UUP'	=> sprintf($user->lang['TOTAL_UUP'], $total_udown_uup[1]),
	)
);
?>
