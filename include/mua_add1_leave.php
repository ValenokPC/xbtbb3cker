<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

$sql='SELECT COUNT(*) mua_count FROM '. XBT_FILES_USERS .' s LEFT JOIN '.XBT_FILES." tt ON (tt.poster_id=s.uid AND tt.fid=s.fid)  WHERE (s.uid='{$user_id}' AND s.completed!='0' AND s.active='0') OR (tt.poster_id='{$user_id}' AND s.active='0')".(count($ex_fid_ary) ? " AND tt.forum_id NOT IN('".implode("', '", $ex_fid_ary)."')" : '');//LEFT JOIN ".ATTACHMENTS_TABLE." a ON (s.fid=a.attach_id) LEFT JOIN ".TOPICS_TABLE." t ON (a.topic_id=t.topic_id)
$result=$db->sql_query($sql);
$mua_count=$db->sql_fetchfield('mua_count');
$db->sql_freeresult($result);

$sql='SELECT s.*, t.forum_id, t.topic_id, t.topic_title, tt.size, a.post_msg_id FROM '. XBT_FILES_USERS .' s LEFT JOIN '.XBT_FILES." tt ON (tt.poster_id=s.uid AND tt.fid=s.fid) LEFT JOIN ".ATTACHMENTS_TABLE." a ON (s.fid=a.attach_id) LEFT JOIN ".TOPICS_TABLE." t ON (a.topic_id=t.topic_id) WHERE (s.uid='{$user_id}' AND s.completed!='0' AND s.active='0') OR (tt.poster_id='{$user_id}' AND s.active='0')".(count($ex_fid_ary) ? " AND tt.forum_id NOT IN('".implode("', '", $ex_fid_ary)."')" : '').($mua_limit ? " LIMIT {$mua_limit}" : '');// ORDER BY $sql_addon";
$result=$db->sql_query($sql);
while($userlist=$db->sql_fetchrow($result))
{
	$i+=1;

	$assign_vars[$i]['TORRENT_URL'] = $userlist['post_msg_id'] ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", "f={$userlist['forum_id']}&amp;t={$userlist['topic_id']}&amp;p={$userlist['post_msg_id']}")."#p{$userlist['post_msg_id']}" : '';

	$assign_vars[$i]['TORRENT_NAME'] = $userlist['topic_title'] ? censor_text($userlist['topic_title']) : ($user->lang['TORRENT_DELETED']);
	$assign_vars[$i]['TORRENT_CNAME'] = $userlist['topic_title'];

	$assign_vars[$i]['TORRENT_UP'] = get_formatted_filesize($userlist['uploaded']);
	$assign_vars[$i]['TORRENT_BUP'] = $userlist['uploaded'];

	$assign_vars[$i]['TORRENT_DOWN'] = get_formatted_filesize($userlist['downloaded']);
	$assign_vars[$i]['TORRENT_BDOWN'] = $userlist['downloaded'];

	$assign_vars[$i]['TORRENT_COMPLETED'] = $userlist['size']  && $userlist['left']!==null? $this->my_float_val(100 - (100 * $userlist['left'] / $userlist['size'])) : '';

	$assign_vars[$i]['TORRENT_ACTIVE'] = $userlist['active'] ? $user->lang['YES'] : $user->lang['NO'];

	$assign_vars[$i]['TORRENT_LAST'] = $userlist['mtime'] ? $user->format_date($userlist['mtime'], 'Y-m-d H:i:s') : '';
	$assign_vars[$i]['TORRENT_SLAST'] = $userlist['mtime'];

	$assign_vars[$i]['TORRENT_ANNCOUNT'] = $userlist['announced'];

	$assign_vars[$i]['TORRENT_RATIO'] = $this->get_ratio_alias($this->get_ratio($userlist['uploaded'], $userlist['downloaded']));
	$assign_vars[$i]['ID'] = $userlist['fid'];
}
$db->sql_freeresult($result);



?>
