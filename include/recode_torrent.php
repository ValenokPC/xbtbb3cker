<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

function recode_torrent_data($physical_filename, $attach_id, $forum_id, $topic_id, $post_id, $torrent_status, $addit_rtracks=array(), $user_id=0)
{
	global $config, $phpbb_root_path, $user, $db, $phpEx, $request;

	@set_time_limit(0);

	$tmpname = $phpbb_root_path.$config['upload_path'].'/'.$physical_filename;

	$dt=time();

	if(!file_exists($tmpname))
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 1);
	}

	if(!$tmpsize=@filesize($tmpname))
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 2);
	}
	$dict=bdecode_f($tmpname, $tmpsize);

	if(isset($dict['info']['files']) && is_array($dict['info']['files']) && count($dict['info']['files']) > 1)
	{
		$filelist=$dict['info']['files'];
	}
	else
	{
		if(isset($dict['info']['length']))
		{
			$filelist[0]['length']=$dict['info']['length'];
			$filelist[0]['path'][0]=$dict['info']['name'];
		}
		else
		{
			$filelist=isset($dict['info']['files']) ? $dict['info']['files'] : '';
		}
	}


	$config['ppkbb_xclisten_port'] ? '' : $config['ppkbb_xclisten_port']=2710;

	$tracker_url=generate_board_url($config['ppkbb_phpannounce_enabled'] && $config['ppkbb_phpannounce_url'] ? false : true);
	if(!$config['ppkbb_announce_url'])
	{
		$config['ppkbb_announce_url']=$tracker_url.($config['ppkbb_phpannounce_enabled'] && $config['ppkbb_phpannounce_url'] ? '' : ':'.$config['ppkbb_xclisten_port']);
	}
	else
	{
		$config['ppkbb_announce_url']=($config['ppkbb_phpannounce_enabled'] && $config['ppkbb_phpannounce_url'] ? $tracker_url : $config['ppkbb_announce_url'].':'.$config['ppkbb_xclisten_port']);
	}

	$rem_announces=$rem_rtracks_array=$forb_rtracks=array();
	if(!$config['ppkbb_tfile_annreplace'][0] || $request->variable('annwarn', 0))
	{
// 		unset($dict['announce-list']);
	}
	else
	{
		$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_forb rtrack_forb, rt.forb_type FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND rt.user_torrent_zone='0' AND rt.rtracker_remote!='0' AND rt.rtracker_type='s'";
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			if(!$row['rtrack_forb'])
			{
				$rem_rtracks_array[]=$row['rtrack_url'];
			}
			else if(in_array($row['rtrack_forb'], array(1, 3)))
			{
				$forb_rtracks[]=$row;
			}
		}
		$db->sql_freeresult($result);
		$addit_rtracks ? $forb_rtracks=array_merge($forb_rtracks, $addit_rtracks) : '';

		$rem_ann_count=0;
		if($config['ppkbb_tfile_annreplace'][0] && isset($dict['announce']) && preg_match('#^(https?|udp):\/\/(\w+|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})#', $dict['announce']) && !in_array($dict['announce'], $rem_rtracks_array) && !in_array($dict['announce'], $rem_announces) && !stristr($dict['announce'], $config['ppkbb_announce_url']) && strlen($dict['announce']) < 256)
		{
			$rtrack_forb=0;
			if(count($forb_rtracks))
			{
				foreach($forb_rtracks as $f)
				{
					if($f['forb_type']=='s' && strstr($dict['announce'], $f['rtrack_url']))
					{
						$rtrack_forb=1;
					}
					else if($f['forb_type']=='i' && stristr($dict['announce'], $f['rtrack_url']))
					{
						$rtrack_forb=1;
					}
					else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $dict['announce']))
					{
						$rtrack_forb=1;
					}
				}
			}
			if(!$rtrack_forb)
			{
				$rem_announces[]=$dict['announce'];
				$rem_ann_count+=1;
			}
		}
		if($config['ppkbb_tfile_annreplace'][0] && isset($dict['announce-list']) && is_array($dict['announce-list']) && count($dict['announce-list']))
		{
			foreach($dict['announce-list'] as $v)
			{
				foreach($v as $v2)
				{
					if($config['ppkbb_tfile_annreplace'][1] && $rem_ann_count > $config['ppkbb_tfile_annreplace'][1])
					{
						break;
					}
					$rtrack_forb=0;
					if(count($forb_rtracks))
					{
						foreach($forb_rtracks as $f)
						{
							if($f['forb_type']=='s' && strstr($v2, $f['rtrack_url']))
							{
								$rtrack_forb=1;
							}
							else if($f['forb_type']=='i' && stristr($v2, $f['rtrack_url']))
							{
								$rtrack_forb=1;
							}
							else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $v2))
							{
								$rtrack_forb=1;
							}
						}
					}
					if(!$rtrack_forb && preg_match('#^(https?|udp):\/\/(\w+|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})#', $v2) && !in_array($v2, $rem_rtracks_array) && !in_array($v2, $rem_announces) && !stristr($v2, $config['ppkbb_announce_url']) && strlen($v2) < 256)
					{
						$rem_announces[]=$v2;
						$rem_ann_count+=1;
					}
				}
			}
		}
// 		unset($dict['announce-list']);
	}

// 	$dict['announce']=$config['ppkbb_announce_url'].'/announce';

	unset($dict['azureus_properties']);
	unset($dict['nodes']);

	$private=isset($dict['info']['private']) && $dict['info']['private'] ? 1 : 0;

	$dict['publisher']=$dict['publisher.utf-8']=$config['server_name'];

	//if(!$config['ppkbb_rtrack_enable'][0])
	//{
		if(($config['ppkbb_tprivate_flag']==1 && $private!=1) || ($config['ppkbb_tprivate_flag']==2 && $private==1))
		{
			unset($dict['info']['crc32']);
			unset($dict['info']['ed2k']);
			unset($dict['info']['md5sum']);
			unset($dict['info']['sha1']);
			unset($dict['info']['tiger']);
		}

		if($config['ppkbb_tprivate_flag']==1)
		{
			$dict['info']['private']=1;
			$private=1;
		}
		else if($config['ppkbb_tprivate_flag']==2)
		{
			isset($dict['info']['private']) ? $dict['info']['private']=0 : '';
			$private=0;
		}

	//}

	$enc_f=bencode($dict);

	$infohash=pack('H*', sha1(bencode($dict['info'])));

	$type_conv='';
	if(function_exists('iconv'))
	{
		$type_conv='iconv';
	}
	else if(function_exists('mb_convert_encoding'))
	{
		$type_conv='mb';
	}

	$tsize=0;
	foreach($filelist as $i => $v)
	{
		$v['length']=@number_format($v['length']+0, 0, '', '');
		$tsize+=$v['length'];

		if($config['ppkbb_addit_options'][2])
		{
			$path=implode('/', (isset($v['path.utf-8']) ? $v['path.utf-8'] : (isset($v['path']) ? $v['path'] : '')));
			if($type_conv && isset($dict['encoding']) && $dict['encoding']!='UTF-8')
			{
				$path=torrent_enconvert($path, $dict['encoding'], 'UTF-8', $type_conv);
			}
			$db->sql_query("INSERT INTO ".TRACKER_FILES_TABLE." (torrent, filename, size) VALUES ('{$attach_id}', '". $db->sql_escape(utf8_normalize_nfc($path)) ."', '{$v['length']}')");
		}
	}

	$bin_salt='';
	if($torrent_status > 0)
	{
		if(!$config['ppkbb_tstatus_salt'])
		{
			$config['ppkbb_tstatus_salt']=substr(sha1(strtolower(gen_rand_string(8))), 0, 8);
			$config->set('ppkbb_tstatus_salt', $config['ppkbb_tstatus_salt'], true);
		}
		$bin_salt=pack('H*', $config['ppkbb_tstatus_salt']);
	}

	$user_id ? '' : $user_id=$user->data['user_id'];
	$result=$db->sql_query("INSERT IGNORE INTO ".XBT_FILES." (fid, info_hash, numfiles, size, ctime, mtime, private, post_msg_id, topic_id, poster_id, forum_id) VALUES ('{$attach_id}', '". $db->sql_escape($infohash.$bin_salt) ."', '".count($filelist)."', '{$tsize}', '{$dt}', '{$dt}', '{$private}', {$post_id}, '{$topic_id}', '{$user_id}', '{$forum_id}')");
	if(!$db->sql_affectedrows($result))
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 7);
	}

	$fp=@fopen($tmpname, "w");
	if($fp)
	{
		fwrite($fp, $enc_f, strlen($enc_f));
		fclose($fp);
	}
	else
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 6);
	}

	$filesize=@filesize($tmpname);
	$db->sql_query("UPDATE " . ATTACHMENTS_TABLE . " SET filesize='{$filesize}' WHERE attach_id='{$attach_id}'");

	if($config['ppkbb_tfile_annreplace'][0])
	{
		if(count($rem_announces))
		{
			$exists_tracker=array();
			$sql="SELECT id, rtracker_md5 FROM ".TRACKER_RTRACKERS_TABLE." WHERE ".$db->sql_in_set('rtracker_md5', array_map('md5', $rem_announces));
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$exists_tracker[$row['rtracker_md5']]=$row['id'];
			}
			$db->sql_freeresult($result);
			foreach($rem_announces as $v)
			{
				$rtracker_md5=md5($v);
				if(!isset($exists_tracker[$rtracker_md5]))
				{
					$sql="INSERT INTO ".TRACKER_RTRACKERS_TABLE."(rtracker_url, rtracker_md5) VALUES('".$db->sql_escape(utf8_normalize_nfc($v))."', '{$rtracker_md5}')";
					$result=$db->sql_query($sql);
					$rtracker_id=$db->sql_nextid();
				}
				else
				{
					$rtracker_id=$exists_tracker[$rtracker_md5];
				}

				$db->sql_query("INSERT INTO ".TRACKER_RTRACK_TABLE."(rtracker_remote, user_torrent_zone, rtracker_id) VALUES('".($config['ppkbb_tfile_annreplace'][0]==1 ? 1 : -1)."', '{$attach_id}', '{$rtracker_id}')");
			}
		}
	}

	return true;
}

function torrent_enconvert($data, $from, $to, $type_conv='')
{
	if(!$type_conv && $data)
	{
		if(function_exists('iconv'))
		{
			$type_conv='iconv';
		}
		else if(function_exists('mb_convert_encoding'))
		{
			$type_conv='mb';
		}
	}

	if($data && $from && $to && $type_conv)
	{
		$data=($type_conv=='iconv' ? @iconv($from, $to, $data) : @mb_convert_encoding($data, $to, $from));
	}

	return $data;
}
?>
