<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}

function check_torrent_data($data, $torrent_statuses)
{
	global $config, $phpbb_root_path, $user, $db, $phpEx;

	@set_time_limit(0);

	$tmpname = "{$phpbb_root_path}{$config['upload_path']}/{$data['physical_filename']}";

	if(!file_exists($tmpname))
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 1);
	}

	if(!$tmpsize=@filesize($tmpname))
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 2);
	}
	$dict=bdecode_f($tmpname, $tmpsize);

	if(!$dict)
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 3);
	}

	if(!isset($dict['info']['name']) || !isset($dict['info']['piece length']) || !isset($dict['info']['pieces']))
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 5);
	}

	if(strlen($dict['info']['pieces']) % 20!=0)
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 4);
	}

	if($dict['info']['name']=='' || !$dict['info']['piece length'] || !$dict['info']['pieces'])
	{
		return sprintf($user->lang['TORRENT_UPLOAD_ERRORS'], 5);
	}

	$private=isset($dict['info']['private']) && $dict['info']['private'] ? 1 : 0;

	if($config['ppkbb_tprivate_flag']==-1 && $private!=1)
	{
		return $user->lang['TORRENT_ERROR_NONPRIVATE'];
	}
	else if($config['ppkbb_tprivate_flag']==-2 && $private==1)
	{
		return $user->lang['TORRENT_ERROR_PRIVATE'];
	}

	if(isset($dict['info']['files']) && is_array($dict['info']['files']) && count($dict['info']['files']) > 1)
	{
		$filelist=$dict['info']['files'];
	}
	else
	{
		if(isset($dict['info']['length']))
		{
			$filelist[0]['length']=$dict['info']['length'];
			$filelist[0]['path'][0]=$dict['info']['name'];
		}
		else
		{
			$filelist=isset($dict['info']['files']) ? $dict['info']['files'] : '';
		}
	}

	if(($config['ppkbb_tprivate_flag']==1 && $private!=1) || ($config['ppkbb_tprivate_flag']==2 && $private==1))
	{
		unset($dict['info']['crc32']);
		unset($dict['info']['ed2k']);
		unset($dict['info']['md5sum']);
		unset($dict['info']['sha1']);
		unset($dict['info']['tiger']);
	}

	if($config['ppkbb_tprivate_flag']==1)
	{
		$dict['info']['private']=1;
		$private=1;
	}
	else if($config['ppkbb_tprivate_flag']==2)
	{
		isset($dict['info']['private']) ? $dict['info']['private']=0 : '';
		$private=0;
	}

	$enc_f=bencode($dict);

	$infohash=pack('H*', sha1(bencode($dict['info'])));

	if(!$config['ppkbb_tstatus_salt'])
	{
		$config['ppkbb_tstatus_salt']=substr(sha1(strtolower(gen_rand_string(8))), 0, 8);
		$config->set('ppkbb_tstatus_salt', $config['ppkbb_tstatus_salt'], true);
	}
	$bin_salt=pack('H*', $config['ppkbb_tstatus_salt']);

	$sql="SELECT fid id, flags, post_msg_id, topic_id, forum_id FROM ".XBT_FILES." WHERE info_hash IN('". $db->sql_escape($infohash) ."', '".$db->sql_escape($infohash.$bin_salt)."') LIMIT 1";
	$result=$db->sql_query($sql);
	$data3=$db->sql_fetchrow($result);
	$db->sql_freeresult($result);

	if($data3)
	{
		$sql="SELECT p.post_id, p.post_visibility, t.forum_id, t.topic_id, t.torrent_status FROM ".POSTS_TABLE." p, ".TOPICS_TABLE." t WHERE t.topic_id='{$data3['topic_id']}' AND p.topic_id=t.topic_id AND p.post_id='{$data3['post_msg_id']}' LIMIT 1";
		$result=$db->sql_query($sql);
		$data2=$db->sql_fetchrow($result);
		$db->sql_freeresult($result);

		if($data2['post_id'] && $data3['id'] && $data3['post_msg_id'] && $data3['topic_id'] && $data3['forum_id'])
		{
			return sprintf(
				$user->lang['TORRENT_UPLOAD_ERROR'],
				append_sid("{$phpbb_root_path}viewtopic.{$phpEx}", "f={$data2['forum_id']}&amp;t={$data2['topic_id']}&amp;p={$data2['post_id']}").'#p'.$data2['post_id'],
				($data2['post_visibility'] ? $user->lang['YES'] : $user->lang['NO']),
				(isset($torrent_statuses['STATUS_REASON'][$data2['torrent_status']]) ? $torrent_statuses['STATUS_REASON'][$data2['torrent_status']] : $user->lang['TORRENT_UNKNOWN_STATUS'])
			);
		}
		else
		{
			$db->sql_query("DELETE FROM ".XBT_FILES." WHERE fid='{$data3['id']}'");
			$db->sql_query("DELETE FROM ".TRACKER_FILES_TABLE." WHERE torrent='{$data3['id']}'");
			$db->sql_query("DELETE FROM ".XBT_FILES_USERS." WHERE fid='{$data3['id']}'");
			$db->sql_query("DELETE FROM ".TRACKER_RANNOUNCES_TABLE." WHERE torrent='{$data3['id']}'");
			$db->sql_query("DELETE FROM ".TRACKER_RTRACK_TABLE." WHERE user_torrent_zone='{$data3['id']}' AND rtracker_type='t'");
		}
	}

	return true;
}

?>
