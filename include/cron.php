<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

error_reporting(0);
@ini_set('register_globals', 0);
@ini_set('magic_quotes_runtime', 0);
@ini_set('magic_quotes_sybase', 0);

function_exists('date_default_timezone_set') ? date_default_timezone_set('Europe/Moscow') : '';

define('IN_PHPBB', true);
define('IN_CRON', true);

$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../../../../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

ignore_user_abort(true);
@set_time_limit(0);
ob_start();

// Output transparent gif
header('Cache-Control: no-cache');
header('Content-type: image/gif');
header('Content-length: 43');

echo base64_decode('R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');

header('Connection: close');

ob_end_flush();
@ob_flush();
flush();

$dt = time();
$id=isset($_GET['id']) && is_array($_GET['id']) ? $_GET['id'] : array();
$ids=count($id);
$c=false;

if($ids)
{
	require($phpbb_root_path . 'config.'.$phpEx);

	if(!in_array($dbms, array('phpbb\\db\\driver\\mysql', 'phpbb\\db\\driver\\mysqli', 'mysql', 'mysqli')))
	{
		err('Only mysql(i) supported');
	}

	require("{$phpbb_root_path}ext/ppk/xbtbb3cker/include/db/".(in_array($dbms, array('phpbb\\db\\driver\\mysql', 'mysql')) ? 'mysql' : 'mysqli').".{$phpEx}");

	$c=my_sql_connect($dbhost.($dbport ? ":{$dbport}" : ''), $dbuser, $dbpasswd);
	if(!$c)
	{
		err('Error connecting database: '.my_sql_error($c).' ['.my_sql_errno($c).']');
	}

	$s=my_sql_select_db($dbname, $c);
	if(!$s)
	{
		err('Error selecting database: '.my_sql_error($c));
	}

	$sql="SET NAMES 'utf8'";
	my_sql_query($sql, $c);
	unset($dbpasswd);

	define('FORUMS_TABLE',				$table_prefix . 'forums');
	define('TOPICS_TABLE',				$table_prefix . 'topics');
	define('USERS_TABLE',				$table_prefix . 'users');
	define('POSTS_TABLE',				$table_prefix . 'posts');
	define('CONFIG_TABLE',				$table_prefix . 'config');

	$config=array();
	if (!defined('PHPBB_ENVIRONMENT'))
	{
		@define('PHPBB_ENVIRONMENT', 'production');
	}
	$tcachedir="{$phpbb_root_path}cache/".PHPBB_ENVIRONMENT.'/';
	$tincludedir="{$phpbb_root_path}ext/ppk/xbtbb3cker/include/tinc/";
	$cincludedir="{$phpbb_root_path}ext/ppk/xbtbb3cker/include/cron/";

	$cache_config=t_getcache('cron_config');
	if($cache_config===false)
	{
		include("{$cincludedir}cconf.{$phpEx}");
	}
	else
	{
		foreach($cache_config as $k => $v)
		{
			$config[$k]=$v;
		}
		unset($cache_config);
	}

	define('TRACKER_CRON_TABLE',			$table_prefix . 'tracker_cron');
	define('TRACKER_RANNOUNCES_TABLE',			$table_prefix . 'tracker_rannounces');
	define('TRACKER_RTRACK_TABLE',			$table_prefix . 'tracker_rtrack');
	define('TRACKER_RTRACKERS_TABLE',			$table_prefix . 'tracker_rtrackers');
	define('TRACKER_FILES_TABLE',			$table_prefix . 'tracker_files');
	define('TRACKER_PEERS_TABLE', $table_prefix . 'tracker_peers');

	define('XBT_ANNOUNCE_LOG', $config['ppkbb_xctable_announce_log']);
	define('XBT_SCRAPE_LOG', $config['ppkbb_xctable_scrape_log']);
	define('XBT_FILES', $config['ppkbb_xctable_files']);
	define('XBT_FILES_USERS', $config['ppkbb_xctable_files_users']);
	define('XBT_USERS', $config['ppkbb_xctable_users']);

	$config['ppkbb_cron_options'][2] && $ids > 1 ? $id=array_slice($id, 0, 1) : '';

	$sql='SELECT * FROM '.TRACKER_CRON_TABLE." WHERE id IN('".implode("', '", array_map('my_int_val', $id))."') AND status='0'";//
	$result0=my_sql_query($sql, $c);
	$cron_data=my_sql_fetch_array($result0);
	foreach($cron_data as $row)
	{
		if($config['ppkbb_cron_options'][3] && $dt-$row['added'] > $config['ppkbb_cron_options'][3])
		{
			continue;
		}

		$sql='UPDATE '.TRACKER_CRON_TABLE." SET status='1' WHERE id='{$row['id']}'";
		my_sql_query($sql, $c);

		if($row['type']=='l_clean')
		{
			$valid_logs_time=$dt-$config['ppkbb_logs_cleanup'][1];

			$sql='DELETE FROM '. XBT_ANNOUNCE_LOG ." WHERE mtime < '{$valid_logs_time}'";
			my_sql_query($sql, $c);
			$sql='DELETE FROM '. XBT_SCRAPE_LOG ." WHERE mtime < '{$valid_logs_time}'";
			my_sql_query($sql, $c);

			my_set_config('ppkbb_logs_last_cleanup', $dt, true);
			//purge_tracker_config(true);
		}
		else if($row['type']=='t_clean')
		{
			$valid_peers_time=$dt-intval($config['ppkbb_xcannounce_interval']*1.25);

			$torrents_cleanup=unserialize(stripslashes($row['data']));
			if(is_array($torrents_cleanup) && count($torrents_cleanup))
			{
				$sql='DELETE FROM '. TRACKER_PEERS_TABLE ." WHERE torrent IN('".(implode("', '", $torrents_cleanup))."') AND last_action < {$valid_peers_time}";
				my_sql_query($sql, $c);

				$sql="SELECT torrent torrent_id, SUM(IF(seeder='1', 1, 0)) seeder, SUM(IF(seeder='0', 1, 0)) leecher FROM ". TRACKER_PEERS_TABLE ." WHERE torrent IN('".(implode("', '", $torrents_cleanup))."') GROUP BY torrent";
				$result2=my_sql_query($sql, $c);
				$r_seeders_leechers=array();
				$cleanup_row=my_sql_fetch_array($result2);
				foreach($cleanup_row as $row_cleanup)
				{
					$r_seeders_leechers[$row_cleanup['torrent_id']][]="seeders='".intval($row_cleanup['seeder'])."'";
					$r_seeders_leechers[$row_cleanup['torrent_id']][]="leechers='".intval($row_cleanup['leecher'])."'";
					$r_seeders_leechers[$row_cleanup['torrent_id']][]="lastcleanup='{$dt}'";
				}
				my_sql_free_result($result2);

				foreach($torrents_cleanup as $k => $v)
				{
					if(!isset($r_seeders_leechers[$k]))
					{
						$r_seeders_leechers[$k][]="seeders='0'";
						$r_seeders_leechers[$k][]="leechers='0'";
						$r_seeders_leechers[$k][]="lastcleanup='{$dt}'";
					}
				}

				foreach($r_seeders_leechers as $k => $t)
				{
					$sql='UPDATE '.XBT_FILES." SET ".implode(', ', $t)." WHERE fid='{$k}'";
					my_sql_query($sql, $c);
				}

			}
			else
			{
				$sql='DELETE FROM '. TRACKER_PEERS_TABLE ." WHERE last_action < {$valid_peers_time}";
				my_sql_query($sql, $c);

				my_set_config('ppkbb_peers_last_cleanup', $dt, true);

			}

			//purge_tracker_config(true);
		}
		else if($row['type']=='t_broken')
		{
			$torrents_broken=unserialize(stripslashes($row['data']));
			$posts_broken=array_keys($torrents_broken);

			$sql='UPDATE '. POSTS_TABLE ." SET post_torrent='0' WHERE post_id IN(".implode(', ', $posts_broken).')';
			my_sql_query($sql, $c);

			$sql="SELECT post_torrent, topic_id FROM ".POSTS_TABLE." WHERE topic_id IN(".implode(', ', $torrents_broken).')';
			$result2=my_sql_query($sql, $c);
			$topics_broken=array();
			$posts_row=my_sql_fetch_array($result2);
			foreach($posts_row as $row_post)
			{
				isset($topics_broken[$row_post['topic_id']]) ? '' : $topics_broken[$row_post['topic_id']]=0;
				$topics_broken[$row_post['topic_id']]+=$row_post['post_torrent'];
			}
			my_sql_free_result($result2);

			if($topics_broken)
			{
				foreach($topics_broken as $topic_id=>$count)
				{
					if(!$count)
					{
						$sql='UPDATE '. TOPICS_TABLE ." SET topic_torrent='0' WHERE topic_id={$topic_id}";
						my_sql_query($sql, $c);
					}
				}
			}
		}
		else if($row['type']=='t_announce')
		{
			$torrents_hashes=unserialize(stripslashes($row['data']));

			if(is_array($torrents_hashes) && count($torrents_hashes))
			{
				include_once("{$phpbb_root_path}ext/ppk/xbtbb3cker/include/rannfunc.{$phpEx}");
				$torrents_id=array_keys($torrents_hashes['torrents_id']);
				$torrents_hashes=$torrents_hashes['torrents_id'];
				$rem_announces=0;
				$r_torr=$r_exs=$r_ann=$torrents_remote=$rem_announced=array();
				$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.user_torrent_zone torrent2 FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND
				(
					(rt.rtracker_remote='1' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')
						OR
					(rt.user_torrent_zone IN(".(implode(', ', $torrents_id)).") AND rt.rtracker_type='t')
				)";
				$result2=my_sql_query($sql, $c);
				$ra=array();
				$remote_row=my_sql_fetch_array($result2);
				foreach($remote_row as $row_remote)
				{
					$ra[$row_remote['id']]=$row_remote;
				}
				my_sql_free_result($result2);

				$sql='SELECT tracker, torrent, next_announce, a_message, s_message, a_interval, err_count, seeders, leechers, times_completed, locked FROM '.TRACKER_RANNOUNCES_TABLE." WHERE  tracker IN('".implode("', '", array_keys($ra))."') AND torrent IN('".(implode("', '", $torrents_id))."')";
				$result2=my_sql_query($sql, $c);
				$remote_row=my_sql_fetch_array($result2);
				foreach($remote_row as $row_remote)
				{
					$ra[$row_remote['tracker']]+=$row_remote;
				}
				my_sql_free_result($result2);
				$forb_rtracks=get_forb_rtrack();

				$r_lock=$config['ppkbb_tcrannounces_options'][4] * ($config['ppkbb_tcrannounces_options'][5] ? $config['ppkbb_tcrannounces_options'][5] * count($torrents_id) : 5 * count($torrents_id));
				$sql='UPDATE '.TRACKER_RANNOUNCES_TABLE." SET next_announce='".($dt+$r_lock)."', locked='1' WHERE torrent IN('".(implode("', '", $torrents_id))."') AND next_announce < {$dt}";
				my_sql_query($sql, $c);
				foreach($ra as $row_remote)
				{
					$rtrack_forb=0;
					if(count($forb_rtracks))
					{
						foreach($forb_rtracks as $f)
						{
							if(in_array($f['rtrack_forb'], array(1, 3)))
							{
								if($f['forb_type']=='s' && strstr($row_remote['rtrack_url'], $f['rtrack_url']))
								{
									$rtrack_forb=1;
								}
								else if($f['forb_type']=='i' && stristr($row_remote['rtrack_url'], $f['rtrack_url']))
								{
									$rtrack_forb=1;
								}
								else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $row_remote['rtrack_url']))
								{
									$rtrack_forb=1;
								}
							}
						}
					}
					if($rtrack_forb || (isset($row_remote['next_announce']) && $row_remote['next_announce'] && $row_remote['next_announce']>$dt) || (isset($row_remote['locked']) && $row_remote['locked']))
					{

					}
					else
					{
						if(!$row_remote['torrent2'])
						{
							$r_torr['all'][$row_remote['id']]=$torrents_hashes;
						}
						else
						{
							isset($torrents_hashes[$row_remote['torrent2']]) ? $r_torr['torr'][$row_remote['id']][$row_remote['torrent2']]=$torrents_hashes[$row_remote['torrent2']] : '';
						}
						if(!isset($r_ann[$row_remote['id']]))
						{
							$rtrack_url=$row_remote['rtrack_url'];
							$r_ann[$row_remote['id']]['rtrack_url']=$rtrack_url;
						}
						if(isset($row_remote['torrent']))
						{
							$r_exs[$row_remote['torrent2'].'_'.$row_remote['torrent']][$row_remote['id']]=$row_remote;
						}
					}
				}

				if(isset($r_torr['all']))
				{
					foreach($r_torr['all'] as $tr_id => $a_data)
					{
						foreach($a_data as $t_id => $t_hash)
						{
							if(($config['ppkbb_tcrannounces_options'][5] && @$rem_announced[$t_id] >= $config['ppkbb_tcrannounces_options'][5]) || ($config['ppkbb_tcrannounces_options'][8] && $rem_announces >= $config['ppkbb_tcrannounces_options'][8]))
							{
								break;
							}
							$t_hash=myhex2bin($t_hash);
							if(isset($r_exs['0_'.$t_id][$tr_id]))
							{
								if($dt > $r_exs['0_'.$t_id][$tr_id]['next_announce'])
								{
									$torrents_remote[$tr_id][$t_id]=array_merge($r_exs['0_'.$t_id][$tr_id], ($r_exs['0_'.$t_id][$tr_id]['s_message'] ? remote_announce($r_ann[$tr_id]['rtrack_url'], $t_hash) : remote_scrape($r_ann[$tr_id]['rtrack_url'], $t_hash)));
									@$rem_announced[$t_id]+=1;
									$rem_announces+=1;
								}
							}
							else
							{
								$torrents_remote[$tr_id][$t_id]=remote_scrape($r_ann[$tr_id]['rtrack_url'], $t_hash);
								@$rem_announced[$t_id]+=1;
								$rem_announces+=1;
							}
						}
					}
				}
				if(isset($r_torr['torr']))
				{
					foreach($r_torr['torr'] as $tr_id => $a_data)
					{
						foreach($a_data as $t_id => $t_hash)
						{
							if(($config['ppkbb_tcrannounces_options'][5] && @$rem_announced[$t_id] >= $config['ppkbb_tcrannounces_options'][5]) || ($config['ppkbb_tcrannounces_options'][8] && $rem_announces >= $config['ppkbb_tcrannounces_options'][8]))
							{
								break;
							}
							$t_hash=myhex2bin($t_hash);
							if(isset($r_exs[$t_id.'_'.$t_id][$tr_id]))
							{
								if($dt > $r_exs[$t_id.'_'.$t_id][$tr_id]['next_announce'])
								{
									$torrents_remote[$tr_id][$t_id]=array_merge($r_exs[$t_id.'_'.$t_id][$tr_id], ($r_exs[$t_id.'_'.$t_id][$tr_id]['s_message'] ? remote_announce($r_ann[$tr_id]['rtrack_url'], $t_hash) : remote_scrape($r_ann[$tr_id]['rtrack_url'], $t_hash)));
									@$rem_announced[$t_id]+=1;
									$rem_announces+=1;
								}
							}
							else
							{
								$torrents_remote[$tr_id][$t_id]=remote_scrape($r_ann[$tr_id]['rtrack_url'], $t_hash);
								@$rem_announced[$t_id]+=1;
								$rem_announces+=1;
							}
						}
					}
				}

				if(is_array($torrents_remote) && count($torrents_remote))
				{
					$r_check=array();
					foreach($torrents_remote as $tr_id => $k)
					{
						foreach($k as $t_id => $v)
						{
							$a_time=!isset($v['interval']) || $v['interval'] < $config['ppkbb_tcrannounces_options'][1] ? $config['ppkbb_tcrannounces_options'][0] : $v['interval'];
							$a_time ? '' : $a_time=$config['ppkbb_xcannounce_interval'];
							if((isset($v['s_message']) && !$v['s_message']) || (isset($v['s_message']) && isset($v['a_message']) && !$v['a_message']) || (!isset($v['s_message']) && isset($v['a_message']) && !$v['a_message']))
							{
								if(isset($v['next_announce']) && $v['next_announce'])
								{
									$sql='UPDATE '.TRACKER_RANNOUNCES_TABLE." SET seeders='".my_int_val($v['seeders'])."', leechers='".my_int_val($v['leechers'])."', times_completed='".my_int_val($v['times_completed'])."', next_announce='".($dt+$a_time)."', a_message='', s_message='', err_count='0', a_interval='{$a_time}', locked='0' WHERE  tracker='{$tr_id}' and torrent='{$t_id}'";
									my_sql_query($sql, $c);
								}
								else
								{
									if(!isset($r_check[$tr_id.'_'.$t_id]))
									{
										/*$sql='UPDATE '.TRACKER_RANNOUNCES_TABLE." SET seeders='".my_int_val($v['seeders'])."', leechers='".my_int_val($v['leechers'])."', peers='".my_int_val(@$v['peers'])."', times_completed='".my_int_val($v['times_completed'])."', next_announce='".($dt+$a_time)."', a_message='', s_message='', err_count='0', a_interval='{$a_time}', locked='0' WHERE  tracker='{$tr_id}' and torrent='{$t_id}'";
										my_sql_query($sql, $c);
										if(!my_sql_affected_rows($c))
										{*/
											$sql='INSERT INTO '.TRACKER_RANNOUNCES_TABLE." (torrent, tracker, seeders, leechers, times_completed, next_announce, a_interval) VALUES('{$t_id}', '{$tr_id}', '".my_int_val(@$v['seeders'])."', '".my_int_val(@$v['leechers'])."', '".my_int_val(@$v['times_completed'])."', '".($dt+$a_time)."', '{$a_time}') ON DUPLICATE KEY UPDATE seeders='".my_int_val($v['seeders'])."', leechers='".my_int_val($v['leechers'])."', times_completed='".my_int_val($v['times_completed'])."', next_announce='".($dt+$a_time)."', a_message='', s_message='', err_count='0', a_interval='{$a_time}', locked='0'";
											my_sql_query($sql, $c);
										//}
										$r_check[$tr_id.'_'.$t_id]=1;
									}
								}
							}
							else
							{
								$mpl_a_time=$config['ppkbb_tcrannounces_options'][6] ? $a_time * $config['ppkbb_tcrannounces_options'][6] : $a_time;
								if(isset($v['next_announce']) && $v['next_announce'])
								{
									$sql='UPDATE '.TRACKER_RANNOUNCES_TABLE." SET a_message='".my_sql_real_escape_string(@$v['a_message'], $c)."', s_message='".my_sql_real_escape_string(@$v['s_message'], $c)."', err_count=err_count+1, next_announce=(err_count*{$mpl_a_time})+".($a_time+$dt).", seeders='0', leechers='0', times_completed='0', a_interval='{$a_time}', locked='0' WHERE tracker='{$tr_id}' AND torrent='{$t_id}'";
									my_sql_query($sql, $c);
								}
								else
								{
									if(!isset($r_check[$tr_id.'_'.$t_id]))
									{
										/*$sql='UPDATE '.TRACKER_RANNOUNCES_TABLE." SET a_message='".my_sql_real_escape_string(@$v['a_message'], $c)."', s_message='".my_sql_real_escape_string(@$v['s_message'], $c)."', err_count=1, next_announce='".intval($dt+$mpl_a_time+$a_time)."', seeders='0', leechers='0', peers='0', times_completed='0', a_interval='{$a_time}', locked='0' WHERE tracker='{$tr_id}' AND torrent='{$t_id}'");
										my_sql_query($sql, $c);
										if(!my_sql_affected_rows($c))
										{*/
											$sql='INSERT INTO '.TRACKER_RANNOUNCES_TABLE." (torrent, tracker, next_announce, a_message, a_interval, s_message, err_count) VALUES('{$t_id}', '{$tr_id}', '".intval($dt+$mpl_a_time+$a_time)."', '".my_sql_real_escape_string(@$v['a_message'], $c)."', '{$a_time}', '".my_sql_real_escape_string(@$v['s_message'], $c)."', '1') ON DUPLICATE KEY UPDATE a_message='".my_sql_real_escape_string(@$v['a_message'], $c)."', s_message='".my_sql_real_escape_string(@$v['s_message'], $c)."', err_count=1, next_announce='".intval($dt+$mpl_a_time+$a_time)."', seeders='0', leechers='0', times_completed='0', a_interval='{$a_time}', locked='0'";
											my_sql_query($sql, $c);
										//}
										$r_check[$tr_id.'_'.$t_id]=1;
									}
								}
							}
						}
					}
				}
				else
				{
					$sql='UPDATE '.TRACKER_RANNOUNCES_TABLE." SET locked='0' WHERE torrent IN('".(implode("', '", $torrents_id))."')";
					my_sql_query($sql, $c);
				}
				$sql='UPDATE '.XBT_FILES." SET
					".XBT_FILES.".rem_seeders=(SELECT SUM(".TRACKER_RANNOUNCES_TABLE.".seeders) FROM ".TRACKER_RANNOUNCES_TABLE." WHERE ".XBT_FILES.".fid=".TRACKER_RANNOUNCES_TABLE.".torrent) ,
					".XBT_FILES.".rem_leechers=(SELECT SUM(".TRACKER_RANNOUNCES_TABLE.".leechers) FROM ".TRACKER_RANNOUNCES_TABLE." WHERE ".XBT_FILES.".fid=".TRACKER_RANNOUNCES_TABLE.".torrent) ,
					".XBT_FILES.".rem_times_completed=(SELECT SUM(".TRACKER_RANNOUNCES_TABLE.".times_completed) FROM ".TRACKER_RANNOUNCES_TABLE." WHERE ".XBT_FILES.".fid=".TRACKER_RANNOUNCES_TABLE.".torrent),
					".XBT_FILES.".lastremote='{$dt}'
					 WHERE ".XBT_FILES.".fid IN('".implode("', '", $torrents_id)."');";
				my_sql_query($sql, $c);
			}
		}
		else if($row['type']=='t_stat')
		{
			$total_rem_peers=$total_rem_leech=$total_rem_seed=$total_rem_complet=0;
			if($config['ppkbb_tcenable_rannounces'][0])
			{
				$sql="SELECT SUM(rem_seeders) seeder, SUM(rem_leechers) leecher, SUM(rem_times_completed) complet FROM ".XBT_FILES."";
				$result3=my_sql_query($sql, $c);
				$total_rem_peer=my_sql_fetch_array($result3, true);
				my_sql_free_result($result3);
				$total_rem_peer ? '' : $total_rem_peer=array('seeder'=>0, 'leecher'=>0, 'complet'=>0);
				$total_rem_peers=my_int_val($total_rem_peer['seeder']+$total_rem_peer['leecher']);
				$total_rem_seed=my_int_val($total_rem_peer['seeder']);
				$total_rem_leech=my_int_val($total_rem_peer['leecher']);
				$total_rem_complet=my_int_val($total_rem_peer['complet']);
			}
			my_set_config('ppkbb_total_remseed_remleech', "{$total_rem_seed} {$total_rem_leech}", true);

			//$sql='SELECT SUM(IF(`left`=0,1,0)) seeder, SUM(IF(`left`!=0,1,0)) leecher FROM '.XBT_FILES_USERS." WHERE active!='0'";
			$sql='SELECT SUM(seeders) seeder, SUM(leechers) leecher, SUM(completed) complet FROM '.XBT_FILES.'';// WHERE mtime > $dt-{$config['ppkbb_xcannounce_interval']}
			$result3=my_sql_query($sql, $c);
			$total_peer=my_sql_fetch_array($result3, true);
			my_sql_free_result($result3);
			$total_peer ? '' : $total_peer=array('seeder'=>0, 'leecher'=>0, 'complet'=>0);
			$total_peers=my_int_val($total_peer['seeder']+$total_peer['leecher']+$total_rem_peers);
			$total_seed=my_int_val($total_peer['seeder']+$total_rem_seed);
			$total_leech=my_int_val($total_peer['leecher']+$total_rem_leech);
			$total_complet=my_int_val($total_peer['complet']+$total_rem_complet);
			my_set_config('ppkbb_total_seed_leech', "{$total_seed} {$total_leech}", true);
			my_set_config('ppkbb_total_complet_remcomplet', "{$total_complet} {$total_rem_complet}", true);

			$sql='SELECT SUM(uploaded) upload, SUM(downloaded) download FROM '.XBT_USERS.'';
			$result3=my_sql_query($sql, $c);
			$total_updown=my_sql_fetch_array($result3, true);
			my_sql_free_result($result3);
			$total_updown ? '' : $total_updown=array('upload'=>0, 'download'=>0);
			$total_up=my_int_val($total_updown['upload']);
			$total_down=my_int_val($total_updown['download']);
			my_set_config('ppkbb_total_up_down', "{$total_up} {$total_down}", true);

			$sql='SELECT SUM(uploaded) upload, SUM(downloaded) download FROM '.XBT_FILES_USERS." WHERE active!='0'";
			$result3=my_sql_query($sql, $c);
			$total_supdown=my_sql_fetch_array($result3, true);
			my_sql_free_result($result3);
			$total_supdown ? '' : $total_supdown=array('upload'=>0, 'download'=>0);
			$total_sup=my_int_val($total_supdown['upload']);
			$total_sdown=my_int_val($total_supdown['download']);
			my_set_config('ppkbb_total_sup_sdown', "{$total_sup} {$total_sdown}", true);

			$sql='SELECT SUM(size) size FROM '.TRACKER_FILES_TABLE.'';
			$result3=my_sql_query($sql, $c);
			$total_size=my_sql_fetch_array($result3, true);
			my_sql_free_result($result3);
			$total_sizes=$total_size ? my_int_val($total_size['size']) : 0;
			my_set_config('ppkbb_total_peers_size', "{$total_peers} {$total_sizes}", true);

			$sql='SELECT COUNT(DISTINCT(fid)) seeders, COUNT(DISTINCT(uid)) seeders2 FROM '.XBT_FILES_USERS." WHERE `left`='0' AND active!='0'";
			$result3=my_sql_query($sql, $c);
			$total_t_seeds=my_sql_fetch_array($result3, true);
			my_sql_free_result($result3);
			$total_t_seeds ? '' : $total_t_seeds=array('seeders'=>0, 'seeders2'=>0);
			$t_seed=my_int_val($total_t_seeds['seeders']);
			$u_seed=my_int_val($total_t_seeds['seeders2']);

			$sql='SELECT COUNT(DISTINCT(fid)) leechers, COUNT(DISTINCT(uid)) leechers2 FROM '.XBT_FILES_USERS." WHERE `left`!='0' AND active!='0'";
			$result3=my_sql_query($sql, $c);
			$total_t_leechs=my_sql_fetch_array($result3, true);
			my_sql_free_result($result3);
			$total_t_leechs ? '' : $total_t_leechs=array('leechers'=>0, 'leechers2'=>0);
			$t_leech=my_int_val($total_t_leechs['leechers']);
			$u_leech=my_int_val($total_t_leechs['leechers2']);
			my_set_config('ppkbb_total_tdown_tup', "{$t_seed} {$t_leech}", true);
			my_set_config('ppkbb_total_udown_uup', "{$u_seed} {$u_leech}", true);

			$tracker_forums=array();
			$sql='SELECT forum_id FROM '.FORUMS_TABLE." WHERE forum_tracker='1'";
			$result4=my_sql_query($sql, $c);
			$forums_data=my_sql_fetch_array($result4);
			foreach($forums_data as $row4)
			{
				$tracker_forums[]=$row4['forum_id'];
			}
			my_sql_free_result($result4);

			$num_torrents=array();
			if($tracker_forums)
			{
				$sql = 'SELECT COUNT(*) AS stat
					FROM '.XBT_FILES."
					WHERE forum_id IN('".implode("', '", $tracker_forums)."')";
				$result = my_sql_query($sql, $c);
				$num_torrents=my_sql_fetch_row($result, true);
				my_sql_free_result($result);
			}
			$num_torrents=isset($num_torrents[0]) ? my_int_val($num_torrents[0]) : 0;
			my_set_config('num_torrents', $num_torrents, true);

			$num_comments=array();
			if($tracker_forums)
			{
				$sql = 'SELECT COUNT(*) AS stat
					FROM '.POSTS_TABLE."
					WHERE forum_id IN('".implode("', '", $tracker_forums)."') AND post_torrent='0'";
				$result = my_sql_query($sql, $c);
				$num_comments=my_sql_fetch_row($result, true);
				my_sql_free_result($result);
			}
			$num_comments=isset($num_comments[0]) ? my_int_val($num_comments[0]) : 0;
			my_set_config('num_comments', $num_comments, true);

			my_set_config('ppkbb_last_stattime', $dt, true);
		}
		else if($row['type']=='u_update')
		{

			$tracker_forums=array();
			$sql='SELECT forum_id FROM '.FORUMS_TABLE." WHERE forum_tracker='1'";
			$result4=my_sql_query($sql, $c);
			$forums_data=my_sql_fetch_array($result4);
			foreach($forums_data as $row4)
			{
				$tracker_forums[]=$row4['forum_id'];
			}
			my_sql_free_result($result4);

			$u_update=unserialize(stripslashes($row['data']));
			$u_cron=array();
			$user_tracker_data=my_split_config($u_update['tracker_data'], 6, 'my_int_val');
			$u_update['user_id']=my_int_val($u_update['user_id']);
			if(isset($u_update['t']) && $config['ppkbb_tccron_jobs'][0])
			{
				$user_torr_data=array();
				if($tracker_forums)
				{
					$sql = 'SELECT COUNT(*) AS torr
						FROM '.XBT_FILES."
						WHERE forum_id IN('".implode("', '", $tracker_forums)."') AND poster_id='{$u_update['user_id']}'";
					$result3=my_sql_query($sql, $c);
					$user_torr_data=my_sql_fetch_array($result3, true);
					my_sql_free_result($result3);
				}
				$user_torr_data=$user_torr_data ? intval($user_torr_data['torr']) : 0;
				$u_cron[]="user_torrents='{$user_torr_data}'";
				$user_tracker_data[0]=$dt;
			}
			if(isset($u_update['c']) && $config['ppkbb_tccron_jobs'][1])
			{
				$user_comm_data=array();
				if($tracker_forums)
				{
					$sql = 'SELECT COUNT(*) AS comm
						FROM '.POSTS_TABLE."
						WHERE forum_id IN('".implode("', '", $tracker_forums)."') AND post_torrent='0' AND poster_id='{$u_update['user_id']}'";
					$result3=my_sql_query($sql, $c);
					$user_comm_data=my_sql_fetch_array($result3, true);
					my_sql_free_result($result3);
				}
				$user_comm_data=$user_comm_data ? intval($user_comm_data['comm']) : 0;
				$u_cron[]="user_comments='{$user_comm_data}'";
				$user_tracker_data[1]=$dt;
			}
			if(isset($u_update['r']))
			{
				$xbt_update=array();
				$xbt_update[]="can_leech='".(isset($u_update['r']['can_leech']) ? $u_update['r']['can_leech'] : 1)."'";
				$xbt_update[]="wait_time='".(isset($u_update['r']['wait_time']) ? $u_update['r']['wait_time'] : 0)."'";
				$xbt_update[]="peers_limit='".(isset($u_update['r']['peers_limit']) ? $u_update['r']['peers_limit'] : 0)."'";
				$xbt_update[]="torrents_limit='".(isset($u_update['r']['torrents_limit']) ? $u_update['r']['torrents_limit'] : 0)."'";
				$sql='UPDATE '.XBT_USERS." SET ".implode(', ', $xbt_update)." WHERE uid='{$u_update['user_id']}'";
				my_sql_query($sql, $c);

				$u_cron[]="user_trestricts='".my_sql_real_escape_string(serialize($u_update['r']), $c)."'";
				$user_tracker_data[5]=$dt;
			}
			if(count($u_cron))
			{
				$u_cron[]="user_tracker_data='".implode(' ', $user_tracker_data)."'";
				$sql='UPDATE '.USERS_TABLE." SET ".implode(', ', $u_cron)." WHERE user_id='{$u_update['user_id']}'";
				my_sql_query($sql, $c);
			}
		}

		$sql='DELETE FROM '.TRACKER_CRON_TABLE." WHERE id='{$row['id']}'";
		my_sql_query($sql, $c);

	}
	my_sql_free_result($result0);

	if($config['ppkbb_cron_options'][0] && $dt - $config['ppkbb_cron_last_cleanup'] > $config['ppkbb_cron_options'][1])
	{
		$valid_cron_time=$dt-$config['ppkbb_cron_options'][0];

		$sql='DELETE FROM '.TRACKER_CRON_TABLE." WHERE added < {$valid_cron_time}";
		my_sql_query($sql, $c);

		my_set_config('ppkbb_cron_last_cleanup', $dt, true);
		//purge_tracker_config(true);
	}
}
else
{

}

if($c)
{
	my_sql_close($c);
}

exit();

//##############################################################################
//if(!function_exists('hex2bin'))
//{
	function myhex2bin($str)
	{
		$bin = "";
		$i = 0;
		do
		{
			$bin .= @chr(hexdec($str{$i}.$str{($i + 1)}));
			$i += 2;
		} while ($i < strlen($str));
		return $bin;
	}
//}

function my_split_config($config, $count=0, $type=false, $split='')
{
	$count=intval($count);

	if(!$count && $config==='')
	{
		return array();
	}

	$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
	$count=$count > 0 ? $count : count($s_config);
	if($count)
	{
		for($i=0;$i<$count;$i++)
		{
			if($type)
			{
				if(is_array($type) && @function_exists(@$type[$i]))
				{
					$s_config[$i]=call_user_func($type[$i], @$s_config[$i]);
				}
				else if(@function_exists($type))
				{
					$s_config[$i]=call_user_func($type, @$s_config[$i]);
				}
				else
				{
					$s_config[$i]=@$s_config[$i];
				}
			}
			else
			{
				$s_config[$i]=@$s_config[$i];
			}
		}
	}

	return $s_config;
}

function my_int_val($v=0, $max=0, $drop=false, $negative=false)
{
	if(!$v || ($v < 0 && !$negative))
	{
		return 0;
	}
	else if($drop && $v>$max)
	{
		return 0;
	}
	else if($max && $v>$max)
	{
		return $max;
	}

	return @number_format($v+0, 0, '', '');
}

function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
{
	if(!$v || ($v < 0 && !$negative))
	{
		return "0.".str_repeat('0', $n);
	}
	else if($drop && $v>$max)
	{
		return "0.".str_repeat('0', $n);
	}
	else if($max && $v>$max)
	{
		return $max;
	}

	return @number_format($v+0, $n, '.', '');
}

function t_getcache($t, $var='')
{
	global $tcachedir, $phpEx;

	$cache_data=array();

	$f_name="{$tcachedir}data_ppkbb3cker_{$t}.{$phpEx}";
	if(@file_exists($f_name))
	{
		include($f_name);

		return $var ? $$var : $cache_data;
	}

	return false;
}

function err()
{
	global $c;

	if($c)
	{
		my_sql_close($c);
	}

	exit();
}

//From includes/functions.php
function my_set_config($config_name, $config_value, $is_dynamic = false, $is_tracker=false)
{
	global $c, $config;

	$is_tracker=false;

	//if(!$is_tracker)
	//{
		$sql = 'UPDATE ' . CONFIG_TABLE . "
			SET config_value = '" . my_sql_real_escape_string($config_value, $c) . "', is_dynamic='".($is_dynamic ? 1 : 0)."'
			WHERE config_name = '" . my_sql_real_escape_string($config_name, $c) . "'";
		$result=my_sql_query($sql, $c);
	/*}
	else
	{
		$sql = 'UPDATE ' . TRACKER_CONFIG_TABLE . "
			SET config_value = '" . my_sql_real_escape_string($config_value, $c) . "'
			WHERE config_name = '" . my_sql_real_escape_string($config_name, $c) . "'";
		$result=my_sql_query($sql, $c);
	}*/

	/*if (!my_sql_affected_rows($c) && !isset($config[$config_name]))
	{
		$sql = 'INSERT INTO ' . CONFIG_TABLE . " (config_name, config_value, is_dynamic) VALUES ('".my_sql_real_escape_string($config_name, $c)."', '".my_sql_real_escape_string($config_value, $c)."', '".($is_dynamic ? 1 : 0)."')";
		my_sql_query($sql, $c);
	}*/

}

function get_forb_rtrack()
{
	global $tincludedir, $phpEx, $c;

	$forb_rtracks=array();

	$forb_rtracks=t_getcache('forb_rtrack');
	if($forb_rtracks===false)
	{
		$forb_rtracks=array();

		$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_forb rtrack_forb, rt.forb_type FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND rt.user_torrent_zone='0' AND rt.rtracker_remote!='0' AND rt.rtracker_type='s' AND rt.rtracker_forb!='0'";
		$result=my_sql_query($sql, $c);
		$forb_row=my_sql_fetch_array($result);
		foreach($forb_row as $row)
		{
			$forb_rtracks[]=$row;
		}
		my_sql_free_result($result);

		include_once("{$tincludedir}tcache.{$phpEx}");

		t_recache('forb_rtrack', $forb_rtracks);
	}

	unset($forb_rtracks['forb_rtrack_cachetime']);

	return $forb_rtracks;
}

?>
