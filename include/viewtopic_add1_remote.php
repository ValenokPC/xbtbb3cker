<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*/

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

global $db, $config, $template, $user;

$forb_rtracks=$this->xbt_functions->get_forb_rtrack();

$sql='SELECT COUNT(*) vt_count FROM '.TRACKER_RTRACK_TABLE." rt WHERE rt.rtracker_enabled='1' AND
(
	(rt.rtracker_remote='1' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')
		OR
	(rt.user_torrent_zone='{$torrent}' AND rt.rtracker_type='t')
)";//; LEFT JOIN '.TRACKER_RANNOUNCES_TABLE." ra ON (rt.rtracker_id=ra.tracker AND ra.torrent='{$torrent}')
$result=$db->sql_query($sql);
$vt_count=$db->sql_fetchfield('vt_count');
$db->sql_freeresult($result);

$torrent_rtrackers=array();
$sql='SELECT rs.id, rs.rtracker_url rtrack_url FROM '.TRACKER_RTRACK_TABLE.' rt, '.TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND
(
	(rt.rtracker_remote='1' AND rt.user_torrent_zone='0' AND rt.rtracker_type='s')
		OR
	(rt.user_torrent_zone='{$torrent}' AND rt.rtracker_type='t')
)".($mua_limit ? " LIMIT {$mua_limit}" : '');// ORDER BY $sql_addon";
$result=$db->sql_query($sql);
while($row_remote=$db->sql_fetchrow($result))
{
	$torrent_rtrackers[$row_remote['id']]=$row_remote;
}
$db->sql_freeresult($result);

if($torrent_rtrackers)
{
	$sql='SELECT ra.tracker, ra.next_announce, ra.a_message, ra.s_message, ra.a_interval, ra.err_count, ra.seeders, ra.leechers, ra.seeders+ra.leechers peers, ra.times_completed, ra.locked FROM '.TRACKER_RANNOUNCES_TABLE." ra WHERE ra.torrent={$torrent} AND ".$db->sql_in_set('ra.tracker', array_keys($torrent_rtrackers));// ORDER BY $sql_addon";
	$result=$db->sql_query($sql);
	while($row_remote=$db->sql_fetchrow($result))
	{
		$torrent_rtrackers[$row_remote['tracker']]+=$row_remote;
	}
	$db->sql_freeresult($result);

	foreach($torrent_rtrackers as $userlist)
	{
		$rtrack_forb=0;
		if(count($forb_rtracks))
		{
			foreach($forb_rtracks as $f)
			{
				if(in_array($f['rtrack_forb'], array(1, 3)))
				{
					if($f['forb_type']=='s' && strstr($userlist['rtrack_url'], $f['rtrack_url']))
					{
						$rtrack_forb=1;
					}
					else if($f['forb_type']=='i' && stristr($userlist['rtrack_url'], $f['rtrack_url']))
					{
						$rtrack_forb=1;
					}
					else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $userlist['rtrack_url']))
					{
						$rtrack_forb=1;
					}
				}
			}
		}
		if(!$rtrack_forb)
		{
			$i+=1;

			$is_admod ? $rtrack_url=$userlist['rtrack_url'] : $rtrack_url=parse_url($userlist['rtrack_url']);
			$assign_vars[$i]['TORRENT_RTRACKURL'] = htmlspecialchars($is_admod ? $rtrack_url : $rtrack_url['scheme'].'://'.$rtrack_url['host'].(isset($rtrack_url['port']) ? ':'.$rtrack_url['port'] : ''));
			if(isset($userlist['locked']))
			{
				$assign_vars[$i]['TORRENT_NANNOUNCE'] = $userlist['locked'] ? $user->lang['IN_ANNOUNCE'] : $td->spelldelta($dt, $userlist['next_announce']);
				$assign_vars[$i]['TORRENT_SNANNOUNCE'] = $userlist['locked'] ? 0 : $userlist['next_announce']-$dt;

				$assign_vars[$i]['TORRENT_AMESSAGE'] = htmlspecialchars($userlist['err_count'] && !$userlist['a_message'] ? $userlist['s_message'] : $userlist['a_message']);

				$assign_vars[$i]['TORRENT_AINTERVAL'] = $userlist['a_interval'] ? $userlist['a_interval'].$user->lang['TSEC'] : '';
				$assign_vars[$i]['TORRENT_SAINTERVAL'] = $userlist['a_interval'];

				$assign_vars[$i]['TORRENT_ERRCOUNT'] = $userlist['err_count'];

				$assign_vars[$i]['TORRENT_SEEDERS'] = $userlist['seeders'];

				$assign_vars[$i]['TORRENT_LEECHERS'] = $userlist['leechers'];

				$assign_vars[$i]['TORRENT_TCOMPLETED'] = $userlist['times_completed'];

				$assign_vars[$i]['TORRENT_PEERS'] = $userlist['peers'];
			}
		}
	}
}
?>
