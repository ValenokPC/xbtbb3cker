<?php

/**
 *
 * @package xbtBB3cker
 * @copyright (c) 2015 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\xbtbb3cker\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class xbtbb3cker_candc_module
{
	public $u_action;

	function main($id, $mode)
	{
		global $cache, $config, $db, $user, $auth, $template, $request, $table_prefix;
		global $phpbb_root_path, $phpEx, $phpbb_admin_path, $phpbb_container;

		$user->add_lang_ext('ppk/xbtbb3cker', 'acp_xbtbb3cker_candc');

		$this->page_title = $user->lang('ACP_XBTBB3CKER_CANDC');
		$this->tpl_name = 'acp_xbtbb3cker_candc';

		$submit = (isset($_POST['submit'])) ? true : false;
		$form_key = 'info_acp_xbtbb3cker_candc';
		add_form_key($form_key);

		$display_vars = array(
			'title'	=> 'ACP_XBTBB3CKER_CANDC',
			'vars'	=> array(
				'legend1'			=> '',
				'ppkbb_cron_options'		=> array('lang' => 'TRACKER_CRON_OPTIONS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_tccron_jobs'		=> array('lang' => 'TRACKER_CRON_JOBS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_tcclean_place'		=> array('lang' => 'TRACKER_CLEAN_PLACE', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
// 				'ppkbb_dead_time'		=> array('lang' => 'TRACKER_DEAD_TIME', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_tccleanup_interval'		=> array('lang' => 'TRACKER_CLEANUP_INTERVAL', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),

				'ppkbb_logs_cleanup'		=> array('lang' => 'TRACKER_LOGS_CLEANUP', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_clear_logs'		=> array('lang' => 'PPKBB_CLEAR_LOGS',	'validate' => 'int:0',	'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true),
				'ppkbb_reset_ratio'		=> array('lang' => 'TRACKER_RESET_RATIO', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_clean_snatch'		=> array('lang' => 'TRACKER_CLEAN_SNATCH', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),

				'ppkbb_trclear_files'		=> array('lang' => 'TRACKER_CLEAR_FILES', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_trclear_torrents'		=> array('lang' => 'TRACKER_CLEAR_TORRENTS', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				//'ppkbb_trclean_polls'		=> array('lang' => 'TRACKER_CLEAN_POLLS', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				//'ppkbb_trclear_pollres'		=> array('lang' => 'TRACKER_CLEAR_POLLRES', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_trclear_rannounces'		=> array('lang' => 'TRACKER_CLEAR_RANNOUNCES', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_trclear_trtrack'		=> array('lang' => 'TRACKER_CLEAR_TRTRACK', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_trclear_urtrack'		=> array('lang' => 'TRACKER_CLEAR_URTRACK', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_trclear_cronjobs'		=> array('lang' => 'TRACKER_CLEAR_CRONJOBS', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_trclear_snatched'		=> array('lang' => 'PPKBB_TRCLEAR_SNATCHED', 'validate' => 'int:0', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_fixu_list'=> array('lang' => 'TRACKER_FIXU_LIST', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),

				'ppkbb_phpclear_peers'		=> array('lang' => 'PPKBB_PHPCLEAR_PEERS',	'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true),
				'ppkbb_trclear_unregtorr'		=> array('lang' => 'TRACKER_CLEAR_UNREGTORR', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),

				'legend2'					=> 'ACP_SUBMIT_CHANGES',
			),
		);

		if (isset($display_vars['lang']))
		{
			$user->add_lang($display_vars['lang']);
		}

		$this->new_config = $config;

		$sql="SELECT * FROM ".CONFIG_TABLE." WHERE config_name IN('".implode("', '", array_keys($display_vars['vars']))."')";
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			isset($this->new_config[$row['config_name']]) ? $this->new_config[$row['config_name']]=$row['config_value'] : '';
		}
		$db->sql_freeresult($result);

		$cfg_array = (isset($_REQUEST['config'])) ? utf8_normalize_nfc($request->variable('config', array('' => ''), true)) : $this->new_config;
		$error = array();

		// We validate the complete config if wished
		validate_config_vars($display_vars['vars'], $cfg_array, $error);


		if ($submit && !check_form_key($form_key))
		{
			$error[] = $user->lang['FORM_INVALID'];
		}
		// Do not write values if there is an error
		if (count($error))
		{
			$submit = false;
		}

		if($submit)
		{
			include($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/config_map.'.$phpEx);
		}

		$dt=time();

		// We go through the display_vars to make sure no one is trying to set variables he/she is not allowed to...
		foreach ($display_vars['vars'] as $config_name => $null)
		{

			if($submit)
			{
				if(isset($config_map[$config_name]) && $config_map[$config_name][0])
				{
					$config_value = $config_map[$config_name][0] > 1 ? $request->variable($config_name, array(0 => '')) : $request->variable($config_name, '');
					if(isset($config_map[$config_name]['field']))
					{
						$config_valuev = $config_map[$config_name][0] > 1 ? $request->variable($config_name.'v', array(0 => '')) : $request->variable($config_name.'v', '');
						foreach($config_map[$config_name]['field'] as $k=>$v)
						{
							$field_type=explode(':', $v);
							if($field_type[0]=='bytes')
							{
								if($config_map[$config_name][0] > 1)
								{
									$config_value[$k]=$this->get_size_value($config_valuev[$k], $config_value[$k]);
									strlen($config_value[$k]) > 20 ? $config_value[$k]=substr($config_value[$k], 0, 20) : '';
								}
								else
								{
									$config_value=$this->get_size_value($config_valuev, $config_value);
									strlen($config_value) > 20 ? $config_value=substr($config_value, 0, 20) : '';
								}
							}
							else if($field_type[0]=='time')
							{
								if($config_map[$config_name][0] > 1)
								{
									$config_value[$k]=$this->get_time_value($config_valuev[$k], $config_value[$k]);
									strlen($config_value[$k]) > 8 ? $config_value[$k]=substr($config_value[$k], 0, 8) : '';
								}
								else
								{
									$config_value=$this->get_time_value($config_valuev, $config_value);
									strlen($config_value) > 8 ? $config_value=substr($config_value, 0, 8) : '';
								}
							}
						}
					}
					$config_map[$config_name][0] > 1 ? $config_value = implode(($config_map[$config_name][2] ? $config_map[$config_name][2] : ' '), $config_value) : '';
					$cfg_array[$config_name]=$config_value;
				}

				if($config_name=='ppkbb_trclear_rannounces')
				{
					$ppkbb_trclear_rannounces = $cfg_array[$config_name];
					if($ppkbb_trclear_rannounces)
					{
						$sql="TRUNCATE TABLE ".TRACKER_RANNOUNCES_TABLE."";
						$db->sql_query($sql);

						$sql="UPDATE ".XBT_FILES." SET rem_seeders='0', rem_leechers='0', rem_times_completed='0', lastremote='0'";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_trclear_trtrack')
				{
					$ppkbb_trclear_trtrack = $cfg_array[$config_name];
					if($ppkbb_trclear_trtrack)
					{
						$sql="DELETE FROM ".TRACKER_RTRACK_TABLE." WHERE user_torrent_zone!='0' AND rtracker_type='t'";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_trclear_urtrack')
				{
					$ppkbb_trclear_urtrack = $cfg_array[$config_name];
					if($ppkbb_trclear_urtrack)
					{
						$sql="DELETE FROM ".TRACKER_RTRACK_TABLE." WHERE user_torrent_zone!='0' AND rtracker_type='u'";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_fixu_list')
				{
					$ppkbb_fixu_list = $cfg_array[$config_name];
					if($ppkbb_fixu_list)
					{
						$fix_u=array();
						$result=$db->sql_query("SELECT user_id FROM ".USERS_TABLE." u LEFT JOIN ".XBT_USERS." xu ON (u.user_id=xu.uid) WHERE u.user_type IN('".USER_NORMAL."', '".USER_FOUNDER."') AND xu.uid IS NULL");
						while($row=$db->sql_fetchrow($result))
						{
							$sql="INSERT INTO ".XBT_USERS." (uid, torrent_pass) VALUES('{$row['user_id']}', '')";
							$db->sql_query($sql);
						}
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_trclear_files')
				{
					$ppkbb_trclear_files = $cfg_array[$config_name];
					if($ppkbb_trclear_files)
					{
						$sql="TRUNCATE TABLE ".TRACKER_FILES_TABLE."";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_trclear_snatched')
				{
					$ppkbb_trclear_snatched = $cfg_array[$config_name];
					if($ppkbb_trclear_snatched==1)
					{
						$sql="TRUNCATE TABLE ".XBT_FILES_USERS."";
						$db->sql_query($sql);
					}
					else if($ppkbb_trclear_snatched==2)
					{
						$sql="DELETE FROM ".XBT_FILES_USERS." WHERE uploaded='0' AND downloaded='0'";
						$db->sql_query($sql);
					}
					else if($ppkbb_trclear_snatched==3)
					{
						$sql="DELETE FROM ".XBT_FILES_USERS." WHERE uploaded!='0' OR downloaded!='0'";
						$db->sql_query($sql);
					}

					$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;


				}
				else if($config_name=='ppkbb_trclear_torrents')
				{
					$ppkbb_trclear_torrents = $cfg_array[$config_name];
					if($ppkbb_trclear_torrents)
					{
						$sql="TRUNCATE TABLE ".XBT_FILES."";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_trclear_cronjobs')
				{
					$ppkbb_trclear_cronjobs = $cfg_array[$config_name];
					if($ppkbb_trclear_cronjobs)
					{
						$sql="TRUNCATE TABLE ".TRACKER_CRON_TABLE."";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_reset_ratio')
				{
					$ppkbb_reset_ratio = $cfg_array[$config_name];
					if($ppkbb_reset_ratio)
					{
						$sql="UPDATE ".XBT_USERS." SET uploaded='0', downloaded='0'";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_clear_logs')
				{
					$ppkbb_clear_logs = $cfg_array[$config_name];

					if($ppkbb_clear_logs=='all')
					{
						$sql="TRUNCATE TABLE ".XBT_ANNOUNCE_LOG."";
						$db->sql_query($sql);

						$sql="TRUNCATE TABLE ".XBT_SCRAPE_LOG."";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
					else if($ppkbb_clear_logs=='time')
					{
						$valid_logs_time=$dt-$config['ppkbb_logs_cleanup'][1];

						$sql="DELETE FROM ".XBT_ANNOUNCE_LOG." WHERE mtime < '{$valid_logs_time}'";
						$db->sql_query($sql);

						$sql="DELETE FROM ".XBT_SCRAPE_LOG." WHERE mtime < '{$valid_logs_time}'";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
				else if($config_name=='ppkbb_clean_snatch')
				{
					$ppkbb_clean_snatch = $cfg_array[$config_name];

					if($ppkbb_clean_snatch)
					{
						//no post, del user stat, torrent, torrent file, rannounces data, rtrack data, download stat
						$sql="SELECT tt.fid id FROM ".XBT_FILES." tt LEFT JOIN ".POSTS_TABLE." p ON (tt.post_msg_id=p.post_id) WHERE ISNULL(p.post_id)";
						$result=$db->sql_query($sql);
						$t_clean=array();
						while($row=$db->sql_fetchrow($result))
						{
							$t_clean[]=$row['id'];
						}
						$db->sql_freeresult($result);

						if(count($t_clean))
						{
							$t_clean=implode("', '", $t_clean);

							$sql="DELETE FROM ".XBT_FILES_USERS." WHERE fid IN('{$t_clean}')";
							$db->sql_query($sql);

							$sql="DELETE FROM ".XBT_FILES." WHERE fid IN('{$t_clean}')";
							$db->sql_query($sql);

							$sql="DELETE FROM ".TRACKER_FILES_TABLE." WHERE torrent IN('{$t_clean}')";
							$db->sql_query($sql);

							$sql="DELETE FROM ".TRACKER_RANNOUNCES_TABLE." WHERE torrent IN('{$t_clean}')";
							$db->sql_query($sql);

							$sql="DELETE FROM ".TRACKER_RTRACK_TABLE." WHERE user_torrent_zone IN('{$t_clean}')  AND rtracker_type='t'";
							$db->sql_query($sql);

							$sql="DELETE FROM ".TRACKER_DOWNLOADS_TABLE." WHERE attach_id IN('{$t_clean}')";
							$db->sql_query($sql);
						}

						//no torrent, del user stat
						$sql="SELECT s.fid id FROM ".XBT_FILES_USERS." s LEFT JOIN ".XBT_FILES." t ON (s.fid=t.fid) WHERE ISNULL(t.fid)";
						$result=$db->sql_query($sql);
						$t_clean2=array();
						while($row=$db->sql_fetchrow($result))
						{
							$t_clean2[]=$row['id'];
						}
						$db->sql_freeresult($result);

						if(count($t_clean2))
						{
							$t_clean2=implode("', '", $t_clean2);

							$sql="DELETE FROM ".XBT_FILES_USERS." WHERE fid IN('{$t_clean2}')";
							$db->sql_query($sql);
						}

						//no user, del user stat
						$sql="SELECT xfu.uid FROM ".XBT_FILES_USERS." xfu LEFT JOIN ".XBT_USERS." xu ON (xfu.uid=xu.uid) WHERE ISNULL(xu.uid)";
						$result=$db->sql_query($sql);
						$t_clean3=array();
						while($row=$db->sql_fetchrow($result))
						{
							$t_clean3[$row['uid']]=$row['uid'];
						}
						$db->sql_freeresult($result);

						if(count($t_clean3))
						{
							$t_clean3=implode("', '", $t_clean3);

							$sql="DELETE FROM ".XBT_FILES_USERS." WHERE uid IN('{$t_clean3}')";
							$db->sql_query($sql);
						}

						//no torrent, del download stat
						$sql="SELECT d.id FROM ".TRACKER_DOWNLOADS_TABLE." d LEFT JOIN ".XBT_FILES." tt ON (d.attach_id=tt.fid) WHERE ISNULL(tt.fid)";
						$result=$db->sql_query($sql);
						$t_clean5=array();
						while($row=$db->sql_fetchrow($result))
						{
							$t_clean5[]=$row['id'];
						}
						$db->sql_freeresult($result);

						if(count($t_clean5))
						{
							$t_clean5=implode("', '", $t_clean5);

							$sql="DELETE FROM ".TRACKER_DOWNLOADS_TABLE." WHERE id IN('{$t_clean5}')";
							$db->sql_query($sql);
						}

						//no torrent, del torrent files
						$sql="SELECT f.id FROM ".TRACKER_FILES_TABLE." f LEFT JOIN ".XBT_FILES." tt ON (f.id=tt.fid) WHERE ISNULL(tt.fid)";
						$result=$db->sql_query($sql);
						$t_clean6=array();
						while($row=$db->sql_fetchrow($result))
						{
							$t_clean6[]=$row['id'];
						}
						$db->sql_freeresult($result);

						if(count($t_clean6))
						{
							$t_clean6=implode("', '", $t_clean6);

							$sql="DELETE FROM ".TRACKER_FILES_TABLE." WHERE id IN('{$t_clean6}')";
							$db->sql_query($sql);
						}

						$db->sql_query('UPDATE '. XBT_FILES ." SET flags='1' WHERE info_hash=''");

						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=0;
					}
				}
// 				else if($config_name=='ppkbb_dead_time')
// 				{
// 					$ppkbb_dead_time = $cfg_array[$config_name];
// 					if($ppkbb_dead_time <= $config['ppkbb_xcannounce_interval'])
// 					{
// 						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=intval($config['ppkbb_xcannounce_interval']*1.25);
// 					}
// 				}
				else if($config_name=='ppkbb_tccleanup_interval')
				{
					$ppkbb_tccleanup_interval = $cfg_array[$config_name];
					if($ppkbb_tccleanup_interval <= $config['ppkbb_xcannounce_interval'])
					{
						$cfg_array[$config_name]=$config_value=$this->new_config[$config_name]=intval($config['ppkbb_xcannounce_interval']*1.5);
					}
				}
				else if($config_name=='ppkbb_phpclear_peers')
				{
					$ppkbb_phpclear_peers = $cfg_array[$config_name];
					if($ppkbb_phpclear_peers=='all')
					{
						$sql="TRUNCATE TABLE ".TRACKER_PEERS_TABLE."";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config['ppkbb_phpclear_peers']=0;
					}
					else if($ppkbb_phpclear_peers=='time')
					{
						$valid_peers_time=$dt-intval($config['ppkbb_xcannounce_interval']*1.25);

						$sql="DELETE FROM ".TRACKER_PEERS_TABLE." WHERE last_action < {$valid_peers_time}";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config['ppkbb_phpclear_peers']=0;
					}
				}
				else if($config_name=='ppkbb_trclear_unregtorr')
				{
					$ppkbb_trclear_unregtorr = $cfg_array[$config_name];
					if($ppkbb_trclear_unregtorr)
					{
						$sql="DELETE FROM ".XBT_FILES." WHERE fid='0' OR (forum_id='0' AND topic_id='0' AND post_msg_id='0')";
						$db->sql_query($sql);
						$cfg_array[$config_name]=$config_value=$this->new_config['ppkbb_trclear_unregtorr']=0;
					}
				}
			}

			if (!isset($cfg_array[$config_name]) || strpos($config_name, 'legend') !== false)
			{
				continue;
			}

			$this->new_config[$config_name] = $config_value = $cfg_array[$config_name];

			if ($submit)
			{
				//$config->set($config_name, $config_value);
				$config->set($config_name, $config_value);
			}
		}

		if ($submit)
		{
			$cache->destroy('_ppkbb3cker_cache');

			trigger_error($user->lang['CONFIG_UPDATED'] . adm_back_link($this->u_action));
		}

		$this->page_title = $display_vars['title'];

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['ACP_XBTBB3CKER_CANDC'],
			'L_TITLE_EXPLAIN'	=> $user->lang['ACP_XBTBB3CKER_CANDC_EXPLAIN'],

			'S_ERROR'			=> (count($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),
		));

		// Output relevant page
		foreach ($display_vars['vars'] as $config_key => $vars)
		{
			if (!is_array($vars) && strpos($config_key, 'legend') === false)
			{
				continue;
			}

			if (strpos($config_key, 'legend') !== false)
			{
				$template->assign_block_vars('options', array(
					'S_LEGEND'		=> true,
					'LEGEND'		=> (isset($user->lang[$vars])) ? $user->lang[$vars] : $vars)
				);

				continue;
			}

			$type = explode(':', $vars['type']);

			$l_explain = '';
			if ($vars['explain'] && isset($vars['lang_explain']))
			{
				$l_explain = (isset($user->lang[$vars['lang_explain']])) ? $user->lang[$vars['lang_explain']] : $vars['lang_explain'];
			}
			else if ($vars['explain'])
			{
				$l_explain = (isset($user->lang[$vars['lang'] . '_EXPLAIN'])) ? $user->lang[$vars['lang'] . '_EXPLAIN'] : '';
			}

			$content = build_cfg_template($type, $config_key, $this->new_config, $config_key, $vars);

			if (empty($content))
			{
				continue;
			}

			$template->assign_block_vars('options', array(
				'KEY'			=> $config_key,
				'TITLE'			=> (isset($user->lang[$vars['lang']])) ? $user->lang[$vars['lang']] : $vars['lang'],
				'S_EXPLAIN'		=> $vars['explain'],
				'TITLE_EXPLAIN'	=> $l_explain,
				'CONTENT'		=> $content,
				)
			);

			unset($display_vars['vars'][$config_key]);
		}
	}

	public function xbtbb3cker_config($value, $key)
	{
		global $user, $config, $phpbb_root_path, $phpEx;

		include($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/config_map.'.$phpEx);

		$new=array();
		$tpl='';
		$name = $config_key = $key;

		if(isset($config_map[$key]))
		{
			$map=$config_map[$key];
			if($map)
			{
				if($map[0])
				{

					$new[$config_key]=$this->my_split_config($value, $map[0], $map[1], $map[2]);
					for($i=0;$i<$map[0];$i++)
					{
						if(isset($map['field']))
						{
							if(isset($map['field'][$i]) && !empty($map['field'][$i]))
							{
								$field_type=explode(':', $map['field'][$i]);
								if(isset($field_type[0]))
								{
									$map[0] > 1 ? $tpl .= '<strong>'.$user->lang['ACP_CONFIG_OPTION'].' '.($i+1).':</strong>&nbsp;' : '';
									switch($field_type[0])
									{
										case 'text':
										case 'password':
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="'.($field_type[0]=='text' ? 'text' : 'password').'"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />';
										break;

										case 'textarea':
											$tpl .= '<textarea id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' cols="' . $field_type[1] . '"' : '') . ($field_type[2] ? ' rows="' . $field_type[2] . '"' : '') .' name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" />' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '</textarea>';
										break;

										case 'select':
										case 'multi':
											$field_count=count($field_type);
											$tpl.='<select id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'"'.($field_type[0]=='multi' ? ' multiple="multiple"' : '').'>';
											for($i2=0;$i2<$field_count;$i2++)
											{
												if($i2)
												{
													$field_value=explode('=', $field_type[$i2]);
													$tpl.='<option value="' . $field_value[0] . '"'.(($new[$config_key][$i]==$field_value[0]) || (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='') ? ' selected="selected"' : '').'>'.(isset($user->lang[strtoupper($key).'_'.$field_value[1]]) ? $user->lang[strtoupper($key).'_'.$field_value[1]] : (isset($user->lang[$field_value[1]]) ? $user->lang[$field_value[1]] : $field_value[1])).'</option>';
												}
											}
											$tpl.='</select>';
										break;

										case 'radio':
										case 'checkbox':
											$field_count=count($field_type);
											for($i2=0;$i2<$field_count;$i2++)
											{
												if($i2)
												{
													$field_value=explode('=', $field_type[$i2]);
													$tpl.='<label><input class="radio" type="'.($field_type[0]=='radio' ? 'radio' : 'checkbox').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . $field_value[0] . '"'.(($new[$config_key][$i]==$field_value[0]) || (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='') ? ' checked="checked"' : '').' /> '.(isset($user->lang[strtoupper($key).$field_value[1]]) ? $user->lang[strtoupper($key).'_'.$field_value[1]] : (isset($user->lang[$field_value[1]]) ? $user->lang[$field_value[1]] : $field_value[1])).'</label>';
												}
											}
										break;

										case 'function':

										break;

										case 'lang':
											$tpl.='<select id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'">';
											$tpl.=language_select($new[$config_key][$i]);
											$tpl.='</select>';
										break;

										case 'time':
											if(!class_exists('timedelta'))
											{
												$user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
												include_once($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$phpEx);
											}
											$td = new \timedelta();
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />&nbsp;<select name="' . $name . 'v'.($map[0] > 1 ? '['.$i.']' : '').'">'.$this->select_time_value('s').'</select>&nbsp;['.$td->spelldelta(0, $new[$config_key][$i]).']';
										break;

										case 'bytes':
										case 'speed':
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />&nbsp;<select name="' . $name . 'v'.($map[0] > 1 ? '['.$i.']' : '').'">'.$this->select_size_value('b', ($field_type[0]=='speed' ? true : false)).'</select>&nbsp;['.get_formatted_filesize($new[$config_key][$i], true, false, ($field_type[0]=='speed' ? true : false)).']';
										break;

										default:
											$tpl .= '<input id="' . $key . '['.$i.']" type="text"' . (($field_type[1]) ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />';
										break;
									}
									$tpl.=(isset($map['append'][$i]) && isset($user->lang[$map['append'][$i]]) ? '&nbsp;'.$user->lang[$map['append'][$i]] : '').'<br />';
								}
							}
						}
						else
						{
							$tpl .= '<strong>'.$user->lang['ACP_CONFIG_OPTION'].' '.($i+1).':</strong>&nbsp;';
							$tpl .= '<input id="' . $key . '['.$i.']" type="text" maxlength="255" name="' . $name . '['.$i.']" value="' . (isset($map['default']) && $map['default']!==false && $new[$config_key][$i]==='' ? $map['default'] : $new[$config_key][$i]) . '" />'.(isset($map['append'][$i]) && isset($user->lang[$map['append'][$i]]) ? '&nbsp;'.$user->lang[$map['append'][$i]] : '').'<br />';
						}
					}
				}
			}
			else
			{
				$tpl = '<span style="color:#FF0000;">Invalid map content ['.$config_key.']</span>';
			}
		}

		return $tpl;
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);
		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : count($s_config);

		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				$s_config[$i]=isset($s_config[$i]) ? $s_config[$i] : '';
				if($type)
				{
					$use_function=false;
					if(is_array($type) && isset($type[$i]) && !empty($type[$i]))
					{
						$use_function=$type[$i];
					}
					else if(is_string($type) && !empty($type))
					{
						$use_function=$type;
					}

					if($use_function)
					{
						$s_config[$i]=@function_exists($use_function) ? call_user_func($use_function, $s_config[$i]) : call_user_func(array($this, $use_function), $s_config[$i]);
					}
				}
			}
		}

		return $s_config;
	}

	public function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	public function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

	public function get_size_value($value='b', $size=0)
	{
		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		switch($value)
		{
			case 'b':
				return $size;
			break;

			case 'kb':
				return 1024*$size;
			break;

			case 'mb':
				return 1024*1024*$size;
			break;

			case 'gb':
				return 1024*1024*1024*$size;
			break;

			case 'tb':
				return 1024*1024*1024*1024*$size;
			break;

			case 'pb':
				return 1024*1024*1024*1024*1024*$size;
			break;

			case 'eb':
				return 1024*1024*1024*1024*1024*1024*$size;
			break;
		}

	}

	public function select_size_value($value='b', $speed=false, $override=true)
	{
		global $user;

		$values=array('b'=>'BYTES', 'kb'=>'KB', 'mb'=>'MB', 'gb'=>'GB', 'tb'=>'TB', 'pb'=>'PB', 'eb'=>'EB');

		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].($speed ? '/'.$user->lang['TSEC'] : '').'</option>';
		}

		return $form;
	}

	public function get_time_value($value='s', $time=0)
	{
		if(!in_array($value, array('s', 'm', 'h', 'd')))
		{
			$value='s';
		}

		switch($value)
		{
			case 's':
				return $time;
			break;

			case 'm':
				return 60*$time;
			break;

			case 'h':
				return 60*60*$time;
			break;

			case 'd':
				return 60*60*24*$time;
			break;
		}

	}

	public function select_time_value($value='s', $override=true)
	{
		global $user;

		$values=array('s'=>'SECONDS', 'm'=>'MINUTES', 'h'=>'HOURS', 'd'=>'DAYS');

		if(!in_array($value, array('s', 'm', 'h', 'd')))
		{
			$value='s';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].'</option>';
		}

		return $form;
	}
}

?>
