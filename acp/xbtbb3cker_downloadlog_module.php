<?php

/**
 *
 * @package xbtBB3cker
 * @copyright (c) 2015 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\xbtbb3cker\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class xbtbb3cker_downloadlog_module
{

	function main($id, $mode)
	{
		global $config, $request, $template, $user, $db, $table_prefix, $cache, $pagination, $phpbb_container, $auth, $phpEx, $phpbb_root_path;

		$user->add_lang_ext('ppk/xbtbb3cker', 'acp_xbtbb3cker_downloadlog');

		$this->page_title = $user->lang('ACP_XBTBB3CKER_DOWNLOADLOG');
		$this->tpl_name = 'acp_xbtbb3cker_downloadlog';

		$submit = (isset($_POST['submit'])) ? true : false;
		$form_key = 'info_acp_xbtbb3cker_downloadlog';
		add_form_key($form_key);

		$error = array();

		$pagination = $phpbb_container->get('pagination');

		$user->add_lang('mcp');

		// Set up general vars
		$action		= $request->variable('action', '');
		$forum_id	= $request->variable('f', 0);
		$topic_id	= $request->variable('t', 0);
		$start		= $request->variable('start', 0);
		$deletemark = $request->variable('delmarked', '');
		$deleteall	= $request->variable('delall', '');
		$marked		= $request->variable('mark', array(0));

		// Sort keys
		$sort_days	= $request->variable('st', 0);
		$sort_key	= $request->variable('sk', 't');
		$sort_dir	= $request->variable('sd', 'd');

		// Delete entries if requested and able
		if (($deletemark || $deleteall) && $auth->acl_get('a_clearlogs'))
		{
			if (confirm_box(true))
			{
				$where_sql = '';

				if ($deletemark && count($marked))
				{
					$sql_in = array();
					foreach ($marked as $mark)
					{
						$sql_in[] = $mark;
					}
					$where_sql = ' WHERE ' . $db->sql_in_set('id', $sql_in);
					unset($sql_in);
				}

				if ($where_sql || $deleteall)
				{
					$sql = 'DELETE FROM ' . TRACKER_DOWNLOADS_TABLE . "
						$where_sql";
					$db->sql_query($sql);

					add_log('admin', 'LOG_CLEAR_DOWNLOADS_' . strtoupper($mode));
				}
			}
			else
			{
				confirm_box(false, $user->lang['CONFIRM_OPERATION'], build_hidden_fields(array(
					'f'			=> $forum_id,
					'start'		=> $start,
					'delmarked'	=> $deletemark,
					'delall'	=> $deleteall,
					'mark'		=> $marked,
					'st'		=> $sort_days,
					'sk'		=> $sort_key,
					'sd'		=> $sort_dir,
					'i'			=> $id,
					'mode'		=> $mode,
					'action'	=> $action))
				);
			}
		}

		// Sorting
		$limit_days = array(
			0 => $user->lang['ALL_ENTRIES'],
			1 => $user->lang['1_DAY'],
			7 => $user->lang['7_DAYS'],
			14 => $user->lang['2_WEEKS'],
			30 => $user->lang['1_MONTH'],
			90 => $user->lang['3_MONTHS'],
			180 => $user->lang['6_MONTHS'],
			365 => $user->lang['1_YEAR'],
		);
		$sort_by_text = array(
			'u' => $user->lang['SORT_USERNAME'],
			't' => $user->lang['SORT_DATE'],
			'f' => $user->lang['SORT_FILENAME'],
			'i' => $user->lang['IP'],
			'p' => $user->lang['SORT_TTITLE'],
		);
		$sort_by_sql = array(
			'u' => 'u.username_clean',
			't' => 'd.dl_time',
			'f' => 'a.real_filename',
			'i' => 'd.dl_ip',
			'p'=>'p.post_subject',
		);

		$s_limit_days = $s_sort_key = $s_sort_dir = $u_sort_param = '';
		gen_sort_selects($limit_days, $sort_by_text, $sort_days, $sort_key, $sort_dir, $s_limit_days, $s_sort_key, $s_sort_dir, $u_sort_param);

		// Define where and sort sql for use in displaying logs
		$sql_where = ($sort_days) ? (time() - ($sort_days * 86400)) : 0;
		$sql_sort = $sort_by_sql[$sort_key] . ' ' . (($sort_dir == 'd') ? 'DESC' : 'ASC');

		// Grab log data
		$log_data = array();
		$log_count = 0;
		$sql="SELECT COUNT(*) log_count FROM ".TRACKER_DOWNLOADS_TABLE." d LEFT JOIN ".USERS_TABLE." u ON (d.downloader_id=u.user_id) LEFT JOIN ".ATTACHMENTS_TABLE." a ON (d.attach_id=a.attach_id) LEFT JOIN ".POSTS_TABLE." p ON (d.post_msg_id=p.post_id)".($sql_where ? ' WHERE d.dl_time > '.$sql_where : '')."";

		$result=$db->sql_query($sql);
		$log_count=$db->sql_fetchrow($result);
		$db->sql_freeresult($result);

		$log_count=intval($log_count['log_count']);
		if($log_count)
		{
			$sql="SELECT d.*, u.username, u.user_colour, a.real_filename, p.post_subject, p.topic_id, p.forum_id FROM ".TRACKER_DOWNLOADS_TABLE." d LEFT JOIN ".USERS_TABLE." u ON (d.downloader_id=u.user_id) LEFT JOIN ".ATTACHMENTS_TABLE." a ON (d.attach_id=a.attach_id) LEFT JOIN ".POSTS_TABLE." p ON (d.post_msg_id=p.post_id)".($sql_where ? ' WHERE d.dl_time > '.$sql_where : '')." ORDER BY $sql_sort LIMIT $start, {$config['topics_per_page']}";
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$log_data[]=$row;
			}
			$db->sql_freeresult($result);
		}

		$template->assign_vars(array(

			'S_ON_PAGE'		=> $pagination->get_on_page($config['topics_per_page'], $start),
			'TOTAL_LOGS'	=> $log_count ? sprintf($user->lang['TOTAL_LOGS'], $log_count) : false,

			'S_LIMIT_DAYS'	=> $s_limit_days,
			'S_SORT_KEY'	=> $s_sort_key,
			'S_SORT_DIR'	=> $s_sort_dir,
			'S_CLEARLOGS'	=> $auth->acl_get('a_clearlogs'),
			)
		);

		$pagination->generate_template_pagination($this->u_action . "&amp;$u_sort_param", 'pagination', 'start', $log_count, $config['topics_per_page'], $start);

		foreach ($log_data as $row)
		{
			$template->assign_block_vars('log', array(
				'USERNAME'			=> empty($row['username']) || $row['downloader_id']==1 ? $user->lang['TRACKER_ANONYMOUS'] : get_username_string('full', $row['downloader_id'], $row['username'], $row['user_colour'], $row['username']),

				'DATE'				=> $user->format_date($row['dl_time'], 'Y-m-d H:i:s'),
				'FILENAME'			=> urldecode($row['real_filename']),
				'TTITLE'			=> !empty($row['post_subject']) ? $row['post_subject'] : $user->lang['TORRENT_DELETED'],
				'URL'			=> !empty($row['post_subject']) ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", "f={$row['forum_id']}&amp;t={$row['topic_id']}&amp;p={$row['post_msg_id']}")."#p{$row['post_msg_id']}" : '',
				'FILEURL'			=> !empty($row['real_filename']) ? append_sid("{$phpbb_root_path}download/file.$phpEx", "id={$row['attach_id']}") : '',
				'IP'				=> $row['dl_ip'],

				'ID'				=> $row['id'],
				)
			);
		}

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['ACP_XBTBB3CKER_DOWNLOADLOG'],
			'L_TITLE_EXPLAIN'	=> $user->lang['ACP_XBTBB3CKER_DOWNLOADLOG_EXPLAIN'],

			'S_ERROR'			=> (count($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),

			'U_ACTION'       => $this->u_action,

		));
	}

	function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

}

?>