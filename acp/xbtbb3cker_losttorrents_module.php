<?php

/**
 *
 * @package xbtBB3cker
 * @copyright (c) 2015 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\xbtbb3cker\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class xbtbb3cker_losttorrents_module
{

	function main($id, $mode)
	{
		global $config, $request, $template, $user, $db, $table_prefix, $cache, $pagination, $phpbb_container, $auth, $phpEx, $phpbb_root_path;

		$user->add_lang_ext('ppk/xbtbb3cker', 'acp_xbtbb3cker_losttorrents');

		$this->page_title = $user->lang('ACP_XBTBB3CKER_LOSTTORRENTS');
		$this->tpl_name = 'acp_xbtbb3cker_losttorrents';

		$submit = (isset($_POST['submit'])) ? true : false;
		$form_key = 'info_acp_xbtbb3cker_losttorrents';
		add_form_key($form_key);

		$error = array();

		$pagination = $phpbb_container->get('pagination');

		$user->add_lang('mcp');

		// Set up general vars
		$action		= $request->variable('action', '');
		$forum_id	= $request->variable('f', 0);
		$topic_id	= $request->variable('t', 0);
		$start		= $request->variable('start', 0);
		$fixmarked = $request->variable('fixmarked', '');
		$fixall	= $request->variable('fixall', '');
		$deletemarked = $request->variable('deletemarked', '');
		$deleteall	= $request->variable('deleteall', '');
		$marked		= $request->variable('mark', array(0));
		$last_id=$request->variable('last_id', 0);

		// Sort keys
		$sort_days	= $request->variable('st', 0);
		$sort_key	= $request->variable('sk', 't');
		$sort_dir	= $request->variable('sd', 'd');

		$forum_trackers=array();
		$sql='SELECT forum_id FROM '.FORUMS_TABLE." WHERE forum_tracker='1'";
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			$forum_trackers[]=$row['forum_id'];
		}
		$db->sql_freeresult($result);

		// Fix entries if requested and able
		if (($fixmarked || $fixall || $deletemarked || $deleteall) && $auth->acl_get('a_clearlogs'))
		{
			if (confirm_box(true) || ($fixall && $last_id) || ($deleteall && $last_id))
			{
				$torrents_id = array();
				if (($fixmarked || $deletemarked) && count($marked))
				{

					foreach ($marked as $mark)
					{
						$torrents_id[] = intval($mark);
					}
				}

				if($fixall || $deleteall)
				{

					$sql='SELECT a.attach_id FROM '.POSTS_TABLE.' p, '.USERS_TABLE.' u, '.ATTACHMENTS_TABLE.' a LEFT JOIN '.XBT_FILES.' x ON(a.attach_id=x.fid) WHERE '.$db->sql_in_set('p.forum_id', $forum_trackers)." AND p.post_id=a.post_msg_id AND a.extension='torrent' AND a.in_message='0' AND p.poster_id=u.user_id AND x.fid IS NULL AND a.attach_id > $last_id ORDER BY a.attach_id LIMIT $start, {$config['topics_per_page']}";
					$result=$db->sql_query($sql);

					while($row=$db->sql_fetchrow($result))
					{
						$torrents_id[]=$row['attach_id'];
					}
					$db->sql_freeresult($result);
				}

				if($deletemarked || $deleteall)
				{
					if (!function_exists('delete_attachments'))
					{
						include_once($phpbb_root_path . 'includes/functions_admin.' . $phpEx);
					}

					if ($torrents_id || $deleteall)
					{
						if(!$torrents_id)
						{
							trigger_error(sprintf($user->lang['FIX_LOSTTORRENTS_FINISH'], $this->u_action));
						}

						delete_attachments('attach', $torrents_id);

						add_log('admin', 'LOG_DELETE_LOSTTORRENTS_' . strtoupper($mode));

						if($deletemarked)
						{
							trigger_error(sprintf($user->lang['FIX_LOSTTORRENTS_FINISH'], $this->u_action));
						}

						if($deleteall && $torrents_id)
						{
							$redirect_url=$this->u_action.'&amp;deleteall=1&amp;last_id=1';

							meta_refresh(3, $redirect_url);
						}

						trigger_error(($deleteall ? $user->lang['FIX_LOSTTORRENTS_WAIT'] : '').sprintf($user->lang['FIX_LOSTTORRENTS_RESULT'], '', $this->u_action));


					}
				}
				else
				{

					if ($torrents_id || $fixall)
					{
						if(!$torrents_id)
						{
							trigger_error(sprintf($user->lang['FIX_LOSTTORRENTS_FINISH'], $this->u_action));
						}

						$db->sql_query('DELETE FROM '.XBT_FILES.' WHERE '.$db->sql_in_set('fid', $torrents_id));
						$db->sql_query('DELETE FROM '.TRACKER_FILES_TABLE.' WHERE '.$db->sql_in_set('torrent', $torrents_id));
						$db->sql_query('DELETE FROM '.XBT_FILES_USERS.' WHERE '.$db->sql_in_set('fid', $torrents_id));
						$db->sql_query('DELETE FROM '.TRACKER_RANNOUNCES_TABLE.' WHERE '.$db->sql_in_set('torrent', $torrents_id));
						$db->sql_query('DELETE FROM '.TRACKER_RTRACK_TABLE.' WHERE '.$db->sql_in_set('user_torrent_zone', $torrents_id))." AND rtracker_type='t'";

						include_once("{$phpbb_root_path}ext/ppk/xbtbb3cker/include/recode_torrent.{$phpEx}");
						include_once("{$phpbb_root_path}ext/ppk/xbtbb3cker/include/bencoding.{$phpEx}");

						$torrent_status=$success_posts=$success_topics=array();
						$sql = 'SELECT a.physical_filename, a.real_filename, a.attach_id, a.topic_id, a.post_msg_id post_id, a.poster_id, a.topic_id, p.forum_id FROM ' . ATTACHMENTS_TABLE . ' a, '.POSTS_TABLE.' p WHERE a.post_msg_id=p.post_id AND '.$db->sql_in_set('a.attach_id', $torrents_id);
						$result=$db->sql_query($sql);

						$message='';
						while($row=$db->sql_fetchrow($result))
						{
							isset($torrent_status[$row['forum_id']]) ? '' : $torrent_status[$row['forum_id']] = ($auth->acl_get('u_canskiptcheck') && $auth->acl_get('f_canskiptcheck', $row['forum_id'])) ? $config['ppkbb_tcdef_statuses'][0] : $config['ppkbb_tcdef_statuses'][1];

							$fix_result=recode_torrent_data($row['physical_filename'], $row['attach_id'], $row['forum_id'], $row['topic_id'], $row['post_id'], $torrent_status[$row['forum_id']], array(), $row['poster_id']);
							$message.=($fix_result===true ? $user->lang['FIX_LOSTTORRENTS_SUCCESS'] : $user->lang['FIX_LOSTTORRENTS_ERROR'].' ('.$fix_result.')')
								.'<a href= "'.append_sid("{$phpbb_root_path}viewtopic.{$phpEx}", "p={$row['post_id']}").'#p'.$row['post_id'].'">'.urldecode($row['real_filename']).'</a><br />';

							if($fix_result===true)
							{
								$success_posts[]=$row['post_id'];
								$success_topics[]=$row['topic_id'];
							}

							$last_id=$row['attach_id'];
						}
						$db->sql_freeresult($result);

						if($success_posts)
						{
							$db->sql_query('UPDATE '.POSTS_TABLE." SET post_torrent='1' WHERE ".$db->sql_in_set('post_id', $success_posts));
							$db->sql_query('UPDATE '.TOPICS_TABLE." SET topic_torrent='1' WHERE ".$db->sql_in_set('topic_id', $success_topics));
						}

						add_log('admin', 'LOG_FIX_LOSTTORRENTS_' . strtoupper($mode));

						if($fixall && $message)
						{
							$redirect_url=$this->u_action.'&amp;fixall=1&amp;last_id='.$last_id;

							meta_refresh(5, $redirect_url);
						}

						trigger_error(($fixall ? $user->lang['FIX_LOSTTORRENTS_WAIT'] : '').sprintf($user->lang['FIX_LOSTTORRENTS_RESULT'], $message, $this->u_action));
					}
				}
			}
			else
			{
				confirm_box(false, $user->lang['CONFIRM_OPERATION'], build_hidden_fields(array(
					'f'			=> $forum_id,
					'start'		=> $start,
					'fixmarked'	=> $fixmarked,
					'fixall'	=> $fixall,
					'deletemarked'	=> $deletemarked,
					'deleteall'	=> $deleteall,
					'mark'		=> $marked,
					'st'		=> $sort_days,
					'sk'		=> $sort_key,
					'sd'		=> $sort_dir,
					'i'			=> $id,
					'mode'		=> $mode,
					'action'	=> $action))
				);
			}
		}

		// Sorting
		$limit_days = array(
			0 => $user->lang['ALL_ENTRIES'],
			1 => $user->lang['1_DAY'],
			7 => $user->lang['7_DAYS'],
			14 => $user->lang['2_WEEKS'],
			30 => $user->lang['1_MONTH'],
			90 => $user->lang['3_MONTHS'],
			180 => $user->lang['6_MONTHS'],
			365 => $user->lang['1_YEAR'],
		);
		$sort_by_text = array(
			'u' => $user->lang['SORT_USERNAME'],
			't' => $user->lang['SORT_DATE'],
			'f' => $user->lang['SORT_FILENAME'],
			'p' => $user->lang['SORT_TTITLE'],
		);
		$sort_by_sql = array(
			'u' => 'u.username_clean',
			't' => 'a.filetime',
			'f' => 'a.real_filename',
			'p'=>'p.post_subject',
		);

		$s_limit_days = $s_sort_key = $s_sort_dir = $u_sort_param = '';
		gen_sort_selects($limit_days, $sort_by_text, $sort_days, $sort_key, $sort_dir, $s_limit_days, $s_sort_key, $s_sort_dir, $u_sort_param);

		// Define where and sort sql for use in displaying logs
		$sql_where = ($sort_days) ? (time() - ($sort_days * 86400)) : 0;
		$sql_sort = $sort_by_sql[$sort_key] . ' ' . (($sort_dir == 'd') ? 'DESC' : 'ASC');

		// Grab log data
		$log_data = array();
		$log_count = 0;
		if($forum_trackers)
		{
			$sql='SELECT COUNT(*) log_count FROM '.POSTS_TABLE.' p, '.USERS_TABLE.' u, '.ATTACHMENTS_TABLE.' a LEFT JOIN '.XBT_FILES.' x ON(a.attach_id=x.fid) WHERE '.$db->sql_in_set('p.forum_id', $forum_trackers)." AND p.post_id=a.post_msg_id AND a.extension='torrent' AND a.in_message='0' AND p.poster_id=u.user_id AND x.fid IS NULL".($sql_where ? ' AND a.filetime > '.$sql_where : '')."";

			$result=$db->sql_query($sql);
			$log_count=$db->sql_fetchrow($result);
			$db->sql_freeresult($result);

			$log_count=intval($log_count['log_count']);
			if($log_count)
			{
				$sql='SELECT u.username, u.user_colour, a.real_filename, a.filetime, a.post_msg_id, a.attach_id, p.post_subject, p.topic_id, p.forum_id, p.poster_id FROM '.POSTS_TABLE.' p, '.USERS_TABLE.' u, '.ATTACHMENTS_TABLE.' a LEFT JOIN '.XBT_FILES.' x ON(a.attach_id=x.fid) WHERE '.$db->sql_in_set('p.forum_id', $forum_trackers)." AND p.post_id=a.post_msg_id AND a.extension='torrent' AND a.in_message='0' AND p.poster_id=u.user_id AND x.fid IS NULL".($sql_where ? ' AND a.filetime > '.$sql_where : '')." ORDER BY $sql_sort LIMIT $start, {$config['topics_per_page']}";
				$result=$db->sql_query($sql);
				while($row=$db->sql_fetchrow($result))
				{
					$log_data[]=$row;
				}
				$db->sql_freeresult($result);
			}
		}

		$template->assign_vars(array(

			'S_ON_PAGE'		=> $pagination->get_on_page($config['topics_per_page'], $start),
			'TOTAL_LOGS'	=> $log_count ? sprintf($user->lang['TOTAL_LOGS'], $log_count) : false,

			'S_LIMIT_DAYS'	=> $s_limit_days,
			'S_SORT_KEY'	=> $s_sort_key,
			'S_SORT_DIR'	=> $s_sort_dir,
			'S_CLEARLOGS'	=> $auth->acl_get('a_clearlogs'),
			)
		);

		$pagination->generate_template_pagination($this->u_action . "&amp;$u_sort_param", 'pagination', 'start', $log_count, $config['topics_per_page'], $start);

		foreach ($log_data as $row)
		{
			$template->assign_block_vars('log', array(
				'USERNAME'			=> empty($row['username']) || $row['poster_id']==1 ? $user->lang['TRACKER_ANONYMOUS'] : get_username_string('full', $row['poster_id'], $row['username'], $row['user_colour'], $row['username']),

				'DATE'				=> $user->format_date($row['filetime'], 'Y-m-d H:i:s'),
				'FILENAME'			=> urldecode($row['real_filename']),
				'TTITLE'			=> !empty($row['post_subject']) ? $row['post_subject'] : $user->lang['TORRENT_DELETED'],
				'URL'			=> !empty($row['post_subject']) ? append_sid("{$phpbb_root_path}viewtopic.$phpEx", "f={$row['forum_id']}&amp;t={$row['topic_id']}&amp;p={$row['post_msg_id']}")."#p{$row['post_msg_id']}" : '',
				'FILEURL'			=> !empty($row['real_filename']) ? append_sid("{$phpbb_root_path}download/file.$phpEx", "id={$row['attach_id']}") : '',

				'ID'				=> $row['attach_id'],
				)
			);
		}

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['ACP_XBTBB3CKER_LOSTTORRENTS'],
			'L_TITLE_EXPLAIN'	=> $user->lang['ACP_XBTBB3CKER_LOSTTORRENTS_EXPLAIN'],

			'S_ERROR'			=> (count($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),

			'U_ACTION'       => $this->u_action,

		));
	}

	function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

}

?>