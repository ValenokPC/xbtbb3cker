<?php

/**
 *
 * @package xbtBB3cker
 * @copyright (c) 2015 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\xbtbb3cker\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class xbtbb3cker_userdata_module
{

	function main($id, $mode)
	{
		global $config, $request, $template, $user, $db, $table_prefix, $cache, $pagination, $phpbb_container, $auth, $phpEx, $phpbb_root_path;

		$user->add_lang_ext('ppk/xbtbb3cker', 'acp_xbtbb3cker_userdata');

		$this->page_title = $user->lang('ACP_XBTBB3CKER_USERDATA');
		$this->tpl_name = 'acp_xbtbb3cker_userdata';

		$submit = (isset($_POST['submit'])) ? true : false;
		$form_key = 'info_acp_xbtbb3cker_userdata';
		add_form_key($form_key);

		$error = array();
		$username	= utf8_normalize_nfc($request->variable('username', '', true));
		$user_id	= $request->variable('u', 0);
		$action		= $request->variable('action', 'user_tracker');

		if ($submit && !check_form_key($form_key))
		{
			$error[] = $user->lang['FORM_INVALID'];
		}

		// Show user selection mask
		if (!$username && !$user_id)
		{
			$this->page_title = 'SELECT_USER';

			$template->assign_vars(array(
				'U_ACTION'			=> $this->u_action,
				'ANONYMOUS_USER_ID'	=> ANONYMOUS,

				'S_SELECT_USER'		=> true,
				'U_FIND_USERNAME'	=> append_sid("{$phpbb_root_path}memberlist.$phpEx", 'mode=searchuser&amp;form=select_user&amp;field=username&amp;select_single=true'),
			));

		}
		else
		{

			if (!$user_id)
			{
				$sql = 'SELECT user_id
					FROM ' . USERS_TABLE . "
					WHERE username_clean = '" . $db->sql_escape(utf8_clean_string($username)) . "'";
				$result = $db->sql_query($sql);
				$user_id = (int) $db->sql_fetchfield('user_id');
				$db->sql_freeresult($result);

				if (!$user_id)
				{
					trigger_error($user->lang['NO_USER'] . adm_back_link($this->u_action), E_USER_WARNING);
				}
			}

			// Generate content for all modes
			$sql = 'SELECT u.*, s.*, x.torrent_pass user_passkey, x.downloaded user_downloaded, x.uploaded user_uploaded
				FROM ' . USERS_TABLE . ' u
					LEFT JOIN ' . SESSIONS_TABLE . ' s ON (s.session_user_id = u.user_id)
					LEFT JOIN '.XBT_USERS.' x ON(u.user_id=x.uid)
				WHERE u.user_id = ' . $user_id . '
				ORDER BY s.session_time DESC';
			$result = $db->sql_query_limit($sql, 1);
			$user_row = $db->sql_fetchrow($result);
			$db->sql_freeresult($result);

			if (!$user_row)
			{
				trigger_error($user->lang['NO_USER'] . adm_back_link($this->u_action), E_USER_WARNING);
			}

			$s_form_options='';
			$form_options=array('user_tracker', 'user_torrents', 'user_rtrackurl');
			foreach($form_options as $v)
			{
				$s_form_options.='<option value="'.$v.'"'.($action==$v ? ' selected="selected"' : '').'>'.$user->lang['USERDATA_'.strtoupper($v)].'</option>';
			}
			!in_array($action, $form_options) ? $action='' : '';

			$template->assign_vars(array(
				'U_BACK'			=> $this->u_action,
				'U_ACTION_SELECT'		=> $this->u_action."&amp;u=$user_id",
				'U_ACTION'			=> $this->u_action . '&amp;u=' . $user_id,
				'S_FORM_OPTIONS'	=> $s_form_options,
				'MANAGED_USERNAME'	=> $user_row['username'],

				'S_'. strtoupper($action)=> true,
				)
			);

			switch($action)
			{
				case 'user_tracker':

					if(!class_exists('timedelta'))
					{
						$user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
						include_once($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$phpEx);

					}
					$td = new \timedelta();

					$data = array(
						'uploaded'			=> $this->my_int_val($request->variable('tr_up', $user_row['user_uploaded'])),
						'downloaded'			=> $this->my_int_val($request->variable('tr_down', $user_row['user_downloaded'])),
						'comments'			=> $this->my_int_val($request->variable('tr_comm', $user_row['user_comments'])),
						'torrents'			=> $this->my_int_val($request->variable('tr_torr', $user_row['user_torrents'])),
						'passkey'			=> $request->variable('tr_passkey', $user_row['user_passkey']),

					);

					$dt=time();

					if ($submit && !count($error))
					{
						$data['uploaded']			= $this->get_size_value($request->variable('tr_upv', 'b'), $data['uploaded']);
						$data['downloaded']			= $this->get_size_value($request->variable('tr_downv', 'b'), $data['downloaded']);
						if($request->variable('reset_ratio', 0))
						{
							$sql="UPDATE ".XBT_USERS." SET uploaded='0', downloaded='0' WHERE uid='{$user_id}'";
							$db->sql_query($sql);
							$data['uploaded']=$data['downloaded']=0;
						}
						if($request->variable('clear_snatch', 0))
						{
							//no post, del user stat, torrent, torrent file, rannounces data, rtrack data, download stat
							$sql="SELECT tt.fid id FROM ".XBT_FILES." tt LEFT JOIN ".POSTS_TABLE." p ON (tt.post_msg_id=p.post_id) WHERE tt.poster_id='{$user_id}' AND ISNULL(p.post_id)";
							$result=$db->sql_query($sql);
							$t_clean=array();
							while($row=$db->sql_fetchrow($result))
							{
								$t_clean[]=$row['id'];
							}
							$db->sql_freeresult($result);

							if(count($t_clean))
							{
								$t_clean=implode("', '", $t_clean);

								$sql="DELETE FROM ".XBT_FILES_USERS." WHERE fid IN('{$t_clean}')";
								$db->sql_query($sql);

								$sql="DELETE FROM ".XBT_FILES." WHERE fid IN('{$t_clean}')";
								$db->sql_query($sql);

								$sql="DELETE FROM ".TRACKER_FILES_TABLE." WHERE torrent IN('{$t_clean}')";
								$db->sql_query($sql);

								$sql="DELETE FROM ".TRACKER_RANNOUNCES_TABLE." WHERE torrent IN('{$t_clean}')";
								$db->sql_query($sql);

								$sql="DELETE FROM ".TRACKER_RTRACK_TABLE." WHERE user_torrent_zone IN('{$t_clean}')  AND rtracker_type='t'";
								$db->sql_query($sql);

								$sql="DELETE FROM ".TRACKER_DOWNLOADS_TABLE." WHERE attach_id IN('{$t_clean}')";
								$db->sql_query($sql);
							}

							//no torrent, del user stat
							$sql="SELECT s.fid id FROM ".XBT_FILES_USERS." s LEFT JOIN ".XBT_FILES." t ON (s.fid=t.fid) WHERE s.uid='{$user_id}' AND ISNULL(t.fid)";
							$result=$db->sql_query($sql);
							$t_clean2=array();
							while($row=$db->sql_fetchrow($result))
							{
								$t_clean2[]=$row['id'];
							}
							$db->sql_freeresult($result);

							if(count($t_clean2))
							{
								$t_clean2=implode("', '", $t_clean2);

								$sql="DELETE FROM ".XBT_FILES_USERS." WHERE fid IN('{$t_clean2}')";
								$db->sql_query($sql);
							}

							//no torrent, del download stat
							$sql="SELECT d.id FROM ".TRACKER_DOWNLOADS_TABLE." d LEFT JOIN ".XBT_FILES." tt ON (d.attach_id=tt.fid) WHERE d.downloader_id='{$user_id}' AND ISNULL(tt.fid)";
							$result=$db->sql_query($sql);
							$t_clean5=array();
							while($row=$db->sql_fetchrow($result))
							{
								$t_clean5[]=$row['id'];
							}
							$db->sql_freeresult($result);

							if(count($t_clean5))
							{
								$t_clean5=implode("', '", $t_clean5);

								$sql="DELETE FROM ".TRACKER_DOWNLOADS_TABLE." WHERE id IN('{$t_clean5}')";
								$db->sql_query($sql);
							}

						}
// 						if($request->variable('clear_peers', '')=='all')
// 						{
// 							$sql="DELETE FROM ".XBT_ANNOUNCE_LOG." WHERE uid='{$user_id}' AND {$dt} - mtime > '{$config['ppkbb_dead_time']}'";
// 							$db->sql_query($sql);
//
// 							$sql="DELETE FROM ".XBT_SCRAPE_LOG." WHERE uid='{$user_id}' AND {$dt} - mtime > '{$config['ppkbb_dead_time']}'";
// 							$db->sql_query($sql);
// 						}
// 						else if($request->variable('clear_peers', '')=='time')
// 						{
// 							$sql="DELETE FROM ".XBT_ANNOUNCE_LOG." WHERE uid='{$user_id}' AND {$dt} - mtime > '{$config['ppkbb_dead_time']}'";
// 							$db->sql_query($sql);
//
// 							$sql="DELETE FROM ".XBT_SCRAPE_LOG." WHERE uid='{$user_id}' AND {$dt} - mtime > '{$config['ppkbb_dead_time']}'";
// 							$db->sql_query($sql);
// 						}

						if ($request->variable('recreate_passkey', 0) && ($npk=$this->create_passkey($user_id)))
						{
							$data['passkey']=$npk;
						}

						$data['uploaded']			= substr($data['uploaded'], 0, 20);
						$data['downloaded']			= substr($data['downloaded'], 0, 20);

						$data['comments']			= substr($data['comments'], 0, 8);
						$data['torrents']			= substr($data['torrents'], 0, 8);
						$data['passkey']			= substr($data['passkey'], 0, 32);

						if($data['passkey'] && !$request->variable('recreate_passkey', 0))
						{
							$sql="SELECT uid FROM ".XBT_USERS." WHERE uid!='{$user_id}' AND torrent_pass='".$db->sql_escape($data['passkey'])."' LIMIT 1";
							$result=$db->sql_query($sql);
							$data2=$db->sql_fetchrow($result);
							$db->sql_freeresult($result);
							if($data2['username'])
							{
								$error[] = $user->lang['DUPLICATED_PASSKEY'];
							}
						}

						if (!count($error))
						{
							$sql_ary = array(
								'user_comments'	=> $data['comments'],
								'user_torrents'	=> $data['torrents'],
							);

							$sql = 'UPDATE ' . USERS_TABLE . '
								SET ' . $db->sql_build_array('UPDATE', $sql_ary) . "
								WHERE user_id = $user_id";
							$db->sql_query($sql);

							$sql_ary2 = array(
								'uploaded'			=> $data['uploaded'],
								'downloaded'			=> $data['downloaded'],
								'torrent_pass'	=> $data['passkey'],
							);

							$sql = 'UPDATE ' . XBT_USERS . '
								SET ' . $db->sql_build_array('UPDATE', $sql_ary2) . "
								WHERE uid = $user_id";
							$db->sql_query($sql);

							trigger_error($user->lang['USER_PREFS_UPDATED'] . adm_back_link($this->u_action . '&amp;u=' . $user_id));
						}

					}
					else if($error)
					{
						$errors = implode('<br />', $error);
						trigger_error($errors . adm_back_link($this->u_action . '&amp;u=' . $user_id));
					}

					$template->assign_vars(array(
						'TR_UP'		=> $data['uploaded'],
						'TR_DOWN'		=> $data['downloaded'],
						'TR_HUP'		=> get_formatted_filesize($data['uploaded']),
						'TR_HDOWN'		=> get_formatted_filesize($data['downloaded']),

						'TR_COMM'			=> $data['comments'],
						'TR_TORR'		=> $data['torrents'],
						'U_TR_COMM'			=> append_sid("{$phpbb_root_path}search.$phpEx", "author_id={$user->data['user_id']}&amp;sr=posts&amp;ot=1&amp;pt=c"),
						'U_TR_TORR'		=> append_sid("{$phpbb_root_path}search.$phpEx", "author_id={$user->data['user_id']}&amp;sr=topics&amp;ot=1&amp;pt=t"),
						'TR_PASSKEY'			=> $data['passkey'],
						'TR_RATIO'		=> $this->get_ratio_alias($this->get_ratio($data['uploaded'], $data['downloaded'], $config['ppkbb_tcratio_start'])),


						)
					);
				break;

				case 'user_torrents':

					$dt=time();

					$torrent_explain=false;

					$torrent_opt=$request->variable('opt', '');

					$torrents_info=array('torrent', 'finished', 'seed', 'leech', 'history', 'leave', 'downloads', 'bookmarks');
					in_array($torrent_opt, $torrents_info) ? '' : $torrent_opt='';

					$del=$request->variable('del', array(0=>''));
					if(count($del))
					{
						$del=array_map('intval', array_keys($del));
						$sql='';
						$sql_in=$db->sql_in_set('fid', $del);
						switch($torrent_opt)
						{
							case 'torrent':
								$sql='DELETE FROM '.XBT_FILES." WHERE {$sql_in} AND poster_id='{$user_id}'";
							break;

							case 'finished':
							case 'history':
							case 'leave':
								$sql='DELETE FROM '.XBT_FILES_USERS." WHERE {$sql_in} AND uid='{$user_id}'";
							break;

							case 'seed':
							case 'leech':
								$sql='DELETE FROM '.XBT_FILES_USERS." WHERE {$sql_in} AND uid='{$user_id}' AND active='1'";
							break;

							case 'downloads':
								$sql='DELETE FROM '.TRACKER_DOWNLOADS_TABLE.' WHERE '.$db->sql_in_set('id', $del)." AND downloader_id='{$user_id}'";
							break;

							case 'bookmarks':
								$sql='DELETE FROM '.TRACKER_BOOKMARKS_TABLE.' WHERE '.$db->sql_in_set('id', $del)." AND user_id='{$user_id}'";
							break;
						}

						if($sql)
						{
							$result=$db->sql_query($sql);
							if($db->sql_affectedrows($result))
							{
								trigger_error($user->lang['DEL_USER_TORRENTS_SUCCESS'] . adm_back_link($this->u_action . '&amp;u=' . $user_id."&amp;opt={$torrent_opt}&amp;action=user_torrents"));
							}
						}
					}

					$torrent_info=array();

					foreach($torrents_info as $k=>$v)
					{
						if($v)
						{
// 							if(!$config['ppkbb_torrent_statml'][$k])
// 							{
// 								$torrent_opt==$v && !$config['ppkbb_torrent_statml'][$k] ? $torrent_opt='' : '';
// 								continue;
// 							}
							$ucp_url=$this->u_action . '&amp;u=' . $user_id."&amp;opt={$v}&amp;action=user_torrents";
							$torrent_info[$v]='<a href="'.$ucp_url.'#torrent_stat">'.($torrent_opt==$v ? '<b>' : '').$user->lang['TORRENT_INFO_HEADER_'.strtoupper($v)].($torrent_opt==$v ? '</b>' : '').'</a>';
						}
					}

					$sort_opt=array(
						'downloads' => array('author', 'topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'dl_time'),
						'finished' => array('topic_title', 'uploaded', 'downloaded', 'active', 'ratio', 'mtime', 'announced', '`left`'),
						'history' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
						'leave' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio'),
						'leech' => array('topic_title', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
						'seed' => array('topic_title', 'uploaded', 'downloaded', 'ratio', 'mtime', 'announced', '`left`'),
						'torrent' => array('topic_title', 'uploaded', 'downloaded', 'ratio', '`left`', 'active', 'mtime', 'announced'),
						'bookmarks' => array('topic_title', 'uploaded', 'downloaded', '`left`', 'active', 'mtime', 'announced', 'ratio', 'add_date'),

					);

					if(!class_exists('timedelta'))
					{
						$user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
						include_once($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$phpEx);

					}
					$td = new \timedelta('D_MINUTES');

					$assign_vars=$postrow_headers=$postrow_footers=array();

					$i=$mua_count=0;

					if($torrent_opt)
					{
						$torrent_explain=true;
					}

					$mua_add1inc=$phpbb_root_path.'ext/ppk/xbtbb3cker/include/';

					$page=$request->variable('pg', 0);
					$mua_limit=($page ? $page-1 : 0)*$config['ppkbb_mua_countlist'].', '.$config['ppkbb_mua_countlist'];

					if($torrent_explain)
					{
						$ex_fid_ary=array_keys($auth->acl_getf('!f_read', true));
						switch($torrent_opt)
						{
							case 'leave':
								include($mua_add1inc.'mua_add1_leave.'.$phpEx);
								break;

							case 'history':
								include($mua_add1inc.'mua_add1_history.'.$phpEx);
								break;

							case 'finished':
								include($mua_add1inc.'mua_add1_finished.'.$phpEx);
								break;

							case 'seed':
								include($mua_add1inc.'mua_add1_seed.'.$phpEx);
								break;

							case 'leech':
								include($mua_add1inc.'mua_add1_leech.'.$phpEx);
								break;

							case 'torrent':
								include($mua_add1inc.'mua_add1_torrent.'.$phpEx);
								break;

							case 'downloads':
								include($mua_add1inc.'mua_add1_downloads.'.$phpEx);
								break;

							case 'bookmarks':
								include($mua_add1inc.'mua_add1_bookmarks.'.$phpEx);
								break;

						}

						$assigned_vars=count($assign_vars);

						if($torrent_explain && $mua_count > $i)
						{
							$mua_page=$this->build_muavt_page($mua_count, $page, $this->u_action . '&amp;u=' . $user_id."&amp;action=user_torrents&amp;opt={$torrent_opt}", 'torrent_stat', $config['ppkbb_mua_countlist']);
							$torrent_info[$torrent_opt].='&nbsp;'.$mua_page;
						}
						$mua_countlist=$this->get_muavt_countlist(min($i, $config['ppkbb_mua_countlist']));

						foreach($sort_opt[$torrent_opt] as $k => $v)
						{
							$v=strtoupper(str_replace('`', '', $v));
							$postrow_headers[]='<th class="my_th"><a class="my_ath" href="javascript:;">'.(isset($user->lang['TORRENT_INFO_HEADER_'.$v]) ? $user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a></th>';
							$postrow_footers[]='<a href="javascript:;" onclick="fnShowHide('.$k.');">'.(isset($user->lang['TORRENT_INFO_HEADER_'.$v]) ? $user->lang['TORRENT_INFO_HEADER_'.$v] : 'TORRENT_INFO_HEADER_'.$v).'</a>';
						}

						if($assign_vars)
						{
							foreach($assign_vars as $k => $v)
							{
								$template->assign_block_vars($torrent_opt.'_option', $v);
							}
						}

						$postrow_headers[]='<th class="my_th"><a class="my_ath" href="javascript:;">'.$user->lang['DELETE'].'</a></th>';
						$postrow_footers[]='<a href="javascript:;" onclick="fnShowHide('.(count($postrow_footers)).');">'.$user->lang['DELETE'].'</a>';

						if($postrow_headers)
						{
							foreach($postrow_headers as $k => $v)
							{
								$template->assign_block_vars('torrent_headers', array('VALUE' => $v));
							}
						}

						$template->assign_vars(array(
							'S_MUA_COUNTLIST_DEFAULT' => count($mua_countlist[0]) > 1 ? 5 : -1,
							'S_MUA_COUNTLIST_KEYS' => implode(', ', $mua_countlist[0]),
							'S_MUA_COUNTLIST_VALUES' => implode(', ', $mua_countlist[1]),

							'S_HAS_TORRENT_EXPLAIN'	=> $torrent_opt ? true : false,

							'S_HAS_TORRENT_EXPLAIN_'.strtoupper($torrent_opt)	=> $torrent_explain ? true : false,

							'S_TORRENT_HEADER' => $postrow_headers ? implode('', $postrow_headers) :false,
							'S_TORRENT_FOOTER' => $postrow_footers ? implode(' : ', $postrow_footers) :false,

						));
					}

					$xbtbb3cker_addons="{$phpbb_root_path}ext/ppk/xbtbb3cker/";

					$template->assign_vars(array(
						'TORRENT_INFO_OPT'	=> true,

						'S_IS_ADMOD'	=> true,

						'XBTBB3CKER_ADDONS'	=> $xbtbb3cker_addons,

						'TRACKER_ADDIT_CSS' => "{$phpbb_root_path}ext/ppk/xbtbb3cker/include/cssjs.{$phpEx}?type=css&amp;css_set=1".(!$config['ppkbb_cssjs_cache'] ? '&amp;no_cache=1' : ''),
						'TRACKER_ADDIT_JS' => "{$phpbb_root_path}ext/ppk/xbtbb3cker/include/cssjs.{$phpEx}?type=js&amp;js_set=1".(!$config['ppkbb_cssjs_cache'] ? '&amp;no_cache=1' : ''),

						'TORRENT_INFO_STAT_HEADERS' => count($torrent_info) ? implode(' : ', $torrent_info) : false,
						'S_DEL_USER_TORRENTS_WARN' => isset($user->lang['DEL_USER_TORRENTS_WARN_'.strtoupper($torrent_opt)]) ? $user->lang['DEL_USER_TORRENTS_WARN_'.strtoupper($torrent_opt)] : '',

						'S_HIDDEN_FIELD' => '<input type="hidden" name="opt" value="'.$torrent_opt.'" />',

						)
					);

				break;

				case 'user_rtrackurl':
				default:

					if ($submit)
					{
						if (!count($error))
						{
							$message='';
							$rtracker_id=$request->variable('rtrack_id', array(0=>''));

							if($request->variable('add_rtrack', 0) && $rtracker_id)
							{
								$forb_rtracks=$this->get_forb_rtrack();

								$rtracker_url=$request->variable('rtrack_url', array(0=>''), true);
								$rtracker_delete=$request->variable('rtrack_delete', array(0=>''));
								$rtracker_enabled=$request->variable('rtrack_enabled', array(0=>''));

								$exists_tracker=array();
								$sql="SELECT rs.id, rs.rtracker_md5, rt.rtracker_id rtrack FROM ".TRACKER_RTRACKERS_TABLE." rs LEFT JOIN ".TRACKER_RTRACK_TABLE." rt ON(rs.id=rt.rtracker_id AND rt.rtracker_type='u' AND rt.user_torrent_zone='{$user_id}') WHERE ".$db->sql_in_set('rs.rtracker_md5', array_map('md5', $rtracker_url));
								$result=$db->sql_query($sql);
								while($row=$db->sql_fetchrow($result))
								{
									$exists_tracker[$row['rtracker_md5']]=array('id'=>$row['id'], 'rtrack'=>$row['rtrack']);
								}
								$db->sql_freeresult($result);

								$user_trackers=0;
								if(count($rtracker_id)==1 && isset($rtracker_id[0]) && !$rtracker_id[0])
								{
									$sql="SELECT COUNT(*) user_trackers FROM ".TRACKER_RTRACK_TABLE." WHERE rtracker_type='u' and user_torrent_zone='{$user_id}'";
									$result=$db->sql_query($sql);
									$user_trackers=(int) $db->sql_fetchfield('user_trackers');
									$db->sql_freeresult($result);
								}

								$rtrack_count=1;
								$d_rtrack=array();
								foreach($rtracker_id as $k=>$v)
								{
									$rtrack_url=isset($rtracker_url[$k]) ? $rtracker_url[$k] : '';
									$rtrack_delete=isset($rtracker_delete[$k]) ? $rtracker_delete[$k] : '';
									$rtrack_enabled=isset($rtracker_enabled[$k]) ? $rtracker_enabled[$k] : '';
									$rtrack_id=$this->my_int_val($k);

									if($rtrack_url==='')
									{
										continue;
									}

									if($rtrack_delete)
									{
										$d_rtrack[]=$rtrack_id;

										$rtrack_count-=1;
									}
									else
									{
										$rtrack_url=utf8_normalize_nfc($rtrack_url);

										if((!preg_match('#^(https?|udp):\/\/(\w+|\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})#', $rtrack_url) || strlen($rtrack_url) > 255))
										{
											$message=$user->lang['INVALID_RTRACK_URL'].': '.htmlspecialchars($rtrack_url);
											trigger_error($message . adm_back_link($this->u_action . '&amp;u=' . $user_id.'&amp;add_new=1&amp;action=user_rtrackurl'));
										}

										$rtrack_forb=0;
										if(count($forb_rtracks))
										{
											foreach($forb_rtracks as $f)
											{
												if($f['forb_type']=='s' && strstr($rtrack_url, $f['rtrack_url']))
												{
													$rtrack_forb=1;
												}
												else if($f['forb_type']=='i' && stristr($rtrack_url, $f['rtrack_url']))
												{
													$rtrack_forb=1;
												}
												else if($f['forb_type']=='r' && preg_match("{$f['rtrack_url']}", $rtrack_url))
												{
													$rtrack_forb=1;
												}
											}
										}
										if($rtrack_forb)
										{
											$error=$user->lang['FORB_RTRACK_URL'].': '.htmlspecialchars($rtrack_url);
											trigger_error($error . adm_back_link($this->u_action . '&amp;u=' . $user_id));
										}
										if($rtrack_count > $config['ppkbb_rtrack_enable'][1] || $user_trackers+$rtrack_count > $config['ppkbb_rtrack_enable'][1])
										{
											$error=sprintf($user->lang['MAXUSERS_ANNOUNCES_LIMIT'], $config['ppkbb_rtrack_enable'][1]);
											trigger_error($error . adm_back_link($this->u_action . '&amp;u=' . $user_id.'&amp;action=user_rtrackurl'));
										}

										$rtrack_md5=md5($rtrack_url);
										if(isset($exists_tracker[$rtrack_md5]))
										{
											$rtrack_id=$exists_tracker[$rtrack_md5]['id'];

											$rtrack_new=false;
										}
										else
										{
											$sql="INSERT INTO ".TRACKER_RTRACKERS_TABLE."(rtracker_url, rtracker_md5) VALUES('".$db->sql_escape($rtrack_url)."', '{$rtrack_md5}')";
											$result=$db->sql_query($sql);
											$rtrack_id=$db->sql_nextid();

											$rtrack_new=true;
										}
										if($rtrack_new || !$exists_tracker[$rtrack_md5]['rtrack'])
										{
											$sql='INSERT INTO '.TRACKER_RTRACK_TABLE."
												(rtracker_id, rtracker_enabled, rtracker_type, user_torrent_zone)
													VALUES(
													'".$rtrack_id."',
													'".($rtrack_enabled ? 1 : 0)."',
													'u',
													'{$user_id}'
											)";
											$result=$db->sql_query($sql);
										}
										else
										{

											$sql='UPDATE '.TRACKER_RTRACK_TABLE." SET
												rtracker_enabled='".($rtrack_enabled ? 1 : 0)."'
												WHERE rtracker_id='".$rtrack_id."' AND rtracker_type='u' AND user_torrent_zone='{$user_id}'";
											$result=$db->sql_query($sql);

										}
										$rtrack_count+=1;
									}
								}

								if($d_rtrack)
								{
									$sql='DELETE FROM '.TRACKER_RTRACK_TABLE." WHERE rtracker_id IN('".implode("', '", $d_rtrack)."') AND user_torrent_zone='{$user_id}' AND rtracker_type='u'";
									$result=$db->sql_query($sql);
								}

								trigger_error($user->lang['USER_PREFS_UPDATED'] . adm_back_link($this->u_action . '&amp;u=' . $user_id.'&amp;action=user_rtrackurl'));
							}

							$message ? trigger_error($message.'<br /><br />'. $user->lang['USER_PREFS_UPDATED'] . adm_back_link($this->u_action . '&amp;u=' . $user_id.'&amp;action=user_rtrackurl')) : '';
						}
					}

					if($request->variable('add_new', ''))
					{
						$template->assign_block_vars('rtracks', array(
							'COUNT'	=> 0,
							'URL'	=> '',
							'ENABLED'	=> '',
							)
						);
						$template->assign_vars(array(
							'S_NEW_RTRACK'	=> true,
							'S_HIDDEN_FIELD' => '<input type="hidden" name="add_rtrack" value="1" />',
							)
						);
					}
					else
					{
						$sql_where="rt.user_torrent_zone='{$user_id}' AND rt.rtracker_type='u'";
						$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_enabled rtrack_enabled FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND {$sql_where}";
						$result=$db->sql_query($sql);
						while($row=$db->sql_fetchrow($result))
						{
							$template->assign_block_vars('rtracks', array(
								'COUNT'	=> $row['id'],
								'URL'	=> htmlspecialchars($row['rtrack_url']),
								'ENABLED'	=> $row['rtrack_enabled'] ? ' checked="checked"' : '',
								)
							);
						}
						$db->sql_freeresult($result);

						$template->assign_vars(array(
							'S_VIEW_RTRACK'	=> true,
							'S_HIDDEN_FIELD' => '<input type="hidden" name="add_rtrack" value="1" />',
							)
						);
					}

				break;

			}
		}

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['ACP_XBTBB3CKER_USERDATA'],
			'L_TITLE_EXPLAIN'	=> $user->lang['ACP_XBTBB3CKER_USERDATA_EXPLAIN'],

			'S_ERROR'			=> (count($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),

			'S_ACTION'		=> $action,

			//'U_ACTION'       => $this->u_action,

		));
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);
		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : count($s_config);

		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				$s_config[$i]=isset($s_config[$i]) ? $s_config[$i] : '';
				if($type)
				{
					$use_function=false;
					if(is_array($type) && isset($type[$i]) && !empty($type[$i]))
					{
						$use_function=$type[$i];
					}
					else if(is_string($type) && !empty($type))
					{
						$use_function=$type;
					}

					if($use_function)
					{
						$s_config[$i]=@function_exists($use_function) ? call_user_func($use_function, $s_config[$i]) : call_user_func(array($this, $use_function), $s_config[$i]);
					}
				}
			}
		}

		return $s_config;
	}

	public function get_size_value($value='b', $size=0)
	{
		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		switch($value)
		{
			case 'b':
				return $size;
			break;

			case 'kb':
				return 1024*$size;
			break;

			case 'mb':
				return 1024*1024*$size;
			break;

			case 'gb':
				return 1024*1024*1024*$size;
			break;

			case 'tb':
				return 1024*1024*1024*1024*$size;
			break;

			case 'pb':
				return 1024*1024*1024*1024*1024*$size;
			break;

			case 'eb':
				return 1024*1024*1024*1024*1024*1024*$size;
			break;
		}

	}

	public function create_passkey($user_id=false)
	{
		global $db, $user;

		$user_id===false ? $user_id=$user->data['user_id'] : '';
		$user_passkey=strtolower(gen_rand_string(8).gen_rand_string(8).gen_rand_string(8).gen_rand_string(8));

		/*$sql="SELECT uid, torrent_pass FROM ".XBT_USERS." WHERE uid='{$user_id}' LIMIT 1";
		$result=$db->sql_query($sql);
		if(!$db->sql_fetchrow($result))
		{
			$sql = 'INSERT INTO ' . XBT_USERS . "
				(uid, torrent_pass) VALUES('{$user_id}', '{$user_passkey}')";
			$result=$db->sql_query($sql);
		}
		else
		{*/
			$sql = 'UPDATE ' . XBT_USERS . "
				SET torrent_pass='{$user_passkey}' WHERE uid='{$user_id}'";
			$result=$db->sql_query($sql);
		//}

		if(!$result)
		{
			return false;
		}

		return $user_passkey;
	}

	public function get_ratio($up, $down, $skip=0)
	{

		if($skip && $down < $skip)
		{
			$ratio='None.';
		}
		else if(!$up && !$down)
		{
			$ratio='Inf.';
		}
		else if(!$up && $down)
		{
			$ratio='Leech.';
		}
		else if(!$down && $up)
		{
			$ratio='Seed.';
		}
		else
		{
			$ratio=number_format($up / $down, 3, '.', '');
		}

		return $ratio;
	}

	public function get_ratio_alias($ratio='')
	{
		global $user;

		if(in_array($ratio, array('Leech.', 'Seed.', 'Inf.', 'None.')) && isset($user->lang['USER_RATIOS'][$ratio]))
		{
			$ratio=$user->lang['USER_RATIOS'][$ratio];
		}

		return $ratio;
	}

	public function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	public function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

	public function build_muavt_page($end, $current, $url, $anchor, $per_page, $type='select')
	{
		if($type=='select')
		{
			$url=str_replace('&amp;', '&', $url);

			$select='<select name="pg" id="pg" onchange="window.location.href=\''.$url.'&pg=\' + this.options[this.selectedIndex].value + \'#'.$anchor.'\';">';

			for($i=0;$i*$per_page<$end;$i++)
			{
				$page=$i+1;
				$select.='<option value="'.$page.'"'.($page==$current || ($page==1 && !$current) ? ' selected="selected"' : '').'>'.$page.'</option>';
			}

			$select.='</select>';
		}
		else
		{
			$select='';
			for($i=0;$i*$per_page<$end;$i++)
			{
				$page=$i+1;
				$select.=' <a class="vt_pg" href="javascript:;" data-href="'.$url.'&amp;pg='.$page.'">'.($page==$current || ($page==1 && !$current) ? "<strong>{$page}</strong>" : $page).'</a> ';
			}
		}

		return $select;

	}

	public function get_muavt_countlist($count)
	{
		global $user;

		$mua_countlist_keys=$mua_countlist_values=array();

		$mua_countlist=array(5=>5, 10=>10, 25=>25, 50=>50, 75=>75, 100=>100, 250=>250, 500=>500, 750=>750, 1000=>1000, 5000=>5000, 10000=>10000);
		foreach($mua_countlist as $k => $v)
		{
			if($count > $k)
			{
				$mua_countlist_keys[]=$k;
				$mua_countlist_values[]=$v;
			}
		}

		$mua_countlist_keys[]=-1;
		$mua_countlist_values[]="'{$user->lang['MUA_ALL']}'";


		return array($mua_countlist_keys, $mua_countlist_values);
	}

	public function get_forb_rtrack()
	{
		global $db;

		$forb_rtracks=array();

		$sql='SELECT rs.id, rs.rtracker_url rtrack_url, rt.rtracker_forb rtrack_forb, rt.forb_type FROM '.TRACKER_RTRACK_TABLE." rt, ".TRACKER_RTRACKERS_TABLE." rs WHERE rt.rtracker_id=rs.id AND rt.rtracker_enabled='1' AND rt.user_torrent_zone='0' AND rt.rtracker_remote!='0' AND rt.rtracker_type='s' AND rt.rtracker_forb!='0'";
		$result=$db->sql_query($sql, 86400);
		while($row=$db->sql_fetchrow($result))
		{
			$forb_rtracks[]=$row;
		}
		$db->sql_freeresult($result);

		return $forb_rtracks;
	}
}

?>