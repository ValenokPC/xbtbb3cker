<?php

/**
 *
 * @package xbtBB3cker
 * @copyright (c) 2015 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\xbtbb3cker\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class xbtbb3cker_set_module
{
	public $u_action;
	public $gs_curr;
	public $view_curr;

	function main($id, $mode)
	{
		global $config, $request, $template, $user, $db, $table_prefix;

		$user->add_lang_ext('ppk/xbtbb3cker', 'acp_xbtbb3cker_set');

		$this->page_title = $user->lang('ACP_XBTBB3CKER_SET');
		$this->tpl_name = 'acp_xbtbb3cker_set';

		$submit = (isset($_POST['submit'])) ? true : false;
		$form_key = 'info_acp_xbtbb3cker_set';
		add_form_key($form_key);

		$gs_select=$request->variable('gs_select', '');
		$error = array();

		if($gs_select && isset($user->lang['GROUPSET'][strtoupper($gs_select)]))
		{
			//0 - only_trackers, 1 - exclude_chat, 2 - torrents_only, 3 - view_curr
			$gs_forum_options=array(
				'ftype' => array(0, 0, 0, 1),
				'qreply' => array(0, 0, 0, 1),
				'statuses' => array(1, 0, 1, 0),
			);

			$gs_name=$gs_action=$gs_addit='';

			$gs_value=$request->variable('gs_value', '');
			$gs_submit=$request->variable('gs_submit', 0);
			$gs_options=$request->variable('gs_options', '');
			in_array($gs_options, array('all', 'selected', 'not_selected')) ? '' : $gs_options='all';
			$gs_forums=$request->variable('gs_forums', array(0=>''));
			$view_curr=$request->variable('view_curr', 0);

			$sql_from=$gs_curr=$gs_curr_name=array();
			$sql_forums='';
			if($gs_submit)
			{
				if($gs_forum_options[$gs_select][0]==1)
				{
					$sql_from[]="forum_tracker='1'";
				}

				$gs_options=='all' ? $gs_forums=array() : '';
				if($gs_forums)
				{
					$gs_forums=array_map('intval', $gs_forums);
					$sql_forums="forum_id ".($gs_options=='selected' ? '' : 'NOT ')."IN('".implode("', '", $gs_forums)."')";
				}
				else if($gs_options!='all')
				{
					$sql_forums="forum_id=0";
				}
				$this->u_action.='&amp;gs_select='.$gs_select;

				if(isset($gs_forum_options[$gs_select][2]) && $gs_forum_options[$gs_select][2] && $sql_from)
				{
					$sql="SELECT forum_id FROM ".FORUMS_TABLE." WHERE ".implode(' AND ', $sql_from);
					$result=$db->sql_query($sql);
					$sql_from=array();
					while($row=$db->sql_fetchrow($result))
					{
						$sql_from[]=$row['forum_id'];
					}
					$db->sql_freeresult($result);
				}
			}

			switch($gs_select)
			{


				case 'ftype':
					if($view_curr)
					{
						$sql="SELECT forum_tracker, forum_id FROM ".FORUMS_TABLE." WHERE forum_type='" . FORUM_POST ."'";
						$result=$db->sql_query($sql);
						while($row=$db->sql_fetchrow($result))
						{
							$gs_curr[$row['forum_id']]=$row['forum_tracker'];
						}
						$db->sql_freeresult($result);

						$gs_curr_name=array(
							0 => $user->lang['GS_TORRENT_FTYPE_FORUM'],
							1 => $user->lang['GS_TORRENT_FTYPE_TRACKER'],

						);
					}
					$gs_name=$user->lang['GS_TORRENT_FTYPE'];
					$gs_action='<select name="gs_value"><option value="0">'.$user->lang['GS_TORRENT_FTYPE_FORUM'].'</option><option value="1">'.$user->lang['GS_TORRENT_FTYPE_TRACKER'].'</option></select>';
					$gs_addit='';

					if($gs_submit)
					{
						in_array($gs_value, array(0, 1, 2)) ? '' : $gs_value=0;
						$sql="UPDATE ".FORUMS_TABLE." SET forum_tracker='{$gs_value}'".
							($sql_from || $sql_forums ? ' WHERE ' : '').
								($sql_from ? implode(' AND ', $sql_from) : '').
									($sql_forums ? ($sql_from ? ' AND ' : '').$sql_forums : '');
						$result=$db->sql_query($sql);
					}
				break;

				case 'qreply':
					//SELECT * FROM  FORUMS_TABLE WHERE (forum_flags & FORUM_FLAG_QUICK_REPLY)=0
					//UPDATE phpbb_forums SET `forum_flags`=`forum_flags` | 64 where forum_id=3
					//PDATE phpbb_forums SET `forum_flags`=`forum_flags` &~ 64 where forum_id=3

					if($view_curr)
					{
						$sql="SELECT forum_flags, forum_id FROM ".FORUMS_TABLE." WHERE forum_type='" . FORUM_POST ."'";
						$result=$db->sql_query($sql);
						while($row=$db->sql_fetchrow($result))
						{
							$gs_curr[$row['forum_id']]=($row['forum_flags'] & FORUM_FLAG_QUICK_REPLY) ? 1 : 0;
						}
						$db->sql_freeresult($result);

						$gs_curr_name=array(
							0 => $user->lang['NO'],
							1 => $user->lang['YES'],
						);
					}
					$gs_name=$user->lang['GS_TORRENT_QUICKREPLY'];
					$gs_action='<label><input type="radio" name="gs_value" value="1" class="radio" /> - '.$user->lang['YES'].'</label><label><input type="radio" name="gs_value" value="0" class="radio" checked="checked" /> - '.$user->lang['NO'].'</label>';
					$gs_addit='';

					if($gs_submit)
					{
						in_array($gs_value, array(0, 1)) ? '' : $gs_value=0;
						$sql="UPDATE ".FORUMS_TABLE." SET forum_flags=forum_flags ".($gs_value ? '|' : '&~')." ".FORUM_FLAG_QUICK_REPLY.
							($sql_from || $sql_forums ? ' WHERE ' : '').
								($sql_from ? implode(' AND ', $sql_from) : '').
									($sql_forums ? ($sql_from ? ' AND ' : '').$sql_forums : '');
						$result=$db->sql_query($sql);
					}
				break;

				case 'statuses':
					$torrent_statuses=$this->get_torrent_statuses();
					$form_forb='';
					if(count($torrent_statuses['STATUS_REASON']))
					{
						ksort($torrent_statuses['STATUS_REASON']);
						$forb_sel=array();
						foreach($torrent_statuses['STATUS_REASON'] as $rk => $rv)
						{
							if($rk < 0 && $rk > -50 && !isset($forb_sel[-1]))
							{
								$form_forb.='<option disabled="disabled">'.$user->lang['STATUS_MREASON'].'</option>';
								$forb_sel[-1]=1;
							}
							if($rk > 0 && !isset($forb_sel[1]))
							{
								$form_forb.='<option disabled="disabled">'.$user->lang['STATUS_PREASON'].'</option>';
								$forb_sel[1]=1;
							}
							if($rk == 0 && !isset($forb_sel[0]))
							{
								$form_forb.='<option disabled="disabled">'.$user->lang['STATUS_UREASON'].'</option>';
								$forb_sel[0]=1;
							}
							$form_forb.='<option value="'.$rk.'">'.$rv.'</option>';
						}
					}

					$gs_name=$user->lang['GS_TORRENT_STATUS'];
					$gs_action='<select name="gs_value">'.$form_forb.'</select>';
					$gs_addit=$user->lang['GS_TORRENT_STATUS_EXPLAIN'];

					if($gs_submit)
					{
						$gs_value=intval($gs_value);
						isset($torrent_statuses['STATUS_REASON'][$gs_value]) ? '' : $gs_value=0;

						$sql='SELECT COUNT(*) torrents_count FROM '.XBT_FILES.
							($sql_from || $sql_forums ? ' WHERE ' : '').
								($sql_from ? " forum_id IN('".implode("', '", $sql_from)."')" : '').
									($sql_forums ? ($sql_from ? ' AND ' : '').$sql_forums : '');
						$result=$db->sql_query($sql);
						$torrents_count=$db->sql_fetchfield('torrents_count');
						$db->sql_freeresult($result);

						if($torrents_count)
						{
							$status_value=$gs_value;
							if(!$config['ppkbb_tstatus_salt'])
							{
								$config['ppkbb_tstatus_salt']=substr(sha1(strtolower(gen_rand_string(8))), 0, 8);
								$config->set('ppkbb_tstatus_salt', $config['ppkbb_tstatus_salt'], true);
							}
							$bin_salt=pack('H*', $config['ppkbb_tstatus_salt']);
							$max_torrents=1000;
							$pre_sql='SELECT x.fid, x.info_hash, t.torrent_status FROM '.XBT_FILES. ' x, '.TOPICS_TABLE .' t WHERE x.forum_id=t.forum_id'.
								($sql_from ? " AND x.forum_id IN('".implode("', '", $sql_from)."')" : '').
									($sql_forums ? ($sql_from ? ' AND ' : '').str_replace('forum_id', 'x.forum_id', $sql_forums) : '');
							for($i=1;$i<=$torrents_count;$i+=$max_torrents)
							{
								$sql=$pre_sql." ORDER BY fid LIMIT {$i}, {$max_torrents}";

								$result=$db->sql_query($sql);
								while($row=$db->sql_fetchrow($result))
								{
									$status_id=$row['torrent_status'];
									if(($status_value < 1 && $status_id > 0) || ($status_value  > 0 && $status_id < 1))
									{
										$salt_add=$status_value < 1 && $status_id > 0 ? false : true;

										$row['info_hash']=substr($row['info_hash'], 0, 20);
										$sql2='UPDATE '.XBT_FILES." SET info_hash='".$db->sql_escape(($salt_add ? $row['info_hash'].$bin_salt : $row['info_hash']))."' WHERE fid='{$row['fid']}'";

										$result2=$db->sql_query($sql2);

									}
								}
								$db->sql_freeresult($result);

							}
						}

						$sql="UPDATE ".TOPICS_TABLE." SET torrent_status='{$gs_value}', status_author='0', status_dt='0', status_reason=''".
							($sql_from || $sql_forums ? ' WHERE ' : '').
								($sql_from ? " forum_id IN('".implode("', '", $sql_from)."')" : '').
									($sql_forums ? ($sql_from ? ' AND ' : '').$sql_forums : '');
						$result=$db->sql_query($sql);
					}
				break;
			}

			$template->assign_vars(array(
				'S_VIEW_GSS'	=> true,
				'GS_FORUMS'	=> $this->select_gs_forums('gs_forums', $gs_forum_options[$gs_select][0], $gs_curr, $gs_curr_name, $view_curr),
				'GS_DESCR' => $user->lang['GROUPSET'][strtoupper($gs_select)],
				'GS_NAME' => $gs_name,
				'GS_ACTION' => $gs_action,
				'GS_ADDIT' => $gs_addit,
				'S_HIDDEN_FIELDS'=>'
					<input type="hidden" name="gs_submit" value="1" >
					<input type="hidden" name="gs_select" value="'.$gs_select.'" >
					',
				'GS_VIEW' => isset($gs_forum_options[$gs_select][3]) && $gs_forum_options[$gs_select][3] ? $this->u_action.="&amp;gs_select={$gs_select}&amp;view_curr=1" : false,
				)
			);

			if ($submit)
			{

				trigger_error($user->lang['CONFIG_UPDATED'] . adm_back_link($this->u_action));
			}
		}
		else
		{
			if(isset($user->lang['GROUPSET']) && count($user->lang['GROUPSET']))
			{
				foreach($user->lang['GROUPSET'] as $k=>$v)
				{
					$template->assign_block_vars('groupsets', array(
						'NAME'	=> $v,
						'DESCR'	=> isset($user->lang['GROUPSET_DESCR'][$k]) && $user->lang['GROUPSET_DESCR'][$k] ? $user->lang['GROUPSET_DESCR'][$k] : '',
						'URL'	=> $this->u_action.'&amp;gs_select='.strtolower($k),
						)
					);
				}
			}

			$template->assign_vars(array(
				'S_VIEW_GROUPSET'	=> true,
				)
			);
		}

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['ACP_XBTBB3CKER_SET'],
			'L_TITLE_EXPLAIN'	=> $user->lang['ACP_XBTBB3CKER_SET_EXPLAIN'],

			'S_ERROR'			=> (count($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),

			'U_ACTION'       => $this->u_action,

		));
	}


	function select_gs_forums($key, $only_trackers=1, $gs_curr=array(), $gs_curr_name=array(), $view_curr=0)
	{
		global $user;

		$forum_list = make_forum_select(false, false, true, true, true, false, true);

		// Build forum options
		$s_forum_options = '<select id="' . $key . '" name="' . $key . '[]" multiple="multiple" size="8">';
		foreach ($forum_list as $f_id => $f_row)
		{
		$s_forum_options .= '<option value="' . $f_id . '"' . ($f_row['disabled'] ? ' disabled="disabled" class="disabled-option"' : '') . '>' . $f_row['padding'] . $f_row['forum_name'].($view_curr && isset($gs_curr[$f_id]) && isset($gs_curr_name[$gs_curr[$f_id]]) ? ' ('.$gs_curr_name[$gs_curr[$f_id]].')' : '') . '</option>';
		}
		$s_forum_options .= '</select>';

		return $s_forum_options;

	}

	function get_torrent_statuses()
	{
		global $user, $cache, $db;

		$torrent_statuses=array();

		$user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_statuses');

		$torrent_statuses=$cache->get("_ppkbb3cker_statuses_{$user->data['user_lang']}_cache");

		if(!$torrent_statuses)
		{
			$sql='SELECT * FROM ' . TRACKER_STATUSES_TABLE." WHERE status_enabled='1'";
			$result=$db->sql_query($sql/*, 3600*/);
			while($row=$db->sql_fetchrow($result))
			{
				$torrent_statuses['STATUS_REASON'][$row['status_id']]=isset($user->lang[$row['status_reason']]) ? $user->lang[$row['status_reason']] : $row['status_reason'];
				$torrent_statuses['STATUS_MARK'][$row['status_id']]=htmlspecialchars_decode(isset($user->lang[$row['status_mark']]) ? $user->lang[$row['status_mark']] : $row['status_mark']);
			}
			$db->sql_freeresult($result);

			$cache->put("_ppkbb3cker_statuses_{$user->data['user_lang']}_cache", $torrent_statuses);
		}

		return $torrent_statuses;
	}
}

?>