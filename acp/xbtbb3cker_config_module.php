<?php

/**
 *
 * @package xbtBB3cker
 * @copyright (c) 2015 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\xbtbb3cker\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class xbtbb3cker_config_module
{
	public $u_action;

	function main($id, $mode)
	{
		global $cache, $config, $db, $user, $auth, $template, $request, $table_prefix;
		global $phpbb_root_path, $phpEx, $phpbb_admin_path, $phpbb_container;

		$user->add_lang_ext('ppk/xbtbb3cker', 'acp_xbtbb3cker_config');

		$this->page_title = $user->lang('ACP_XBTBB3CKER_CONFIG');
		$this->tpl_name = 'acp_xbtbb3cker_config';

		$submit = (isset($_POST['submit'])) ? true : false;
		$form_key = 'info_acp_xbtbb3cker_config';
		add_form_key($form_key);

		$display_vars = array(
			'title'	=> 'ACP_XBTBB3CKER_CONFIG',
			'vars'	=> array(
				'legend1'			=> '',

				'ppkbb_tcratio_start'		=> array('lang' => 'TRACKER_RATIO_START', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_tprivate_flag'		=> array('lang' => 'PPKBB_TPRIVATE_FLAG', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_append_tfile'		=> array('lang' => 'PPKBB_APPEND_TFILE', 'validate' => 'string', 'type' => 'text:32:64', 'method' => false, 'explain' => true,),
				'ppkbb_display_trstat'		=> array('lang' => 'PPKBB_DISPLAY_TRSTAT', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_ipreg_countrestrict'	=> array('lang' => 'TRIPREG_COUNTRESTRICT', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_tracker_top'		=> array('lang' => 'TRACKER_TOP', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_torrent_downlink'		=> array('lang' => 'PPKBB_TORRENT_DOWNLINK', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_max_torrents'		=> array('lang' => 'PPKBB_MAX_TORRENTS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_trestricts_options'		=> array('lang' => 'PPKBB_TRESTRICTS_OPTIONS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),

				'legend2'				=> 'ACP_PPKBB_ADDONS',
				'ppkbb_disable_fpquote'		=> array('lang' => 'PPKBB_DISABLE_FPQUOTE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_torrent_statvt'		=> array('lang' => 'TRACKER_TORRENT_STATVT', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_torrent_statml'		=> array('lang' => 'TRACKER_TORRENT_STATML', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_torr_blocks'		=> array('lang' => 'PPKBB_TORR_BLOCKS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_noticedisclaimer_blocks'		=> array('lang' => 'PPKBB_NOTICEDISCLAIMER_BLOCKS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_search_astracker'		=> array('lang' => 'PPKBB_SEARCH_ASTRACKER', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_cssjs_cache'		=> array('lang' => 'PPKBB_CSSJS_CACHE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),
				'ppkbb_addit_options'		=> array('lang' => 'PPKBB_ADDIT_OPTIONS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_mua_countlist'		=> array('lang' => 'PPKBB_MUA_COUNTLIST', 'validate' => 'int:0', 'type' => 'text:4:4', 'method' => false, 'explain' => true,),
				'ppkbb_poll_options'		=> array('lang' => 'PPKBB_POLL_OPTIONS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_tstatus_notify'		=> array('lang' => 'TRACKER_TSTATUS_NOTIFY', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_tracker_bookmarks'		=> array('lang' => 'PPKBB_TRACKER_BOOKMARKS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),

				'legend3'				=> 'PPKBB_TOPDOWN_TORRENTS',
				'ppkbb_topdown_torrents'		=> array('lang' => 'PPKBB_TOPDOWN_TORRENTS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_topdown_torrents_options'		=> array('lang' => 'PPKBB_TOPDOWN_TORRENTS_OPTIONS', 'validate' => 'array', 'type' => 'custom', 'method' => 'xbtbb3cker_config', 'explain' => true,),
				'ppkbb_topdown_torrents_exclude'		=> array('lang' => 'PPKBB_TOPDOWN_TORRENTS_EXCLUDE', 'validate' => 'array', 'type' => 'custom', 'method' => 'select_tracker_forums', 'explain' => true,),
				'ppkbb_topdown_torrents_trueexclude'		=> array('lang' => 'PPKBB_TOPDOWN_TORRENTS_TRUEEXCLUDE', 'validate' => 'int:0', 'type' => 'radio:yes_no', 'method' => false, 'explain' => true,),

				'legend4'					=> 'ACP_SUBMIT_CHANGES',
			),
		);

		if (isset($display_vars['lang']))
		{
			$user->add_lang($display_vars['lang']);
		}

		$this->new_config = $config;

		$sql="SELECT * FROM ".CONFIG_TABLE." WHERE config_name IN('".implode("', '", array_keys($display_vars['vars']))."')";
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			isset($this->new_config[$row['config_name']]) ? $this->new_config[$row['config_name']]=$row['config_value'] : '';
		}
		$db->sql_freeresult($result);

		$cfg_array = (isset($_REQUEST['config'])) ? utf8_normalize_nfc($request->variable('config', array('' => ''), true)) : $this->new_config;
		$error = array();

		// We validate the complete config if wished
		validate_config_vars($display_vars['vars'], $cfg_array, $error);

		if ($submit && !check_form_key($form_key))
		{
			$error[] = $user->lang['FORM_INVALID'];
		}
		// Do not write values if there is an error
		if (count($error))
		{
			$submit = false;
		}

		if($submit)
		{
			include($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/config_map.'.$phpEx);
		}

		$dt=time();
		$tracker_arrays_value=array('ppkbb_topdown_torrents_exclude');
// 		$config_text_value=array('ppkbb_topdown_torrents_exclude', 'ppkbb_poll_options');
// 		$config_text = $phpbb_container->get('config_text');

		// We go through the display_vars to make sure no one is trying to set variables he/she is not allowed to...
		foreach ($display_vars['vars'] as $config_name => $null)
		{
			if($submit)
			{
				if(isset($config_map[$config_name]) && $config_map[$config_name][0])
				{
					$config_value = $config_map[$config_name][0] > 1 ? $request->variable($config_name, array(0 => '')) : $request->variable($config_name, '');

					if(isset($config_map[$config_name]['field']))
					{
						$config_valuev = $config_map[$config_name][0] > 1 ? $request->variable($config_name.'v', array(0 => '')) : $request->variable($config_name.'v', '');
						foreach($config_map[$config_name]['field'] as $k=>$v)
						{
							$field_type=explode(':', $v);
							if($field_type[0]=='bytes')
							{
								if($config_map[$config_name][0] > 1)
								{
									$config_value[$k]=$this->get_size_value($config_valuev[$k], $config_value[$k]);
									strlen($config_value[$k]) > 20 ? $config_value[$k]=substr($config_value[$k], 0, 20) : '';
								}
								else
								{
									$config_value=$this->get_size_value($config_valuev, $config_value);
									strlen($config_value) > 20 ? $config_value=substr($config_value, 0, 20) : '';
								}
							}
							else if($field_type[0]=='time')
							{
								if($config_map[$config_name][0] > 1)
								{
									$config_value[$k]=$this->get_time_value($config_valuev[$k], $config_value[$k]);
									strlen($config_value[$k]) > 8 ? $config_value[$k]=substr($config_value[$k], 0, 8) : '';
								}
								else
								{
									$config_value=$this->get_time_value($config_valuev, $config_value);
									strlen($config_value) > 8 ? $config_value=substr($config_value, 0, 8) : '';
								}
							}
						}
					}

					if($config_name=='ppkbb_trestricts_options')
					{
						$new_ppkbb_trestricts_options = $config_value;
						$ppkbb_trestricts_options = $this->my_split_config($config['ppkbb_trestricts_options'], $config_map[$config_name][0], $config_map[$config_name][1], $config_map[$config_name][2]);
						if($new_ppkbb_trestricts_options[1]!=$ppkbb_trestricts_options[1])
						{
							$sql="UPDATE ".XBT_USERS." SET can_leech='1', wait_time='0', peers_limit='0', torrents_limit='0'";
							$db->sql_query($sql);
						}
					}

					$config_map[$config_name][0] > 1 ? $config_value = implode(($config_map[$config_name][2] ? $config_map[$config_name][2] : ' '), $config_value) : '';
					$cfg_array[$config_name]=$config_value;
				}
				else if(in_array($config_name, $tracker_arrays_value))
				{
					$config_value = $request->variable($config_name, array(0 => ''));
					$config_value = implode(',', $config_value);
					$cfg_array[$config_name]=$config_value;
				}
			}

			if (!isset($cfg_array[$config_name]) || strpos($config_name, 'legend') !== false)
			{
				continue;
			}

			$this->new_config[$config_name] = $config_value = $cfg_array[$config_name];

			if ($submit)
			{
// 				if(in_array($config_name, $text_config_value))
// 				{
// 					$config_text->set($config_name, $config_value);
// 				}
// 				else
// 				{
					$config->set($config_name, $config_value);
// 				}
			}
		}

		if ($submit)
		{
			$cache->destroy('_ppkbb3cker_cache');

			trigger_error($user->lang['CONFIG_UPDATED'] . adm_back_link($this->u_action));
		}

		$this->page_title = $display_vars['title'];

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['ACP_XBTBB3CKER_CONFIG'],
			'L_TITLE_EXPLAIN'	=> $user->lang['ACP_XBTBB3CKER_CONFIG_EXPLAIN'],

			'S_ERROR'			=> (count($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),
		));

		// Output relevant page
		foreach ($display_vars['vars'] as $config_key => $vars)
		{
			if (!is_array($vars) && strpos($config_key, 'legend') === false)
			{
				continue;
			}

			if (strpos($config_key, 'legend') !== false)
			{
				$template->assign_block_vars('options', array(
					'S_LEGEND'		=> true,
					'LEGEND'		=> (isset($user->lang[$vars])) ? $user->lang[$vars] : $vars)
				);

				continue;
			}

			$type = explode(':', $vars['type']);

			$l_explain = '';
			if ($vars['explain'] && isset($vars['lang_explain']))
			{
				$l_explain = (isset($user->lang[$vars['lang_explain']])) ? $user->lang[$vars['lang_explain']] : $vars['lang_explain'];
			}
			else if ($vars['explain'])
			{
				$l_explain = (isset($user->lang[$vars['lang'] . '_EXPLAIN'])) ? $user->lang[$vars['lang'] . '_EXPLAIN'] : '';
			}

			$content = build_cfg_template($type, $config_key, $this->new_config, $config_key, $vars);

			if (empty($content))
			{
				continue;
			}

			$template->assign_block_vars('options', array(
				'KEY'			=> $config_key,
				'TITLE'			=> (isset($user->lang[$vars['lang']])) ? $user->lang[$vars['lang']] : $vars['lang'],
				'S_EXPLAIN'		=> $vars['explain'],
				'TITLE_EXPLAIN'	=> $l_explain,
				'CONTENT'		=> $content,
				)
			);

			unset($display_vars['vars'][$config_key]);
		}
	}

	public function xbtbb3cker_config($value, $key)
	{
		global $user, $config, $phpbb_root_path, $phpEx;

		include($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/config_map.'.$phpEx);

		$new=array();
		$tpl='';
		$name = $config_key = $key;

		if(isset($config_map[$key]))
		{
			$map=$config_map[$key];
			if($map)
			{
				if($map[0])
				{

					$new[$config_key]=$this->my_split_config($value, $map[0], $map[1], $map[2]);
					for($i=0;$i<$map[0];$i++)
					{
						if(isset($map['field']))
						{
							if(isset($map['field'][$i]) && !empty($map['field'][$i]))
							{
								$field_type=explode(':', $map['field'][$i]);
								if(isset($field_type[0]))
								{
									$map[0] > 1 ? $tpl .= '<strong>'.$user->lang['ACP_CONFIG_OPTION'].' '.($i+1).':</strong>&nbsp;' : '';
									switch($field_type[0])
									{
										case 'text':
										case 'password':
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="'.($field_type[0]=='text' ? 'text' : 'password').'"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />';
										break;

										case 'textarea':
											$tpl .= '<textarea id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' cols="' . $field_type[1] . '"' : '') . ($field_type[2] ? ' rows="' . $field_type[2] . '"' : '') .' name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" />' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '</textarea>';
										break;

										case 'select':
										case 'multi':
											$field_count=count($field_type);
											$tpl.='<select id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'"'.($field_type[0]=='multi' ? ' multiple="multiple"' : '').'>';
											for($i2=0;$i2<$field_count;$i2++)
											{
												if($i2)
												{
													$field_value=explode('=', $field_type[$i2]);
													$tpl.='<option value="' . $field_value[0] . '"'.(($new[$config_key][$i]==$field_value[0]) || (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='') ? ' selected="selected"' : '').'>'.(isset($user->lang[strtoupper($key).'_'.$field_value[1]]) ? $user->lang[strtoupper($key).'_'.$field_value[1]] : (isset($user->lang[$field_value[1]]) ? $user->lang[$field_value[1]] : $field_value[1])).'</option>';
												}
											}
											$tpl.='</select>';
										break;

										case 'radio':
										case 'checkbox':
											$field_count=count($field_type);
											for($i2=0;$i2<$field_count;$i2++)
											{
												if($i2)
												{
													$field_value=explode('=', $field_type[$i2]);
													$tpl.='<label><input class="radio" type="'.($field_type[0]=='radio' ? 'radio' : 'checkbox').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . $field_value[0] . '"'.(($new[$config_key][$i]==$field_value[0]) || (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='') ? ' checked="checked"' : '').' /> '.(isset($user->lang[strtoupper($key).$field_value[1]]) ? $user->lang[strtoupper($key).'_'.$field_value[1]] : (isset($user->lang[$field_value[1]]) ? $user->lang[$field_value[1]] : $field_value[1])).'</label>';
												}
											}
										break;

										case 'function':

										break;

										case 'lang':
											$tpl.='<select id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'">';
											$tpl.=language_select($new[$config_key][$i]);
											$tpl.='</select>';
										break;

										case 'time':
											if(!class_exists('timedelta'))
											{
												$user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
												include_once($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$phpEx);
											}
											$td = new \timedelta();
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />&nbsp;<select name="' . $name . 'v'.($map[0] > 1 ? '['.$i.']' : '').'">'.$this->select_time_value('s').'</select>&nbsp;['.$td->spelldelta(0, $new[$config_key][$i]).']';
										break;

										case 'bytes':
										case 'speed':
											$tpl .= '<input id="' . $key .($map[0] > 1 ?  '['.$i.']' : '').'" type="text"' . ($field_type[1] ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />&nbsp;<select name="' . $name . 'v'.($map[0] > 1 ? '['.$i.']' : '').'">'.$this->select_size_value('b', ($field_type[0]=='speed' ? true : false)).'</select>&nbsp;['.get_formatted_filesize($new[$config_key][$i], true, false, ($field_type[0]=='speed' ? true : false)).']';
										break;

										default:
											$tpl .= '<input id="' . $key . '['.$i.']" type="text"' . (($field_type[1]) ? ' size="' . $field_type[1] . '"' : '') . ' maxlength="' . (($field_type[2]) ? $field_type[2] : 255) . '" name="' . $name .($map[0] > 1 ?  '['.$i.']' : '').'" value="' . (isset($map['default'][$i]) && $map['default'][$i]!==false && $new[$config_key][$i]==='' ? $map['default'][$i] : $new[$config_key][$i]) . '" />';
										break;
									}
									$tpl.=(isset($map['append'][$i]) && isset($user->lang[$map['append'][$i]]) ? '&nbsp;'.$user->lang[$map['append'][$i]] : '').'<br />';
								}
							}
						}
						else
						{
							$tpl .= '<strong>'.$user->lang['ACP_CONFIG_OPTION'].' '.($i+1).':</strong>&nbsp;';
							$tpl .= '<input id="' . $key . '['.$i.']" type="text" maxlength="255" name="' . $name . '['.$i.']" value="' . (isset($map['default']) && $map['default']!==false && $new[$config_key][$i]==='' ? $map['default'] : $new[$config_key][$i]) . '" />'.(isset($map['append'][$i]) && isset($user->lang[$map['append'][$i]]) ? '&nbsp;'.$user->lang[$map['append'][$i]] : '').'<br />';
						}
					}
				}
			}
			else
			{
				$tpl = '<span style="color:#FF0000;">Invalid map content ['.$config_key.']</span>';
			}
		}

		return $tpl;
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);
		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : count($s_config);

		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				$s_config[$i]=isset($s_config[$i]) ? $s_config[$i] : '';
				if($type)
				{
					$use_function=false;
					if(is_array($type) && isset($type[$i]) && !empty($type[$i]))
					{
						$use_function=$type[$i];
					}
					else if(is_string($type) && !empty($type))
					{
						$use_function=$type;
					}

					if($use_function)
					{
						$s_config[$i]=@function_exists($use_function) ? call_user_func($use_function, $s_config[$i]) : call_user_func(array($this, $use_function), $s_config[$i]);
					}
				}
			}
		}

		return $s_config;
	}

	public function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	public function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

	public function get_size_value($value='b', $size=0)
	{
		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		switch($value)
		{
			case 'b':
				return $size;
			break;

			case 'kb':
				return 1024*$size;
			break;

			case 'mb':
				return 1024*1024*$size;
			break;

			case 'gb':
				return 1024*1024*1024*$size;
			break;

			case 'tb':
				return 1024*1024*1024*1024*$size;
			break;

			case 'pb':
				return 1024*1024*1024*1024*1024*$size;
			break;

			case 'eb':
				return 1024*1024*1024*1024*1024*1024*$size;
			break;
		}

	}

	public function select_size_value($value='b', $speed=false, $override=true)
	{
		global $user;

		$values=array('b'=>'BYTES', 'kb'=>'KB', 'mb'=>'MB', 'gb'=>'GB', 'tb'=>'TB', 'pb'=>'PB', 'eb'=>'EB');

		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].($speed ? '/'.$user->lang['TSEC'] : '').'</option>';
		}

		return $form;
	}

	public function get_time_value($value='s', $time=0)
	{
		if(!in_array($value, array('s', 'm', 'h', 'd')))
		{
			$value='s';
		}

		switch($value)
		{
			case 's':
				return $time;
			break;

			case 'm':
				return 60*$time;
			break;

			case 'h':
				return 60*60*$time;
			break;

			case 'd':
				return 60*60*24*$time;
			break;
		}

	}

	public function select_time_value($value='s', $override=true)
	{
		global $user;

		$values=array('s'=>'SECONDS', 'm'=>'MINUTES', 'h'=>'HOURS', 'd'=>'DAYS');

		if(!in_array($value, array('s', 'm', 'h', 'd')))
		{
			$value='s';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].'</option>';
		}

		return $form;
	}

	public function select_tracker_forums($value, $key)
	{
		global $user, $config;

		$forum_list = make_forum_select(false, false, true, true, true, false, true);
		$tracker_forums=$this->get_tracker_forums();

		$selected = array();
		if(isset($config[$key]))
		{
			if(is_array($config[$key]))
			{
				$selected = $config[$key];
			}
			else if(strlen($config[$key]) > 0)
			{
				$selected = explode(',', $config[$key]);
			}
		}
		// Build forum options
		$s_forum_options = '<select id="' . $key . '" name="' . $key . '[]" multiple="multiple">';
		foreach ($forum_list as $f_id => $f_row)
		{
			$s_forum_options .= '<option value="' . $f_id . '"' . ((in_array($f_id, $selected)) ? ' selected="selected"' : '') . (($f_row['disabled'] || !isset($tracker_forums[$f_id])) ? ' disabled="disabled" class="disabled-option"' : '') . '>' . $f_row['padding'] . $f_row['forum_name'] . '</option>';
		}
		$s_forum_options .= '</select>';

		return $s_forum_options;

	}

	public function get_tracker_forums()
	{
		global $db;

		$tracker_forums=array();
		$sql='SELECT forum_id FROM '.FORUMS_TABLE." WHERE forum_tracker='1'";
		$result=$db->sql_query($sql);
		while($row=$db->sql_fetchrow($result))
		{
			$tracker_forums[$row['forum_id']]=$row['forum_id'];
		}
		$db->sql_freeresult($result);

		return $tracker_forums;
	}
}

?>