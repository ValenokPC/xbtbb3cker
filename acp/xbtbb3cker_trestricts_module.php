<?php

/**
 *
 * @package xbtBB3cker
 * @copyright (c) 2015 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\xbtbb3cker\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class xbtbb3cker_trestricts_module
{
	public $u_action;

	function main($id, $mode)
	{
		global $config, $request, $template, $user, $db, $table_prefix, $cache, $phpEx, $phpbb_root_path;

		$user->add_lang_ext('ppk/xbtbb3cker', 'acp_xbtbb3cker_trestricts');

		$this->page_title = $user->lang('ACP_XBTBB3CKER_TRESTRICTS');
		$this->tpl_name = 'acp_xbtbb3cker_trestricts';

		$submit = (isset($_POST['submit'])) ? true : false;
		$form_key = 'info_acp_xbtbb3cker_trestricts';
		add_form_key($form_key);

		$error = array();

		if(!class_exists('timedelta'))
		{
			$user->add_lang_ext('ppk/xbtbb3cker', 'posts_merging');
			include_once($phpbb_root_path . 'ext/ppk/xbtbb3cker/include/time_delta.'.$phpEx);
		}
		$td = new \timedelta();

		$trestrict_ratio=$request->variable('trestrict_ratio', array(''=>''));
		$trestrict_upload=$request->variable('trestrict_upload', array(0=>''));
		$trestrict_uploadv=$request->variable('trestrict_uploadv', array(''=>''));
		$trestrict_download=$request->variable('trestrict_download', array(0=>''));
		$trestrict_downloadv=$request->variable('trestrict_downloadv', array(''=>''));
		$trestrict_leech=$request->variable('trestrict_leech', array(0=>''));
		$trestrict_wait=$request->variable('trestrict_wait', array(0=>''));
		$trestrict_waitv=$request->variable('trestrict_waitv', array(''=>''));
		$trestrict_peers=$request->variable('trestrict_peers', array(0=>''));
		$trestrict_torrents=$request->variable('trestrict_torrents', array(0=>''));
		$trestrict_enabled=$request->variable('trestrict_enabled', array(0=>''));
		$trestrict_include=$request->variable('trestrict_include', array(0=>''));
		$trestrict_days=$request->variable('trestrict_days', array(0=>''));

		$valid_ratio=array('Leech.', 'Seed.', 'Inf.', 'None.');

		if($submit && $trestrict_ratio)
		{


			$trestrict_delete=$request->variable('trestrict_delete', array(0=>''));

			$d_trestrict=array();
			foreach($trestrict_ratio as $k=>$v)
			{
				if(!in_array($trestrict_ratio[$k], $valid_ratio))
				{
					$trestrict_ratio[$k] = $this->my_float_val($trestrict_ratio[$k]);
					$trestrict_ratio[$k] > 999.999 || $trestrict_ratio[$k] < 0.000 ? $trestrict_ratio[$k]=0.000 : '';
				}

				$trestrict_upload[$k] = $this->get_size_value($trestrict_uploadv[$k], substr($trestrict_upload[$k], 0, 20));
				$trestrict_upload[$k] < 0 ? $trestrict_upload[$k]=0 : '';
				$trestrict_download[$k] = $this->get_size_value($trestrict_downloadv[$k], substr($trestrict_download[$k], 0, 20));
				$trestrict_download[$k] < 0 ? $trestrict_download[$k]=0 : '';
				$trestrict_wait[$k] = $this->get_time_value($trestrict_waitv[$k], substr($trestrict_wait[$k], 0, 20));
				$trestrict_wait[$k] < 0 ? $trestrict_wait[$k]=0 : '';

				if(isset($trestrict_delete[$k]))
				{
					$d_trestrict[]=$this->my_int_val($k);
				}
				else
				{

					if((!in_array($trestrict_ratio[$k], $valid_ratio) && $trestrict_ratio[$k]==0.000) && !$trestrict_upload[$k] && !$trestrict_download[$k])
					{
						trigger_error($user->lang['INVALID_TRESTRICT_KEY'].sprintf($user->lang['TRESTRICT_BACK'], $this->u_action));
					}
					if($trestrict_leech[$k] && !$trestrict_wait[$k] && !$trestrict_peers[$k] && !$trestrict_torrents[$k] && !$trestrict_days[$k])
					{
						trigger_error($user->lang['INVALID_TRESTRICT_VALUE'].sprintf($user->lang['TRESTRICT_BACK'], $this->u_action));
					}

					if($k==0)
					{
						$sql='INSERT INTO '.TRACKER_TRESTRICTS_TABLE."
							(user_ratio, user_upload, user_download, can_leech, wait_time, peers_limit, torrents_limit, trestrict_enabled, include_torrent, days_limit)
								VALUES(
								'".$db->sql_escape($trestrict_ratio[$k])."',
								'".$trestrict_upload[$k]."',
								'".$trestrict_download[$k]."',
								'".($trestrict_leech[$k] ? 1 : 0)."',
								'".$trestrict_wait[$k]."',
								'".$trestrict_peers[$k]."',
								'".$trestrict_torrents[$k]."',
								'".(isset($trestrict_enabled[$k]) ? 1 : 0)."',
								'".(isset($trestrict_include[$k]) ? 1 : 0)."',
								'".$trestrict_days[$k]."'
								)";
						$result=$db->sql_query($sql);
					}
					else if($trestrict_ratio[$k]!='')
					{
						$sql='UPDATE '.TRACKER_TRESTRICTS_TABLE." SET
							user_ratio='".$db->sql_escape($trestrict_ratio[$k])."',
							user_upload='".$trestrict_upload[$k]."',
							user_download='".$trestrict_download[$k]."',
							can_leech='".($trestrict_leech[$k] ? 1 : 0)."',
							wait_time='".$trestrict_wait[$k]."',
							peers_limit='".$trestrict_peers[$k]."',
							torrents_limit='".$trestrict_torrents[$k]."',
							trestrict_enabled='".(isset($trestrict_enabled[$k]) ? 1 : 0)."',
							include_torrent='".(isset($trestrict_include[$k]) ? 1 : 0)."',
							days_limit='".$trestrict_days[$k]."'
							WHERE id='".$this->my_int_val($k)."'";
						$result=$db->sql_query($sql);
					}
				}
			}
			if($d_trestrict)
			{
				$sql='DELETE FROM '.TRACKER_TRESTRICTS_TABLE.' WHERE '.$db->sql_in_set('id', $d_trestrict).'';
				$result=$db->sql_query($sql);
			}

			$cache->destroy('_ppkbb3cker_trestricts');
		}


		if($request->variable('add_new', ''))
		{
			$template->assign_block_vars('trestricts', array(
				'COUNT' => 0,
				'RATIO'	=> '0.000',
				'UPLOAD'	=> 0,
				'UPLOAD_VALUE'	=> get_formatted_filesize(0),
				'DOWNLOAD'	=> 0,
				'DOWNLOAD_VALUE'	=> get_formatted_filesize(0),

				'LEECH'	=> '
					<select name="trestrict_leech[0]" style="width:80px;">
						<option value="1">'.$user->lang['YES'].'</option>
						<option value="0" selected="selected">'.$user->lang['NO'].'</option>
					</select>
				',
				'WAIT'	=> 0,
				'WAIT_VALUE'	=> $td->spelldelta(0, 0),
				'PEERS'	=> 0,
				'TORRENTS'	=> 0,
				'DAYS'	=> 0,

				'ENABLED' => '',
				'INCLUDE' => '',

				'SIZE_VALUE' => $this->select_size_value(),
				'TIME_VALUE' => $this->select_time_value(),
				)
			);
			$template->assign_vars(array(
				'S_NEW_TRESTRICT'	=> true,
				'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_trestrict" value="1" >',
				)
			);
		}
		else
		{
			$sql='SELECT * FROM '.TRACKER_TRESTRICTS_TABLE.' ORDER BY user_ratio, user_upload, user_download';
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$template->assign_block_vars('trestricts', array(
					'COUNT'	=> $row['id'],
					'RATIO'	=> htmlspecialchars($row['user_ratio']),
					'UPLOAD'	=> $row['user_upload'],
					'UPLOAD_VALUE'	=> get_formatted_filesize($row['user_upload']),
					'DOWNLOAD'	=> $row['user_download'],
					'DOWNLOAD_VALUE'	=> get_formatted_filesize($row['user_download']),
					'LEECH'	=> '
						<select name="trestrict_leech['.$row['id'].']" style="width:80px;">
							<option value="1"'.($row['can_leech'] ? ' selected="selected"' : '').'>'.$user->lang['YES'].'</option>
							<option value="0"'.(!$row['can_leech'] ? ' selected="selected"' : '').'>'.$user->lang['NO'].'</option>
						</select>
					',
					'WAIT'	=> $row['wait_time'],
					'WAIT_VALUE'	=> $td->spelldelta(0, $row['wait_time']),
					'PEERS'	=> $row['peers_limit'],
					'TORRENTS'	=> $row['torrents_limit'],
					'DAYS'	=> $row['days_limit'],

					'ENABLED' => $row['trestrict_enabled'] ? ' checked="checked"' : '',
					'INCLUDE' => $row['include_torrent'] ? ' checked="checked"' : '',

					'SIZE_VALUE' => $this->select_size_value(),
					'TIME_VALUE' => $this->select_time_value(),
					)
				);
			}
			$db->sql_freeresult($result);

			$template->assign_vars(array(
				'S_VIEW_TRESTRICT'	=> true,
				'S_HIDDEN_FIELDS'=>'<input type="hidden" name="add_trestrict" value="1" >',
				)
			);
		}

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['ACP_XBTBB3CKER_TRESTRICTS'],
			'L_TITLE_EXPLAIN'	=> $user->lang['ACP_XBTBB3CKER_TRESTRICTS_EXPLAIN'],

			'S_ERROR'			=> (count($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),

			'U_ACTION'       => $this->u_action,

		));
	}

	function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

	function get_size_value($value='b', $size=0)
	{
		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		switch($value)
		{
			case 'b':
				return $size;
			break;

			case 'kb':
				return 1024*$size;
			break;

			case 'mb':
				return 1024*1024*$size;
			break;

			case 'gb':
				return 1024*1024*1024*$size;
			break;

			case 'tb':
				return 1024*1024*1024*1024*$size;
			break;

			case 'pb':
				return 1024*1024*1024*1024*1024*$size;
			break;

			case 'eb':
				return 1024*1024*1024*1024*1024*1024*$size;
			break;
		}

	}

	function select_size_value($value='b', $speed=false, $override=true)
	{
		global $user;

		$values=array('b'=>'BYTES', 'kb'=>'KB', 'mb'=>'MB', 'gb'=>'GB', 'tb'=>'TB', 'pb'=>'PB', 'eb'=>'EB');

		if(!in_array($value, array('b', 'kb', 'mb', 'gb', 'tb', 'pb', 'eb')))
		{
			$value='b';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].($speed ? '/'.$user->lang['TSEC'] : '').'</option>';
		}

		return $form;
	}

	function get_time_value($value='s', $time=0)
	{
		if(!in_array($value, array('s', 'm', 'h', 'd')))
		{
			$value='s';
		}

		switch($value)
		{
			case 's':
				return $time;
			break;

			case 'm':
				return 60*$time;
			break;

			case 'h':
				return 60*60*$time;
			break;

			case 'd':
				return 60*60*24*$time;
			break;
		}

	}

	function select_time_value($value='s', $override=true)
	{
		global $user;

		$values=array('s'=>'SECONDS', 'm'=>'MINUTES', 'h'=>'HOURS', 'd'=>'DAYS');

		if(!in_array($value, array('s', 'm', 'h', 'd')))
		{
			$value='s';
		}

		$form='';

		foreach($values as $k => $v)
		{
			$form.='<option'.(!$override && $k!=$value ? ' disabled="disabled" class="disabled"' : '').' value="'.$k.'"'.($k==$value ? ' selected="selected"' : '').'>'.$user->lang[$v].'</option>';
		}

		return $form;
	}

}

?>