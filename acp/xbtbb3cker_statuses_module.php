<?php

/**
 *
 * @package xbtBB3cker
 * @copyright (c) 2015 PPK
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 *
 */

namespace ppk\xbtbb3cker\acp;

/**
 * @ignore
 */
if (!defined('IN_PHPBB'))
{
	exit;
}

/**
 * @package acp
 */
class xbtbb3cker_statuses_module
{
	public $u_action;

	function main($id, $mode)
	{
		global $config, $request, $template, $user, $db, $table_prefix, $cache;

		$user->add_lang_ext('ppk/xbtbb3cker', 'acp_xbtbb3cker_statuses');
		$user->add_lang_ext('ppk/xbtbb3cker', 'xbtbb3cker_statuses');

		$this->page_title = $user->lang('ACP_XBTBB3CKER_STATUSES');
		$this->tpl_name = 'acp_xbtbb3cker_statuses';

		$submit = (isset($_POST['submit'])) ? true : false;
		$form_key = 'info_acp_xbtbb3cker_statuses';
		add_form_key($form_key);

		$error = array();

		$to_status=$request->variable('to_status', '');
		$status_reason=$request->variable('status_reason', array(0=>''), true);
		$status_mark=$request->variable('status_mark', array(0=>''), true);
		$status_sign=$request->variable('status_sign', array(0=>''));
		$status_delete=$request->variable('status_delete', array(0=>''));
		$status_enabled=$request->variable('status_enabled', array(0=>''));
		$author_candown=$request->variable('author_candown', array(0=>''));
		$guest_cantdown=$request->variable('guest_cantdown', array(0=>''));
		$status_count=$request->variable('status_count', array(0=>''));

		if($to_status)
		{
			$to_status=$this->my_split_config($to_status, 2, array('strval', 'my_int_val'), '_');
			if(in_array($to_status[0], array('p', 'm')))
			{
				$result=$db->sql_query("SELECT status_id FROM ".TRACKER_STATUSES_TABLE." WHERE id='{$to_status[1]}'");
				$old_status=intval($db->sql_fetchfield('status_id'));
				$db->sql_freeresult($result);
				if($old_status)
				{
					$new_status=$this->get_unassigned_statuses($to_status[0]);
					if($new_status)
					{
						$result=$db->sql_query("UPDATE ".TOPICS_TABLE." SET torrent_status='{$new_status}' WHERE torrent_status='{$old_status}'");
						$result=$db->sql_query("UPDATE ".TRACKER_STATUSES_TABLE." SET status_id='{$new_status}' WHERE id='{$to_status[1]}'");

						$result=$db->sql_query("SELECT status_id FROM ".TRACKER_STATUSES_TABLE." WHERE def_forb!=0  LIMIT 1");
						$def_forb=intval($db->sql_fetchfield('status_id'));
						$db->sql_freeresult($result);

						$result=$db->sql_query("SELECT status_id FROM ".TRACKER_STATUSES_TABLE." WHERE def_notforb!=0 LIMIT 1");
						$def_notforb=intval($db->sql_fetchfield('status_id'));
						$db->sql_freeresult($result);

						$config->set('ppkbb_tcdef_statuses', $def_forb.' '.$def_notforb);

						$sql = 'SELECT lang_iso
							FROM ' . LANG_TABLE;
						$result = $db->sql_query($sql);
						while ($row = $db->sql_fetchrow($result))
						{
							$cache->destroy("_xbtbb3cker_statuses_{$row['lang_iso']}_cache");
						}
						$db->sql_freeresult($result);

// 						purge_tracker_config(true);
						$cache->destroy('_ppkbb3cker_cache');

						trigger_error(sprintf($user->lang['STATUS_SUCCESS'], $this->u_action));
					}
				}
			}
		}

		if($submit && $status_reason)
		{
			$d_statuses=$statuses_candown=$statuses_guest_cantdown=array();

			$exchange_replace_from=$request->variable('exchange_replace_from', '');
			$exchange_replace_to=$request->variable('exchange_replace_to', 0);
			$result=false;

			if($exchange_replace_from && $exchange_replace_to)
			{
				$exchange_replace_from=$this->my_split_config($exchange_replace_from, 2, array('strval', 'intval'), '_');
				if($exchange_replace_from[1]!=$exchange_replace_to)
				{
					if($exchange_replace_from[0]=='ex')
					{
						$exchange_replace_from[2]=$this->get_unassigned_statuses('p');
						if($exchange_replace_from[2])
						{
							$result=$db->sql_query("UPDATE ".TOPICS_TABLE." SET torrent_status='{$exchange_replace_from[2]}' WHERE torrent_status='{$exchange_replace_from[1]}'");
							$result=$db->sql_query("UPDATE ".TOPICS_TABLE." SET torrent_status='{$exchange_replace_from[1]}' WHERE torrent_status='{$exchange_replace_to}'");
							$result=$db->sql_query("UPDATE ".TOPICS_TABLE." SET torrent_status='{$exchange_replace_to}' WHERE torrent_status='{$exchange_replace_from[2]}'");
						}
					}
					else if($exchange_replace_from[0]=='re')
					{
						$result=$db->sql_query("UPDATE ".TOPICS_TABLE." SET torrent_status='{$exchange_replace_to}' WHERE torrent_status='{$exchange_replace_from[1]}'");
					}
				}
			}

			if(!$result)
			{
				$def_forb=$request->variable('def_forb', 0);
				$def_notforb=$request->variable('def_notforb', 0);
				foreach($status_reason as $k=>$v)
				{
					$k=$this->my_int_val($k);
					$status_sign[$k]=$this->my_split_config($status_sign[$k], 2, array('strval', 'my_int_val'), '_');
					$status_sign[$k]=$status_sign[$k][0];
					if(!in_array($status_sign[$k], array('p', 'm', 'd')))
					{
						continue;
					}
					if(isset($status_delete[$k]) && $status_sign[$k]!='d')
					{
						$d_statuses[]=$k;
					}
					else
					{

						if($k==0 && $status_reason[$k]!=''/* && $status_mark[$k]!=''*/)
						{
							$status_id=$this->get_unassigned_statuses($status_sign[$k]);
							if(!$status_id)
							{
								trigger_error(sprintf($user->lang['STATUSES_FULL'], $this->u_action));
							}
							$sql='INSERT INTO '.TRACKER_STATUSES_TABLE."
								(status_id, status_reason, status_mark, status_enabled)
									VALUES(
									'{$status_id}',
									'".$db->sql_escape(strip_tags($status_reason[$k]))."',
									'".$db->sql_escape($status_mark[$k])."',
									'".(isset($status_enabled[$k]) ? 1 : 0)."')";
							$result=$db->sql_query($sql);
						}
						else if($status_reason[$k]!='')
						{
							$sql='UPDATE '.TRACKER_STATUSES_TABLE." SET
								status_reason='".$db->sql_escape(strip_tags($status_reason[$k]))."',
								status_mark='".$db->sql_escape($status_mark[$k])."',
								status_enabled='".(isset($status_enabled[$k]) ? 1 : 0)."',
								author_candown='".(isset($author_candown[$k]) ? 1 : 0)."',
								guest_cantdown='".(isset($guest_cantdown[$k]) ? 1 : 0)."',
								def_forb='".($def_forb==$k ? 1 : 0)."',
								def_notforb='".($def_notforb==$k ? 1 : 0)."'
									WHERE id='{$k}'";
							$result=$db->sql_query($sql);

							isset($author_candown[$k]) ? $statuses_candown[]=intval($status_count[$k]) : '';
							isset($guest_cantdown[$k]) ? $statuses_guest_cantdown[]=intval($status_count[$k]) : '';
						}
					}
				}

				$config->set('ppkbb_tcdef_statuses', (isset($status_count[$def_forb]) ? $status_count[$def_forb] : 0).' '.(isset($status_count[$def_notforb]) ? $status_count[$def_notforb] : 0));
				$config->set('ppkbb_tcauthor_candown', implode(' ', $statuses_candown));
				$config->set('ppkbb_tcguest_cantdown', implode(' ', $statuses_guest_cantdown));

				if($d_statuses)
				{
					$sql='DELETE FROM '.TRACKER_STATUSES_TABLE.' WHERE '.$db->sql_in_set('id', $d_statuses);
					$result=$db->sql_query($sql);
				}
// 				$this->u_action=append_sid("{$phpbb_admin_path}index.{$phpEx}", 'i=board&amp;mode=statuses');

				//@unlink($phpbb_root_path.'cache/'.PHPBB_ENVIRONMENT.'/sql_'.md5('SELECT * FROM ' . TRACKER_STATUSES_TABLE." WHERE status_enabled='1'").".{$phpEx}");
				$sql = 'SELECT lang_iso
					FROM ' . LANG_TABLE;
				$result = $db->sql_query($sql);
				while ($row = $db->sql_fetchrow($result))
				{
					$cache->destroy("_xbtbb3cker_statuses_{$row['lang_iso']}_cache");
				}
				$db->sql_freeresult($result);

// 				purge_tracker_config(true);
				//obtain_tracker_config();
				$cache->destroy('_ppkbb3cker_cache');
			}
		}

		if($request->variable('add_new', ''))
		{
			$template->assign_block_vars('statuses', array(
				'STATUS' => $this->get_statuses_sign(),
				'COUNT'	=> 0,
				'ID'	=> '',
				'REASON'	=> '',
				'MARK' => '',
				'ENABLED' => '',
				'FORB' => '',
				'NOTFORB' => '',
				)
			);
			$template->assign_vars(array(
				'S_NEW_STATUS'	=> true,
				'S_HIDDEN_FIELDS'=>'<input type="hidden" name="view_statuses" value="1" >',
				)
			);
		}
		else
		{
			$statuses_descr=0;
			$status_sign='';
			$forb_sel=array();
			$sql="SELECT * FROM ".TRACKER_STATUSES_TABLE." ORDER BY status_id DESC";
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$statuses_descr=false;
				if($row['status_id'] < 0 && $row['status_id'] > -50 && !isset($forb_sel[-1]))
				{
					$statuses_descr=$user->lang['STATUSES_SIGN']['m'];
					$forb_sel[-1]=1;
					$status_sign='m';
				}
				if($row['status_id'] > 0 && !isset($forb_sel[1]))
				{
					$statuses_descr=$user->lang['STATUSES_SIGN']['p'];
					$forb_sel[1]=1;
					$status_sign='p';
				}
				if($row['status_id'] == 0 && !isset($forb_sel[0]))
				{
					$statuses_descr=$user->lang['STATUSES_SIGN']['d'];
					$forb_sel[0]=1;
					$status_sign='d';
				}

				$template->assign_block_vars('statuses', array(
					'STATUS' => $this->get_statuses_sign($row['id'], $status_sign),
					'COUNT' => $row['id'],
					'ID' => $row['status_id'],
					'REASON' => $row['status_reason'],
					'MARK' => $row['status_mark'],
					'LANG_REASON' => isset($user->lang[$row['status_reason']]) ? $user->lang[$row['status_reason']] : $row['status_reason'],
					'LANG_MARK' => isset($user->lang[$row['status_mark']]) ? $user->lang[$row['status_mark']] : htmlspecialchars_decode($row['status_mark']),
					'ENABLED' => $row['status_enabled'] ? ' checked="checked"' : '',
					'CANDOWN' => $row['author_candown'] ? ' checked="checked"' : '',
					'GUEST_CANTDOWN' => $row['guest_cantdown'] ? ' checked="checked"' : '',
					'FORB' => $row['def_forb'] ? ' checked="checked"' : '',
					'NOTFORB' => $row['def_notforb'] ? ' checked="checked"' : '',
					'CANDOWN_OPT' => $row['status_id'] > 0 ? true : false,
					'GUEST_CANTDOWN_OPT' => $row['status_id'] < 0 ? true : false,
					'DESCR' => $statuses_descr,
					'EXCHANGE_FROM' => '<input class="radio" type="radio" name="exchange_replace_from" value="ex_'.$row['status_id'].'">',
					'REPLACE_FROM' => '<input class="radio" type="radio" name="exchange_replace_from" value="re_'.$row['status_id'].'">',
					'EXCHANGE_REPLACE_TO' => '<input class="radio" type="radio" name="exchange_replace_to" value="'.$row['status_id'].'">',
					)
				);
			}
			$db->sql_freeresult($result);

			$template->assign_vars(array(
				'S_VIEW_STATUS'	=> true,
				'S_HIDDEN_FIELDS'=>'<input type="hidden" name="view_statuses" value="1" >',
				)
			);
		}

		$template->assign_vars(array(
			'L_TITLE'			=> $user->lang['ACP_XBTBB3CKER_STATUSES'],
			'L_TITLE_EXPLAIN'	=> $user->lang['ACP_XBTBB3CKER_STATUSES_EXPLAIN'],

			'S_ERROR'			=> (count($error)) ? true : false,
			'ERROR_MSG'			=> implode('<br />', $error),

			'U_ACTION'       => $this->u_action,

		));
	}

	public function my_int_val($v=0, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return 0;
		}
		else if($drop && $v>$max)
		{
			return 0;
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, 0, '', '');
	}

	public function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
	{
		if(!$v || ($v < 0 && !$negative))
		{
			return "0.".str_repeat('0', $n);
		}
		else if($drop && $v>$max)
		{
			return "0.".str_repeat('0', $n);
		}
		else if($max && $v>$max)
		{
			return $max;
		}

		return @number_format($v+0, $n, '.', '');
	}

	public function my_split_config($config, $count=0, $type=false, $split='')
	{
		$count=intval($count);
		$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
		$count=$count > 0 ? $count : count($s_config);

		if($count)
		{
			for($i=0;$i<$count;$i++)
			{
				$s_config[$i]=isset($s_config[$i]) ? $s_config[$i] : '';
				if($type)
				{
					$use_function=false;
					if(is_array($type) && isset($type[$i]) && !empty($type[$i]))
					{
						$use_function=$type[$i];
					}
					else if(is_string($type) && !empty($type))
					{
						$use_function=$type;
					}

					if($use_function)
					{
						$s_config[$i]=@function_exists($use_function) ? call_user_func($use_function, $s_config[$i]) : call_user_func(array($this, $use_function), $s_config[$i]);
					}
				}
			}
		}

		return $s_config;
	}

	public function get_unassigned_statuses($sign)
	{
		global $db;

		$signs=array('p'=>array(99, 1), 'm'=>array(-1, -49), 'd'=>array(0, 0));

		if(!isset($signs[$sign]))
		{
			return false;
		}

		$assigned_statuses=array();
		//if(!isset($assigned_statuses[$sign]))
		//{
			$sql="SELECT id, status_id FROM ".TRACKER_STATUSES_TABLE." WHERE status_id<='{$signs[$sign][0]}' AND status_id>='{$signs[$sign][1]}'";
			$result=$db->sql_query($sql);
			while($row=$db->sql_fetchrow($result))
			{
				$assigned_statuses[$sign][$row['id']]=$row['status_id'];
			}
			$db->sql_freeresult($result);
		//}

		if(count($assigned_statuses[$sign]))
		{
			for($i=$signs[$sign][0];$i>=$signs[$sign][1];$i--)
			{
				if(!in_array($i, $assigned_statuses[$sign]))
				{
					return $i;
				}
			}
		}
		else
		{
			return $signs[$sign][0];
		}

		return false;

	}

	public function get_statuses_sign($i=0, $c='')
	{
		global $user;

		$statuses='<select name="status_sign['.$i.']" style="width:125px;"'.($i==0 ? '' : ' onchange="if(this.options[this.selectedIndex].value != \'d\'){if(confirm(\''.$user->lang['STATUS_CHANGE_SIGN'].'\')){window.location.href=window.location+\'&amp;to_status=\'+this.options[this.selectedIndex].value;} }"').'>';
		foreach($user->lang['STATUSES_SIGN'] as $k => $v)
		{
			$statuses.='<option value="'.$k.'_'.$i.'"'.(($k=='d' && $c!='d') || ($c=='d' && $c!=$k) ? ' disabled="disabled"' : '').($c && $c==$k ? ' selected="selected"' : '').'>'.$v.'</option>';
		}
		$statuses.='</select>';

		return $statuses;
	}

	public function remove_tracker_cache($filename, $type='sql', $check = false)
	{
		global $phpbb_root_path, $phpEx;

		$cache_dir = $phpbb_root_path . 'cache/'.PHPBB_ENVIRONMENT.'/';

		if(!in_array($type, array('sql', 'data', 'cache')))
		{
			return false;
		}
		if ($check && !@is_writable($cache_dir))
		{
			// E_USER_ERROR - not using language entry - intended.
			trigger_error('Unable to remove files within ' . $cache_dir . '. Please check directory permissions.', E_USER_ERROR);
		}

		return @unlink("{$cache_dir}{$type}_".($type=='sql' ? md5($filename) : $filename).".$phpEx");
	}
}

?>