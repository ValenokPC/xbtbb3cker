<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\acp;

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

class xbtbb3cker_rtracker_info
{
	function module()
	{
		return array(
			'filename'	=> '\ppk\xbtbb3cker\xbtbb3cker_rtracker_module',
			'title'		=> 'ACP_XBTBB3CKER_RTRACKER',
			'modes'		=> array(
				'xbtbb3cker' => array('title' => 'ACP_XBTBB3CKER_RTRACKER', 'auth' => 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker', 'cat' => array('ACP_XBTBB3CKER')),
			),
		);
	}
}

?>