<?php

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
	exit;
}

define('CONFIG_TABLE', $table_prefix . 'config');
define('XBT_CONFIG', 'xbt_config');

// include_once("{$phpbb_root_path}tracker/include/config_map.{$phpEx}");
$trcache_config=array();

$sql = 'SELECT config_name, config_value FROM '.CONFIG_TABLE." WHERE config_name IN('ppkbb_phpannounce_enabled', 'ppkbb_tcgz_rewrite', 'ppkbb_tciptype')";
$result = my_sql_query($sql, $c);
$config_row=my_sql_fetch_array($result);
foreach($config_row as $row)
{
// 	isset($config_map[$row['config_name']]) ? $row['config_value']=$config_map[$row['config_name']][0]==1 ? $row['config_value'] : my_split_config($row['config_value'], $config_map[$row['config_name']][0], $config_map[$row['config_name']][1], $config_map[$row['config_name']][2]) : '';

	$config[$row['config_name']]=$row['config_value'];
	$trcache_config[$row['config_name']]=$row['config_value'];
}
my_sql_free_result($result);

$sql = 'SELECT name, value FROM '.XBT_CONFIG;
$result = my_sql_query($sql, $c);
$xbt_config_row=my_sql_fetch_array($result);
foreach($xbt_config_row as $row)
{
// 	isset($config_map[$row['config_name']]) ? $row['config_value']=$config_map[$row['config_name']][0]==1 ? $row['config_value'] : my_split_config($row['config_value'], $config_map[$row['config_name']][0], $config_map[$row['config_name']][1], $config_map[$row['config_name']][2]) : '';

	$config[$row['name']]=$row['value'];
	$trcache_config[$row['name']]=$row['value'];
}
my_sql_free_result($result);

include_once("{$tincludedir}tcache.{$phpEx}");

t_recache('tracker_sconfig', $trcache_config);
?>
