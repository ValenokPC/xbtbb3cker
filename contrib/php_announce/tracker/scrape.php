<?php

error_reporting(0);
@ini_set('register_globals', 0);
@ini_set('magic_quotes_runtime', 0);
@ini_set('magic_quotes_sybase', 0);

function_exists('date_default_timezone_set') ? date_default_timezone_set('Europe/Moscow') : '';

define('IN_PHPBB', true);

$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

require($phpbb_root_path . 'config.'.$phpEx);
if (!defined('PHPBB_ENVIRONMENT'))
{
	@define('PHPBB_ENVIRONMENT', 'production');
}
$tcachedir="{$phpbb_root_path}cache/".PHPBB_ENVIRONMENT.'/';
$tincludedir="{$phpbb_root_path}tracker/tinc/";

if(isset($_SERVER['HTTP_ACCEPT_CHARSET'])/* || isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])*/ || isset($_SERVER['HTTP_REFERER'])/*  || isset($_SERVER['HTTP_COOKIE'])*/ || isset($_SERVER['HTTP_X_REQUESTED_WITH']))//ut 3.2.2, iis
{
	err('Unknown Client');
}

if(!in_array($dbms, array('phpbb\\db\\driver\\mysql', 'phpbb\\db\\driver\\mysqli', 'mysql', 'mysqli')))
{
	err('Only mysql(i) supported');
}

require("{$phpbb_root_path}ext/ppk/xbtbb3cker/include/db/".(in_array($dbms, array('phpbb\\db\\driver\\mysql', 'mysql')) ? 'mysql' : 'mysqli').".{$phpEx}");

$c=my_sql_connect($dbhost.($dbport ? ":{$dbport}" : ''), $dbuser, $dbpasswd);
if(!$c)
{
	err('Error connecting database: '.my_sql_error($c).' ['.my_sql_errno($c).']');
}

$s=my_sql_select_db($dbname, $c);
if(!$s)
{
	err('Error selecting database: '.my_sql_error($c));
}

$sql="SET NAMES 'utf8'";
my_sql_query($sql, $c);
unset($dbpasswd);

$dt=time();

$config=array();

$cache_config=t_getcache('tracker_sconfig');
if($cache_config===false)
{
	include($tincludedir.'tsconf.'.$phpEx);
}
else
{
	foreach($cache_config as $k => $v)
	{
		$config[$k]=$v;
	}
	unset($cache_config);
}

if(!$config['ppkbb_phpannounce_enabled'])
{
	err('Tracker disabled');
}

if(!$config['full_scrape'])
{
	err("Scrape functions disabled");
}

define('XBT_FILES', $config['table_files']);
define('XBT_USERS', $config['table_users']);
define('XBT_SCRAPE_LOG', $config['table_scrape_log']);

//includes/startup.php
// if (version_compare(PHP_VERSION, '5.4.0-dev', '>='))
// {
// 	define('STRIP', false);
// }
// else
// {
// 	define('STRIP', (get_magic_quotes_gpc()) ? true : false);
// }

$passkey=isset($_GET['passkey']) ? $_GET['passkey'] : '';
// STRIP ? '' : $passkey=my_sql_real_escape_string($passkey, $c);
$passkey=my_sql_real_escape_string($passkey, $c);

if(!$config['anonymous_scrape'] && !$passkey)
{
	err('Disabled for guests');
}

$user_id=0;
$is_guest=false;
if($config['anonymous_scrape'] && !$passkey)
{
	$is_guest=true;
	$user_id=1;
}

if(!$is_guest && !$passkey)
{
	err('Passkey not defined');
}

if(!$config['anonymous_scrape'] || (!$is_guest && $config['log_scrape']))
{
	$sql='SELECT uid FROM '.XBT_USERS." WHERE torrent_pass='{$passkey}' LIMIT 1";
	$result=my_sql_query($sql, $c);
	$user=my_sql_fetch_array($result, true);
	my_sql_free_result($result);

	if(!$user)
	{
		err('User not found on tracker');
	}

	$user_id=$user['uid'];
}

preg_match_all('/info_hash=([^&]*)/i', $_SERVER['QUERY_STRING'], $info_hashs);
if(isset($info_hashs[1]) && count($info_hashs[1]))
{
	foreach($info_hashs[1] as $k => $info_hash)
	{
		$info_hash=urldecode($info_hash);
// 		STRIP ? $info_hash=stripslashes($info_hash) : '';
		$l_info_hash=strlen($info_hash);
		if($l_info_hash!=20)
		{
			err("invalid info_hash: {$info_hash} ({$l_info_hash})");
		}
		$info_hashs[1][$k] = my_sql_real_escape_string($info_hash, $c);
	}
}
else
{
	err("Invalid info hash(s)");
}

$sql = "SELECT info_hash, seeders, leechers, completed FROM ".XBT_FILES." WHERE info_hash IN('".implode("', '", $info_hashs[1])."')";
$result=my_sql_query($sql, $c);
$torrents_row = my_sql_fetch_array($result);

if (!$torrents_row)
{
	err("Torrent(s) not registered on tracker - hash(s): " . implode(", ", $info_hashs[1]));
}

$resp='d5:filesd';
foreach($torrents_row as $row)
{
	$resp.='20:'.$row['info_hash'].'d';
	$resp.='8:completei'.$row['seeders'].'e';
	$resp.='10:downloadedi'.$row['completed'].'e';
	$resp.='10:incompletei'.$row['leechers'].'e';
	$resp.='e';
}
my_sql_free_result($result);
$resp.='ee';
//resp.="5:flagsd20:min_request_intervali{$config['ppkbb_minscrape_interval']}eee";

benc_resp_raw($resp, $config['ppkbb_tcgz_rewrite']);

if(!$is_guest && $config['log_scrape'])
{
	$ip=false;
	if($config['ppkbb_tciptype']==2 && isset($_GET['ip']))
	{
		$ip=check_ip($_GET['ip']);
	}
	else if($config['ppkbb_tciptype']==1 && isset($_SERVER['HTTP_X_FORWARDED_FOR']))
	{
		$ip=check_ip($_SERVER['HTTP_X_FORWARDED_FOR']);
	}
	if(!$ip)
	{
		$ip=isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
		if($ip!=check_ip($ip))
		{
			err('Invalid IP address');
		}
	}
// 	!STRIP ? $ip=my_sql_real_escape_string($ip, $c) : '';
	$ip=my_sql_real_escape_string($ip, $c);

	$sql="INSERT INTO ". XBT_SCRAPE_LOG ." (ipa, uid, mtime) VALUES (INET_ATON('{$ip}'), '{$user_id}', '{$dt}')";
	my_sql_query($sql, $c);
}

if($c)
{
	my_sql_close($c);
}

exit();

//############################################################

function err($msg)
{
	global $c;

	if($msg)
	{
		benc_resp(array("failure reason" => array('type' => "string", 'value' => $msg)));
	}

	if($c)
	{
		my_sql_close($c);
	}

	exit();
}

function warn($msg)
{
	global $c;

	if($msg)
	{
		benc_resp(array("warning message" => array('type' => "string", 'value' => $msg)));
	}

	if($c)
	{
		my_sql_close($c);
	}

	exit();
}

function benc($obj)
{
	if (!is_array($obj) || !isset($obj['type']) || !isset($obj['value']))
	{
		return;
	}
	$c = $obj['value'];
	switch ($obj['type'])
	{
		case "string":
			return benc_str($c);
		case "integer":
			return benc_int($c);
		case "list":
			return benc_list($c);
		case "dictionary":
			return benc_dict($c);
		default:
			return;
	}
}

function benc_str($s)
{
	return strlen($s) . ":$s";
}

function benc_int($i)
{
	return "i" . $i . "e";
}

function benc_list($a)
{
	$s = "l";
	foreach ($a as $e)
	{
		$s .= benc($e);
	}
	$s .= "e";
	return $s;
}

function benc_dict($d)
{
	$s = "d";
	$keys = array_keys($d);
	sort($keys);
	foreach ($keys as $k)
	{
		$v = $d[$k];
		$s .= benc_str($k);
		$s .= benc($v);
	}
	$s .= "e";
	return $s;
}

function benc_resp($d)
{
	global $config;

	benc_resp_raw(benc(array('type' => "dictionary", 'value' => $d)), $config['ppkbb_tcgz_rewrite']);
}

function benc_resp_raw($x, $c=0)
{
	$gz_enc=isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') && @extension_loaded('zlib') && (ini_get('zlib.output_compression')=='Off' || !ini_get('zlib.output_compression')) ? 1 : 0;

	header("Content-Type: text/plain");
	if($c==1 || (!$c && $gz_enc))
	{
		header("Content-Encoding: gzip");

		print gzencode($x, 9, FORCE_GZIP);
	}
	else if($c==2 || (!$c && !$gz_enc) || !$gz_enc)
	{
		header("Pragma: no-cache");

		print($x);
	}
}

function t_getcache($t, $var='')
{
	global $tcachedir, $phpEx;

	$cache_data=array();

	$f_name="{$tcachedir}data_ppkbb3cker_{$t}.{$phpEx}";
	if(@file_exists($f_name))
	{
		include($f_name);

		return $var ? $$var : $cache_data;
	}

	return false;
}

function check_ip($ip)
{
	$long=ip2long($ip);

	if($long==-1 || $long===false)
	{
		return false;
	}

	return $ip;
}
?>
