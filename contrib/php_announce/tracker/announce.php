<?php

error_reporting(0);
@ini_set('register_globals', 0);
@ini_set('magic_quotes_runtime', 0);
@ini_set('magic_quotes_sybase', 0);
//@ini_set('zlib.output_compression', 'Off');

function_exists('date_default_timezone_set') ? date_default_timezone_set('Europe/Moscow') : '';

define('IN_PHPBB', true);

$phpbb_root_path=(defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx=substr(strrchr(__FILE__, '.'), 1);

require($phpbb_root_path . 'config.'.$phpEx);
if (!defined('PHPBB_ENVIRONMENT'))
{
	@define('PHPBB_ENVIRONMENT', 'production');
}
$tcachedir="{$phpbb_root_path}cache/".PHPBB_ENVIRONMENT.'/';
$tincludedir="{$phpbb_root_path}tracker/tinc/";

if(isset($_SERVER['HTTP_ACCEPT_CHARSET'])/* || isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])*/ || isset($_SERVER['HTTP_REFERER'])/*  || isset($_SERVER['HTTP_COOKIE'])*/ || isset($_SERVER['HTTP_X_REQUESTED_WITH']))//ut 3.2.2, iis
{
	err('Unknown Client');
}

if(!in_array($dbms, array('phpbb\\db\\driver\\mysql', 'phpbb\\db\\driver\\mysqli', 'mysql', 'mysqli')))
{
	err('Only mysql(i) supported');
}

require("{$phpbb_root_path}ext/ppk/xbtbb3cker/include/db/".(in_array($dbms, array('phpbb\\db\\driver\\mysql', 'mysql')) ? 'mysql' : 'mysqli').".{$phpEx}");

$c=my_sql_connect($dbhost.($dbport ? ":{$dbport}" : ''), $dbuser, $dbpasswd);
if(!$c)
{
	err('Error connecting database: '.my_sql_error($c).' ['.my_sql_errno($c).']');
}

$s=my_sql_select_db($dbname, $c);
if(!$s)
{
	err('Error selecting database: '.my_sql_error($c));
}

$sql="SET NAMES 'utf8'";
my_sql_query($sql, $c);
unset($dbpasswd);

$dt=time();

//includes/startup.php
// if (version_compare(PHP_VERSION, '5.4.0-dev', '>='))
// {
// 	define('STRIP', false);
// }
// else
// {
// 	define('STRIP', (get_magic_quotes_gpc()) ? true : false);
// }

$uploaded=isset($_GET['uploaded']) ? my_int_val($_GET['uploaded']) : 0;
$downloaded=isset($_GET['downloaded']) ? my_int_val($_GET['downloaded']) : 0;
$left=isset($_GET['left']) ? my_int_val($_GET['left']) : 0;
$compact=isset($_GET['compact']) ? intval($_GET['compact']) : 0;
$no_peer_id=isset($_GET['no_peer_id']) ? intval($_GET['no_peer_id']) : 0;
$event=isset($_GET['event']) ? $_GET['event'] : '';
in_array($event, array('stopped', 'completed', 'started')) ? '' : $event='';
$peer_id=isset($_GET['peer_id']) ? $_GET['peer_id'] : '';

// STRIP ? $peer_id=stripslashes($peer_id) : '';
$l_peer_id=strlen($peer_id);
if($l_peer_id != 20)
{
	err("invalid peer_id: {$peer_id} ({$l_peer_id})");
}
$c_peer_id=$peer_id;
$peer_id=my_sql_real_escape_string($peer_id, $c);

$passkey=isset($_GET['passkey']) ? $_GET['passkey'] : '';
// STRIP ? '' : $passkey=my_sql_real_escape_string($passkey, $c);
$passkey=my_sql_real_escape_string($passkey, $c);

$info_hash=isset($_GET['info_hash']) ? $_GET['info_hash'] : '';
// STRIP ? $info_hash=stripslashes($info_hash) : '';
$l_info_hash=strlen($info_hash);
$c_info_hash=$info_hash;
if($l_info_hash != 20)
{
	err("Invalid info_hash: {$info_hash} ({$l_info_hash})");
}
$info_hash=my_sql_real_escape_string($info_hash, $c);

$seeder=($left==0) ? 1 : 0;
// $seeder && $event!='completed' ? $downloaded=0 : '';

define('USERS_TABLE', $table_prefix . 'users');
define('TRACKER_PEERS_TABLE', $table_prefix . 'tracker_peers');
define('FORUMS_TABLE', $table_prefix . 'forums');
define('POSTS_TABLE', $table_prefix . 'posts');
define('ATTACHMENTS_TABLE', $table_prefix . 'attachments');

define('ANONYMOUS', 1);
define('USER_NORMAL', 0);
define('USER_FOUNDER', 3);

$config=array();

$cache_config=t_getcache('tracker_config');
if($cache_config===false)
{
	include($tincludedir.'tconf.'.$phpEx);
}
else
{
	foreach($cache_config as $k => $v)
	{
		$config[$k]=$v;
	}
	unset($cache_config);
}

if(!$config['ppkbb_phpannounce_enabled'])
{
	err('Tracker disabled');
}

if(!$config['anonymous_announce'] && !$passkey)
{
	err('Disabled for guests');
}

$is_guest=false;
if($config['anonymous_announce'] && !$passkey)
{
	$is_guest=true;
}

if(!$is_guest && !$passkey)
{
	err('Passkey not defined');
}

// $agent=isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';
// STRIP ? $agent=stripslashes($agent) : '';
//
// $agent=substr($agent, 0, 64);
// $agent=my_sql_real_escape_string($agent, $c);

$port=isset($_GET['port']) ? my_int_val($_GET['port']) : 0;
if (!$port || $port > 65535)
{
	err("Invalid port");
}

$ip=false;
if($config['ppkbb_tciptype']==2 && isset($_GET['ip']))
{
	$ip=check_ip($_GET['ip']);
}
else if($config['ppkbb_tciptype']==1 && isset($_SERVER['HTTP_X_FORWARDED_FOR']))
{
	$ip=check_ip($_SERVER['HTTP_X_FORWARDED_FOR']);
}
if(!$ip)
{
	$ip=isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';
	if($ip!=check_ip($ip))
	{
		err('Invalid IP address');
	}
}
// !STRIP ? $ip=my_sql_real_escape_string($ip, $c) : '';
$ip=my_sql_real_escape_string($ip, $c);

define('XBT_ANNOUNCE_LOG', $config['table_announce_log']);
//define('XBT_DENY_FROM_HOSTS', $config['table_deny_from_hosts']);
//define('XBT_DENY_FROM_CLIENTS', $config['table_deny_from_clients']);
define('XBT_FILES', $config['table_files']);
define('XBT_FILES_USERS', $config['table_files_users']);
define('XBT_SCRAPE_LOG', $config['table_scrape_log']);
define('XBT_USERS', $config['table_users']);

if($is_guest)
{
	$sql='SELECT tt.* FROM '.XBT_FILES . " tt WHERE tt.info_hash='{$info_hash}' LIMIT 1";
}
else
{
	$sql='SELECT tt.*, u.uid, u.can_leech, u.wait_time, u.peers_limit, u.torrents_limit, s.left FROM '.XBT_USERS.' u LEFT JOIN ' . XBT_FILES . " tt ON (tt.info_hash='{$info_hash}') LEFT JOIN ".XBT_FILES_USERS." s ON (tt.fid=s.fid AND u.uid=s.uid) WHERE u.torrent_pass='{$passkey}' LIMIT 1";
}

$result=my_sql_query($sql, $c);
$user_torrent=my_sql_fetch_array($result, true);
my_sql_free_result($result);

$config['announce_interval'] ? '' : $config['announce_interval']=1800;
$valid_peers_time=$dt-intval($config['announce_interval']*1.25);

$unregistered_torrent=0;
$user_id=0;
if($is_guest)
{
	if(!$config['auto_register'] && !$user_torrent)
	{
		err('Torrent not registred');
	}
	$user_id=1;

	if(!$user_torrent || !$user_torrent['fid'])
	{
		$unregistered_torrent=true;
	}

}
else
{
	if(!$user_torrent)
	{
		err('User not found on tracker');
	}
	$user_id=$user_torrent['uid'];

	if(!$config['auto_register'] && !$user_torrent['fid'])
	{
		err('Torrent not registred');
	}

	if(!$user_torrent['fid'])
	{
		$unregistered_torrent=true;
	}

	if(!$unregistered_torrent)
	{
		if($user_torrent['torrents_limit'] || $user_torrent['peers_limit'])
		{
			$sql="SELECT SUM(1) torrents, SUM(IF(seeder='0', 1, 0)) leech FROM ".TRACKER_PEERS_TABLE. " WHERE userid='{$user_id}' AND last_action > {$valid_peers_time}";
			$result=my_sql_query($sql, $c);
			$user_torrents=my_sql_fetch_array($result, true);
		}

		if(!$seeder)
		{
			if(!$user_torrent['can_leech'])
			{
				err('You can\'t download torrents');
			}

			if($user_torrent['wait_time'] && $dt-$user_torrent['ctime'] < $user_torrent['wait_time'])
			{
				err('You can\'t download torrents, please wait');
			}

			if($user_torrent['torrents_limit'])
			{
// 				$sql='SELECT COUNT(*) user_leech FROM '.TRACKER_PEERS_TABLE." WHERE userid='{$user_id}' AND seeder='0' AND last_action > {$valid_peers_time}";
// 				$result=my_sql_query($sql, $c);
// 				$user_leech=my_sql_fetch_array($result, true);
// 				$user_leech=$user_leech ? my_int_val($user_leech['user_leech']) : 0;

				if($user_torrents['leech'] >= $user_torrent['torrents_limit']+1)
				{
					err("Maximum torrents to download: {$user_torrent['torrents_limit']}");
				}
			}
		}

		if($user_torrent['peers_limit'])
		{
// 			$sql='SELECT COUNT(*) user_peers FROM '.TRACKER_PEERS_TABLE." WHERE userid='{$user_id}' AND last_action > {$valid_peers_time}";
// 			$result=my_sql_query($sql, $c);
// 			$user_peers=my_sql_fetch_array($result, true);
// 			$user_peers=$user_peers ? my_int_val($user_peers['user_peers']) : 0;

			if($user_torrents['torrents'] >= $user_torrent['peers_limit']+1)
			{
				err("Maximum torrents to announce: {$user_torrent['peers_limit']}");
			}
		}
	}
}

if(!$config['auto_register'] && $unregistered_torrent)
{
	err('Only registered torrents');
}

$torrent_id=$unregistered_torrent ? 0 : $user_torrent['fid'];

if($config['ppkbb_tccheck_ban'])
{
	include($tincludedir.'tban.'.$phpEx);
}

$update_files_users=array();

if($unregistered_torrent)
{
	if(!isset($user_torrent['info_hash']))
	{
		$sql="INSERT INTO ".XBT_FILES." (info_hash, mtime, ctime, poster_id) VALUES('{$info_hash}', '{$dt}', '{$dt}', '{$user_id}')";
		my_sql_query($sql, $c);

		$unregistered_torrent=my_sql_insert_id($c);
		$numpeers=0;
	}
	else
	{
		$unregistered_torrent=$user_torrent['tid'];
		$numpeers=my_int_val($user_torrent['seeders']+$user_torrent['leechers']);
	}

// 	include($tincludedir.'tunreg.'.$phpEx);
}
else
{
	$numpeers=my_int_val($user_torrent['seeders']+$user_torrent['leechers']);

}

$self_where_select=($unregistered_torrent ? "tp.tid='{$unregistered_torrent}'" : "tp.torrent='{$torrent_id}'")
	." AND tp.userid='{$user_id}'"
		.($is_guest ? " AND tp.peer_id='{$peer_id}'" : '');
$self_where_update=($unregistered_torrent ? "tid='{$unregistered_torrent}'" : "torrent='{$torrent_id}'")
	." AND userid='{$user_id}'"
		.($is_guest ? " AND peer_id='{$peer_id}'" : '');

$sql='SELECT * FROM '.TRACKER_PEERS_TABLE." tp WHERE {$self_where_select}";
$result=my_sql_query($sql, $c);
$user_peer=my_sql_fetch_array($result, true);
my_sql_free_result($result);

$self_torrent=$user_id==$user_torrent['poster_id'] ? true : false;

if($user_peer)
{
	if(!$event && $dt - $user_peer['last_action'] < ($config['announce_interval'] - 10))
	{
		err("Minimum announce interval: {$config['announce_interval']} sec.");
	}

}

if($user_peer)
{
	$upthis=$uploaded ? my_int_val($uploaded - $user_peer['uploaded']) : 0;
	$downthis=$downloaded ? my_int_val($downloaded - $user_peer['downloaded']) : 0;

	if(!$is_guest && !$unregistered_torrent)
	{
		include($tincludedir.'taself1.'.$phpEx);
	}
}
else
{
	$upthis=$downthis=0;
	if(!$self_torrent)
	{
// 		include($tincludedir.'tanotself1.'.$phpEx);
	}
}

$update_files=array();
if($event=='stopped')
{
	if($user_peer)
	{
		include($tincludedir.'taselfstopped.'.$phpEx);
	}
}
else
{
	if($event=='completed')
	{
		$update_files[]='completed=completed+1';

		$update_files_users['completed']='completed+1';

// 		include($tincludedir.'tacompleted.'.$phpEx);
	}

	if($user_peer)
	{
		include($tincludedir.'taself2.'.$phpEx);
	}
	else
	{
		include($tincludedir.'tanotself2.'.$phpEx);
	}
}

if(!$is_guest && !$unregistered_torrent && count($update_files_users))
{
	$sql_set=array();
	foreach($update_files_users as $k => $v)
	{
		$sql_set[]=$k.'='.$v;
	}

	$sql='UPDATE '. XBT_FILES_USERS .' SET ' . implode(', ', $sql_set) . " WHERE fid='{$torrent_id}' AND uid='{$user_id}'";
	my_sql_query($sql, $c);
}

if(count($update_files))
{
	$sql='UPDATE '. XBT_FILES .' SET ' . implode(', ', $update_files) . ' WHERE '.($unregistered_torrent ? "tid='{$unregistered_torrent}'" : "fid='{$torrent_id}'");
	my_sql_query($sql, $c);
}

$limit='';
$rsize=my_int_val($config['ppkbb_tcmaxpeers_limit']);
if(!$config['ppkbb_tcmaxpeers_rewrite'])
{
	$rsize=isset($_GET['numwant']) ? my_int_val($_GET['numwant']) : 0;
}
if($rsize && $numpeers && $numpeers > $rsize)
{
	$limit="ORDER BY RAND() LIMIT {$rsize}";
}
if(!$is_guest && !$unregistered_torrent && !$config['ppkbb_tcignore_connectable'])
{
	$limit="AND tp.connectable='1' {$limit}";
}

$plist='';
$resp="d8:completei{$user_torrent['seeders']}e10:downloadedi{$user_torrent['completed']}e10:incompletei{$user_torrent['leechers']}e";
$resp .= benc_str('interval') . 'i' . $config['announce_interval'] . 'e'
	. benc_str('min interval') . 'i' . $config['announce_interval'] . 'e'
	. benc_str('peers') . ($compact ? '' : 'l');

$sql='SELECT INET_NTOA(tp.ip) ip, tp.port'
	.($no_peer_id ? '' : ', tp.peer_id')
		.' FROM '. TRACKER_PEERS_TABLE .' tp WHERE '
			.($unregistered_torrent ? "tp.tid='{$unregistered_torrent}'" : "tp.torrent='{$torrent_id}'")." AND tp.last_action > {$valid_peers_time}".($user_peer ? ($is_guest ? " AND tp.peer_id!='{$peer_id}'" : " AND tp.userid!='{$user_id}'") : '').($seeder ? " AND tp.seeder!='1'" : '')." {$limit}";
$result=my_sql_query($sql, $c);
$peers=my_sql_fetch_array($result);
foreach($peers as $row)
{
	if($compact)
	{
		$peer_ip=explode('.', $row['ip']);
		$plist .= pack("C*", $peer_ip[0], $peer_ip[1], $peer_ip[2], $peer_ip[3]). pack("n*", (int) $row['port']);
	}
	else
	{
		$resp .= "d" .
			benc_str('ip') . benc_str($row['ip']) .
			(!$no_peer_id ? benc_str('peer id') . benc_str(str_pad(stripslashes($row['peer_id']), 20)) : '') .
			benc_str('port') . "i" . $row['port'] . "ee";
	}
}
my_sql_free_result($result);

$resp .= ($compact ? benc_str($plist) : '') . 'ee';

benc_resp_raw($resp, $config['ppkbb_tcgz_rewrite']);

if(!$is_guest && !$unregistered_torrent && $config['log_announce'])
{
	$events=array(''=>0, 'completed'=>1, 'started'=>2, 'stopped'=>3);

	$sql='INSERT INTO '. XBT_ANNOUNCE_LOG ." (ipa, port, event, info_hash, peer_id, downloaded, left0, uploaded, uid, mtime) VALUES (INET_ATON('{$ip}'), '{$port}', '".$events[$event]."', '{$info_hash}', '{$peer_id}', '{$downloaded}', '{$left}', '{$uploaded}', '{$user_id}', '{$dt}')";
	my_sql_query($sql, $c);
}

// if($config['ppkbb_tccleanup_interval'] && $dt-$config['ppkbb_peers_last_cleanup'] > $config['ppkbb_tccleanup_interval'])
// {
// 	$sql='DELETE FROM '. TRACKER_PEERS_TABLE ." WHERE last_action < {$valid_peers_time}";
// 	my_sql_query($sql, $c);
// }

if($c)
{
	my_sql_close($c);
}

exit();

//############################################################

function my_int_val($v=0, $max=0, $drop=false, $negative=false)
{
	if(!$v || ($v < 0 && !$negative))
	{
		return 0;
	}
	else if($drop && $v>$max)
	{
		return 0;
	}
	else if($max && $v>$max)
	{
		return $max;
	}

	return @number_format($v+0, 0, '', '');
}

function my_float_val($v=0, $n=3, $max=0, $drop=false, $negative=false)
{
	if(!$v || ($v < 0 && !$negative))
	{
		return "0.".str_repeat('0', $n);
	}
	else if($drop && $v>$max)
	{
		return "0.".str_repeat('0', $n);
	}
	else if($max && $v>$max)
	{
		return $max;
	}

	return @number_format($v+0, $n, '.', '');
}

function t_getcache($t, $var='')
{
	global $tcachedir, $phpEx;

	$cache_data=array();
	$f_name="{$tcachedir}data_ppkbb3cker_{$t}.{$phpEx}";
	if(@file_exists($f_name))
	{
		include($f_name);

		return $var ? $$var : $cache_data;
	}

	return false;
}

function err($msg)
{
	global $c;

	if($msg)
	{
		benc_resp(array("failure reason" => array('type' => "string", 'value' => $msg)));
	}

	if($c)
	{
		my_sql_close($c);
	}

	exit();
}

function warn($msg)
{
	global $c;

	if($msg)
	{
		benc_resp(array("warning message" => array('type' => "string", 'value' => $msg)));
	}

	if($c)
	{
		my_sql_close($c);
	}

	exit();
}

function benc($obj)
{
	if(!is_array($obj) || !isset($obj['type']) || !isset($obj['value']))
	{
		return;
	}
	$c=$obj['value'];
	switch ($obj['type'])
	{
		case "string":
			return benc_str($c);
		case "integer":
			return benc_int($c);
		case "list":
			return benc_list($c);
		case "dictionary":
			return benc_dict($c);
		default:
			return;
	}
}

function benc_str($s)
{
	return strlen($s) . ":$s";
}

function benc_int($i)
{
	return "i" . $i . "e";
}

function benc_list($a)
{
	$s="l";
	foreach ($a as $e)
	{
		$s .= benc($e);
	}
	$s .= "e";
	return $s;
}

function benc_dict($d)
{
	$s="d";
	$keys=array_keys($d);
	sort($keys);
	foreach ($keys as $k)
	{
		$v=$d[$k];
		$s .= benc_str($k);
		$s .= benc($v);
	}
	$s .= "e";
	return $s;
}

function benc_resp($d)
{
	global $config;

	benc_resp_raw(benc(array('type' => "dictionary", 'value' => $d)), $config['ppkbb_tcgz_rewrite']);
}

function benc_resp_raw($x, $c=0)
{
	$gz_enc=isset($_SERVER['HTTP_ACCEPT_ENCODING']) && strstr($_SERVER['HTTP_ACCEPT_ENCODING'], 'gzip') && @extension_loaded('zlib') && (ini_get('zlib.output_compression')=='Off' || !ini_get('zlib.output_compression')) ? 1 : 0;

	header("Content-Type: text/plain");
	if($c==1 || (!$c && $gz_enc))
	{
		header("Content-Encoding: gzip");

		print gzencode($x, 9, FORCE_GZIP);
	}
	else if($c==2 || (!$c && !$gz_enc) || !$gz_enc)
	{
		header("Pragma: no-cache");

		print($x);
	}
}

function my_split_config($config, $count=0, $type=false, $split='')
{
	$count=intval($count);

	if(!$count && $config==='')
	{
		return array();
	}

	$s_config=$count > 0 ? @explode($split ? $split : ' ', $config, $count) : @explode($split ? $split : ' ', $config);
	$count=$count > 0 ? $count : count($s_config);
	if($count)
	{
		for($i=0;$i<$count;$i++)
		{
			if($type)
			{
				if(is_array($type) && @function_exists(@$type[$i]))
				{
					$s_config[$i]=call_user_func($type[$i], @$s_config[$i]);
				}
				else if(@function_exists($type))
				{
					$s_config[$i]=call_user_func($type, @$s_config[$i]);
				}
				else
				{
					$s_config[$i]=@$s_config[$i];
				}
			}
			else
			{
				$s_config[$i]=@$s_config[$i];
			}
		}
	}

	return $s_config;
}

function check_ip($ip)
{
	$long=ip2long($ip);

	if($long==-1 || $long===false)
	{
		return false;
	}

	return $ip;
}

?>
