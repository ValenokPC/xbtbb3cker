<?php
/**
*
* @package ppkBB3cker
* @version $Id: install_update.php 1.000 2012-04-19 11:06:06 PPK $
* @copyright (c) 2012 PPK
* http://www.ppkbb3cker.ru, http://protoneutron.narod.ru
*
*/

error_reporting(0);
set_time_limit(0);

define('IN_PHPBB', true);
$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

header("Content-type: text/html; charset=UTF-8");

require($phpbb_root_path . 'config.'.$phpEx);

if(!in_array($dbms, array('phpbb\\db\\driver\\mysql', 'phpbb\\db\\driver\\mysqli', 'mysql', 'mysqli')))
{
	err('Only mysql(i) supported');
}

require("{$phpbb_root_path}ext/ppk/xbtbb3cker/include/db/".(in_array($dbms, array('phpbb\\db\\driver\\mysql', 'mysql')) ? 'mysql' : 'mysqli').".{$phpEx}");

$c=my_sql_connect($dbhost.($dbport ? ":{$dbport}" : ''), $dbuser, $dbpasswd);
if(!$c)
{
	err('Error connecting database: '.my_sql_error($c).' ['.my_sql_errno($c).']');
}

$s=my_sql_select_db($dbname, $c);
if(!$s)
{
	err('Error selecting database: '.my_sql_error($c));
}
unset($dbpasswd);

define('MY_SQL_ERR_DEBUG', true);

$sql="SET NAMES 'utf8'";
my_sql_query($sql, $c);

// $result=my_sql_query('SELECT VERSION() AS version', $c);
// $mv=my_sql_fetch_row($result);
//
// $m_version=$mv[0];
//
// if(version_compare($m_version, '4.1.3', '>='))
// {
// 	$mysql_dbms=41;
// }
// else
// {
// 	$mysql_dbms=40;
// }
//
// if($mysql_dbms!=41)
// {
// 	err("This mysql version not supported, please upgrade to version 4.1.3 or higher");
// }

$steps=8;
$start=isset($_GET['start']) && $_GET['start'] ? (int) $_GET['start'] : 0;

if(!$start)
{
	err('<a href="'.$_SERVER['SCRIPT_NAME'].'?start='.($start+1).'">Start update</a><br />');
}

if($start==1)
{
// 	$sql="
// 		CREATE TABLE IF NOT EXISTS `{$table_prefix}tracker_rtrackers` (
// 		`id` int(11) unsigned NOT NULL AUTO_INCREMENT,
// 		`rtracker_url` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
// 		`rtracker_md5` varchar(32) COLLATE utf8_bin NOT NULL DEFAULT '',
// 		PRIMARY KEY (`id`),
// 		KEY `rtracker_md5` (`rtracker_md5`)
// 		) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin;
// 	";
// 	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` ADD `rtracker_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' AFTER `forb_type`, ADD INDEX (`rtracker_id`)";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` ADD `rtracker_type` ENUM('s','u','t') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 't' AFTER `rtracker_id`";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` ADD `user_torrent_zone` INT(11) UNSIGNED NOT NULL DEFAULT '0' AFTER `rtracker_type`, ADD INDEX (`user_torrent_zone`)";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` CHANGE `forb_type` `forb_type` ENUM('r','s','i','') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT ''";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` CHANGE `rtrack_remote` `rtracker_remote` TINYINT(1) NOT NULL DEFAULT '0'";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` CHANGE `rtrack_forb` `rtracker_forb` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0'";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` CHANGE `rtrack_enabled` `rtracker_enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1'";
	$result=my_sql_query($sql, $c);

	next_step($start, $steps);

}
else if($start==2)
{
	$sql="SELECT * FROM `{$table_prefix}tracker_rtrack` GROUP BY rtrack_url";
	$result=my_sql_query($sql, $c);
	$rtrackurl_row=my_sql_fetch_array($result);
	foreach($rtrackurl_row as $row)
	{
		if(strlen($row['rtrack_url']) < 256)
		{
			$sql="INSERT INTO `{$table_prefix}tracker_rtrackers` (id, rtracker_url) VALUES(NULL, '{$row['rtrack_url']}')";
			$result2=my_sql_query($sql, $c);
		}
	}
	my_sql_free_result($result);

	next_step($start, $steps);

}
else if($start==3)
{
	$sql="UPDATE `{$table_prefix}tracker_rtrackers` SET rtracker_md5=MD5(rtracker_url)";
	$result=my_sql_query($sql, $c);

	next_step($start, $steps);

}
else if($start==4)
{
	//torrent
	$sql="UPDATE `{$table_prefix}tracker_rtrack` SET rtracker_type='t', user_torrent_zone=torrent, forb_type='', rtracker_enabled=1 WHERE torrent!=0";
	$result=my_sql_query($sql, $c);

	//user
	$sql="UPDATE `{$table_prefix}tracker_rtrack` SET rtracker_type='u', user_torrent_zone=zone_id, forb_type='', rtracker_remote=0 WHERE zone_id!=0 AND rtrack_user=1";
	$result=my_sql_query($sql, $c);

	//zone
	$sql="UPDATE `{$table_prefix}tracker_rtrack` SET rtracker_type='s', user_torrent_zone=zone_id, forb_type='', rtracker_remote=0 WHERE zone_id!=0 AND rtrack_user=0 AND torrent=0";
	$result=my_sql_query($sql, $c);

	//system
	$sql="UPDATE `{$table_prefix}tracker_rtrack` SET rtracker_type='s', user_torrent_zone=0 WHERE zone_id=0 AND rtrack_user=0 AND torrent=0";
	$result=my_sql_query($sql, $c);

	next_step($start, $steps);

}
else if($start==5)
{
	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` DROP `torrent`";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` DROP `zone_id`";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` DROP `rtrack_user`";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` DROP INDEX `rtrack_en`";
	$result=my_sql_query($sql, $c);

	next_step($start, $steps);

}
else if($start==6)
{
	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` ADD `rtracker_md5` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' AFTER `user_torrent_zone`, ADD INDEX (`rtracker_md5`)";
	$result=my_sql_query($sql, $c);

	$sql="UPDATE `{$table_prefix}tracker_rtrack` SET rtracker_md5=MD5(rtrack_url)";
	$result=my_sql_query($sql, $c);

	next_step($start, $steps);
}
else if($start==7)
{
	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` DROP `id`";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` DROP `rtrack_url`";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rannounces` DROP INDEX `track`";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE  `{$table_prefix}tracker_rannounces` ADD INDEX (  `tracker` )";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE  `{$table_prefix}tracker_rannounces` ADD INDEX (  `torrent` )";
	$result=my_sql_query($sql, $c);

// 	$sql="ALTER TABLE  `{$table_prefix}tracker_rtrack` ADD INDEX (  `rtracker_type` )";
// 	$result=my_sql_query($sql, $c);

	next_step($start, $steps);
}
else if($start==8)
{
	$sql="UPDATE `{$table_prefix}tracker_rtrack` a LEFT JOIN `{$table_prefix}tracker_rtrackers` b ON a.rtracker_md5 = b.rtracker_md5 SET rtracker_id = b.id";
	$result=my_sql_query($sql, $c);

	$sql="ALTER TABLE `{$table_prefix}tracker_rtrack` DROP `rtracker_md5`";
	$result=my_sql_query($sql, $c);

	$sql="DELETE FROM `{$table_prefix}config` WHERE config_name='ppkbb_manual_update'";
	$result=my_sql_query($sql, $c);

	err('<b>Completed</b><br />');
}

my_sql_close($c);
exit();

function next_step($current, $steps=0)
{
	err('<a href="'.$_SERVER['SCRIPT_NAME'].'?start='.($current+1).'">Next >></a>'.($steps ? ' ('.$current.' of '.$steps.')' : '').'<br />');
}

function err($str)
{
	global $c;

	echo $str;

	if($c)
	{
		my_sql_close($c);
	}

	exit();
}

?>
