<?php

function my_sql_connect($dbhost, $dbuser, $dbpasswd)
{
	return @mysqli_connect($dbhost, $dbuser, $dbpasswd);
}

function my_sql_select_db($dbname, $c)
{
	return @mysqli_select_db($c, $dbname);
}

function my_sql_query($sql, $c)
{
	global $result_file, $success_file, $log_success;

	$result=@mysqli_query($c, $sql);

	if(!$result)
	{
// 		err('Sql error ['.mysqli_errno($c).']');
// 		mysqli_close($c);
		$error='<span style="color:#FF0000;font-weight:bold">Ошибка</span> '.$sql.'<br />&raquo; '.mysqli_error($c).' ['.mysqli_errno($c).']<br />';
		echo $error;

		$fo=@fopen($result_file, 'a+');
		if($fo)
		{
			fwrite($fo, $error."\n");
			fclose($fo);
		}
	}
	else
	{
		$success='<b>Успешно</b> '.$sql.'<br />';

		if($log_success)
		{
			$fo=@fopen($success_file, 'a+');
			if($fo)
			{
				fwrite($fo, $success."\n");
				fclose($fo);
			}
		}
		echo $success;
	}

	return $result;
}

function my_sql_numrows($result)
{
	return mysqli_num_rows($result);
}

function my_sql_fetch_row($result, $first_row=false)
{
	$fetch_row=array();
	while($row=mysqli_fetch_row($result))
	{
		$fetch_row[]=$row;
	}

	$first_row && count($fetch_row) ? $fetch_row=$fetch_row[0] : '';

	return $fetch_row;
}

function my_sql_free_result($result)
{
	mysqli_free_result($result);
}

function my_sql_real_escape_string($string, $c)
{
	return mysqli_real_escape_string($c, $string);
}

function my_sql_fetch_array($result, $first_row=false)
{
	$fetch_array=array();
	while($row=mysqli_fetch_assoc($result))
	{
		$fetch_array[]=$row;
	}

	$first_row && count($fetch_array) ? $fetch_array=$fetch_array[0] : '';

	return $fetch_array;
}

function my_sql_affected_rows($c)
{
	return mysqli_affected_rows($c);
}

function my_sql_insert_id($c)
{
	return mysqli_insert_id($c);
}

function my_sql_close($c)
{
	mysqli_close($c);
}

function my_sql_error($c)
{
	return mysqli_error($c);
}

function my_sql_errno($c)
{
	return mysqli_errno($c);
}

?>