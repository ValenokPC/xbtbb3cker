<?php

function my_sql_connect($dbhost, $dbuser, $dbpasswd)
{
	return @mysql_connect($dbhost, $dbuser, $dbpasswd);
}

function my_sql_select_db($dbname, $c)
{
	return @mysql_select_db($dbname, $c);
}

function my_sql_query($sql, $c)
{
	global $result_file, $success_file, $log_success;

	$result=@mysql_query($sql, $c);

	if(!$result)
	{
// 		err('Sql error ['.mysql_errno($c).']');
// 		mysql_close($c);
		$error='<span style="color:#FF0000;font-weight:bold">Ошибка</span> '.$sql.'<br />&raquo; '.mysql_error($c).' ['.mysql_errno($c).']<br />';
		echo $error;

		$fo=@fopen($result_file, 'a+');
		if($fo)
		{
			fwrite($fo, $error."\n");
			fclose($fo);
		}
	}
	else
	{
		$success='<b>Успешно</b> '.$sql.'<br />';

		if($log_success)
		{
			$fo=@fopen($success_file, 'a+');
			if($fo)
			{
				fwrite($fo, $success."\n");
				fclose($fo);
			}
		}
		echo $success;
	}

	return $result;
}

function my_sql_numrows($result)
{
	return mysql_num_rows($result);
}

function my_sql_fetch_row($result, $first_row=false)
{
	$fetch_row=array();
	while($row=mysql_fetch_row($result))
	{
		$fetch_row[]=$row;
	}

	$first_row && count($fetch_row) ? $fetch_row=$fetch_row[0] : '';

	return $fetch_row;
}

function my_sql_free_result($result)
{
	mysql_free_result($result);
}

function my_sql_real_escape_string($string, $c)
{
	return mysql_real_escape_string($string, $c);
}

function my_sql_fetch_array($result, $first_row=false)
{
	$fetch_array=array();
	while($row=mysql_fetch_array($result, MYSQL_ASSOC))
	{
		$fetch_array[]=$row;
	}

	$first_row && count($fetch_array) ? $fetch_array=$fetch_array[0] : '';

	return $fetch_array;
}

function my_sql_affected_rows($c)
{
	return mysql_affected_rows($c);
}

function my_sql_insert_id($c)
{
	return mysql_insert_id($c);
}

function my_sql_close($c)
{
	mysql_close($c);
}

function my_sql_error($c)
{
	return mysql_error($c);
}

function my_sql_errno($c)
{
	return mysql_errno($c);
}

?>