<?php

error_reporting(0);
@ini_set('register_globals', 0);
@ini_set('magic_quotes_runtime', 0);
@ini_set('magic_quotes_sybase', 0);
@set_time_limit(0);

function_exists('date_default_timezone_set') ? date_default_timezone_set('Europe/Moscow') : '';

define('IN_PHPBB', true);

$phpbb_root_path = (defined('PHPBB_ROOT_PATH')) ? PHPBB_ROOT_PATH : './../';
$phpEx = substr(strrchr(__FILE__, '.'), 1);

require($phpbb_root_path . 'config.'.$phpEx);

if(!in_array($dbms, array('phpbb\\db\\driver\\mysql', 'phpbb\\db\\driver\\mysqli', 'mysql', 'mysqli')))
{
	err('Only mysql(i) supported');
}

require("{$phpbb_root_path}install/db/".(in_array($dbms, array('phpbb\\db\\driver\\mysql', 'mysql')) ? 'mysql' : 'mysqli').".{$phpEx}");

$c=my_sql_connect($dbhost.($dbport ? ":{$dbport}" : ''), $dbuser, $dbpasswd);
if(!$c)
{
	err('Error connecting database: '.my_sql_error($c).' ['.my_sql_errno($c).']');
}

$s=my_sql_select_db($dbname, $c);
if(!$s)
{
	err('Error selecting database: '.my_sql_error($c));
}
unset($dbpasswd);
if (!defined('PHPBB_ENVIRONMENT'))
{
	@define('PHPBB_ENVIRONMENT', 'production');
}
$page_url=$phpbb_root_path.'install/ppkbb3cker_30to31.'.$phpEx;
$result_file=$phpbb_root_path.'cache/'.PHPBB_ENVIRONMENT.'/data_ppkbb3cker_30to31.php';
$success_file=$phpbb_root_path.'cache/'.PHPBB_ENVIRONMENT.'/data_ppkbb3cker_30to31_success.php';

$step=isset($_GET['step']) ? intval($_GET['step']) : 0;
$start=isset($_GET['start']) ? intval($_GET['start']) : 0;
$stop=isset($_GET['stop']) ? intval($_GET['stop']) : 0;

$steps_descr=array(
	1=>'Выполнение общих запросов',
	2=>'Импорт внешних изображений',
	3=>'Синхронизация торрентов',
	4=>'Синхронизация изображений',
	5=>'Синхронизация пользователей',
	6=>'Синхронизация статистики',
	7=>'Синхронизация логов',
	8=>'Импорт спасибо',
	9=>'Удаление неиспользуемых данных',
	10=>'Результат',
);
$steps=count($steps_descr);

//##############################################################################
$pause=5;//пауза между шагами (сек.)
$steps_pause=array(
	2=>1,//пауза в шаге 2 (сек.)
	3=>1,//пауза в шаге 3 (сек.)
	4=>1,//пауза в шаге 4 (сек.)
	5=>1,//пауза в шаге 5 (сек.)
	6=>1,//пауза в шаге 6 (сек.)
	7=>1,//пауза в шаге 7 (сек.)
	8=>1,//пауза в шаге 8 (сек.)
);
$images_per_once=100;//изображений за один раз
$torrents_per_once=100;//торрентов за один раз
$users_per_once=200;//пользователей за один раз
$snatch_per_once=100;//записей статистики за один раз
$logs_per_once=100;//записей логов за один раз
$posts_per_once=100;//сообщений за один раз
$thanks_per_once=100;//спасибо за один раз

$log_success=false;//логировать успешные sql запросы (не рекомендуется)

//true - да, false - нет
$delete_other=array(
	'addfields'=>false,//удалить данные доп. полей
	'lsic'=>true,//удалить данные мода списка подфорумов в колонку
	'similar'=>true,//удалить данные мода похожих тем
	'fpep'=>true,//удалить данные мода первого сообщения на каждой странице
	'chat'=>true,//удалить данные чата
	'thanks'=>true,//true - удалить данные "спасибо за торрент", false - импортировать в расширение Thanks for posts
	'board3'=>true,//удалить данные мода портала
	'cbb'=>true,//удалить данные мода портала cbb
);
//##############################################################################

script_header();

if(!$step)
{
	echo '<a href="'.$page_url.'?step=1">Начать &raquo;<a/>';
	echo '<br /><br /><b>Будут выполнены следующие шаги</b>:';

	foreach($steps_descr as $s => $d)
	{
		if($s==$steps)
		{
			continue;
		}
		echo '<br /><a href="'.$page_url.'?step='.$s.'&amp;stop=1" title="Выполнить только этот шаг">'.$d.'</a>';
	}

	$fo=@fopen($result_file, 'w');
	if($fo)
	{
		fwrite($fo, '');
		fclose($fo);
	}

	$fo=@fopen($success_file, 'w');
	if($fo)
	{
		fwrite($fo, '');
		fclose($fo);
	}
}
else
{

	$step_text='<u>Шаг '.$step.' из '.$steps.': <b>'.$steps_descr[$step].'</b></u><br /><br />';

	echo $step_text;

	if($log_success && $step < $steps)
	{
		$fo=@fopen($success_file, 'a+');
		if($fo)
		{
			fwrite($fo, '<br />'.$step_text."\n");
			fclose($fo);
		}

	}

	if($step && $step < $steps)
	{
		$sql="SET NAMES 'utf8'";
		my_sql_query($sql, $c);

		$sql='SELECT config_value FROM '.$table_prefix."tracker_config WHERE config_name='ppkbb_ppkbb3cker_version'";
		$result=my_sql_query($sql, $c);
		$tracker_version=my_sql_fetch_array($result, true);
		my_sql_free_result($result);
		$tracker_version=explode(' ', $tracker_version['config_value']);
		$tracker_type=$tracker_version[1];
	}

	if($step==1)
	{
		$sql="ALTER TABLE  `{$table_prefix}tracker_files` CHANGE  `id`  `torrent` INT( 10 ) UNSIGNED NOT NULL DEFAULT  '0'";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE  `{$table_prefix}tracker_files` ADD  `id` INT( 11 ) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}tracker_peers` CHANGE  `peer_id`  `peer_id` VARBINARY( 255 ) NOT NULL DEFAULT  ''";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}tracker_peers` DROP `rights`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}tracker_peers` ADD `tid` INT( 11 ) UNSIGNED NOT NULL DEFAULT '0'";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}tracker_peers` ADD INDEX (  `tid` )";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}tracker_peers` DROP INDEX seeder";
		my_sql_query($sql, $c);

		$sql="UPDATE `{$table_prefix}forums` SET forum_tracker=1 WHERE forumas=1";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}forums` DROP `forumas`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE  `{$table_prefix}tracker_rtrack` CHANGE  `forb_type`  `forb_type` VARCHAR( 1 ) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT  ''";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}tracker_rannounces` DROP INDEX torrent";
		my_sql_query($sql, $c);

		if($tracker_type=='donate')
		{
			$sql="ALTER TABLE `{$table_prefix}tracker_statuses` DROP `newtorrents_freeze`";
			my_sql_query($sql, $c);
		}

		$sql="ALTER TABLE  `{$table_prefix}tracker_statuses` CHANGE  `status_id`  `status_id` TINYINT( 2 ) NOT NULL DEFAULT  '0'";
		my_sql_query($sql, $c);

		$sql='SELECT id, status_mark FROM '.$table_prefix."tracker_statuses";
		$result=my_sql_query($sql, $c);
		$status_row=my_sql_fetch_array($result);
		foreach($status_row as $row)
		{
			$sql='UPDATE '.$table_prefix."tracker_statuses SET status_mark='".my_sql_real_escape_string(htmlspecialchars($row['status_mark']), $c)."' WHERE id='{$row['id']}'";
			my_sql_query($sql, $c);
		}
		my_sql_free_result($result);

		if($stop)
		{
			show_errors($result_file, $success_file, $log_success);
			script_footer();
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==2)
	{
		$sql='SELECT COUNT(*) images_count FROM '.$table_prefix."attachments WHERE i_external=1";
		$result=my_sql_query($sql, $c);
		$images_count=my_sql_fetch_array($result, true);
		$images_count=intval($images_count['images_count']);

		echo "<br />Всего изображений: {$images_count}<br /><br />";

		if($images_count)
		{
			if($start < $images_count)
			{
				$next=$start+$images_per_once;

				$sql='SELECT * FROM '.$table_prefix."attachments WHERE i_external=1 ORDER BY attach_id LIMIT {$start}, {$images_per_once}";
				$result=my_sql_query($sql, $c);
				$attach_row=my_sql_fetch_array($result);
				foreach($attach_row as $row)
				{
					$sql='INSERT INTO '.$table_prefix."tracker_images (post_msg_id, topic_id, poster_id, i_width, i_height, i_poster, real_filename, extension, mimetype, filesize, filetime)
						VALUES({$row['post_msg_id']}, {$row['topic_id']}, {$row['poster_id']}, {$row['i_width']}, {$row['i_height']}, ".($row['i_poster']==1 ? 1 : 0).", '{$row['real_filename']}', '{$row['extension']}', '{$row['mimetype']}', {$row['filesize']}, {$row['filetime']})";
					my_sql_query($sql, $c);
				}
				my_sql_free_result($result);

				$url=$page_url.'?step='.$step.($stop ? '&stop=1' : '').'&start='.$next;
				echo "
				<script>
					function redirect()
					{
						window.location.href='".$url."';
					}
					setTimeout(redirect, ".($steps_pause[$step]*1000).");
				</script>
				";
				echo '<br />Изображения с '.($start+1).' по '.($next > $images_count ? $images_count : $next);

				script_footer(false);
			}
		}
		$sql='DELETE FROM '.$table_prefix."attachments WHERE i_external=1";
		my_sql_query($sql, $c);

		$sql='UPDATE '.$table_prefix."attachments SET i_poster=1 WHERE i_poster=2";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}attachments` DROP `i_width`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}attachments`DROP `i_height`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}attachments`DROP `i_external`";
		my_sql_query($sql, $c);

		if($stop)
		{
			show_errors($result_file, $success_file, $log_success);
			script_footer();
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==3)
	{
		$sql='SELECT COUNT(*) torrents_count FROM '.$table_prefix."tracker_torrents WHERE unreg=0 AND post_msg_id!=0 AND topic_id!=0";
		$result=my_sql_query($sql, $c);
		$torrents_count=my_sql_fetch_array($result, true);
		$torrents_count=intval($torrents_count['torrents_count']);

		echo "<br />Всего торрентов: {$torrents_count}<br /><br />";

		if($torrents_count)
		{
			if($start < $torrents_count)
			{
				$next=$start+$torrents_per_once;
				$posts_update=$topics_update=array();

				$sql='SELECT * FROM '.$table_prefix."tracker_torrents WHERE unreg=0 AND post_msg_id!=0 AND topic_id!=0 ORDER BY id LIMIT {$start}, {$torrents_per_once}";
				$result=my_sql_query($sql, $c);
				$attach_row=my_sql_fetch_array($result);
				foreach($attach_row as $row)
				{
					if(!$row['info_hash'])
					{
						continue;
					}

					$posts_update[]=$row['post_msg_id'];
					$topics_update[]=$row['topic_id'];

					$sql="INSERT INTO xbt_files (fid, info_hash, leechers, seeders, completed, mtime, ctime, post_msg_id, topic_id, poster_id, numfiles, size, private, forum_id, lastcleanup, rem_seeders, rem_leechers, rem_times_completed, lastremote) VALUES('{$row['id']}', '".my_sql_real_escape_string($row['info_hash'], $c)."', '{$row['leechers']}', '{$row['seeders']}', '{$row['times_completed']}', '{$row['added']}', '{$row['added']}', '{$row['post_msg_id']}', '{$row['topic_id']}', '{$row['poster_id']}', '{$row['numfiles']}', '{$row['size']}', '{$row['private']}', '{$row['forum_id']}', '{$row['lastcleanup']}', '{$row['rem_seeders']}', '{$row['rem_leechers']}', '{$row['rem_times_completed']}', '{$row['lastremote']}')";
					my_sql_query($sql, $c);

					$sql='UPDATE '.$table_prefix."topics SET torrent_status='{$row['forb']}', status_author='{$row['forb_user_id']}', status_dt='{$row['forb_date']}', status_reason='".my_sql_real_escape_string($row['forb_reason'], $c)."', torrent_reqratio='{$row['req_ratio']}', torrent_requpload='{$row['req_upload']}' WHERE topic_id='{$row['topic_id']}'";
					my_sql_query($sql, $c);
				}
				my_sql_free_result($result);

				if($posts_update)
				{
					$sql='UPDATE '.$table_prefix.'posts SET post_torrent=1 WHERE post_id IN('.implode(', ', $posts_update).')';
					my_sql_query($sql, $c);
				}
				if($topics_update)
				{
					$sql='UPDATE '.$table_prefix.'topics SET topic_torrent=1 WHERE topic_id IN('.implode(', ', $topics_update).')';
					my_sql_query($sql, $c);
				}

				$url=$page_url.'?step='.$step.($stop ? '&stop=1' : '').'&start='.$next;
				echo "
				<script>
					function redirect()
					{
						window.location.href='".$url."';
					}
					setTimeout(redirect, ".($steps_pause[$step]*1000).");
				</script>
				";
				echo '<br />Торренты с '.($start+1).' по '.($next > $torrents_count ? $torrents_count : $next);

				script_footer(false);
			}
		}

		if($stop)
		{
			show_errors($result_file, $success_file, $log_success);
			script_footer();
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==4)
	{
		$sql='SELECT COUNT(*) posts_count FROM '.$table_prefix."posts WHERE post_torrent=1";
		$result=my_sql_query($sql, $c);
		$posts_count=my_sql_fetch_array($result, true);
		$posts_count=intval($posts_count['posts_count']);

		echo "<br />Всего сообщений: {$posts_count}<br /><br />";

		if($posts_count)
		{
			if($start < $posts_count)
			{
				$next=$start+$posts_per_once;
				$posts_id=array();
				$sql='SELECT post_id FROM '.$table_prefix."posts WHERE post_torrent=1 ORDER BY post_id LIMIT {$start}, {$posts_per_once}";
				$result=my_sql_query($sql, $c);
				$post_row=my_sql_fetch_array($result);
				foreach($post_row as $row)
				{
					$posts_id[]=$row['post_id'];
				}
				my_sql_free_result($result);

				if($posts_id)
				{
					$posters=array();
					$sql='SELECT post_msg_id FROM '.$table_prefix.'attachments WHERE i_poster=1 AND post_msg_id IN('.implode(', ', $posts_id).')';
					$result=my_sql_query($sql, $c);
					$poster_row=my_sql_fetch_array($result);
					foreach($poster_row as $row)
					{
						$posters[$row['post_msg_id']]=110;
					}
					my_sql_free_result($result);

					$sql='SELECT post_msg_id FROM '.$table_prefix.'tracker_images WHERE i_poster=1 AND post_msg_id IN('.implode(', ', $posts_id).')';
					$result=my_sql_query($sql, $c);
					$poster_row=my_sql_fetch_array($result);
					foreach($poster_row as $row)
					{
						$posters[$row['post_msg_id']]=isset($posters[$row['post_msg_id']]) ? 111 : 101;
					}
					my_sql_free_result($result);

					if($posters)
					{
						foreach($posters as $post_id => $post_poster)
						{
							$sql='UPDATE '.$table_prefix."posts SET post_poster={$post_poster} WHERE post_id={$post_id}";
							my_sql_query($sql, $c);
						}
					}
				}

				$url=$page_url.'?step='.$step.($stop ? '&stop=1' : '').'&start='.$next;
				echo "
				<script>
					function redirect()
					{
						window.location.href='".$url."';
					}
					setTimeout(redirect, ".($steps_pause[$step]*1000).");
				</script>
				";
				echo '<br />Сообщения с '.($start+1).' по '.($next > $posts_count ? $posts_count : $next);

				script_footer(false);
			}
		}

		if($stop)
		{
			show_errors($result_file, $success_file, $log_success);
			script_footer();
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==5)
	{
		$sql='SELECT COUNT(*) users_count FROM '.$table_prefix."users WHERE user_type!=2";
		$result=my_sql_query($sql, $c);
		$users_count=my_sql_fetch_array($result, true);
		$users_count=intval($users_count['users_count']);

		echo "<br />Всего пользователей: {$users_count}<br /><br />";

		if(!$start)
		{
			$sql="TRUNCATE TABLE xbt_users";
			my_sql_query($sql, $c);
		}

		if($users_count)
		{
			if($start < $users_count)
			{
				$next=$start+$users_per_once;
				$sql='SELECT user_id, user_passkey, user_downloaded, user_uploaded FROM '.$table_prefix."users WHERE user_type!=2 ORDER BY user_id LIMIT {$start}, {$users_per_once}";
				$result=my_sql_query($sql, $c);
				$user_row=my_sql_fetch_array($result);
				foreach($user_row as $row)
				{
					if(!$row['user_passkey'])
					{
						$row['user_passkey']=strtolower(md5(gen_rand_string(rand(8, 16)).($row['user_id']+time()).gen_rand_string(rand(8, 16)).$row['user_id'].gen_rand_string(rand(8, 16)).(time()-$row['user_id']).gen_rand_string(rand(8, 16))));
					}

					$sql="INSERT INTO xbt_users (uid, torrent_pass, downloaded, uploaded) VALUES('{$row['user_id']}', '".my_sql_real_escape_string($row['user_passkey'], $c)."', '{$row['user_downloaded']}', '{$row['user_uploaded']}')";
					my_sql_query($sql, $c);
				}
				my_sql_free_result($result);

				$url=$page_url.'?step='.$step.($stop ? '&stop=1' : '').'&start='.$next;
				echo "
				<script>
					function redirect()
					{
						window.location.href='".$url."';
					}
					setTimeout(redirect, ".($steps_pause[$step]*1000).");
				</script>
				";
				echo '<br />Пользователи с '.($start+1).' по '.($next > $users_count ? $users_count : $next);

				script_footer(false);
			}
		}

		if($stop)
		{
			show_errors($result_file, $success_file, $log_success);
			script_footer();
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==6)
	{
		$sql='SELECT COUNT(*) snatch_count FROM '.$table_prefix."tracker_snatched";
		$result=my_sql_query($sql, $c);
		$snatch_count=my_sql_fetch_array($result, true);
		$snatch_count=intval($snatch_count['snatch_count']);

		echo "<br />Всего записей: {$snatch_count}<br /><br />";

		if($snatch_count)
		{
			if($start < $snatch_count)
			{
				$next=$start+$snatch_per_once;
				$sql='SELECT torrent, userid, finished, downloaded, to_go, uploaded, last_action  FROM '.$table_prefix."tracker_snatched ORDER BY id LIMIT {$start}, {$snatch_per_once}";
				$result=my_sql_query($sql, $c);
				$snatch_row=my_sql_fetch_array($result);
				foreach($snatch_row as $row)
				{
					$sql="INSERT INTO xbt_files_users (fid, uid, completed, downloaded, `left`, uploaded, mtime) VALUES('{$row['torrent']}', '{$row['userid']}', '{$row['finished']}', '{$row['downloaded']}', '{$row['to_go']}', '{$row['uploaded']}', '{$row['last_action']}')";
					my_sql_query($sql, $c);
				}
				my_sql_free_result($result);

				$url=$page_url.'?step='.$step.($stop ? '&stop=1' : '').'&start='.$next;
				echo "
				<script>
					function redirect()
					{
						window.location.href='".$url."';
					}
					setTimeout(redirect, ".($steps_pause[$step]*1000).");
				</script>
				";
				echo '<br />Записи с '.($start+1).' по '.($next > $snatch_count ? $snatch_count : $next);

				script_footer(false);
			}
		}

		if($stop)
		{
			show_errors($result_file, $success_file, $log_success);
			script_footer();
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==7)
	{
		if($tracker_type=='donate')
		{
			if(!$start)
			{
				$sql="ALTER TABLE  `{$table_prefix}tracker_stats` ADD PRIMARY KEY (  `id` )";
				my_sql_query($sql, $c);

				$sql="ALTER TABLE  `{$table_prefix}tracker_stats` CHANGE  `id`  `id` INT( 10 ) UNSIGNED NOT NULL AUTO_INCREMENT";
				my_sql_query($sql, $c);

			}

			$sql='SELECT COUNT(*) logs_count FROM '.$table_prefix.'tracker_stats s, '.$table_prefix."tracker_torrents t WHERE s.torrent=t.id";
			$result=my_sql_query($sql, $c);
			$logs_count=my_sql_fetch_array($result, true);
			$logs_count=intval($logs_count['logs_count']);

			echo "<br />Всего записей: {$logs_count}<br /><br />";

			if($logs_count)
			{
				if($start < $logs_count)
				{
					//(0=>'NONE', 1=>'COMPLETED', 2=>'STARTED', 3=>'STOPPED') xbt
					//(0=>'', 1=>'<span style="color:#0000FF">started</span>', 2=>'<span style="color:#FF0000">stopped</span>', 3=>'<span style="color:#00FF00">completed</span>') ppk
					$event_map=array(0=>0, 1=>2, 2=>3, 3=>1);
					$next=$start+$logs_per_once;
					$sql='SELECT s.torrent, s.userid, s.uploaded, s.downloaded, s.to_go, s.event, s.added, t.info_hash FROM '.$table_prefix.'tracker_stats s, '.$table_prefix."tracker_torrents t WHERE s.torrent=t.id ORDER BY s.id LIMIT {$start}, {$logs_per_once}";
					$result=my_sql_query($sql, $c);
					$log_row=my_sql_fetch_array($result);
					foreach($log_row as $row)
					{
						$sql="INSERT INTO xbt_announce_log (event, info_hash, downloaded, left0, uploaded, uid, mtime) VALUES('{$event_map[$row['event']]}', '".my_sql_real_escape_string($row['info_hash'], $c)."', '{$row['downloaded']}', '{$row['to_go']}', '{$row['uploaded']}', '{$row['userid']}', '{$row['added']}')";
						my_sql_query($sql, $c);
					}
					my_sql_free_result($result);

					$url=$page_url.'?step='.$step.($stop ? '&stop=1' : '').'&start='.$next;
					echo "
					<script>
						function redirect()
						{
							window.location.href='".$url."';
						}
						setTimeout(redirect, ".($steps_pause[$step]*1000).");
					</script>
					";
					echo '<br />Записи с '.($start+1).' по '.($next > $logs_count ? $logs_count : $next);

					script_footer(false);
				}
			}

			if($stop)
			{
				show_errors($result_file, $success_file, $log_success);
				script_footer();
			}
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==8)
	{
		$source_thanks_table=$table_prefix.'tracker_thanks';//tracker_thanks, user_thanks

		if(isset($delete_other['thanks']) && $delete_other['thanks'])
		{
			$sql='SELECT COUNT(*) thanks_count FROM '.$source_thanks_table;
			$result=my_sql_query($sql, $c);
			$thanks_count=my_sql_fetch_array($result, true);
			$thanks_count=intval($thanks_count['thanks_count']);

			echo "<br />Всего спасибо: {$thanks_count}<br /><br />";

			if($thanks_count)
			{
				if($start < $thanks_count)
				{
					$next=$start+$thanks_per_once;
					$sql='SELECT t.*, p.topic_id, p.forum_id FROM '.$source_thanks_table." t, ".$table_prefix."posts p WHERE t.post_id=p.post_id ORDER BY t.id LIMIT {$start}, {$thanks_per_once}";
					$result=my_sql_query($sql, $c);
					$thank_row=my_sql_fetch_array($result);
					foreach($thank_row as $row)
					{
						$sql='INSERT IGNORE INTO '.$table_prefix."thanks VALUES('{$row['post_id']}', '{$row['to_user']}', '{$row['user_id']}', '{$row['topic_id']}', '{$row['forum_id']}', '{$row['tadded']}')";
						my_sql_query($sql, $c);
					}
					my_sql_free_result($result);

					$url=$page_url.'?step='.$step.($stop ? '&stop=1' : '').'&start='.$next;
					echo "
					<script>
						function redirect()
						{
							window.location.href='".$url."';
						}
						setTimeout(redirect, ".($steps_pause[$step]*1000).");
					</script>
					";
					echo '<br />Записи с '.($start+1).' по '.($next > $thanks_count ? $thanks_count : $next);

					script_footer(false);
				}
			}
		}

		$sql="ALTER TABLE `{$table_prefix}users` DROP `user_fromthanks_count`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}users` DROP `user_tothanks_count`";
		my_sql_query($sql, $c);

		$sql="DROP TABLE `{$source_thanks_table}`";
		my_sql_query($sql, $c);

		if($stop)
		{
			show_errors($result_file, $success_file, $log_success);
			script_footer();
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==9)
	{

		if(isset($delete_other['addfields']) && $delete_other['addfields'])
		{
			$sql="ALTER TABLE `{$table_prefix}forums` DROP `forum_addfields`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}tracker_afftpl`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}tracker_afsets`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}tracker_aftpl`";
			my_sql_query($sql, $c);

			if($tracker_type=='donate')
			{
				$sql="DROP TABLE `{$table_prefix}tracker_af`";
				my_sql_query($sql, $c);

				$sql="ALTER TABLE `{$table_prefix}forums` DROP `addf_type`";
				my_sql_query($sql, $c);
			}

		}

		if(isset($delete_other['lsic']) && $delete_other['lsic'])
		{
			$sql="ALTER TABLE `{$table_prefix}forums` DROP `forum_subforumslist_type`";
			my_sql_query($sql, $c);
		}

		if(isset($delete_other['similar']) && $delete_other['similar'])
		{
			$sql="ALTER TABLE `{$table_prefix}forums` DROP `similar_topic_forums`";
			my_sql_query($sql, $c);

			$sql="DELETE FROM `{$table_prefix}config` WHERE config_name LIKE '%similar_topics%'";
			my_sql_query($sql, $c);
		}

		if(isset($delete_other['fpep']) && $delete_other['fpep'])
		{
			$sql="ALTER TABLE `{$table_prefix}forums` DROP `forum_first_post_show`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}topics` DROP `topic_first_post_show`";
			my_sql_query($sql, $c);
		}

		if(isset($delete_other['chat']) && $delete_other['chat'])
		{
			if($tracker_type=='donate')
			{
				$sql="DROP TABLE `{$table_prefix}tracker_chatmessages`";
				my_sql_query($sql, $c);

				$sql="DROP TABLE `{$table_prefix}tracker_chatusers`";
				my_sql_query($sql, $c);
			}
			else
			{
				$sql="DROP TABLE `{$table_prefix}ppkchat_messages`";
				my_sql_query($sql, $c);

				$sql="DROP TABLE `{$table_prefix}ppkchat_users`";
				my_sql_query($sql, $c);
			}

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_chatkey`";
			my_sql_query($sql, $c);

			$sql="DELETE FROM `{$table_prefix}config` WHERE config_name LIKE '%chat%'";
			my_sql_query($sql, $c);
		}

		if(isset($delete_other['board3']) && $delete_other['board3'])
		{
			$sql="DROP TABLE `{$table_prefix}portal_modules`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}portal_config`";
			my_sql_query($sql, $c);

			$sql="DELETE FROM `{$table_prefix}config` WHERE config_name LIKE '%board3%'";
			my_sql_query($sql, $c);
		}

		if(isset($delete_other['cbb']) && $delete_other['cbb'] && $tracker_type=='donate')
		{
			$sql="DROP TABLE `{$table_prefix}blockgets_blocks`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}blockgets_events`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}blockgets_lists`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}blockgets_lists_data`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}blockgets_pages`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}blockgets_plugins`";
			my_sql_query($sql, $c);

			$sql="DELETE FROM `{$table_prefix}config` WHERE config_name LIKE 'blg%'";
			my_sql_query($sql, $c);
		}

		if($tracker_type=='donate')
		{

			$sql="DROP TABLE `{$table_prefix}error_log`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}tracker_crestricts`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}tracker_seedrequest`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}tracker_zaddr`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}tracker_zname`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}forums` DROP `tprivate_flag`";
			my_sql_query($sql, $c);

			$sql="DROP TABLE `{$table_prefix}tracker_stats`";
			my_sql_query($sql, $c);

			$sql="DELETE FROM `{$table_prefix}config` WHERE config_name LIKE 'errors%'";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}topics` DROP `topic_desc`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_rsskey`";
			my_sql_query($sql, $c);


			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_bonus2`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_fromseedreq_count`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_toseedreq_count`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_lowratio_checks`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_shadow_uploaded`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_last_seedreq`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_torrents_size`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_rreset_count`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_stickip`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_accparked`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_accpark_reason`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_stealth_mode`";
			my_sql_query($sql, $c);
			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_seed`";
			my_sql_query($sql, $c);

			$sql="ALTER TABLE `{$table_prefix}users` DROP `user_leech`";
			my_sql_query($sql, $c);
		}

		$sql="DROP TABLE `{$table_prefix}tracker_guests`";
		my_sql_query($sql, $c);

		$sql="DROP TABLE `{$table_prefix}tracker_snatched`";
		my_sql_query($sql, $c);

		$sql="DROP TABLE `{$table_prefix}tracker_torrents`";
		my_sql_query($sql, $c);

		$sql="DELETE FROM `{$table_prefix}config` WHERE config_name LIKE 'cgp%'";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}sessions` DROP `session_topic_id`";
		my_sql_query($sql, $c);


		$sql="ALTER TABLE `{$table_prefix}topics` DROP INDEX `topic_first_post_id`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}topics` DROP INDEX `topic_last_post_id`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}topics` DROP INDEX `topic_title`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}topics` DROP INDEX `topic_title2`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}users` DROP `user_passkey`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}users` DROP `user_uploaded`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}users` DROP `user_downloaded`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}users` DROP `user_bonus`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}users` DROP `user_shadow_downloaded`";
		my_sql_query($sql, $c);

		$sql="ALTER TABLE `{$table_prefix}users` DROP `user_uploaded_self`";
		my_sql_query($sql, $c);


		$valid_configs=array(
			'ppkbb_xcquery_log',
			'ppkbb_xcpid_file',
			'ppkbb_xclog_scrape',
			'ppkbb_xcannounce_interval',
			'ppkbb_xclog_announce',
			'ppkbb_xclog_access',
			'ppkbb_xcgzip_scrape',
			'ppkbb_xcfull_scrape',
			'ppkbb_xcdaemon',
			'ppkbb_xcclean_up_interval',
			'ppkbb_xcauto_register',
			'ppkbb_xcoffline_message',
			'ppkbb_xcanonymous_scrape',
			'ppkbb_xcanonymous_announce',
			'ppkbb_xcscrape_interval',
			'ppkbb_xcread_config_interval',
			'ppkbb_xcread_db_interval',
			'ppkbb_xcwrite_db_interval',
			'ppkbb_xcredirect_url',
			'ppkbb_xclisten_ipa',
			'ppkbb_xctable_announce_log',
			'ppkbb_xctable_scrape_log',
			'ppkbb_xctable_files',
			'ppkbb_xctable_files_users',
			'ppkbb_xctable_users',
			'ppkbb_xclisten_port',
			'ppkbb_xcdebug',
			'ppkbb_forb_extpostscr',
			'ppkbb_forb_extpostscr_trueexclude',
			'ppkbb_topdown_torrents_options',
			'ppkbb_tccleanup_interval',
			'ppkbb_tcclean_place',
			'ppkbb_tcratio_start',
			'ppkbb_tcenable_rannounces',
			'ppkbb_tcrannounces_options',
			'ppkbb_announce_url',
			'ppkbb_max_screenshots',
			'ppkbb_max_posters',
			'ppkbb_max_torrents',
			'ppkbb_tprivate_flag',
			'ppkbb_torrent_statvt',
			'ppkbb_clean_snatch',
			'ppkbb_clear_logs',
			'ppkbb_reset_ratio',
			'ppkbb_append_tfile',
			'ppkbb_rtrack_enable',
			'ppkbb_trclear_files',
			'ppkbb_trclear_snatched',
			'ppkbb_trclear_torrents',
			'ppkbb_ipreg_countrestrict',
			'ppkbb_torrent_downlink',
			'ppkbb_poll_options',
			'ppkbb_tcdef_statuses',
			'ppkbb_tcauthor_candown',
			'ppkbb_tcguest_cantdown',
			'ppkbb_feed_enable',
			'ppkbb_feed_overall_trackers',
			'ppkbb_feed_torrents',
			'ppkbb_feed_comments',
			'ppkbb_feed_overall',
			'ppkbb_feed_forum',
			'ppkbb_feed_topic',
			'ppkbb_feed_torrents_new',
			'ppkbb_feed_torrents_active',
			'ppkbb_feed_enblist',
			'ppkbb_feed_trueenblist',
			'ppkbb_feed_torrents_limit',
			'ppkbb_feed_comments_limit',
			'ppkbb_feed_downloads',
			'ppkbb_tracker_top',
			'ppkbb_tfile_annreplace',
			'ppkbb_cron_options',
			'ppkbb_tccron_jobs',
			'ppkbb_fixu_list',
			'ppkbb_topdown_torrents',
			'ppkbb_torr_blocks',
			'ppkbb_trclear_rannounces',
			'ppkbb_trclear_trtrack',
			'ppkbb_trclear_urtrack',
			'ppkbb_torrent_statml',
			'ppkbb_search_astracker',
			'ppkbb_noticedisclaimer_blocks',
			'ppkbb_disable_fpquote',
			'ppkbb_trclear_cronjobs',
			'ppkbb_max_extposters',
			'ppkbb_max_extscreenshots',
			'ppkbb_topdown_torrents_exclude',
			'ppkbb_topdown_torrents_trueexclude',
			'ppkbb_extposters_exclude',
			'ppkbb_extposters_trueexclude',
			'ppkbb_extscreenshots_exclude',
			'ppkbb_extscreenshots_trueexclude',
			'ppkbb_cssjs_cache',
			'ppkbb_addit_options',
			'ppkbb_mua_countlist',
			'ppkbb_postscr_opentype',
			'ppkbb_display_trstat',
			'ppkbb_total_up_down',
			'ppkbb_total_sup_sdown',
			'ppkbb_total_seed_leech',
			'ppkbb_total_tdown_tup',
			'ppkbb_total_udown_uup',
			'ppkbb_total_peers_size',
			'ppkbb_total_remseed_remleech',
			'ppkbb_total_complet_remcomplet',
			'ppkbb_last_stattime',
			'ppkbb_tstatus_notify',
			'ppkbb_tstatus_salt',
			'ppkbb_trestricts_options',
			'ppkbb_tracker_bookmarks',
			'ppkbb_clear_logs',
			'ppkbb_phpannounce_enabled',
			'ppkbb_phpannounce_url',
			'ppkbb_tcgz_rewrite',
			'ppkbb_tciptype',
			'ppkbb_tccheck_ban',
			'ppkbb_tcignore_connectable',
			'ppkbb_tcmaxpeers_limit',
			'ppkbb_tcmaxpeers_rewrite',
			'ppkbb_tcignored_upload',
			'ppkbb_logs_cleanup',
			'ppkbb_logs_last_cleanup',
			'ppkbb_peers_last_cleanup',
			'ppkbb_peerstable_memory',
			'ppkbb_trclear_unregtorr',
			'ppkbb_phpclear_peers',
			'ppkbb_cron_last_cleanup',
// 			'ppkbb3cker_version',
// 			'num_torrents',
// 			'num_comments',
		);
		$sql='SELECT config_name FROM '.$table_prefix."config WHERE config_name LIKE '%ppkbb%'";
		$result=my_sql_query($sql, $c);
		$config_row=my_sql_fetch_array($result);
		foreach($config_row as $row)
		{
			if(!in_array($row['config_name'], $valid_configs))
			{
				$sql='DELETE FROM '.$table_prefix."config WHERE config_name='{$row['config_name']}'";
				my_sql_query($sql, $c);
			}
		}
		my_sql_free_result($result);

		$sql="DROP TABLE `{$table_prefix}tracker_config`";
		my_sql_query($sql, $c);

		if($stop)
		{
			show_errors($result_file, $success_file, $log_success);
			script_footer();
		}

		$url=$page_url.'?step='.($step+1).($stop ? '&stop=1' : '');
		echo "
		<script>
			function redirect()
			{
				window.location.href='".$url."';
			}
			setTimeout(redirect, ".($pause*1000).");
		</script>
		";
	}
	else if($step==$steps)
	{
		echo 'Конвертация завершена<br />';

		show_errors($result_file, $success_file, $log_success);
		script_footer();
	}
}

script_footer(false);

function show_errors($result_file, $success_file, $log_success)
{
// 		$convert_result='';
	if(@file_exists($result_file))
	{
		$error_data=@file($result_file);
		if($error_data)
		{
			$convert_result='<br /><b>Обнаружены ошибки</b><br />';

			echo $convert_result.'<br />'.implode('', $error_data);
		}
		else
		{
			$convert_result='<br /><b>Без ошибок</b><br />';

			echo $convert_result;
		}
	}

	if($log_success)
	{
// 			if($convert_result)
// 			{
// 				$fo=@fopen($success_file, 'a+');
// 				if($fo)
// 				{
// 					fwrite($fo, $convert_result."\n");
// 					fclose($fo);
// 				}
// 			}

		if(@file_exists($success_file))
		{
			$success_data=@file($success_file);
			if($success_data)
			{
				echo '<br /><b>Выполненные запросы</b><br />'.implode('', $success_data);
			}
		}
	}
}

function script_header()
{
	echo '
		<!DOCTYPE html>
		<html>
		<head>
		<title>Конвертация данных ppkBB3cker v.2 в xbtBB3cker v.3</title>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<style>
		.row1, .row1 td { background: #EFEFEF;}
		.row2, .row2 td { background: #DEE3E7;}
		a:link, a:visited { text-decoration:none; color: #006699; }
		a:hover { text-decoration:none; color: #0000C0; }
		</style>
		</head>
		<body bgcolor="#DEE3E7">
		<p style="padding: 6px 8px 7px;text-align:center;color:#D5E4EC;font-weight:bold;background:#006699;">Конвертация данных ppkBB3cker v.2 в xbtBB3cker v.3</p>
		<div>
	';
}

function script_footer($end=true)
{
	global $page_url;

	if($end)
	{
		echo '<br /><a href="'.$page_url.'">&laquo; Вернуться в начало</a><br />';
	}
	echo '
		</div>
		</body>
		</html>
	';
	exit();
}


function err($str)
{
	global $c;

	echo $str;

	if($c)
	{
		my_sql_close($c);
	}

	exit();
}

function gen_rand_string($num_chars = 8)
{
	// [a, z] + [0, 9] = 36
	return substr(strtoupper(base_convert(uniqid(), 16, 36)), 0, $num_chars);
}

?>
