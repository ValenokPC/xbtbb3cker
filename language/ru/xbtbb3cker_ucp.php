<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(

	'TRACKER_USER_COMMENTS' => 'Комментариев',
	'TRACKER_USER_DOWNLOAD' => 'Скачал',
	'TRACKER_USER_UPLOAD' => 'Раздал',
	'TRACKER_USER_RATIO' => 'Ратио',


	'UCP_PREFS_TRACKER'	=> 'Настройки трекера',
	'UCP_PREFS_TRACKER_DETAILS'	=> 'Настройки трекера',
	'UCP_PREFS_RETRACKER'	=> 'Мультитрекер',
	'UCP_PREFS_RETRACKER_DETAILS'	=> 'Мультитрекер',
	'RECREATE_PASSKEY' => 'Пересоздать пасскей',
	'RECREATE_PASSKEY_EXPLAIN' => '<strong style="color:#FF0000;">Внимание!</strong>, после этого необходимо будет заново скачать с трекера все раздаваемые и скачиваемые торрент файлы!',
	'USER_PASSKEY_CREATED'			=> 'Пасскей пересоздан.',

	'TRACKER_USER_PASSKEY' => 'Пасскей',
	'SETTINGS_UPDATED'		=> 'Настройки обновлены.',


	'TORRENT_INFO_HEADER_TORRENT'		=> 'Загрузил',
	'TORRENT_INFO_HEADER_SEED'		=> 'Раздаю',
	'TORRENT_INFO_HEADER_LEECH'		=> 'Скачиваю',
	'TORRENT_INFO_HEADER_LEAVE'		=> 'Не раздаю',
	'TORRENT_INFO_HEADER_HISTORY'		=> 'История',
	'TORRENT_INFO_HEADER_AUTHOR'		=> 'Автор',
	'TORRENT_INFO_HEADER_BOOKMARKS'		=> 'Будущие закачки',
	'TORRENT_INFO_HEADER_ADD_DATE'		=> 'Дата',

	'TORRENT_INFO_HEADER_FINISHED'		=> 'Скачал',
	'TORRENT_INFO_HEADER_DOWNLOADS'		=> 'Скачал файлы',

	'RTRACK'	=> 'Мультитрекер',
	'ACTION'	=> 'Действие',
	'ZONE_RTRACK_URL' => 'Url трекера',
	'ZONE_RTRACK_EXPLAIN' => 'Адреса данных трекеров будут автоматически добавляться в скачиваемые вами торрент файлы.<br />Строка {YOUR_PASSKEY} будет заменяться вашим текущим пасскеем на трекере.',
	'RTRACK_DISABLED' => 'Система личных дополнительных анонс URL отключена',
	'RTRACK_SUCCESS' => 'Дополнительные анонс URL изменены',
	'RTRACK_BACK' => '<br /><br /><a href="%s">Вернуться назад</a>',

	'INVALID_RTRACK_URL' => 'Некорректный URL трекера',
	'FORB_RTRACK_URL' => 'Запрещённый URL трекера',
	'MAXUSERS_ANNOUNCES_LIMIT' => 'Максимально допустимое количество трекеров: <strong>%d</strong>',

	'NO_SELF_SEND_MESSAGE' => 'Вы отправляете личное сообщение самому себе, пожалуйста выберите другого пользователя.<br /><br /><a onclick="history.back(); return false;" href="#">Вернуться назад</a>',

	'UTO_TDT' => 'Не отображать топ скачиваемых торрентов',
	'UTO_TDT_EXPLAIN' => 'Не отображать топ скачиваемых торрентов на страницах списка форумов и тем',
	'UTO_TDT2' => 'Не отображать новинки трекера',
	'UTO_TDT2_EXPLAIN' => 'Не отображать новинки трекера на страницах списка форумов и тем',
	'TORRENT_INFO_HEADER_ANNCOUNT' => 'Анонсов',
	'TORRENT_INFO_HEADER_ACTIVE' => 'Активен',

	'SHOW_BOOKMARKS' => 'Показывать будущие закачки',
	'SHOW_BOOKMARKS_EXPLAIN' => 'Показывать ваши будущие закачки другим пользователям',
	'AUTODEL_BOOKMARKS' => 'Удаление будущих закачек',
	'AUTODEL_BOOKMARKS_EXPLAIN' => 'Автоматически удалять торренты из будущих закачек после скачивания',
	'DELETE_BOOKMARKS' => 'Удалить будущие закачки',
	'DELETE_BOOKMARKS_EXPLAIN' => '',
	'DELETE_BOOKMARKS_FINISHED' => 'Только скачанные',
));
