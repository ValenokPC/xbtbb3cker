<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'SORT_UPLOADED' => 'Роздано',
	'SORT_DOWNLOADED' => 'Скачано',
	'SORT_LEFT' => 'Осталось скачать',
	'SORT_EVENT' => 'Событие',
	'SORT_PEERID' => 'ID пира',
	'SORT_TTITLE' => 'Торрент',
	'SORT_PORT' => 'Порт',

	'EVENT_NONE' => '',
	'EVENT_STARTED' => 'Старт',
	'EVENT_STOPPED' => 'Стоп',
	'EVENT_COMPLETED' => 'Завершено',

	'TOTAL_LOGS' => 'Всего записей: <strong>%d</strong>',
));
