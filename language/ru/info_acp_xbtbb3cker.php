<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_XBTBB3CKER'				=> 'xbtBB3cker',
	'ACP_XBTBB3CKER_EXPLAIN'		=> 'Настройки',
	'ACP_XBTBB3CKER_XBTCONFIG' => 'Настройки XBT',
	'ACP_XBTBB3CKER_XBTCONFIG_EXPLAIN' => 'Настройки XBT',
	'ACP_XBTBB3CKER_PHPANNOUNCE' => 'Настройки PHP анонса',
	'ACP_XBTBB3CKER_PHPANNOUNCE_EXPLAIN' => 'Настройки PHP анонса. Для работы PHP анонса необходимо скопировать файлы из директории расширения /contrib/php_announce/ в корневую директорию форума. В секции общих настроек находятся общие с XBT настройки.',
	'ACP_XBTBB3CKER_SET' => 'Групповые действия',
	'ACP_XBTBB3CKER_SET_EXPLAIN' => 'Групповые действия',
	'ACP_XBTBB3CKER_RTRACKER'			=> 'Внешние анонс URL и мультитрекер',
	'ACP_XBTBB3CKER_RTRACKER_EXPLAIN'			=> 'Внешние анонс URL и мультитрекер',
	'ACP_XBTBB3CKER_RTRACKURL'			=> 'Дополнительные анонс URL',
	'ACP_XBTBB3CKER_RTRACKURL_EXPLAIN'			=> 'Дополнительные анонс URL',
	'ACP_XBTBB3CKER_ANNOUNCELOG'			=> 'Лог анонса',
	'ACP_XBTBB3CKER_ANNOUNCELOG_EXPLAIN'			=> 'Лог анонса',
	'ACP_XBTBB3CKER_SCRAPELOG'			=> 'Лог скрейпа',
	'ACP_XBTBB3CKER_SCRAPELOG_EXPLAIN'			=> 'Лог скрейпа',
	'ACP_XBTBB3CKER_DOWNLOADLOG'			=> 'Лог скачиваний торрент файлов',
	'ACP_XBTBB3CKER_DOWNLOADLOG_EXPLAIN'			=> 'Лог скачиваний торрент файлов',
	'ACP_XBTBB3CKER_CANDC'			=> 'Обслуживание, удаление и очистка',
	'ACP_XBTBB3CKER_CANDC_EXPLAIN'			=> 'Обслуживание, удаление и очистка',
	'ACP_XBTBB3CKER_CONFIG'			=> 'Настройки трекера',
	'ACP_XBTBB3CKER_CONFIG_EXPLAIN'			=> 'Настройки трекера',
	'ACP_XBTBB3CKER_RSS'			=> 'Каналы торрентов и комментариев',
	'ACP_XBTBB3CKER_RSS_EXPLAIN'			=> 'Каналы торрентов и комментариев',
	'ACP_XBTBB3CKER_IMGSET'			=> 'Настройки постеров и скриншотов',
	'ACP_XBTBB3CKER_IMGSET_EXPLAIN'			=> 'Настройки постеров и скриншотов',
	'ACP_XBTBB3CKER_USERDATA'			=> 'Данные пользователей',
	'ACP_XBTBB3CKER_USERDATA_EXPLAIN'			=> 'Данные пользователей',
	'ACP_XBTBB3CKER_STATUSES'			=> 'Статусы торрентов',
	'ACP_XBTBB3CKER_STATUSES_EXPLAIN'			=> 'Статусы торрентов',
	'ACP_XBTBB3CKER_TRESTRICTS'			=> 'Ограничения трекера',
	'ACP_XBTBB3CKER_TRESTRICTS_EXPLAIN'			=> 'Ограничения трекера',
	'ACP_XBTBB3CKER_LOSTTORRENTS'			=> 'Потерянные торренты',
	'ACP_XBTBB3CKER_LOSTTORRENTS_EXPLAIN'			=> 'Потерянные торренты',

	'ACP_CONFIG_OPTION' => 'опция',
	'NO_DATA' => 'Нет данных',
	'ACL_CAT_PPKEXT' => 'Расширения PPK',

	'ACL_U_ATTACHFONLY' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может загружать торренты на трекер',
	'ACL_U_CANDOWNTORR' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть и скачивать торрент файлы',
	'ACL_U_CANDOWNPOSTSCR' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть и скачивать постеры и скриншоты',
	'ACL_U_CANSEARCHTR' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может вести поиск с опциями трекера',
	'ACL_U_CANSETSTATUS' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может управлять статусами торрентов',
	'ACL_U_CANSKIPTCHECK' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может загружать торренты на трекер со статусом доступным для скачивания без предварительной проверки',

	'ACL_U_CANVIEWTRTOP' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может просматривать топ трекера',
	'ACL_U_CANVIEWTOPDOWNTORRENTS' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть топ скачиваемых торрентов',
	'ACL_U_CANVIEWVTSTATS' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть статистику по торрентам на странице торрента',
	'ACL_U_CANVIEWMUASTATR' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть данные по трекеру в профиле пользователей',
	'ACL_U_CANVIEWMUASTATORR' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть данные по торрентам в профиле пользователей',
	'ACL_U_CANVIEWTRSTAT' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть статистику трекера',
	'ACL_U_CANSKIPREQUPRATIO'	=> '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может "обходить" ограничение на требуемое ратио и аплоад для скачивания торрента',
	'ACL_U_CANSKIPTRESTRICTS'	=> '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может "обходить" ограничения трекера на скачивание содержимого торрент-файлов',
	'ACL_U_CANSETREQUPRATIO'	=> '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может делать торренты с ограничениями по ратио и аплоаду для скачивания',

	'ACL_F_ATTACHFONLY' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может загружать торренты на трекер',
	'ACL_F_CANDOWNTORR' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть и скачивать торрент файлы',
	'ACL_F_CANDOWNPOSTSCR' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть и скачивать постеры и скриншоты',
	'ACL_F_CANSETSTATUS' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может управлять статусами торрентов',
	'ACL_F_CANSKIPTCHECK' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может загружать торренты на трекер со статусом доступным для скачивания без предварительной проверки',
	'ACL_F_CANVIEWVTSTATS' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может видеть статистику по торрентам на странице торрента',
	'ACL_F_CANSKIPREQUPRATIO'	=> '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может "обходить" ограничение на требуемое ратио и аплоад для скачивания торрента',
	'ACL_F_CANSETREQUPRATIO'	=> '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может делать торренты с ограничениями по ратио и аплоаду для скачивания',

	'ACL_A_XBTBB3CKER' => '<span style="font-weight:bold;color:#FF0000;">xbtBB3cker</span>: Может изменять настройки трекера',

	'LOG_CLEAR_ANNOUNCE_XBTBB3CKER' => '<strong>Очищен лог анонсов</strong>',
	'LOG_CLEAR_SCRAPE_XBTBB3CKER' => '<strong>Очищен лог скрейпа</strong>',
	'LOG_CLEAR_DOWNLOADS_XBTBB3CKER' => '<strong>Очищен лог скачиваний торрент файлов</strong>',
	'LOG_FIX_LOSTTORRENTS_XBTBB3CKER' => '<strong>Исправлены потерянные торренты</strong>',
	'LOG_DELETE_LOSTTORRENTS_XBTBB3CKER' => '<strong>Удалены потерянные торренты</strong>',
));
