<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'TOP_BY_UPLOAD' => 'Топ %d авторов по розданному',
	'TOP_BY_RATIO' => 'Топ %d авторов по ратио',
	'TOP_BY_DOWNSUM' => 'Топ %d авторов по сумме скачанных торрентов',
	'TOP_BY_AUTHOR' => 'Топ %d авторов по количеству торрентов',
	'TOP_BY_TORRENTS' => 'Топ %d торрентов по количеству скачиваний',

	'TRACKER_AVG' => 'В среднем',
	'TOP_DISABLED' => 'Топ трекера отключён',
	'TOP_NOAUTH' => 'Нет прав для просмотра',

	'TOP_BY' => 'За',
	'TOP_WEEK' => 'Неделю',
	'TOP_MONTH' => 'Месяц',
	'TOP_ALL' => 'Всё время',
	'NO_DATA' => 'Нет данных',

));
