<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_TRFEED_ENABLE'					=> 'Включить каналы новостей трекера',
	'ACP_TRFEED_ENABLE_EXPLAIN'			=> 'Включение или отключение каналов новостей трекера для всей конференции. При отключении этой опции будут отключены все каналы новостей, независимо от указанных ниже параметров.',

	'ACP_FEED_OVERALL_TRACKERS'			=> 'Включить канал трекеров',
	'ACP_FEED_OVERALL_TRACKERS_EXPLAIN'	=> 'Включает канал «Все трекеры», который позволяет получить список форумов трекеров.',
	'ACP_TRFEED_OVERALL'					=> 'Включить общий канал торрентов и комментариев',
 	'ACP_TRFEED_OVERALL_EXPLAIN'			=> 'Новые торренты и комментарии конференции.',

	'ACP_FEED_TRTORRENTS' => 'Включить каналы для торрентов',
	'ACP_FEED_TRTORRENTS_EXPLAIN' => 'Эта опция действует глобально на все остальные каналы кроме каналов новых, активных торрентов и общий канал',
	'ACP_FEED_TRCOMMENTS' => 'Включить каналы для комментариев',
	'ACP_FEED_TRCOMMENTS_EXPLAIN' => 'Эта опция действует глобально на все остальные каналы кроме общего канала',

	'ACP_TRFEED_FORUM'					=> 'Включить каналы форумов',
	'ACP_TRFEED_FORUM_EXPLAIN'			=> 'Новые торренты и комментарии отдельных форумов.',
	'ACP_TRFEED_TOPIC'					=> 'Включить каналы тем',
	'ACP_TRFEED_TOPIC_EXPLAIN'			=> 'Новые торренты и комментарии отдельных тем.',

 	'ACP_FEED_TORRENTS_NEW'				=> 'Включить канал новых торрентов',
 	'ACP_FEED_TORRENTS_NEW_EXPLAIN'		=> 'Включает канал «Новые торренты», который позволяет получать информацию о новых торрентах.',
 	'ACP_FEED_TORRENTS_ACTIVE'			=> 'Включить канал активных торрентов',
 	'ACP_FEED_TORRENTS_ACTIVE_EXPLAIN'	=> 'Включает канал «Активные торренты», который позволяет получать новые активные торренты.',

	'ACP_FEED_ENBLIST' => 'Исключить эти форумы',
	'ACP_FEED_ENBLIST_EXPLAIN' => 'Исключать указанные форумы из каналов',
	'ACP_FEED_TRUEENBLIST' => 'Исключить эти форумы',
	'ACP_FEED_TRUEENBLIST_EXPLAIN' => 'Если выбрано Да - исключать из каналов форумы определённые выше, иначе наоборот включать только указанные выше форумы',

	'ACP_FEED_TORRLIMIT' => 'Количество элементов на странице для отображения в каналах торрентов',
	'ACP_FEED_TORRLIMIT_EXPLAIN' => '',
	'ACP_FEED_COMMLIMIT' => 'Количество элементов на странице для отображения в каналах комментариев',
	'ACP_FEED_COMMLIMIT_EXPLAIN' => '',

 	'ACP_FEED_DOWNLOADS' => 'Торрент файлы в каналах',
 	'ACP_FEED_DOWNLOADS_EXPLAIN' => 'Отображать торрент файлы в текстах каналов',
// 	'ACP_FEED_TORRSORT' => 'Сортировка по дате темы',
// 	'ACP_FEED_TORRSORT_EXPLAIN' => 'Сортировать сообщения в каналах торрентов по дате создания темы, иначе по дате создания/изменения торрента',

// 	'ACP_FEED_TORRTIME' => 'Отображаемые торренты',
// 	'ACP_FEED_TORRTIME_EXPLAIN' => 'Отображать в каналах торрентов только торренты не старее указанного числа (в днях)',
// 	'ACP_FEED_COMMTIME' => 'Отображаемые комментарии',
// 	'ACP_FEED_COMMTIME_EXPLAIN' => 'Отображать в каналах комментариев только комментарии не старее указанного числа (в днях)',



));
