<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(

	'SORT_TTITLE' => 'Торрент',
	'SORT_FILENAME' => 'Имя файла',

	'TOTAL_LOGS' => 'Всего записей: <strong>%d</strong>',
));
