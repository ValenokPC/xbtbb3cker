<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(

	'TRACKER_CLEAR_FILES'	=> 'Удаление списков файлов',
	'TRACKER_CLEAR_FILES_EXPLAIN'	=> '<span style="color:#FF0000;">Безвозвратно удаляет все списки файлов находящиеся в торрентах</span>',

	'TRACKER_CLEAR_TORRENTS'	=> 'Удаление списков торрентов',
	'TRACKER_CLEAR_TORRENTS_EXPLAIN'	=> '<span style="color:#FF0000;">Безвозвратно удаляет все списки торрентов которые были загружены на трекер</span>',
	'PPKBB_TRCLEAR_SNATCHED'	=> 'Удаление статистики по торрентам',
	'PPKBB_TRCLEAR_SNATCHED_EXPLAIN'	=> '<span style="color:#FF0000;">Безвозвратно удаляет всю статистику по торрентам</span>, Только нулевую - удалить только "нулевую" статистику, Только не нулевую - удалить только НЕ "нулевую" статистику (торрентом с нулевой статистикой будет считаться торрент на котором не было ничего скачано и роздано)',
	'PPKBB_TRCLEAR_SNATCHED_NULL'	=> 'Только нулевую',
	'PPKBB_TRCLEAR_SNATCHED_NOTNULL'	=> 'Только НЕ нулевую',
	//'TRACKER_CLEAN_POLLS'	=> 'Удаление опросов по торрентам',
	//'TRACKER_CLEAN_POLLS_EXPLAIN'	=> '<span style="color:#FF0000;">Безвозвратно удаляет все имеющиеся опросы (включая ответы) по торрентам</span>',
	//'TRACKER_CLEAR_POLLRES'	=> 'Удаление результатов опросов по торрентам',
	//'TRACKER_CLEAR_POLLRES_EXPLAIN'	=> '<span style="color:#FF0000;">Безвозвратно удаляет результаты опросов (ответы) по торрентам</span>',
	'TRACKER_CLEAR_RANNOUNCES' => 'Удаление данных внешних трекеров',
	'TRACKER_CLEAR_RANNOUNCES_EXPLAIN' => '<span style="color:#FF0000;">Безвозвратно удаляет данные о пирах полученных с внешних трекеров</span>',
	'TRACKER_CLEAR_TRTRACK' => 'Удаление внешних трекеров из торрентов',
	'TRACKER_CLEAR_TRTRACK_EXPLAIN' => '<span style="color:#FF0000;">Безвозвратно удаляет внешние трекеры из загруженных торрент файлов</span>',
	'TRACKER_CLEAR_URTRACK' => 'Удаление пользовательских внешних трекеров',
	'TRACKER_CLEAR_URTRACK_EXPLAIN' => '<span style="color:#FF0000;">Безвозвратно удаляет внешние трекеры добавленные пользователями</span>',
	'TRACKER_CLEAR_CRONJOBS'	=> 'Удаление заданий крона',
	'TRACKER_CLEAR_CRONJOBS_EXPLAIN'	=> '<span style="color:#FF0000;">Безвозвратно удаляет задания крона для трекера</span>',

	'TRACKER_CRON_OPTIONS' => 'Функции "крона"',
	'TRACKER_CRON_OPTIONS_EXPLAIN' => 'Опции заданий крона, пять опций,
		<br /><strong>опция 1</strong> очищать задания крона старее указанного значения,
		<br /><strong>опция 2</strong> производить очистку устаревших значений крона через указанное время, при значении опции 1 равным 0 - функции очистки заданий крона работать не будут,
		<br /><strong>опция 3</strong> каждое задание крона запускать отдельным файлом, иначе все задания крона запускать в одном файле,
		<br /><strong>опция 4</strong> не выполнять задание если с момента его добавления до момента запуска прошло указанное время,
		<br /><strong>опция 5</strong> не используется',
	'TRACKER_CRON_JOBS' => 'Задания "крона"',
	'TRACKER_CRON_JOBS_EXPLAIN' => 'Пять опций,
		<br /><strong>опция 1</strong> выполнять пересчёт торрентов пользователя через указанное время,
		<br /><strong>опция 2</strong> выполнять пересчёт комментариев пользователя через указанное время,
		<br /><strong>опция 3</strong> не используется,
		<br /><strong>опция 4</strong> не используется,
		<br /><strong>опция 5</strong> не используется',
	'TRACKER_CLEAN_SNATCH'				=> 'Удалить данные торрентов',
	'TRACKER_CLEAN_SNATCH_EXPLAIN'				=> 'Удаляет статистику торрентов которые были удалены и/или торренты которые были потеряны, а также списки файлов в соответствующих торрентах',
	'PPKBB_CLEAR_LOGS'				=> 'Очистить данные логов',
	'PPKBB_CLEAR_LOGS_EXPLAIN'				=> 'Удаляет записи из логов анонса и скрейпа, "мёртвыми" будут считаться записи старее значения указанного в опции <u>Время очистки информации в логах</u>',
	'PPKBB_CLEAR_LOGS_TIME'					=> 'только "мёртвые"',
	'PPKBB_CLEAR_LOGS_ALL'					=> 'все',
	'PPKBB_CLEAR_LOGS_OFF'					=> 'нет',
	'TRACKER_RESET_RATIO'					=> 'Сбросить значение ратио у всех пользователей',
	'TRACKER_RESET_RATIO_EXPLAIN'					=> '<span style="color:#FF0000;">Безвозвратно удаляет данные о количестве скачанного/розданного у пользователей</span>',
	'TRACKER_FIXU_LIST' => 'Исправить список пользователей',
	'TRACKER_FIXU_LIST_EXPLAIN'=> 'Пересоздать удалённые или потерянные данные в таблице данных пользователей XBT',

	'TRACKER_CLEAN_PLACE' => 'Очистка данных мёртвых пиров',
	'TRACKER_CLEAN_PLACE_EXPLAIN' => 'Проверять и производить очистку данных мёртвых/дублированных пиров (только при использовании PHP анонса), шесть опций,
		<br /><strong>опция 1</strong> не используется
		<br /><strong>опция 2</strong> в файле поиска (search)
		<br /><strong>опция 3</strong> в файле просмотра форума/списка тем (viewforum)
		<br /><strong>опция 4</strong> в файле просмотра темы торрента (viewtopic)
		<br /><strong>опция 5</strong> не используется
		<br /><strong>опция 6</strong> не используется',
	'TRACKER_LOGS_CLEANUP'	=> 'Время очистки информации в логах',
	'TRACKER_LOGS_CLEANUP_EXPLAIN' => 'Две опции,
		<br /><strong>опция 1</strong> удалять данные из логов анонса и скрейпа через указанной промежуток времени,
		<br /><strong>опция 2</strong> считать устаревшими записи старее указанного времени',
// 	'TRACKER_DEAD_TIME' => 'Время жизни информации о сидерах и личерах',
// 	'TRACKER_DEAD_TIME_EXPLAIN' => 'Данные сидеров и личеров старее этого значения будут удаляться, данное значение должно быть <em>больше</em> значения <u>Интервала обращений к трекеру</u>',
	'TRACKER_CLEANUP_INTERVAL' => 'Время очистки информации о сидерах и личерах',
	'TRACKER_CLEANUP_INTERVAL_EXPLAIN' => 'Через указанный промежуток времени будет проводиться чистка устаревших сидеров и личеров (только при использовании PHP анонса)',
	'TRACKER_CLEAR_UNREGTORR' => 'Удаление незарегистрированных торрентов',
	'TRACKER_CLEAR_UNREGTORR_EXPLAIN' => '<span style="color:#FF0000;">Безвозвратно удаляет незарегистрированные (не загруженные на сервер) торренты</span> (только при использовании PHP анонса)',
	'PPKBB_PHPCLEAR_PEERS'				=> 'Очистить список пиров',
	'PPKBB_PHPCLEAR_PEERS_EXPLAIN'				=> 'Удаляет списки пиров по всем скачиваемым и раздаваемым пользователями торрентов, при этом возможна частичная потеря статистики для некоторых пользователей (только при использовании PHP анонса)',
	'PPKBB_PHPCLEAR_PEERS_TIME'					=> 'только "мёртвые"',
	'PPKBB_PHPCLEAR_PEERS_ALL'					=> 'все',
	'PPKBB_PHPCLEAR_PEERS_OFF'					=> 'нет',
));
?>
