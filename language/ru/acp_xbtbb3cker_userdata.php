<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'SELECT_FORM'			=> 'Выбрать форму',
	'SELECT_USER'			=> 'Выбрать пользователя',

	'USERDATA_USER_TRACKER' => 'Данные трекера',
	'USERDATA_USER_TORRENTS' => 'Данные торрентов',
	'USERDATA_USER_RTRACKURL' => 'Дополнительные анонс URL',

	'USER_ADMIN'					=> 'Управление пользователями',
	'USER_ADMIN_EXPLAIN'			=> 'Здесь вы можете изменять информацию о пользователях и некоторые специальные настройки.',

	'TORRENT_INFO_HEADER_TORRENT'		=> 'Загрузил',
	'TORRENT_INFO_HEADER_SEED'		=> 'Раздаёт',
	'TORRENT_INFO_HEADER_LEECH'		=> 'Скачивает',
	'TORRENT_INFO_HEADER_LEAVE'		=> 'Не раздаёт',
	'TORRENT_INFO_HEADER_HISTORY'		=> 'История',
	'TORRENT_INFO_HEADER_DOWNLOADS'		=> 'Скачал файлы',
	'TORRENT_INFO_HEADER_AUTHOR'		=> 'Автор',

	'TORRENT_INFO_HEADER_FINISHED'		=> 'Скачал',

	'ACP_ZONE_RTRACK_EXPLAIN' => 'Адреса данных трекеров будут автоматически добавляться в скачиваемые пользователем торрент файлы.<br />Строка {YOUR_PASSKEY} будет заменяться текущим пасскеем пользователя на трекере.',

	'ACP_USER_TRACKER'				=> 'Трекер',
	'ACP_USER_RTRACKER'				=> 'Мультитрекер',
	'ACP_USER_TORRENTS'				=> 'Торренты',

	'ACP_PREFS_TRACKER'				=> 'Данные трекера',
	'ACP_PREFS_PERSONAL'				=> 'Личные настройки',
	'ACP_PREFS_ADMIN'				=> 'Администраторские настройки',

	'ACP_TR_RATIO'					=> 'Ратио',
	'ACP_TR_RATIO_EXPLAIN'					=> '',
	'ACP_TR_DOWN'					=> 'Скачал',
	'ACP_TR_DOWN_EXPLAIN'					=> '',
	'ACP_TR_UP'					=> 'Раздал',
	'ACP_TR_UP_EXPLAIN'					=> '',

	'ACP_TR_COMM'					=> 'Комментариев',
	'ACP_TR_COMM_EXPLAIN'					=> '',
	'ACP_TR_TORR'					=> 'Торрентов',
	'ACP_TR_TORR_EXPLAIN'					=> '',

	'ACP_TR_PASSKEY'					=> 'Пасскей',
	'ACP_TR_PASSKEY_EXPLAIN'					=> '',

	'TR_CLEAR_PEERS'					=> 'Удалить данные пользователя из логов анонса и скрейпа',
	'TR_CLEAR_PEERS_EXPLAIN'			=> 'Удаляет все записи с данными пользователя из логов анонса и скрейпа, "мёртвыми" будут считаться записи старее значения указанного в опции <u>Время жизни информации о сидерах и личерах</u>',
	'TR_CLEAR_PEERS_TIME'					=> 'Только "мёртвые"',
	'TR_CLEAR_PEERS_ALL'					=> 'Все',
	'TR_CLEAR_SNATCH'					=> 'Удалить данные торрентов пользователя',
	'TR_CLEAR_SNATCH_EXPLAIN'			=> 'Удаляет пользовательскую статистику торрентов которые были удалены и/или торренты которые были потеряны, а также списки файлов в соответствующих торрентах',

	'TR_RESET_RATIO'					=> 'Сбросить значение ратио пользователя',
	'TR_RESET_RATIO_EXPLAIN'					=> '',

	'ACP_PREFS_TORRENTS'				=> 'Данные торрентов',
	'DEL_USER_TORRENTS_SUCCESS' => 'Выбранные записи успешно удалены',

	'RECREATE_PASSKEY' => 'Пересоздать пасскей',
	'RECREATE_PASSKEY_EXPLAIN' => '<strong>Внимание!</strong>, после этого необходимо будет перекачать заново все раздаваемые и скачиваемые торренты!',
	'USER_PREFS_UPDATED'			=> 'Настройки пользователя обновлены.',
	'ACP_ZONE_RTRACK_EXPLAIN' => 'Адреса данных трекеров будут автоматически добавляться в скачиваемые пользователем торрент файлы.<br />Строка {YOUR_PASSKEY} будет заменяться текущим пасскеем пользователя на трекере.',


	'RTRACK'	=> 'Мультитрекер',
	'ACTION'	=> 'Действие',
	'ZONE_RTRACK_URL' => 'Url трекера',
	'ZONE_RTRACK_EXPLAIN' => 'Адреса данных трекеров будут автоматически добавляться в скачиваемые вами торрент файлы.<br />Строка {YOUR_PASSKEY} будет заменяться вашим текущим пасскеем на трекере.',
	'RTRACK_SUCCESS' => 'Дополнительные анонс URL изменены',
	'RTRACK_BACK' => '<br /><br /><a href="%s">Вернуться назад</a>',

	'INVALID_RTRACK_URL' => 'Некорректный URL трекера',
	'FORB_RTRACK_URL' => 'Запрещённый URL трекера',
	'MAXUSERS_ANNOUNCES_LIMIT' => 'Максимально допустимое количество трекеров: <strong>%d</strong>',

	'DEL_USER_TORRENTS_WARN_FINISHED' => 'Удаление статистики пользователя по скачанным торрентам',
	'DEL_USER_TORRENTS_WARN_SEED' => 'Удаление записей и статистики пользователя по раздаваемым торрентам',
	'DEL_USER_TORRENTS_WARN_LEECH' => 'Удаление записей и статистики пользователя по скачиваемым торрентам',
	'DEL_USER_TORRENTS_WARN_HISTORY' => 'Удаление статистики пользователя по торрентам',
	'DEL_USER_TORRENTS_WARN_LEAVE' => 'Удаление статистики пользователя по торрентам которые он не раздаёт',
	'DEL_USER_TORRENTS_WARN_SEEDREQ' => 'Удаление записей о сделанных пользователем запросах сидеров',
	'DEL_USER_TORRENTS_WARN_DOWNLOADS' => 'Удаление записей о скачанных пользователем торрент файлах',
	'DEL_USER_TORRENTS_WARN_TORRENT' => 'Удаление записей о загруженных пользователем торрентах',
	'DEL_USER_TORRENTS_WARN_BOOKMARKS' => 'Удаление записей о будущих закачках пользователя',

));
