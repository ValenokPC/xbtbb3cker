<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(

	'TRACKER_ANNOUNCE_INTERVAL'			=> 'Интервал обращений к трекеру',
	'TRACKER_ANNOUNCE_INTERVAL_EXPLAIN' => 'Чем меньше число в этой опции, тем больше нагрузка на сервер',
	'TRACKER_LISTEN_PORT'			=> 'Анонс порт трекера',
	'TRACKER_LISTEN_PORT_EXPLAIN'			=> 'Порт трекера на котором работает XBT',
	'TRACKER_ANNOUNCE_URL'			=> 'Анонс URL трекера',
	'TRACKER_ANNOUNCE_URL_EXPLAIN'			=> 'URL трекера на котором запущен XBT (исключая слэш в конце), например: http://127.0.0.1',
	'TRACKER_SCRAPE_INTERVAL'			=> 'Интервал скрейпа',
	'TRACKER_SCRAPE_INTERVAL_EXPLAIN'			=> '',
	'TRACKER_ANONYMOUS_ANNOUNCE' => 'Анонс для незарегистрированных пользователей',
	'TRACKER_ANONYMOUS_ANNOUNCE_EXPLAIN' => 'Разрешить незарегистрированным пользователям анонсировать торренты',
	'TRACKER_ANONYMOUS_CONNECT' => 'Соединения для незарегистрированных пользователей',
	'TRACKER_ANONYMOUS_CONNECT_EXPLAIN' => 'Разрешить незарегистрированным пользователям соединяться с трекером',
	'TRACKER_ANONYMOUS_SCRAPE' => 'Скрейп для незарегистрированных пользователей',
	'TRACKER_ANONYMOUS_SCRAPE_EXPLAIN' => 'Разрешить незарегистрированным пользователям скрейпить торренты',
	'TRACKER_AUTO_REGISTER' => 'Автоматическая регистрация торрентов',
	'TRACKER_AUTO_REGISTER_EXPLAIN' => 'Разрешить анонсировать незарегистрированные (не загруженные на трекер) торренты',
	'TRACKER_CLEAN_UP_INTERVAL' => 'Интервал очистки торрентов',
	'TRACKER_CLEAN_UP_INTERVAL_EXPLAIN' => 'Удалять данные торрентов отмеченных как удалённые из БД через указанный промежуток времени',
	'TRACKER_DAEMON' => 'XBT в режиме демона',
	'TRACKER_DAEMON_EXPLAIN' => 'Запускать XBT как демон',
	'TRACKER_DEBUG' => 'Дебаг XBT',
	'TRACKER_DEBUG_EXPLAIN' => 'Включить дебаг для XBT (информация будет доступна по адресу: http://адрес_трекера:порт/debug)',
	'TRACKER_FULL_SCRAPE' => 'Скрейп торрентов',
	'TRACKER_FULL_SCRAPE_EXPLAIN' => 'Включить скрейп функции трекера, скрейп позволяет без анонса торрента узнать количество скачавших, скачивающих и раздающих на торренте',
	//'TRACKER_GZIP_DEBUG' => 'Дебаг в режиме Gzip',
	//'TRACKER_GZIP_DEBUG_EXPLAIN' => 'Отправлять данные дебага в сжатом виде',
	'TRACKER_GZIP_SCRAPE' => 'Скрейп в режиме Gzip',
	'TRACKER_GZIP_SCRAPE_EXPLAIN' => 'Отправлять данные скрейпа в сжатом виде',
	'TRACKER_LOG_ACCESS' => 'Логирование доступа',
	'TRACKER_LOG_ACCESS_EXPLAIN' => 'Включить логирование доступа к трекеру, запись лога будет вестись в файл xbt_tracker_raw.log (<span style="color:#FF0000;">включение данной опции может привести к большому росту места занимаемого данным файлом</span>)',
	'TRACKER_LOG_ANNOUNCE' => 'Логирование анонсов',
	'TRACKER_LOG_ANNOUNCE_EXPLAIN' => 'Включить логирование анонсов торрентов (<span style="color:#FF0000;">включение данной опции может привести к большому росту размера базы данных</span>)',
	'TRACKER_LOG_SCRAPE' => 'Логирование скрейпа',
	'TRACKER_LOG_SCRAPE_EXPLAIN' => 'Включить логирование скрейпа торрентов (<span style="color:#FF0000;">включение данной опции может привести к большому росту размера базы данных</span>)',
	'TRACKER_PID_FILE' => 'Файл идентификатора процесса XBT',
	'TRACKER_PID_FILE_EXPLAIN' => 'Файл в котором будет храниться идентификатор процесса XBT (pid файл)',
	'TRACKER_QUERY_LOG' => 'Файл логирования запросов',
	'TRACKER_QUERY_LOG_EXPLAIN' => 'Файл для логирования запросов к трекеру',
	'TRACKER_READ_CONFIG_INTERVAL' => 'Периодичность чтения файла конфигурации',
	'TRACKER_READ_CONFIG_INTERVAL_EXPLAIN' => 'Период времени через которое XBT будет перечитывать файл конфигурации',
	'TRACKER_READ_DB_INTERVAL' => 'Периодичность чтения конфигурации из БД',
	'TRACKER_READ_DB_INTERVAL_EXPLAIN' => 'Период времени через которое XBT будет перечитывать конфигурацию из базы данных',
	'TRACKER_WRITE_DB_INTERVAL' => 'Периодичность записи в БД',
	'TRACKER_WRITE_DB_INTERVAL_EXPLAIN' => 'Период времени через которое XBT будет записывать данные в базу данных',
	'TRACKER_REDIRECT_URL' => 'URL перенаправления для браузеров',
	'TRACKER_REDIRECT_URL_EXPLAIN' => 'URL на который будет перенаправлен пользователь в случае запроса адреса анонса через браузер',
	'TRACKER_TORRENT_PASS_PRIVATE_KEY' => 'Идентификатор трекера',
	'TRACKER_TORRENT_PASS_PRIVATE_KEY_EXPLAIN' => 'Уникальный идентификационный ключ трекера',
	'TRACKER_LISTEN_IPA' => 'IP адрес запуска XBT',
	'TRACKER_LISTEN_IPA_EXPLAIN' => 'IP адрес на котором запускать XBT',
	'TRACKER_TABLE_ANNOUNCE_LOG' => 'Таблица логирования анонсов',
	'TRACKER_TABLE_ANNOUNCE_LOG_EXPLAIN' => 'Имя таблицы в БД для логирования анонсов торрентов',
	'TRACKER_TABLE_SCRAPE_LOG' => 'Таблица логирования скрейпа',
	'TRACKER_TABLE_SCRAPE_LOG_EXPLAIN' => 'Имя таблицы в БД для логирования скрейпа торрентов',
	'TRACKER_TABLE_FILES' => 'Таблица файлов',
	'TRACKER_TABLE_FILES_EXPLAIN' => 'Имя таблицы в БД для файлов торрентов',
	'TRACKER_TABLE_FILES_USERS' => 'Таблица данных анонсов торрентов пользователей',
	'TRACKER_TABLE_FILES_USERS_EXPLAIN' => 'Имя таблицы в БД для данных анонсов торрентов пользователей',
	'TRACKER_TABLE_USERS' => 'Таблица данных пользователей',
	'TRACKER_TABLE_USERS_EXPLAIN' => 'Имя таблицы в БД для данных пользователей',
	'TRACKER_TABLE_DENY_FROM_HOSTS' => 'Таблица блокированных IP адресов',
	'TRACKER_TABLE_DENY_FROM_HOSTS_EXPLAIN' => 'Имя таблицы в БД для списка блокированных на трекере IP адресов',
	'TRACKER_FIXU_LIST' => 'Исправить список пользователей',
	'TRACKER_FIXU_LIST_EXPLAIN' => 'Пересоздать удалённые или потерянные данные в таблице данных пользователей XBT',
	'ACP_PPKBB_XBT' =>'Настройки XBT',
	'ACP_TRACKER'				=> 'Трекер',
	'ACP_TRACKER_ZONES'				=> 'Блокированные IP адреса',
	'ACP_TRACKER_ZONES_EXPLAIN'				=> 'Анонс торрентов на трекере с указанных IP адресов будет отклоняться',
	'ACP_TRACKER_ZONES_SETTINGS'				=> 'Управление блокированными IP адресами',
	'ZONE_ADDR_START' => 'Начало диапазона',
	'ZONE_ADDR_END' => 'Конец диапазона',
	'ACP_ANNOUNCELOG_LOGS' => 'Логи анонса',
	'ACP_ANNOUNCELOG_LOGS_EXPLAIN' => 'Логи анонса',
	'ACP_SCRAPELOG_LOGS' => 'Логи скрейпа',
	'ACP_SCRAPELOG_LOGS_EXPLAIN' => 'Логи скрейпа',
	'SORT_UPLOADED' => 'Роздано',
	'SORT_DOWNLOADED' => 'Скачано',
	'SORT_LEFT' => 'Осталось скачать',
	'SORT_EVENT' => 'Событие',
	'SORT_PEERID' => 'ID пира',
	'SORT_TTITLE' => 'Торрент',
	'SORT_PORT' => 'Порт',
	'EVENT_NONE' => '',
	'EVENT_STARTED' => 'Старт',
	'EVENT_STOPPED' => 'Стоп',
	'EVENT_COMPLETED' => 'Завершено',
	'TRACKER_OFFLINE_MESSAGE'		=> 'Отключить трекер',
	'TRACKER_OFFLINE_MESSAGE_EXPLAIN'		=> 'Отключает функции анонса и скрейпа, загрузка и скачиваниие торрентов не отключается, (пустая строка - не отключать, любое другое значение отключает трекер и будет возвращаться клиенту в качестве ответа)',

	'PPKBB_TCGZ_REWRITE' => 'Управление gz сжатием',
	'PPKBB_TCGZ_REWRITE_EXPLAIN' => 'Ответ клиенту при анонсе, автоматически - определять на основе принятых от клиента заголовков',
	'PPKBB_TCGZ_REWRITE_AUTO' => 'автоматически',
	'PPKBB_TCGZ_REWRITE_GZ' => 'принудительно в сжатом',
	'PPKBB_TCGZ_REWRITE_NONGZ' => 'принудительно в несжатом',
	'PPKBB_TCIPTYPE' => 'Определение IP адреса',
	'PPKBB_TCIPTYPE_EXPLAIN' => 'Метод определения IP адреса, (IP адреса в методах <em>по заголовку</em> и <em>по данным от клиента</em> могут быть подделаны пользователем, если эти методы вернут неправильный IP адрес, будет использован стандартный метод определения IP адреса)',
	'PPKBB_TCIPTYPE_STANDART' => 'стандартный',
	'PPKBB_TCIPTYPE_HEADER' => 'по заголовку X_FORWARDED_FOR',
	'PPKBB_TCIPTYPE_CLIENT' => 'по данным от клиента',
	'TRACKER_CHECK_BAN' => 'Проверка бана по форуму',
	'TRACKER_CHECK_BAN_EXPLAIN' => 'Проверять пользователя при анонсе на бан на форуме по имени, IP адресу, email адресу',
	'PPKBB_TCIGNORE_CONNECTABLE' => 'Игнорировать проверку на соединяемость',
	'PPKBB_TCIGNORE_CONNECTABLE_EXPLAIN' => 'Отключает проверку на возможность подключения к пиру, т.е. при анонсе будет выдавать список всех клиентов, а не только имеющих флаг connectable=yes',
	'TRACKER_IGNORED_UPLOAD' => 'Игнорировать значение аплоада',
	'PPKBB_TCIGNORE_CONNECTABLE_NOIGNORE' => 'не игнорировать',
	'PPKBB_TCIGNORE_CONNECTABLE_IGNORENOCHECK' => 'игнорировать и не проверять',
	'PPKBB_TCIGNORE_CONNECTABLE_IGNOREYESCHECK' => 'игнорировать и проверять',
	'TRACKER_MAXPEERS_LIMIT' => 'Ограничение на количество возвращаемых подсоединённых клиентов',
	'TRACKER_MAXPEERS_LIMIT_EXPLAIN' => 'Если клиентская программа не устанавливает ограничение на возвращаемое количество подключённых к торренту клиентов, используется это значение (0 - использовать данные программы, если не переопределено ниже)',
	'TRACKER_MAXPEERS_REWRITE' => 'Переопределение количества возвращаемых подсоединённых клиентов',
	'TRACKER_MAXPEERS_REWRITE_EXPLAIN' => 'Переопределять данные клиентской программы указанным выше значением',
	'TRACKER_IGNORED_UPLOAD' => 'Игнорировать значение аплоада',
	'TRACKER_IGNORED_UPLOAD_EXPLAIN' => 'Игнорировать аплоад если он превышает указанный здесь, рассчитывается из максимально возможного аплоада за указанный анонс интервал (на один торрент, 0 - отключить)',

	'TRACKER_PHPANNOUNCE_URL' => 'Анонс URL трекера',
	'TRACKER_PHPANNOUNCE_URL_EXPLAIN' => 'Путь от корневой директории форума до файла announce.php, по умолчанию: /tracker/announce.php, для корректного определения анонс URL так же необходимо проверить правильность настроек в опциях блока <u>Настройки URL сервера</u> (Администраторский раздел - Настройки сервера - Настройки URL сервера), пустая строка - будет использоваться анонс URL XBT трекера, при этом необходимо будет настроить переадресацию с этого адреса на announce.php',
	'TRACKER_PHPANNOUNCE_ENABLED' => 'Включить PHP анонс',
	'TRACKER_PHPANNOUNCE_ENABLED_EXPLAIN' => 'Включить возможность использования встроенного анонса и скрейпа',
	'PPKBB_PEERSTABLE_MEMORY' => 'Таблица пиров в памяти БД',
	'PPKBB_PEERSTABLE_MEMORY_EXPLAIN' => 'Использовать в БД тип <em>Memory</em> для таблицы пиров трекера (иначе будет использоваться <em>MyIsam</em>)',

	'PHPANNOUNCE_UNITED_SETTINGS' => 'Общие настройки',
));
