<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'AJAX_INVALID_ACTION' => 'Ошибка, неправильный режим.',
	'AJAX_NO_POST' => 'Ошибка, не указано сообщение.',
	'AJAX_POST_NOTFOUND' => 'Ошибка, не найдено сообщение.',
	'AJAX_NOT_TRACKER' => 'Ошибка, форум не является трекером.',
	'AJAX_NO_RIGHTS' => 'Ошибка, нет прав.',

	'SET_STATUS_TITLE' => 'Изменение статуса торрента',
	'SET_STATUS_SUCCESS' => 'Статус торрента успешно изменён, с <strong>%s</strong> на <strong>%s</strong>.',

	'STATUS_REASON' => 'Причина изменения статуса',
	'STATUS_PREASON'	=> '-- нельзя скачать',
	'STATUS_MREASON'	=> '-- можно скачать',
	'STATUS_UREASON'	=> '-- без статуса',

	'STATUS_NOTIFY_SUBJECT' => 'Изменение статуса торрента',
	'STATUS_NOTIFY_TEXT' => "Изменение статуса Вашего торрента: <a href='%s'>%s</a>\n\nНовый статус: %s\nПредыдущий статус: %s\nСтатус был изменён: %s\nПричини изменения: %s\nДата изменения: %s\n\n--\nЭто письмо было сгенерировано автоматически системой информирования статуса торрентов",

	'SET_REQUPRATIO_TITLE' => 'Изменение требуемого ратио или аплоада',
	'SET_REQUPRATIO_SUCCESS' => 'Значения успешно изменены.',
	'TORRENT_REQRATIO' => 'Требуемое ратио',
	'TORRENT_REQUPLOAD' => 'Требуемый аплоад',

	'BOOKMARKS_TITLE' => 'Будущие закачки',
	'BOOKMARKS_SUCCESS' => 'Будущие закачки успешно изменены',
	'ADD_BOOKMARKS' => 'Добавить',
));
?>
