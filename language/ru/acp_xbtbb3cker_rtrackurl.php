<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_XBTBB3CKER_RTRACKURL_EXPLAIN'				=> 'Дополнительный - данный анонс URL будет автоматически добавляться в каждый торрент файл.<br />Внешний и дополнительный - данный анонс URL будет добавляться во все торрент файлы, а так же самостоятельно анонсироваться трекером.<br />Строка {YOUR_PASSKEY} будет заменяться текущим пасскеем пользователя на трекере.',

	'RTRACKER_STAT'=>'Сидеров: <b>%d</b>, личеров: <b>%d</b>, пиров: <b>%d</b>, скачавших: <b>%d</b>, торрентов: <b>%d</b>, ошибок: <b>%d</b>, в среднем ошибок на каждый торрент: <b style="color:#FF0000;">%01.2f</b>',
	'RTRACKER'=>'Дополнительные анонс URL',
	'RTRACKER_UNITED'=>'дополнительный',
	'RTRACKER_REMOTE_UNITED'=>'внешний и дополнительный',
	'INVALID_RTRACK_URL' => 'Некорректный URL трекера',
	'FORB_RTRACK_URL' => 'Запрещённый URL трекера',
	'TYPE' => 'Тип',
	'ZONE_RTRACK_URL' => 'Url трекера',
	'RTRACK_ENABLED' => 'Включён',
	'RTRACK_FORB' => 'Запрещён',
	'RTF_TYPE' => array('r'=>'Регулярное выражение', 's' => 'Строка с учётом регистра', 'i' => 'Строка без учёта регистра'),
	'RTRACK_FORBS' => array(0=>'Нет', 1=>'в загружаемых торрентах', 2=>'в пользовательских трекерах', 3=>'везде'),

	'RTRACK_BACK' => '<br /><br /><a href="%s">Вернуться назад</a>',
));
