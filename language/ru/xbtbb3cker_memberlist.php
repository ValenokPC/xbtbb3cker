<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(

	//memberlist.php
	'TRACKER'				=> 'Трекер',
	'TRACKER_UP'				=> 'З',
	'TRACKER_DOWN'				=> 'С',
	'TRACKER_RATIO'				=> 'Р',
	'TRACKER_TORR'				=> 'Т',
	'TRACKER_COMM'				=> 'К',

	'TRACKER_UP_DESCR'				=> 'Загружено',
	'TRACKER_DOWN_DESCR'				=> 'Скачано',
	'TRACKER_RATIO_DESCR'				=> 'Ратио',
	'TRACKER_TORR_DESCR'				=> 'Торрентов',
	'TRACKER_COMM_DESCR'				=> 'Комментариев',

	'TORRENT_INFO_HEADER_TORRENT'		=> 'Загрузил',
	'TORRENT_INFO_HEADER_SEED'		=> 'Раздаёт',
	'TORRENT_INFO_HEADER_LEECH'		=> 'Скачивает',
	'TORRENT_INFO_HEADER_LEAVE'		=> 'Не раздаёт',
	'TORRENT_INFO_HEADER_HISTORY'		=> 'История',
	'TORRENT_INFO_HEADER_AUTHOR'		=> 'Автор',

	'TORRENT_INFO_HEADER_FINISHED'		=> 'Скачал',
	'TORRENT_INFO_HEADER_DOWNLOADS'		=> 'Скачал файлы',

	'TORRENT_USER_DATA' => 'Данные торрентов',
	'TRACKER_USER_DATA' => 'Данные трекера',
	'TRACKER_USER_COMMENTS' => 'Комментариев',
	'TRACKER_USER_DOWNLOAD' => 'Скачал',
	'TRACKER_USER_UPLOAD' => 'Раздал',
	'TRACKER_USER_RATIO' => 'Ратио',
	'TRACKER_USER_PASSKEY' => 'Пасскей',
	'UCP_PREFS_TRACKER'	=> 'Настройки трекера',
	'UCP_PREFS_TRACKER_DETAILS'	=> 'Настройки трекера',
	'UCP_PREFS_RETRACKER'	=> 'Мультитрекер',
	'UCP_PREFS_RETRACKER_DETAILS'	=> 'Мультитрекер',
	'UCP_MAIN_TRACKER' => 'Трекер',
	'TORRENT_INFO_HEADER_ANNCOUNT' => 'Анонсов',
	'TORRENT_INFO_HEADER_ACTIVE' => 'Активен',

	'POST_FROM_IP' => 'оставлял сообщения',
	'SEED_FROM_IP' => 'раздаёт торренты',
	'LEECH_FROM_IP' => 'скачивает торренты',
	'REG_FROM_IP' => 'зарегистрировался',
	'TORRENT_FROM_IP' => 'загружал торренты',
	'ONLINE_FROM_IP' => 'сейчас на форуме',
	'OFFLINE_FROM_IP' => 'посещал форум',

));
