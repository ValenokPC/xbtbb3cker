<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_XBTBB3CKER_LOSTTORRENTS_EXPLAIN'			=> 'Этот раздел можно использовать для просмотра потерянных (не зарегистрированных на трекере) торрентов и их восстановления, а также восстановления торрентов после удаления данных расширения или превращения обычных форумов с загруженными торрент файлами в трекер. Потерянные торренты отображаются только из форумов-трекеров.',

	'SORT_TTITLE' => 'Торрент',
	'SORT_FILENAME' => 'Имя файла',

	'FIX_ALL' => 'Исправить все',
	'FIX_MARKED' => 'Исправить отмеченные',
	'FIX_LOSTTORRENTS_RESULT' => '%s<br /><a href="%s">Вернуться назад</a>',
	'FIX_LOSTTORRENTS_FINISH' => 'Завершено<br /><br /><a href="%s">Вернуться назад</a>',
	'FIX_LOSTTORRENTS_SUCCESS' => 'Успех: ',
	'FIX_LOSTTORRENTS_ERROR' => '<span style="color:#FF0000;">Ошибка</span>: ',
	'FIX_LOSTTORRENTS_WAIT' => 'Подождите ..<br /><br />',

	'TOTAL_LOGS' => 'Всего записей: <strong>%d</strong>',
));
