<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'ACP_TRACKER_GROUPSET' => 'Групповые действия',
	'ACP_TRACKER_GROUPSET_EXPLAIN' => 'Изменение опций или тем/торрентов одного или нескольких форумов',
	'ACP_TRACKER_GROUPSET_SETTINGS' => 'Групповые действия',

	'GROUPSET' => array(
		'FTYPE' => 'Назначение типа форума',
		'QREPLY' => 'Назначение быстрого ответа в форумах',
		'STATUSES' => 'Назначение статусов на торренты',

	),
	'GROUPSET_DESCR' => array(
		'FTYPE' => 'Установить тип форума',
		'QREPLY' => 'Установить возможность быстрого ответа в форумах',
		'STATUSES' => 'Установить статус торрента на все торренты в выбранных форумах',

	),

	'GROUPSETS_ACTION' => 'Действие',
	'GROUPSETS_SET' => 'Назначить',
	'GROUPSETS_FORUMS' => 'На форумы',
	'GROUPSETS_DESCR' => 'Описание',

	'GS_OPTIONS_ALL' => 'Все',
	'GS_OPTIONS_SELECTED' => 'Только выбранные',
	'GS_OPTIONS_NOTSELECTED' => 'Только не выбранные',

	'GS_TORRENT_FTYPE' => 'Тип форума',
	'GS_TORRENT_FTYPE_FORUM' => 'Форум',
	'GS_TORRENT_FTYPE_TRACKER' => 'Трекер',

	'GS_TORRENT_QUICKREPLY' => 'Форма быстрого ответа в форумах',

	'GS_TORRENT_STATUS' => 'Статус торрента',
	'GS_TORRENT_STATUS_EXPLAIN' => '<span style="color:#FF0000;">При большом количестве торрентов и выборе большого количества форумов или назначении статуса на все форумы, выполнение действия может занять определённое время и создать нагрузку на сервер.</span>',
	'STATUS_PREASON'	=> '-- нельзя скачать',
	'STATUS_MREASON'	=> '-- можно скачать',
	'STATUS_UREASON'	=> '-- без статуса',

	'GS_VIEW_CURR' => 'Показать текущие (в скобках)',
));
?>
