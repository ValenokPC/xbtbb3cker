<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(


	//viewtopic
	'USER_TORRENTS'				=> 'Торренты',
	'USER_COMMENTS'				=> 'Комментарии',
	'USER_RATIO'				=> 'Ратио',
	'USER_DOWNLOADS' => 'Скачал',
	'USER_UPLOADS' => 'Раздал',
	'TORRENT_OPTION' => 'Опции',
	'TORRENT_ACTION' => '=>',
	'TORRENT_LASTSEED'			=> 'Последний сидер',
	'TORRENT_LASTLEECH'			=> 'Последний личер',
	'TORRENT_LAST_TIME'			=> 'назад',
	'TORRENT_SSPEED' => 'Скорость раздачи',
	'TORRENT_LSPEED' => 'Скорость скачивания',

	'TORRENT_INFO_HEADER_TRACKER' => 'Трекер',
	'TORRENT_INFO_HEADER_NANNOUNCE' => 'Анонс через',
	'TORRENT_INFO_HEADER_AMESSAGE' => 'Сообщение',
	'TORRENT_INFO_HEADER_AINTERVAL' => 'Интервал',
	'TORRENT_INFO_HEADER_ERRCOUNT' => 'Ошибок',
	'TORRENT_INFO_HEADER_PEERS' => 'Пиров',
	'TORRENT_INFO_HEADER_BOOKMARKS'		=> 'В Будущих закачках',

	'TORRENT_AUTOR'	=> 'Автор',
	'TORRENT_STATUS_AUTHOR'	=> 'Изменил',
	'TORRENT_STATUS_DT'	=> 'Дата',
	'TORRENT_STATUS_REASON'	=> 'Причина',
	'TORRENT_PRIVATE'	=> 'Приватный',
	'TORRENT_PRIVATE_YES'	=> 'Да (DHT отключён)',
	'TORRENT_PRIVATE_NO'	=> 'Нет (DHT включён)',

	'TRACKER_USER_TORRENTS' => 'Загрузил',
	'TRACKER_USER_SEED' => 'Раздаёт',
	'TRACKER_USER_LEECH' => 'Скачивает',

	'TRACKER_REMSEEDS' => ' Сидеров на внешних трекерах: <b>%d</b>',
	'TRACKER_REMLEECHS' => 'Личеров на внешних трекерах: <b>%d</b>',
	'TRACKER_REMCOMPLETED' => 'Скачавших на внешних трекерах: <b>%d</b>',
	'IN_ANNOUNCE' => 'Анонсируется ..',

	'TORRENT_NOTIFY_SUBJECT' => 'Изменение статуса торрента',
	'TORRENT_NOTIFY_TEXT' => "Изменение статуса вашего торрента: <a href='%s'>%s</a>\n\nНовый статус: %s\nПредыдущий статус: %s\nСтатус был изменён: %s\nПричини изменения: %s\nДата изменения: %s\n\n--\nЭто письмо было сгенерировано автоматически системой информирования статуса торрентов.",

	'WAIT' => 'Подождите',
	'WO_REM_PEERS' => 'без учёта внешних пиров',

	'TORRENT_INFO_HEADER_ANNCOUNT' => 'Анонсов',
	'TORRENT_INFO_HEADER_ACTIVE' => 'Активен',

	'FILE_SIZE' => 'Размер',
	'FILE_DOWNLOADED' => 'Скачиваний',
	'TORRENT_INFO_ADDIT' => 'Подробнее',
	'TORRENT_INFO_STAT' => 'Статистика',

	'TORRENT_STATUS' => 'Статус',
	'SET_STATUS' => 'Изменить статус',
	'STATUS_EXPLAIN' => 'Установлен: %1$s [%2$s], причина: %3$s',
	'SET_REQUPRATIO' => 'Установить требуемое для скачивание торрента ратио или аплоад',
	'TORRENT_REQUPRATIO' => 'Ратио: <strong>%s</strong>, Аплоад: <strong>%s</strong>',
	'TORRENT_REQUPRATIO_EXPLAIN' => 'Для скачивания торрента необходимы значения ратио и аплоада не ниже указанных',

	'ADD_BOOKMARKS' => 'Добавить в будущие закачки',
	'ADD_BOOKMARKS_EXPLAIN' => ', в всплывающей подсказке указано количество пользователей добавивших торрент в будущие закачки',
));
