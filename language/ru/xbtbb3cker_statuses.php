<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
   exit;
}

if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'WO_STATUS_REASON' => 'без статуса',
	'WO_STATUS_MARK' => 'без статуса',
	'FORBIDDEN_STATUS_REASON' => 'запрещённый торрент',
	'FORBIDDEN_STATUS_MARK' => '<font color="#FF0000">Запрещён</font>',
	'FORMALIZATION_STATUS_REASON' => 'недооформленный торрент',
	'FORMALIZATION_STATUS_MARK' => '<font color="#FF3F47">Недооформлен</font>',
	'REPEAT_STATUS_REASON' => 'повторный торрент',
	'REPEAT_STATUS_MARK' => '<font color="#FF3F47">Повторный</font>',
	'ABSORBED_STATUS_REASON' => 'поглощённый торрент',
	'ABSORBED_STATUS_MARK' => '<font color="#FF3F47">Поглощён</font>',
	'NONCHECKED_STATUS_REASON' => 'непроверенный торрент',
	'NONCHECKED_STATUS_MARK' => '<font color="#FF3F47">Непроверен</font>',
	'UPDATED_STATUS_REASON' => 'обновлённый торрент',
	'UPDATED_STATUS_MARK' => '<font color="#09AF00">Обновлён</font>',
	'CHECKED_STATUS_REASON' => 'проверенный торрент',
	'CHECKED_STATUS_MARK' => 'Проверен',
	'NEW_STATUS_REASON' => 'новый торрент',
	'NEW_STATUS_MARK' => '<font color="#0000FF">Новый</font>',
	'COPYRIGHT_STATUS_REASON' => 'жалоба правообладателя',
	'COPYRIGHT_STATUS_MARK' => '<font color="#000000">Закрыт правообладетелем</font>',
));
?>
