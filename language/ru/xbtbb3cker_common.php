<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

if (!defined('IN_PHPBB'))
{
	exit;
}
if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	//common
	'PPK_TRACKER'					=> 'Трекер',
	'TRACKER_ANNOUNCE_URL'				=> 'Анонс URL трекера',
	'ATTACHMENTS_TORRENT'					=> 'Торрент',
	'ATTACHMENTS_FILE'					=> 'Файл',
	'ATTACHMENTS_POSTER'					=> 'Постеры',
	'ATTACHMENTS_SCREENSHOT'					=> 'Скриншоты',
	'TORRENT_CREATED_FOR'			=>'Торрент создан для',
	'TORRENT_LEECHERS'			=> 'Личеров',
	'TORRENT_SEEDERS'			=> 'Сидеров',
	'TORRENT_COMPLETED'			=> 'Скачан',
	'AUTHOR_TORRENT_COMPLETED'			=> 'Скачавших',
	'TORRENT_REAL_LEECHERS'			=> 'Трекерных личеров',
	'TORRENT_REAL_SEEDERS'			=> 'Трекерных сидеров',
	'TORRENT_REAL_COMPLETED'			=> ' Скачали с трекера',
	'TORRENT_REM_LEECHERS'			=> 'Внешних личеров',
	'TORRENT_REM_SEEDERS'			=> 'Внешних сидеров',
	'TORRENT_REM_COMPLETED'			=> 'Скачали с внешних трекеров',
	'TORRENT_VIEWED'			=> 'Просмотров',
	'TORRENT_HEALTH'			=> 'Здоровье',
	'TORRENT_PERCENT'			=> 'Раздающих',
	'TORRENT_FILES'			=> 'Файлов',
	'TORRENT_SIZE'			=> 'Размер',
	'TORRENT_STATUS'	=> 'Статус',
	'TORRENT_ADDED'			=> 'Добавлен',
	'TORRENT_AUTHOR'			=> 'Автор',
	'TORRENT_NAME'			=> 'Название',
	'VOTING_TORRENT'			=> 'Оценка торрента',
	'VOTING_TORRENT_VALS'			=> array('5', '4', '3', '2', '1'),
	'MESS_MAX_TORRENTS'			=> 'Максимальное количество торрент файлов в одном сообщении: %d',
	'MESS_MAX_POSTERS'			=> 'Максимальное количество постеров в одном сообщении: %d',
	'MESS_MAX_SCREENSHOTS'			=> 'Максимальное количество скриншотов в одном сообщении: %d',
	'MESS_MIN_TORRENTS'			=> 'Минимальное количество торрент файлов в одном сообщении: %d',
	'MESS_MIN_POSTERS'			=> 'Минимальное количество постеров в одном сообщении: %d',
	'MESS_MIN_SCREENSHOTS'			=> 'Минимальное количество скриншотов в одном сообщении: %d',
	'G_UPLOADERS'			=> 'Аплоадеры',
	'G_VIPUPLOADERS'			=> 'VIP аплоадеры',
	'G_SUPERUPLOADERS'			=> 'Супераплоадеры',
	'G_EXPUPLOADERS'			=> 'Опытные аплоадеры',
	'G_LEECHERS'			=> 'Личеры',
	'G_EXPLEECHERS'			=> 'Опытные личеры',
	'G_TRUSERS'			=> 'Посетители трекера',
	'G_CHATUSERS'			=> 'Пользователи чата',
	'G_PRIVTRUSERS'			=> 'Приватные пользователи',
	'G_MODERATORS'			=> 'Модераторы',
	'G_JRMODERATORS'			=> 'Младшие модераторы',
	'DOWNLOAD_TORRENT_SHORT'	=> 'Скачать',
	'DOWNLOAD_TORRENT'	=> 'Скачать торрент',
	'DOWNLOAD_POSTER'	=> 'Скачать постер',
	'DOWNLOAD_SCREENSHOT'	=> 'Скачать скриншот',
	'DISPLAY_TORRENTS'		=> 'Отображать торренты',
	'DISPLAY_TORRENTS_ALL'		=> 'все',
	'DISPLAY_TORRENTS_WBAD'		=> 'без испорченных',
	'DISPLAY_TORRENTS_GOOD'		=> 'активные',
	'DISPLAY_TORRENTS_DEAD'		=> 'мёртвые',
	'DISPLAY_TORRENTS_NOSEED'		=> 'без сидеров',
	'DISPLAY_TORRENTS_BAD'		=> 'испорченные',

	'TORRENT_INFO'		=> 'О торренте',

	'TORRENT_INFO_HEADER_'		=> '',
	'TORRENT_INFO_HEADER_FILELIST'		=> 'Список файлов',
	'TORRENT_INFO_HEADER_SEED'		=> 'Раздают',
	'TORRENT_INFO_HEADER_LEECH'		=> 'Скачивают',
	'TORRENT_INFO_HEADER_FINISHED'		=> 'Кто скачал',
	'TORRENT_INFO_HEADER_HISTORY'		=> 'История',
	'TORRENT_INFO_HEADER_LEAVE'		=> 'Не раздающие',
	'TORRENT_INFO_HEADER_REMOTE'		=> 'Внешние трекеры',
	'TORRENT_INFO_HEADER_DOWNLOADS'		=> 'Скачали файл',
	'TORRENT_INFO_HEADER_BOOKMARKS'		=> 'Будущие закачки',
	'TORRENT_INFO_HEADER_ADD_DATE'		=> 'Дата',

	'TORRENT_INFO_HEADER_FILENAME'	=> 'Файл',
	'TORRENT_INFO_HEADER_SIZE'	=> 'Размер',
	'TORRENT_INFO_HEADER_USERNAME'	=> 'Пользователь',
	'TORRENT_INFO_HEADER_DOWNLOADED'	=> 'Скачал',
	'TORRENT_INFO_HEADER_UPLOADED'	=> 'Раздал',
	'TORRENT_INFO_HEADER_RATIO'	=> 'Ратио',
	'TORRENT_INFO_HEADER_TORRENTS'	=> 'Торренты',
	'TORRENT_INFO_HEADER_SEEDER'	=> 'Сидирует',
	'TORRENT_INFO_HEADER_SEEDER2'	=> 'Сидирую',
	'TORRENT_INFO_HEADER_NOWSEED_YES'	=> 'Да',
	'TORRENT_INFO_HEADER_NOWSEED_NO'	=> 'Нет',
	'TORRENT_INFO_HEADER_DOWNTIME'	=> 'Скачал за',
	'TORRENT_INFO_HEADER_TO_GO'	=> 'Завершено',
	'TORRENT_INFO_HEADER_COND'	=> 'Состояние',
	'TORRENT_INFO_HEADER_CONDSEED'	=> 'Сидер',
	'TORRENT_INFO_HEADER_CONDLEECH'	=> 'Личер',
	'TORRENT_INFO_HEADER_PERCENT'	=> 'Завершено',
	'TORRENT_INFO_HEADER_AGENT'	=> 'Клиент',
	'TORRENT_INFO_HEADER_A'	=> 'Соединение',
	'TORRENT_INFO_HEADER_INA'	=> 'Бездействие',
	'TORRENT_INFO_HEADER_CONNECTABLE'	=> 'Соединяемый',
	'TORRENT_INFO_HEADER_UPSPEED'	=> 'Скорость',
	'TORRENT_INFO_HEADER_DOWNSPEED'	=> 'Скорость',
	'TORRENT_INFO_HEADER_COMPLETEDAT'	=> 'Конец',
	'TORRENT_INFO_HEADER_STARTDAT'	=> 'Начало',
	'TORRENT_INFO_HEADER_CONNECT'	=> 'Соединение',
	'TORRENT_INFO_HEADER_LAST_ACTION'	=> 'Последнее',
	'TORRENT_INFO_HEADER_IP'	=> 'IP адрес',
	'TORRENT_INFO_HEADER_PORT'	=> 'Порт',
	'TORRENT_INFO_HEADER_TOPIC_TITLE'	=> 'Торрент',
	'TORRENT_INFO_HEADER_TADDED'	=> 'Дата',
	'TORRENT_INFO_HEADER_DL_TIME'	=> 'Дата',
	'TORRENT_INFO_HEADER_ADDED'	=> 'Дата',
	'TORRENT_INFO_HEADER_BONUSES'	=> 'Бонусы',
	'TORRENT_INFO_HEADER_RTRACK_URL'	=> 'Трекер',
	'TORRENT_INFO_HEADER_NEXT_ANNOUNCE'	=> 'Анонс через',
	'TORRENT_INFO_HEADER_A_MESSAGE'	=> 'Сообщение',
	'TORRENT_INFO_HEADER_A_INTERVAL'	=> 'Интервал',
	'TORRENT_INFO_HEADER_ERR_COUNT'	=> 'Ошибок',
	'TORRENT_INFO_HEADER_SEEDERS'	=> 'Сидеров',
	'TORRENT_INFO_HEADER_LEECHERS'	=> 'Личеров',
	'TORRENT_INFO_HEADER_TIMES_COMPLETED'	=> 'Скачали',
	'TORRENT_INFO_HEADER_NOWSEED2' => 'Сидирую',
	'TORRENT_INFO_HEADER_ANNOUNCED' => 'Анонсов',
	'TORRENT_INFO_HEADER_ACTIVE' => 'Активен',
	'TORRENT_INFO_HEADER_MTIME' => 'Последнее',
	'TORRENT_INFO_HEADER_LEFT' => 'Завершено',

	'TORRENT_UNKNOWN_HEALTH'	=> 'неизвестно',
	'TORRENT_UNKNOWN_CONNECTABLE'	=> 'неизвестно',
	'TORRENT_UNKNOWN_SIZE'	=> 'неизвестен',
	'TORRENT_UNKNOWN_STATUS'	=> 'неизвестен',
	'STATUS_NO_REASON' => 'не указана',
	'TSEC'	=> 'сек',
	'TMIN'	=> 'мин',
	'THOUR'	=> 'ч',
	'TDAY'	=> 'д',
	'SEARCH_UNANSWERED_TORRENT'	=> 'Торренты без комментариев',
	'SEARCH_UNREAD_TORRENT'	=> 'Непрочитанное в торрентах',
	'SEARCH_ACTIVE_TORRENT'	=> 'Активные торренты',
	'SEARCH_ALL_TORRENT'	=> 'Торренты',
	'SEARCH_NEW_TORRENT'	=> 'Новые торренты',
	'SEARCH_SELF_TORRENT'	=> 'Ваши торренты',
	'SEARCH_USER_TORRENT'	=> 'Торренты пользователя',
	'SEARCH_ALL_COMMENT'	=> 'Комментарии',
	'SEARCH_SELF_COMMENT'	=> 'Ваши комментарии',
	'SEARCH_USER_COMMENT'	=> 'Комментарии пользователя',
	'SEARCH_NEW_COMMENT'	=> 'Новые комментарии',

	'TOTAL_TORRENT'	=> 'Торрентов: <strong>%d</strong>',
	'TOTAL_COMMENT'	=> 'Комментариев: <strong>%d</strong>',
	'TOTAL_PEER'	=> 'Пиров: <strong>%d</strong>',
	'TOTAL_SEED'	=> 'Раздающих: <strong>%d</strong>',
	'TOTAL_LEECH'	=> 'Скачивающих: <strong>%d</strong>',
	'TOTAL_SEED_WR'	=> 'Раздающих: <strong class="my_tt" title="Внешних: %2$d">%1$d</strong>',
	'TOTAL_LEECH_WR'	=> 'Скачивающих: <strong class="my_tt" title="Внешних: %2$d">%2$d</strong>',
	'TOTAL_COMPLET'	=> 'Скачано торрентов: <strong>%d</strong>',
	'TOTAL_COMPLET_WR'	=> 'Скачано торрентов: <strong class="my_tt" title="На внешних трекерах: %2$d">%1$d</strong>',
	'TOTAL_REMSEED'	=> 'Внешних раздающих: %d',
	'TOTAL_REMLEECH'	=> 'Внешних скачивающих: %d',
	'TOTAL_REMCOMPLET'	=> 'Скачано торрентов на внешних трекерах: %d',
	'TOTAL_SIZE'	=> 'Общий размер торрентов: <strong>%s</strong>',
	'TOTAL_UP'	=> 'Всего роздано: <strong>%s</strong>',
	'TOTAL_DOWN'	=> 'Всего скачано: <strong>%s</strong>',
	'TOTAL_SUP'	=> 'Роздано за сессию: <strong>%s</strong>',
	'TOTAL_SDOWN'	=> 'Скачано за сессию: <strong>%s</strong>',
	'TOTAL_TDOWN'	=> 'Раздаваемых торрентов: <strong>%s</strong>',
	'TOTAL_TUP'	=> 'Скачиваемых торрентов: <strong>%s</strong>',
	'TOTAL_UDOWN'	=> 'Раздающих пользователей: <strong>%s</strong>',
	'TOTAL_UUP'	=> 'Скачивающих пользователей: <strong>%s</strong>',
	'TOTAL_SPEEDDOWN'	=> 'Суммарная скорость скачивания: <strong>%s</strong>',
	'TOTAL_SPEEDUP'	=> 'Суммарная скорость раздачи: <strong>%s</strong>',

	'TORRENT_HASH'			=> 'Хэш',
	'DUPLICATED_PASSKEY'			=> 'Данный пасскей уже существует',
	'CANT_DOWN_POSTSCR'	=> 'Вы НЕ можете скачивать постеры и скриншоты',

	'TORRENT_WAIT_NEVER'	=> 'Скачивание торрента невозможно, большое значение скачанного, необходимо поднять ратио или увеличить аплоад',

	'TORRENT_SUBJECT'	=> 'Тема торрента',

	'TRACKER_SEARCHED_TRACKER'			=> 'на трекере',
	'TRACKER_SEARCHED_TORRENT'			=> 'в торрентах',
	'TRACKER_SEARCHED_COMMENT'			=> 'в комментариях',
	'TRACKER_SEARCHED_FORUM'			=> 'на форуме',
	'TRACKER_SEARCHED_ALL'			=> 'везде',
	'DISPLAY_RESULTS'		=> 'Показывать результаты как',
	'RETURN_BACK' => '<br /><br /><a href="%s">Вернуться назад</a>',


	'PB'					=> 'ПБ',
	'PIB'					=> 'ПБ',
	'EB'					=> 'ЭБ',
	'EIB'					=> 'ЭБ',

	'TRACKER_USER_TORRENTS' => 'Торрентов',
	'TRACKER_TOP' => 'Топ %d трекера',

	'TRACKER_RATIO_ANNOT'	=> 'от 0.001',

	'DOWNLOAD_FOR'	=> 'Скачан',

	'TRGUESTS_DISABLED' => 'Трекер для гостей отключён, для скачивания необходимо зарегистрироваться и/или войти на трекер',

	'TORRENT_DELETED' => 'Торрент не существует',
	'TORRENT_UNREGISTERED' => 'Незарегистрированный торрент',
	'USER_DELETED' => 'Пользователь не существует',

	'PPKBB_VERSION_UPDATED' => 'Версия трекера обновлена',
	'USER_RATIOS' => array(),//array('Leech.'=>'Личер', 'Seed.'=>'Сидер', 'Inf.'=>'Новичок', 'None.'=>'Нет ратио'),//НЕ используйте html код
	'SHOW_HIDE' => 'Показать/скрыть',
	'UPLOAD_TORRENT' => 'Загрузить торрент',

	'FEED_ALL_TORRENTS' => 'Все торренты',
	'FEED_ALL_COMMENTS' => 'Все комментарии',
	'FEED_TORRENTS' => 'Торренты в форуме',
	'FEED_COMMENTS' => 'Комментарии в форуме',
	'FEED_COMMENT' => 'Комментарии в теме',

	'DT_SLENGTHMENU' => 'Отображаемое количество строк: _MENU_',
	'DT_SZERORECORDS' => 'Нет записей',
	'DT_SINFO' => 'Строки с _START_ по _END_ (всего: _TOTAL_)',
	'DT_SINFOEMTPY' => 'Строки с 0 по 0 (всего: 0)',
	'DT_SINFOFILTERED' => '(отфильтровано из _MAX_ строк)',
	'DT_SSEARCH' => 'Фильтр: ',
	'DT_SNEXT' => '>',
	'DT_SPREVIOUS' => '<',
	'DT_SFIRST' => '|<<',
	'DT_SLAST' => '>>|',
	'DT_HIDE' => 'Скрыть',

	'TOPDOWN_TORRENTS_ASNEWTORRENTS'		=> 'Новинки трекера',
	'TOPDOWN_TORRENTS_ASNEWTORRENTS_INFORUM'		=> 'Новинки трекера в форуме "%s"',
	'TOPDOWN_TORRENTS' => 'Топ скачиваемых торрентов',
	'TOPDOWN_TORRENTS_INFORUM' => 'Топ скачиваемых торрентов в форуме "%s"',
	'NO_TDT' => 'Нет подходящих торрентов для отображения',
	'TDT_WAIT' => 'Пожалуйста подождите, торренты загружаются ..',

	'TORRENT_TOP_NOTICE' => '',
	'TORRENT_BOTTOM_NOTICE' => '',
	'TORRENT_TOP_NOTICE_GUEST' => '',
	'TORRENT_BOTTOM_NOTICE_GUEST' => '',

	'TRACKER_BOTTOM_DISCLAIMER' => 'Ресурс не предоставляет электронные версии произведений, а занимается лишь коллекционированием и каталогизацией ссылок, присылаемых и публикуемых на форуме нашими читателями. Если вы являетесь правообладателем какого-либо представленного материала и не желаете чтобы ссылка на него находилась в нашем каталоге, свяжитесь с нами и мы незамедлительно удалим её. Файлы для обмена на трекере предоставлены пользователями сайта, и администрация не несёт ответственности за их содержание. Просьба не заливать файлы, защищенные авторскими правами, а также файлы нелегального содержания!',
	'TRACKER_ANONYMOUS' => 'Гость',
	'TRDATA_WITHOUT_GUESTS' => 'Незарегистрированные пользователи (гости) не отображаются',

	'SEARCH_FULL_PHRASE' => 'Искать фразу целиком',
	'SEARCH_RESULTS_ASTOPICS' => 'Результаты в виде тем',
	'SEARCH_TITLE' => 'Название',
	'SEARCH_PHRASE' => 'Фразу',
	'SEARCH_TITLE_ONLY'			=> 'Только по названию темы',
	'TRSEARCH_OPTIONS' => 'Опции',
	'TORRENT_MAGNET_LINK' => 'Скачать через магнет ссылку',
	'TORRENT_HASH_LINK' => 'Скачать через хэш',
	'LOGORREG_DOWNLOAD' => 'Для скачивания торрента необходимо <a href="%s"><strong>зарегистрироваться</strong></a> или <a href="%s"><u>войти</u></a> на трекер',

	'TORRENT_UPLOAD_ERRORS'=>'Ошибка загрузки торрента (%d)',
	'TORRENT_ERROR_PRIVATE'=>'Загружаемый торрент является приватным, разрешено загружать только НЕ приватные торренты',
	'TORRENT_ERROR_NONPRIVATE'=>'Загружаемый торрент НЕ является приватным, разрешено загружать только приватные торренты',
	'TORRENT_UPLOAD_ERROR'	=> 'Ошибка загрузки торрента, данный торрент уже <a class="postlink" href="%s">существует</a>, сообщение одобрено: <span style="color:#FF0000">%s</span>, статус торрента: <strong>%s</strong>',

	'TRACKER_STATISTICS' => 'Трекер',
	'STAT_UPDATE_INTERVAL' => 'Интервал обновления статистики (в минутах): %d',
	'TORRENTS_COUNT' => 'Торрентов',

	'USER_RESTRICTS_HEADER' => 'Предупреждение',
	'USER_RESTRICTS_EXPLAIN' => 'Из-за низкого значения ратио, загруженного или высокого значения скачанного (%s), для вас автоматически были применены ограничения (<em>период перепроверки ограничениий %s</em>):<br />',
	'USER_RESTRICTS_RATIO' => 'ратио меньше %s',
	'USER_RESTRICTS_RATIO_TEXT' => 'ратио равно %s',
	'USER_RESTRICTS_UPLOAD' => 'загружено меньше %s',
	'USER_RESTRICTS_DOWNLOAD' => 'скачано больше %s',
	'USER_RESTRICTS_LEECH' => 'Вы можете раздавать торренты, но <strong>не можете скачивать</strong><br />',
	'USER_RESTRICTS_WAIT' => 'Время ожидания для скачивания торрентов%2$s: <strong>%1$s</strong><br />',
	'USER_RESTRICTS_INCLUDE' => ' (включая торрент файлы)',
	'USER_RESTRICTS_PEERS' => 'Максимальное количество одновременных соединений: <strong>%d</strong><br />',
	'USER_RESTRICTS_TORRENTS' => 'Максимальное количество торрентов для одновременного скачивания: <strong>%d</strong><br />',
	'USER_RESTRICTS_DAYS' => 'Максимальное количество торрентов файлов для скачивания в день: <strong>%d</strong><br />',

	'FUNCTION_DISABLED' => 'Функция отключена',
	'TORRENT_FINISHED' => 'Скачан',
	'TRACKER_PAGE' => 'Страница',
	'MUA_ALL' => 'Все',

	//rss
	'TRFEED' => 'Канал трекера',
	'ALL_TRACKERS' => 'Все трекеры',
	'FEED_TORRENTS_NEW' => 'Новые торренты',
	'FEED_TORRENTS_ACTIVE' => 'Активные торренты',
	'TRFEED_OVERALL' => 'Торренты и комментарии',
	'TRACKER_TORRENTS' => 'Торренты',
	'TRACKER_COMMENTS' => 'Комментарии',



	//posting
	'VIEW_TORRENT'				=> '%s<b>Скачайте</b>%s загруженный на трекер торрент файл и откройте его в своём торрент клиенте',
	'VIEW_EDITED_TORRENT'				=> 'Если Вы перезагрузили торрент файл %s<b>скачайте</b>%s заново загруженный на трекер торрент файл и откройте его в своём торрент клиенте',

	'TRACKER_ADD_ATTACHMENT' => 'Загрузить торрент, постер или скриншот',
	'TRACKER_ADD_ATTACHMENT_EXPLAIN' => 'Максимально допустимое количество торрентов: %d (%d), постеров: %d (%d), скриншотов %d (%d), в (скобках) указано минимально необходимое количество',
	'UPAS_POSTER' => 'Загрузить как постер',
	'TRACKER_ANNWARN' => 'Удалить другие анонс URL',
	'TRACKER_ANNWARN_EXPLAIN' => 'на трекере включена обработка анонс URL содержащихся в загружаемых торрент файлах, если в загружаемом торрент файле содержится приватный (личный) пасскей или вы не хотите, чтобы торрент файл содержал другие анонс URL - отметьте эту опцию',

	'TORRENT_UPLOAD_ERRORS'=>'Ошибка загрузки торрента (%d)',
	'TORRENT_ERROR_PRIVATE'=>'Загружаемый торрент является приватным, разрешено загружать только НЕ приватные торренты',
	'TORRENT_ERROR_NONPRIVATE'=>'Загружаемый торрент НЕ является приватным, разрешено загружать только приватные торренты',

	'POSTSCR_URL_INVALID'			=> 'Указанный адрес изображения недопустим',
	'POSTSCR_NO_SIZE'				=> 'Не удалось определить размеры указанного изображения, Пожалуйста, введите их вручную',
	'POSTSCR_WRONG_SIZE'				=> 'Размеры указанного изображения — %5$d×%6$d. Размеры изображения должны быть не менее %1$d×%2$d, но не более %3$d×%4$d. Все размеры указаны в пикселах',
	'POSTSCR_WRONG_FILESIZE'				=> 'Вес указанного изображения — %1$d байт (%2$s). Вес изображения должен быть не более %3$d байт (%4$s)',
	'POSTSCR_WHMANUAL_TITLE'		=> 'Вручную указать размеры изображения',
	'POSTSCR_WIDTH'		=> 'Ширина',
	'POSTSCR_HEIGHT'		=> 'Высота',
	'FORB_EXTPOSTSCR' => 'Внешние постеры и/или скриншоты с указанных доменов запрещены: %s',
	'FORB_EXTPOSTSCR_TRUEEXCLUDE' => 'Разрешены внешние постеры и/или скриншоты только с указанных доменов: %s',
	'EXT_POSTERS' => 'Внешние постеры',
	'EXT_SCREENSHOTS' => 'Внешние скриншоты',
	'EXT_POSTER' => 'Постер',
	'EXT_SCREENSHOT' => 'Скриншот',
	'MAX_EXT_POSTERS' => 'Максимально допустимое количество внешних постеров: %d (%d), в (скобках) указано минимально необходимое количество',
	'MAX_EXT_SCREENSHOTS' => 'Максимально допустимое количество внешних скриншотов: %d (%d), в (скобках) указано минимально необходимое количество',
	'EXTPOSTERS_REQUIRED' => 'Минимально необходимое количество внешних постеров: %d',
	'EXTSCREENSHOTS_REQUIRED' => 'Минимально необходимое количество внешних скриншотов: %d',
	'SUCCESS_BUT' => '<strong>Сообщение успешно создано, но в процессе произошли следующие ошибки</strong>: %s, пожалуйста проверьте сообщение',

	'UPLOAD_TORRENT_DISABLED' => 'Страница отключена',
	'UPLOAD_TORRENT_NOAUTH' => 'Нет доступных форумов',

	'TORRENT_APPROVAL_NOTIFY' => '<span style="color:#FF0000; font-weight:bold;">Ваш торрент будет доступен для скачивания после одобрения модератором,  Вы будете уведомлены об одобрении вашего торрента</span>',

	'TRACKER_POLL_QUEST' => 'Оценка торрента',
	'TRACKER_POLL_ANSW5' => 'Отлично',
	'TRACKER_POLL_ANSW4' => 'Хорошо',
	'TRACKER_POLL_ANSW3' => 'Нормально',
	'TRACKER_POLL_ANSW2' => 'Плохо',
	'TRACKER_POLL_ANSW1' => 'Ужасно',


	//file.php
	'NOT_IN_TRACKER' => 'Торрент не находится в форуме-трекере',
	'NOT_TORRENT_EXTENSION' => 'Файл не является торрентом',
	'CANT_DOWN_TORRENT'	=> 'Вы НЕ можете скачивать торрент файлы',
	'CANTDOWN_TORRENT_STATUS'	=> 'Торрент имеет статус запрещающий скачивание',
	'TORRENT_NOT_REGISTERED'	=> 'Торрент не зарегистрирован на трекере',
	'NO_TORRENTS_IN_PM'	=> 'Торрент прикреплён в личном сообщении, скачивание невозможно',
	'MAGNET_DOWNLOADS_DISABLED'	=> 'Скачивание через магнет ссылки запрещено',
	'HASH_DOWNLOADS_DISABLED'	=> 'Скачивание через хэш запрещено',
	'TORRENT_DOWNLOADS_DISABLED'	=> 'Скачивание через торрент файлы запрещено',
	'BOTS_NOT_ALLOWED'	=> 'WTF bro?!',
	'TORRENT_REQUPLOAD_ERROR' => 'Ограничения по аплоаду, вы НЕ можете скачать этот торрент файл, необходимый аплоад: %s (ваш: %s)',
	'TORRENT_REQRATIO_ERROR' => 'Ограничения по ратио, вы НЕ можете скачать этот торрент файл, необходимое ратио: %s (ваше: %s)',
	'TORRENT_REQUPRATIO_ERROR' => 'Ограничения по ратио и/или аплоаду, вы НЕ можете скачать этот торрент файл, необходимое ратио: %s (ваше: %s), необходимый аплоад: %s (ваш: %s)',
	'GUEST_DOWNLOADS_LIMIT' => 'Исчерпан лимит скачиваний торрент файлов в день для гостей, для скачивания нужно <a href="%s"><strong>зарегистририроваться</strong></a> и/или <a href="%s">войти</a> на форрум',
	'GUEST_DOWNLOADS_IPLIMIT' => 'Исчерпан лимит скачиваний торрент файлов в день для гостей с одного IP адреса, вы не можете скачать этот торрент файл, попробуйте позже',
	'USER_DOWNLOADS_LIMIT' => 'Исчерпан лимит скачиваний торрент файлов в день, вы не можете скачать этот торрент файл, попробуйте позже',
	'USER_DOWNLOADS_IPLIMIT' => 'Исчерпан лимит скачиваний торрент файлов в день с одного IP адреса, вы не можете скачать этот торрент файл, попробуйте позже',
	'USER_RESTRICT_DOWNLOADS_LIMIT' => 'Ограничение на количество скачиваемых торрент файлов в день (<strong>%d</strong>), вы не можете скачать этот торрент файл, попробуйте позже',
	'USER_WAIT_TIME' => 'Ограничение на время ожидания для скачивания торрент файлов (<strong>%s</strong>), вы не можете скачать этот торрент файл, попробуйте позже',
	'TORRENT_MAGNET_DLINK'	=> 'Откройте <a href="%1$s"><strong>данную ссылку</strong></a> в своём торрент-клиенте',
	'TORRENT_HASH_DLINK'	=> 'Для скачивания торрента используйте данный <strong>%s</strong> хэш',
	'TRACKER_RETURN_BACK' => '<br /><br /><a onclick="history.back(); return false;" href="#">Вернуться назад</a>',
	'DOWNLOAD_NOT_ALLOWED' => 'Вы не можете скачать этот файл',

	//search.php
	'ONLY_TRACKERS' => 'В форумах трекерах',
	'ONLY_TRACKERS_EXPLAIN' => 'Искать только в форумах трекерах',
	'POST_TORRENT' => 'Искать',
	'POST_TORRENT_ALL' => 'Везде',
	'POST_TORRENT_TORRENT' => 'В торрентах',
	'POST_TORRENT_COMMENT' => 'В комментариях',

	'UCP_XBTBB3CKER_TRACKER_DATA' => 'Трекер',
	'UCP_XBTBB3CKER_TORRENT_DATA' => 'Торренты',
	'UCP_XBTBB3CKER_TRACKER_SETTINGS'	=> 'Настройки трекера',
	'UCP_XBTBB3CKER_TRACKER_RTRACKURL'	=> 'Дополнительные анонс URL',

	//updates
	'MANUAL_UPDATE_190' => '
		<strong>Необходимо обновление трекера</strong><br /><br />
		1. Отключите форум<br />
		2. Сделайте бэкап базы данных<br />
		3. Скопируйте директорию install из директории /ext/ppk/xbtbb3cker/contrib/updates/ в корневую директорию форума<br />
		4. Запустите http://адрес_трекера/install/install_update_1.9.0.php, нажмите на ссылку <u>Start update</u> (не обновляйте страницу), ждите появления ссылки <u>Next >></u>, нажмите её, дожидайтесь каждый раз её появления и нажимайте до появления надписи <u>Completed</u><br />
		5. Удалите директорию /install/
	',

));
