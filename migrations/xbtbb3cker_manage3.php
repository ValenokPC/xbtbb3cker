<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2016 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\migrations;

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

class xbtbb3cker_manage3 extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['xbtbb3cker_version']) && version_compare($this->config['xbtbb3cker_version'], '1.9.0', '>=');
	}

	static public function depends_on()
	{
		return array('\ppk\xbtbb3cker\migrations\xbtbb3cker_manage2');
	}

	public function update_schema()
	{
		return array(
			'add_tables' => array(
				$this->table_prefix . 'tracker_rtrackers' => array(
					'COLUMNS' => array(
						'id' => array('INT:11', null, 'auto_increment'),
						'rtracker_url' => array('VCHAR_UNI:255', ''),
						'rtracker_md5' => array('VCHAR_UNI:32', ''),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
						'rtrack' => array('INDEX', array('rtracker_md5')),
					),
				),
			),
		);
	}

	public function update_data()
	{
		return array(

			array('config.update', array('xbtbb3cker_version', '1.9.6')),

			array('custom', array(array($this, 'modify_tables'))),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_tables' => array(
				$this->table_prefix . 'tracker_rtrackers',
			)
		);
	}

	public function revert_data()
	{
		return array(

		);

	}

	public function modify_tables()
	{

		$sql="SELECT COUNT(*) rows FROM `{$this->table_prefix}tracker_rtrack`";
		$result=$this->db->sql_query($sql);
		$rows=(int) $this->db->sql_fetchfield('rows');
		$this->db->sql_freeresult($result);

		if($rows)
		{
			$this->db->sql_query("INSERT IGNORE INTO `{$this->table_prefix}config` (config_name, config_value, is_dynamic) VALUES('ppkbb_manual_update', '190', '1')");
		}
		else
		{
			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` ADD `rtracker_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' AFTER `forb_type`, ADD INDEX (`rtracker_id`)";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` ADD `rtracker_type` ENUM('s','u','t') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT 't' AFTER `rtracker_id`";
			$result=$this->db->sql_query($sql);
			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` ADD `user_torrent_zone` INT(11) UNSIGNED NOT NULL DEFAULT '0' AFTER `rtracker_type`, ADD INDEX (`user_torrent_zone`)";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` CHANGE `forb_type` `forb_type` ENUM('r','s','i','') CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT ''";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` CHANGE `rtrack_remote` `rtracker_remote` TINYINT(1) NOT NULL DEFAULT '0'";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` CHANGE `rtrack_forb` `rtracker_forb` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0'";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` CHANGE `rtrack_enabled` `rtracker_enabled` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1'";
			$result=$this->db->sql_query($sql);

			$sql="SELECT * FROM `{$this->table_prefix}tracker_rtrack` GROUP BY rtrack_url";
			$result=$this->db->sql_query($sql);
			while($row=$this->db->sql_fetchrow($result))
			{
				if(strlen($row['rtrack_url']) < 256)
				{
					$sql="INSERT INTO `{$this->table_prefix}tracker_rtrackers` (id, rtracker_url) VALUES(NULL, '{$row['rtrack_url']}')";
					$result2=$this->db->sql_query($sql);
				}
			}
			$this->db->sql_freeresult($result);

			$sql="UPDATE `{$this->table_prefix}tracker_rtrackers` SET rtracker_md5=MD5(rtracker_url)";
			$result=$this->db->sql_query($sql);

			$sql="UPDATE `{$this->table_prefix}tracker_rtrack` SET rtracker_type='t', user_torrent_zone=torrent, forb_type='', rtracker_enabled=1 WHERE torrent!=0";
			$result=$this->db->sql_query($sql);

			$sql="UPDATE `{$this->table_prefix}tracker_rtrack` SET rtracker_type='u', user_torrent_zone=zone_id, forb_type='', rtracker_remote=0 WHERE zone_id!=0 AND rtrack_user=1";
			$result=$this->db->sql_query($sql);

			$sql="UPDATE `{$this->table_prefix}tracker_rtrack` SET rtracker_type='s', user_torrent_zone=zone_id, forb_type='', rtracker_remote=0 WHERE zone_id!=0 AND rtrack_user=0 AND torrent=0";
			$result=$this->db->sql_query($sql);

			$sql="UPDATE `{$this->table_prefix}tracker_rtrack` SET rtracker_type='s', user_torrent_zone=0 WHERE zone_id=0 AND rtrack_user=0 AND torrent=0";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` DROP `torrent`";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` DROP `zone_id`";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` DROP `rtrack_user`";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` DROP INDEX `rtrack_en`";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` ADD `rtracker_md5` VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '' AFTER `user_torrent_zone`, ADD INDEX (`rtracker_md5`)";
			$result=$this->db->sql_query($sql);

			$sql="UPDATE `{$this->table_prefix}tracker_rtrack` SET rtracker_md5=MD5(rtrack_url)";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` DROP `id`";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` DROP `rtrack_url`";
			$result=$this->db->sql_query($sql);

			$sql="UPDATE `{$this->table_prefix}tracker_rtrack` a LEFT JOIN `{$this->table_prefix}tracker_rtrackers` b ON a.rtracker_md5 = b.rtracker_md5 SET rtracker_id = b.id";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rtrack` DROP `rtracker_md5`";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE `{$this->table_prefix}tracker_rannounces` DROP INDEX `track`";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE  `{$this->table_prefix}tracker_rannounces` ADD INDEX (  `tracker` )";
			$result=$this->db->sql_query($sql);

			$sql="ALTER TABLE  `{$this->table_prefix}tracker_rannounces` ADD INDEX (  `torrent` )";
			$result=$this->db->sql_query($sql);
		}
	}

}
