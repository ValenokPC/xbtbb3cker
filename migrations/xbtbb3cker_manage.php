<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2015 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\migrations;

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

class xbtbb3cker_manage extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['xbtbb3cker_version']);
	}

	public function update_schema()
	{
		return array(
			'add_columns' => array(
				$this->table_prefix . 'forums' => array(
					'forum_tracker' => array('TINT:1', '0'),
					'forum_torrents' => array('UINT:8', '0'),
					'forum_comments' => array('UINT:8', '0'),
				),
				$this->table_prefix . 'posts' => array(
					'post_torrent' => array('TINT:1', '0'),
					'post_poster' => array('UINT:3', '0'),
				),
				$this->table_prefix . 'topics' => array(
					'torrent_status' => array('TINT:2', '0'),
					'status_author' => array('UINT:8', '0'),
					'status_dt' => array('UINT:11', '0'),
					'status_reason' => array('VCHAR:255', ''),
					'torrent_reqratio' => array('PDECIMAL', '0.000'),
					'torrent_requpload' => array('BINT', '0'),
				),
				$this->table_prefix . 'attachments' => array(
					'i_poster' => array('TINT:1', '0'),
				),
				$this->table_prefix . 'users' => array(
					'user_torrents' => array('UINT:8', '0'),
					'user_comments' => array('UINT:8', '0'),
					'user_tracker_data' => array('VCHAR:128', ''),
					'user_tracker_options' => array('VCHAR:64', ''),
					'user_trestricts' => array('VCHAR:255', ''),
					'user_tracker_bookmarks' => array('TINT:1', '0'),
				)
			),
			'add_index' => array(
				$this->table_prefix . 'posts' => array(
					'post_poster' => array('post_poster'),
				),
				$this->table_prefix . 'topics' => array(
					'torrent_status' => array('torrent_status'),
				),
				$this->table_prefix . 'attachments' => array(
					'i_poster' => array('i_poster'),
				),
			),
			'add_tables' => array(
				$this->table_prefix . 'tracker_cron' => array(
					'COLUMNS' => array(
						'id' => array('INT:10', null, 'auto_increment'),
						'`type`' => array('VCHAR_UNI:32', ''),
						'`data`' => array('TEXT_UNI', ''),
						'forum_id' => array('UINT:8', '0'),
						'topic_id' => array('UINT:8', '0'),
						'`status`' => array('TINT:1', '0'),
						'added' => array('UINT:11', '0'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
					),
				),
				$this->table_prefix . 'tracker_downloads' => array(
					'COLUMNS' => array(
						'id' => array('INT:10', null, 'auto_increment'),
						'downloader_id' => array('UINT:8', '0'),
						'dl_time' => array('UINT:11', '0'),
						'dl_ip' => array('VCHAR_UNI:15', '0'),
						'attach_id' => array('UINT:8', '0'),
						'post_msg_id' => array('UINT:8', '0'),
						'guests' => array('TINT:1', '0'),
						'dl_date' => array('UINT:8', '0'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
						'downlo' => array('INDEX', array('downloader_id')),
						'attach' => array('INDEX', array('attach_id')),
						'post_m' => array('INDEX', array('post_msg_id')),
						'dl_dat' => array('INDEX', array('dl_date')),
					),
				),
				$this->table_prefix . 'tracker_files' => array(
					'COLUMNS' => array(
						'id' => array('INT:11', null, 'auto_increment'),
						'torrent' => array('UINT:10', '0'),
						'filename' => array('VCHAR_UNI:255', ''),
						'size' => array('BINT', '0'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
						'torrent' => array('INDEX', array('torrent')),
					),
				),
				$this->table_prefix . 'tracker_rannounces' => array(
					'COLUMNS' => array(
						'id' => array('INT:10', null, 'auto_increment'),
						'torrent' => array('UINT:10', '0'),
						'tracker' => array('UINT:11', '0'),
						'next_announce' => array('UINT:11', '0'),
						'next_scrape' => array('UINT:11', '0'),
						'a_message' => array('VCHAR_UNI:255', ''),
						's_message' => array('VCHAR_UNI:255', ''),
						'err_count' => array('USINT', '0'),
						'a_interval' => array('UINT:6', '0'),
						'seeders' => array('UINT:10', '0'),
						'leechers' => array('UINT:10', '0'),
						'times_completed' => array('UINT:10', '0'),
						'locked' => array('TINT:1', '0'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
						'track' => array('UNIQUE', array('tracker', 'torrent')),
					),
				),
				$this->table_prefix . 'tracker_rtrack' => array(
					'COLUMNS' => array(
						'id' => array('INT:10', null, 'auto_increment'),
						'zone_id' => array('INT:8', '0'),
						'rtrack_url' => array('TEXT_UNI', ''),
						'rtrack_user' => array('TINT:1', '0'),
						'rtrack_remote' => array('TINT:1', '0'),
						'torrent' => array('UINT:10', '0'),
						'rtrack_forb' => array('TINT:1', '0'),
						'rtrack_enabled' => array('TINT:1', '0'),
						'forb_type' => array('VCHAR_UNI:1', 's'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
						'zone_id' => array('INDEX', array('zone_id')),
						'rtrack_us' => array('INDEX', array('rtrack_user')),
						'torrent' => array('INDEX', array('torrent')),
						'rtrack_en' => array('INDEX', array('rtrack_enabled')),
					),
				),
				$this->table_prefix . 'tracker_images' => array(
					'COLUMNS' => array(
						'image_id' => array('INT:10', null, 'auto_increment'),
						'post_msg_id' => array('UINT:8', '0'),
						'topic_id' => array('UINT:8', '0'),
						'poster_id' => array('UINT:8', '0'),
						'i_width' => array('UINT:6', '0'),
						'i_height' => array('UINT:6', '0'),
						'i_poster' => array('TINT:1', '0'),
						'real_filename' => array('VCHAR_UNI:255', ''),
						'extension' => array('VCHAR_UNI:100', ''),
						'mimetype' => array('VCHAR_UNI:100', ''),
						'filesize' => array('UINT:20', '0'),
						'filetime' => array('UINT:11', '0'),
					),
					'PRIMARY_KEY' => array('image_id'),
					'KEYS' => array(
						'post_msg' => array('INDEX', array('post_msg_id')),
						'topic_id' => array('INDEX', array('topic_id')),
						'poster_id' => array('INDEX', array('poster_id')),
						'i_poster' => array('INDEX', array('i_poster')),
					),
				),
				$this->table_prefix . 'tracker_statuses' => array(
					'COLUMNS' => array(
						'id' => array('INT:8', null, 'auto_increment'),
						'status_id' => array('TINT:2', '0'),
						'status_reason' => array('VCHAR_UNI:255', ''),
						'status_mark' => array('VCHAR_UNI:255', ''),
						'def_forb' => array('TINT:1', '0'),
						'def_notforb' => array('TINT:1', '0'),
						'author_candown' => array('TINT:1', '0'),
						'guest_cantdown' => array('TINT:1', '0'),
						'status_enabled' => array('TINT:1', '0'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
						'id' => array('UNIQUE', array('status_id')),
					),
				),
				$this->table_prefix . 'tracker_trestricts' => array(
					'COLUMNS' => array(
						'`id`' => array('INT:10', null, 'auto_increment'),
						'user_ratio' => array('VCHAR_UNI:7', '0.000'),
						'user_upload' => array('BINT', '0'),
						'user_download' => array('BINT', '0'),
						'can_leech' => array('TINT:1', '0'),
						'wait_time' => array('BINT', '0'),
						'peers_limit' => array('USINT', '0'),
						'torrents_limit' => array('USINT', '0'),
						'trestrict_enabled' => array('TINT:1', '0'),
						'include_torrent' => array('TINT:1', '0'),
						'days_limit' => array('USINT', '0'),
					),
					'PRIMARY_KEY' => array('`id`'),
					'KEYS' => array(
					),
				),
				$this->table_prefix . 'tracker_bookmarks' => array(
					'COLUMNS' => array(
						'`id`' => array('INT:10', null, 'auto_increment'),
						'user_id' => array('UINT:8', '0'),
						'attach_id' => array('UINT:8', '0'),
						'post_msg_id' => array('UINT:8', '0'),
						'add_date' => array('UINT:11', '0'),
					),
					'PRIMARY_KEY' => array('`id`'),
					'KEYS' => array(
						'user_i' => array('UNIQUE', array('user_id', 'attach_id')),
						'post_m' => array('INDEX', array('post_msg_id')),
					),
				),
				'xbt_announce_log' => array(
					'COLUMNS' => array(
						'id' => array('INT:11', null, 'auto_increment'),
						'ipa' => array('UINT:10', '0'),
						'`port`' => array('INT:11', '0'),
						'`event`' => array('INT:11', '0'),
						'info_hash' => array('VARBINARY', ''),
						'peer_id' => array('VARBINARY', ''),
						'downloaded' => array('BINT', '0'),
						'left0' => array('BINT', '0'),
						'uploaded' => array('BINT', '0'),
						'uid' => array('INT:11', '0'),
						'mtime' => array('INT:11', '0'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
					),
				),
				'xbt_config' => array(
					'COLUMNS' => array(
						'`name`' => array('VCHAR:255', ''),
						'`value`' => array('VCHAR:255', ''),
					),
					'PRIMARY_KEY' => array('name'),
					'KEYS' => array(
					),
				),
				/*'xbt_deny_from_clients' => array(
					'COLUMNS' => array(
						'id' => array('INT:8', null, 'auto_increment'),
						'peer_id' => array('CHAR:20', ''),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
					),
				),
				'xbt_deny_from_hosts' => array(
					'COLUMNS' => array(
						'id' => array('INT:8', null, 'auto_increment'),
						'`begin`' => array('UINT:10', '0'),
						'`end`' => array('UINT:10', '0'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
					),
				),*/
				'xbt_files' => array(
					'COLUMNS' => array(
						'fid' => array('INT:11', '0'),
						'info_hash' => array('VARBINARY', ''),
						'leechers' => array('UINT:11', '0'),
						'seeders' => array('UINT:11', '0'),
						'completed' => array('UINT:11', '0'),
						'flags' => array('UINT:11', '0'),
						'mtime' => array('UINT:11', '0'),
						'ctime' => array('UINT:11', '0'),
						'post_msg_id' => array('UINT:8', '0'),
						'topic_id' => array('UINT:8', '0'),
						'poster_id' => array('UINT:8', '0'),
						'numfiles' => array('UINT:8', '0'),
						'size' => array('BINT', '0'),
						'private' => array('TINT:1', '0'),
// 						'ip' => array('VCHAR_UNI:15', ''),
// 						'lastseed' => array('UINT:11', '0'),
// 						'lastleech' => array('UINT:11', '0'),
						'forum_id' => array('UINT:8', '0'),
						'lastcleanup' => array('UINT:11', '0'),
						'rem_seeders' => array('UINT:11', '0'),
						'rem_leechers' => array('UINT:11', '0'),
						'rem_times_completed' => array('UINT:11', '0'),
						'lastremote' => array('UINT:11', '0'),
					),
					'PRIMARY_KEY' => array('fid'),
					'KEYS' => array(
						'info_hash' => array('UNIQUE', array('info_hash')),
						'topic_id' => array('INDEX', array('topic_id')),
						'poster_id' => array('INDEX', array('poster_id')),
						'forum_id' => array('INDEX', array('forum_id')),
						'post_msg_id' => array('INDEX', array('post_msg_id')),
					),
				),
				'xbt_files_users' => array(
					'COLUMNS' => array(
						'fid' => array('UINT:11', '0'),
						'uid' => array('UINT:11', '0'),
						'active' => array('TINT:4', '0'),
						'announced' => array('UINT:11', '0'),
						'completed' => array('UINT:11', '0'),
						'downloaded' => array('BINT', '0'),
						'`left`' => array('BINT', '0'),
						'uploaded' => array('BINT', '0'),
						'mtime' => array('UINT:11', '0'),
						'down_rate' => array('UINT:10', '0'),
						'up_rate' => array('UINT:10', '0'),
					),
					'KEYS' => array(
						'fid' => array('UNIQUE', array('fid', 'uid')),
						'uid' => array('INDEX', array('uid')),
					),
				),
				'xbt_scrape_log' => array(
					'COLUMNS' => array(
						'id' => array('INT:11', null, 'auto_increment'),
						'ipa' => array('UINT:10', '0'),
						'uid' => array('INT:11', '0'),
						'mtime' => array('INT:11', '0'),
					),
					'PRIMARY_KEY' => array('id'),
					'KEYS' => array(
					),
				),
				'xbt_users' => array(
					'COLUMNS' => array(
						'uid' => array('INT:11', '0'),
						'can_leech' => array('TINT:4', '1'),
						'wait_time' => array('INT:11', '0'),
						'peers_limit' => array('INT:11', '0'),
						'torrents_limit' => array('INT:11', '0'),
						'torrent_pass' => array('CHAR:32', ''),
						'torrent_pass_version' => array('INT:11', '0'),
						'downloaded' => array('BINT', '0'),
						'uploaded' => array('BINT', '0'),
					),
					'PRIMARY_KEY' => array('uid'),
					'KEYS' => array(
					),
				),
			),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_columns' => array(
				$this->table_prefix . 'forums' => array(
					'forum_tracker',
					'forum_torrents' ,
					'forum_comments',
				),
				$this->table_prefix . 'posts' => array(
					'post_torrent',
					'post_poster',
				),
				$this->table_prefix . 'topics' => array(
					'torrent_status',
					'status_author',
					'status_dt',
					'status_reason',
					'torrent_reqratio',
					'torrent_requpload',
				),
				$this->table_prefix . 'attachments' => array(
					'i_poster',
				),
				$this->table_prefix . 'users' => array(
					'user_torrents',
					'user_comments',
					'user_tracker_data',
					'user_tracker_options',
					'user_trestricts',
					'user_tracker_bookmarks',
				)
			),
			'drop_index' => array(
				$this->table_prefix . 'posts' => array(
					'post_poster',
				),
				$this->table_prefix . 'topics' => array(
					'torrent_status',
				),
				$this->table_prefix . 'attachments' => array(
					'i_poster',
				)
			),
			'drop_tables' => array(
				$this->table_prefix . 'tracker_cron',
				$this->table_prefix . 'tracker_downloads',
				$this->table_prefix . 'tracker_files',
				$this->table_prefix . 'tracker_rannounces',
				$this->table_prefix . 'tracker_rtrack',
				$this->table_prefix . 'tracker_images',
				$this->table_prefix . 'tracker_statuses',
				$this->table_prefix . 'tracker_trestricts',
				$this->table_prefix . 'tracker_bookmarks',
				'xbt_announce_log',
				'xbt_config',
				//'xbt_deny_from_clients',
				//'xbt_deny_from_hosts',
				'xbt_files',
				'xbt_files_users',
				'xbt_scrape_log',
				'xbt_users',
			)
		);
	}

	public function update_data()
	{

		$sql="INSERT IGNORE INTO xbt_config (`name`, `value`) VALUES
			('query_log', ''),
			('pid_file', 'xbt.pid'),
			('log_scrape', '0'),
			('announce_interval', '1800'),
			('log_announce', '0'),
			('log_access', '0'),
			('gzip_scrape', '1'),
			('full_scrape', '1'),
			('daemon', '1'),
			('clean_up_interval', '3600'),
			('auto_register', '0'),
			('offline_message', ''),
			('anonymous_scrape', '0'),
			('anonymous_announce', '0'),
			('scrape_interval', '900'),
			('read_config_interval', '60'),
			('read_db_interval', '60'),
			('write_db_interval', '15'),
			('redirect_url', ''),
			('listen_ipa', '0.0.0.0'),
			('table_announce_log', 'xbt_announce_log'),
			('table_scrape_log', 'xbt_scrape_log'),
			('table_files', 'xbt_files'),
			('table_files_users', 'xbt_files_users'),
			('table_users', 'xbt_users'),
			('listen_port', '2710'),
			('debug', '0')";
		$this->db->sql_query($sql);

		$sql="INSERT IGNORE INTO {$this->table_prefix}tracker_statuses (`status_id`, `status_reason`, `status_mark`, `def_forb`, `def_notforb`, `author_candown`, `status_enabled`) VALUES
			(0, 'WO_STATUS_REASON', 'WO_STATUS_MARK', 0, 0, 0, 1),
			(1, 'FORBIDDEN_STATUS_REASON', 'FORBIDDEN_STATUS_MARK', 0, 0, 0, 1),
			(2, 'FORMALIZATION_STATUS_REASON', 'FORMALIZATION_STATUS_MARK', 0, 0, 0, 1),
			(3, 'REPEAT_STATUS_REASON', 'REPEAT_STATUS_MARK', 0, 0, 0, 1),
			(4, 'ABSORBED_STATUS_REASON', 'ABSORBED_STATUS_MARK', 0, 0, 0, 1),
			(5, 'NONCHECKED_STATUS_REASON', 'NONCHECKED_STATUS_MARK', 0, 1, 1, 1),
			(6, 'COPYRIGHT_STATUS_REASON', 'COPYRIGHT_STATUS_MARK', 0, 0, 0, 1),
			(-1, 'UPDATED_STATUS_REASON', 'UPDATED_STATUS_MARK', 0, 0, 0, 1),
			(-2, 'CHECKED_STATUS_REASON', 'CHECKED_STATUS_MARK', 0, 0, 0, 1),
			(-3, 'NEW_STATUS_REASON', 'NEW_STATUS_MARK', 1, 0, 0, 1)";
		$this->db->sql_query($sql);

		return array(

			// Add new config vars
			array('config.add', array('ppkbb_xcquery_log', '')),
			array('config.add', array('ppkbb_xcpid_file', 'xbt.pid')),
			array('config.add', array('ppkbb_xclog_scrape', '0')),
			array('config.add', array('ppkbb_xcannounce_interval', '1800')),
			array('config.add', array('ppkbb_xclog_announce', '0')),
			array('config.add', array('ppkbb_xclog_access', '0')),
			array('config.add', array('ppkbb_xcgzip_scrape', '1')),
			//array('config.add', array('ppkbb_xcgzip_debug', '1')),
			array('config.add', array('ppkbb_xcfull_scrape', '1')),
			array('config.add', array('ppkbb_xcdaemon', '1')),
			array('config.add', array('ppkbb_xcclean_up_interval', '3600')),
			array('config.add', array('ppkbb_xcauto_register', '0')),
			array('config.add', array('ppkbb_xcoffline_message', '')),
			array('config.add', array('ppkbb_xcanonymous_scrape', '0')),
			array('config.add', array('ppkbb_xcanonymous_announce', '0')),
			array('config.add', array('ppkbb_xcscrape_interval', '900')),
			array('config.add', array('ppkbb_xcread_config_interval', '60')),
			array('config.add', array('ppkbb_xcread_db_interval', '60')),
			array('config.add', array('ppkbb_xcwrite_db_interval', '15')),
			array('config.add', array('ppkbb_xcredirect_url', '')),
			array('config.add', array('ppkbb_xclisten_ipa', '0.0.0.0')),
			array('config.add', array('ppkbb_xctable_announce_log', 'xbt_announce_log')),
			array('config.add', array('ppkbb_xctable_scrape_log', 'xbt_scrape_log')),
			array('config.add', array('ppkbb_xctable_files', 'xbt_files')),
			array('config.add', array('ppkbb_xctable_files_users', 'xbt_files_users')),
			array('config.add', array('ppkbb_xctable_users', 'xbt_users')),
			array('config.add', array('ppkbb_xclisten_port', '2710')),
			array('config.add', array('ppkbb_xcdebug', '0')),

			array('config.add', array('ppkbb_forb_extpostscr', '')),//text
			array('config.add', array('ppkbb_forb_extpostscr_trueexclude', '1')),
			array('config.add', array('ppkbb_topdown_torrents_options', '1 1 1 1 1 slide 0 1 1')),
			array('config.add', array('ppkbb_tccleanup_interval', '3600')),
// 			array('config.add', array('ppkbb_dead_time', '43200')),
			array('config.add', array('ppkbb_tcclean_place', '0 1 1 1 0 0')),
			array('config.add', array('ppkbb_tcratio_start', '0')),
			array('config.add', array('ppkbb_tcenable_rannounces', '1 1 1 1 1')),
			array('config.add', array('ppkbb_tcrannounces_options', '1800 1800 50 75 5 1 0.1 1 0 uTorrent/1820 -UT1820-')),
			array('config.add', array('ppkbb_announce_url', '')),
			array('config.add', array('ppkbb_max_screenshots', '0 5')),
			array('config.add', array('ppkbb_max_posters', '0 1')),
			array('config.add', array('ppkbb_max_torrents', '1 1')),
			array('config.add', array('ppkbb_tprivate_flag', '0')),
			array('config.add', array('ppkbb_torrent_statvt', '1 1 1 1 1 1 1 1 1 1 1')),
			array('config.add', array('ppkbb_clean_snatch', '0')),
			array('config.add', array('ppkbb_clear_logs', '0')),
			array('config.add', array('ppkbb_reset_ratio', '0')),
			array('config.add', array('ppkbb_append_tfile', '[{DOMAIN}]_{FILENAME}')),
			array('config.add', array('ppkbb_rtrack_enable', '3 3 0')),
			array('config.add', array('ppkbb_trclear_files', '0')),
			array('config.add', array('ppkbb_trclear_snatched', '0')),
			array('config.add', array('ppkbb_trclear_torrents', '0')),
			array('config.add', array('ppkbb_ipreg_countrestrict', '0 0')),
			array('config.add', array('ppkbb_torrent_downlink', '1 1 1')),
			array('config.add', array('ppkbb_poll_options', "0 TRACKER_POLL_QUEST\nTRACKER_POLL_ANSW5\nTRACKER_POLL_ANSW4\nTRACKER_POLL_ANSW3\nTRACKER_POLL_ANSW2\nTRACKER_POLL_ANSW1 1")),
			array('config.add', array('ppkbb_tcdef_statuses', '-3 5')),//text
			array('config.add', array('ppkbb_tcauthor_candown', '5')),
			array('config.add', array('ppkbb_tcguest_cantdown', '')),

			array('config.add', array('ppkbb_feed_enable', '0')),
			array('config.add', array('ppkbb_feed_overall_trackers', '0')),
			array('config.add', array('ppkbb_feed_torrents', '0')),
			array('config.add', array('ppkbb_feed_comments', '0')),
			array('config.add', array('ppkbb_feed_overall', '0')),
			array('config.add', array('ppkbb_feed_forum', '0')),
			array('config.add', array('ppkbb_feed_topic', '0')),
			array('config.add', array('ppkbb_feed_torrents_new', '0')),
			array('config.add', array('ppkbb_feed_torrents_active', '0')),
			array('config.add', array('ppkbb_feed_enblist', '')),
			array('config.add', array('ppkbb_feed_trueenblist', '1')),
			array('config.add', array('ppkbb_feed_torrents_limit', '10')),
			array('config.add', array('ppkbb_feed_comments_limit', '20')),
			array('config.add', array('ppkbb_feed_downloads', '0')),

			array('config.add', array('ppkbb_tracker_top', '10 3600')),
			array('config.add', array('ppkbb_tfile_annreplace', '1 5 0')),
			array('config.add', array('ppkbb_cron_options', '3600 3600 1 60 10')),
			array('config.add', array('ppkbb_tccron_jobs', '3600 3600')),
			array('config.add', array('ppkbb_fixu_list', '0')),
			array('config.add', array('ppkbb_topdown_torrents', '0 0 0 20 100% 150 3600 1 10 1 0 0')),
			array('config.add', array('ppkbb_torr_blocks', '1 3 2 1 1 1 64 200 200 1 1')),
			array('config.add', array('ppkbb_trclear_rannounces', '0')),
			array('config.add', array('ppkbb_trclear_trtrack', '0')),
			array('config.add', array('ppkbb_trclear_urtrack', '0')),
			array('config.add', array('ppkbb_torrent_statml', '1 1 1 1 1 1 1 1 1 1 1')),
			array('config.add', array('ppkbb_search_astracker', '1')),
			array('config.add', array('ppkbb_noticedisclaimer_blocks', '0 0 0 0 0 0')),
			array('config.add', array('ppkbb_disable_fpquote', '1')),
			array('config.add', array('ppkbb_trclear_cronjobs', '0')),
			array('config.add', array('ppkbb_max_extposters', '0 0 1 10 10 2160 3840 5 0 0')),
			array('config.add', array('ppkbb_max_extscreenshots', '0 0 5 10 10 3840 2160 5 0 0')),
			array('config.add', array('ppkbb_topdown_torrents_exclude', '')),//text
			array('config.add', array('ppkbb_topdown_torrents_trueexclude', '1')),
			array('config.add', array('ppkbb_extposters_exclude', '')),//text
			array('config.add', array('ppkbb_extposters_trueexclude', '1')),
			array('config.add', array('ppkbb_extscreenshots_exclude', '')),//text
			array('config.add', array('ppkbb_extscreenshots_trueexclude', '1')),
			array('config.add', array('ppkbb_cssjs_cache', '1')),
			array('config.add', array('ppkbb_addit_options', '1 2 1')),
			array('config.add', array('ppkbb_mua_countlist', '100')),
			array('config.add', array('ppkbb_postscr_opentype', '1 1')),
			array('config.add', array('num_torrents', '0')),
			array('config.add', array('num_comments', '0')),
			array('config.add', array('ppkbb_cron_last_cleanup', '0', true)),
			array('config.add', array('ppkbb_display_trstat', '1 1800')),
			array('config.add', array('ppkbb_total_up_down', '')),
			array('config.add', array('ppkbb_total_sup_sdown', '')),
			array('config.add', array('ppkbb_total_seed_leech', '')),
			array('config.add', array('ppkbb_total_tdown_tup', '')),
			array('config.add', array('ppkbb_total_udown_uup', '')),
			array('config.add', array('ppkbb_total_peers_size', '')),
			array('config.add', array('ppkbb_total_remseed_remleech', '')),
			array('config.add', array('ppkbb_total_complet_remcomplet', '')),
			array('config.add', array('ppkbb_last_stattime', '0')),
			array('config.add', array('ppkbb_tstatus_notify', '0 0')),
			array('config.add', array('ppkbb_tstatus_salt', '', true)),
			array('config.add', array('ppkbb_trestricts_options', '0 0 3600 0 0 0 0 0')),
			array('config.add', array('ppkbb_tracker_bookmarks', '0 0')),

			array('config.add', array('xbtbb3cker_version', '1.0.2')),

			array('permission.add', array('u_attachfonly', true)),
			array('permission.add', array('u_candowntorr', true)),
			array('permission.add', array('u_candownpostscr', true)),
			array('permission.add', array('u_cansearchtr', true)),
			array('permission.add', array('u_canskiptcheck', true)),
			array('permission.add', array('u_cansetstatus', true)),
			array('permission.add', array('u_canviewtrtop', true)),
			array('permission.add', array('u_canviewtopdowntorrents', true)),
			array('permission.add', array('u_canviewvtstats', true)),
			array('permission.add', array('u_canviewmuastatr', true)),
			array('permission.add', array('u_canviewmuastatorr', true)),
			array('permission.add', array('u_canviewtrstat', true)),
			array('permission.add', array('u_canskiprequpratio', true)),
			array('permission.add', array('u_canskiptrestricts', true)),
			array('permission.add', array('u_cansetrequpratio', true)),

			array('permission.add', array('f_attachfonly', false)),
			array('permission.add', array('f_candowntorr', false)),
			array('permission.add', array('f_candownpostscr', false)),
			array('permission.add', array('f_canskiptcheck', false)),
			array('permission.add', array('f_cansetstatus', false)),
			array('permission.add', array('f_canviewvtstats', false)),
			array('permission.add', array('f_canskiprequpratio', false)),
			array('permission.add', array('f_cansetrequpratio', false)),

			array('permission.add', array('a_xbtbb3cker', true)),

			// Add new modules
			array('module.add', array('acp', 'ACP_CAT_DOT_MODS', 'ACP_XBTBB3CKER',)),

			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_xbtconfig_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_XBTCONFIG',
					'module_mode' => 'xbtbb3cker',
					'module_auth' => 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',
				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_config_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_CONFIG',
					'module_mode' => 'xbtbb3cker',
					'module_auth' => 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',
				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_rtracker_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_RTRACKER',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_rtrackurl_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_RTRACKURL',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_announcelog_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_ANNOUNCELOG',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_scrapelog_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_SCRAPELOG',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_downloadlog_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_DOWNLOADLOG',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_candc_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_CANDC',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_set_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_SET',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_rss_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_RSS',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_imgset_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_IMGSET',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_trestricts_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_TRESTRICTS',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_losttorrents_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_LOSTTORRENTS',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_userdata_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_USERDATA',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker && acl_a_user',

				),
			)),
			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_statuses_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_STATUSES',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker && acl_a_user',

				),
			)),

			array('module.add', array('ucp', 'UCP_MAIN', 'UCP_XBTBB3CKER_TRACKER_DATA')),
			array('module.add', array('ucp', 'UCP_XBTBB3CKER_TRACKER_DATA', array(
					'module_basename'	=> '\ppk\xbtbb3cker\ucp\ucp_xbtbb3cker_tracker_data_module',
					'modes'				=> array('tracker_data'),
					'module_auth'		=> 'ext_ppk/xbtbb3cker',
				),
			)),
			array('module.add', array('ucp', 'UCP_MAIN', 'UCP_XBTBB3CKER_TORRENT_DATA')),
			array('module.add', array('ucp', 'UCP_XBTBB3CKER_TORRENT_DATA', array(
					'module_basename'	=> '\ppk\xbtbb3cker\ucp\ucp_xbtbb3cker_torrent_data_module',
					'modes'				=> array('torrent_data'),
					'module_auth'		=> 'ext_ppk/xbtbb3cker',
				)
			)),
			array('module.add', array('ucp', 'UCP_PREFS', 'UCP_XBTBB3CKER_TRACKER_SETTINGS')),
			array('module.add', array('ucp', 'UCP_XBTBB3CKER_TRACKER_SETTINGS', array(
					'module_basename'	=> '\ppk\xbtbb3cker\ucp\ucp_xbtbb3cker_tracker_settings_module',
					'modes'				=> array('tracker_settings'),
					'module_auth'		=> 'ext_ppk/xbtbb3cker',
				),
			)),
			array('module.add', array('ucp', 'UCP_PREFS', 'UCP_XBTBB3CKER_TRACKER_RTRACKURL')),
			array('module.add', array('ucp', 'UCP_XBTBB3CKER_TRACKER_RTRACKURL', array(
					'module_basename'	=> '\ppk\xbtbb3cker\ucp\ucp_xbtbb3cker_tracker_rtrackurl_module',
					'modes'				=> array('tracker_rtrackurl'),
					'module_auth'		=> 'ext_ppk/xbtbb3cker',
				),
			)),

		);
	}

	public function revert_data()
	{
		return array(
			array('config.remove', array('ppkbb_xcquery_log')),
			array('config.remove', array('ppkbb_xcpid_file')),
			array('config.remove', array('ppkbb_xclog_scrape')),
			array('config.remove', array('ppkbb_xcannounce_interval')),
			array('config.remove', array('ppkbb_xclog_announce')),
			array('config.remove', array('ppkbb_xclog_access')),
			array('config.remove', array('ppkbb_xcgzip_scrape')),
			//array('config.remove', array('ppkbb_xcgzip_debug')),
			array('config.remove', array('ppkbb_xcfull_scrape')),
			array('config.remove', array('ppkbb_xcdaemon')),
			array('config.remove', array('ppkbb_xcclean_up_interval')),
			array('config.remove', array('ppkbb_xcauto_register')),
			array('config.remove', array('ppkbb_xcoffline_message')),
			array('config.remove', array('ppkbb_xcanonymous_scrape')),
			array('config.remove', array('ppkbb_xcanonymous_announce')),
			array('config.remove', array('ppkbb_xcscrape_interval')),
			array('config.remove', array('ppkbb_xcread_config_interval')),
			array('config.remove', array('ppkbb_xcread_db_interval')),
			array('config.remove', array('ppkbb_xcwrite_db_interval')),
			array('config.remove', array('ppkbb_xcredirect_url')),
			array('config.remove', array('ppkbb_xclisten_ipa')),
			array('config.remove', array('ppkbb_xctable_announce_log')),
			array('config.remove', array('ppkbb_xctable_scrape_log')),
			array('config.remove', array('ppkbb_xctable_files')),
			array('config.remove', array('ppkbb_xctable_files_users')),
			array('config.remove', array('ppkbb_xctable_users')),
			array('config.remove', array('ppkbb_xclisten_port')),
			array('config.remove', array('ppkbb_xcdebug')),

			array('config.remove', array('ppkbb_forb_extpostscr')),
			array('config.remove', array('ppkbb_forb_extpostscr_trueexclude')),
			array('config.remove', array('ppkbb_topdown_torrents_options')),
			array('config.remove', array('ppkbb_tccleanup_interval')),
// 			array('config.remove', array('ppkbb_dead_time')),
			array('config.remove', array('ppkbb_tcclean_place')),
			array('config.remove', array('ppkbb_tcratio_start')),
			array('config.remove', array('ppkbb_tcenable_rannounces')),
			array('config.remove', array('ppkbb_tcrannounces_options')),
			array('config.remove', array('ppkbb_announce_url')),
			array('config.remove', array('ppkbb_max_screenshots')),
			array('config.remove', array('ppkbb_max_posters')),
			array('config.remove', array('ppkbb_max_torrents')),
			array('config.remove', array('ppkbb_tprivate_flag')),
			array('config.remove', array('ppkbb_torrent_statvt')),
			array('config.remove', array('ppkbb_clean_snatch')),
			array('config.remove', array('ppkbb_clear_logs')),
			array('config.remove', array('ppkbb_reset_ratio')),
			array('config.remove', array('ppkbb_append_tfile')),
			array('config.remove', array('ppkbb_rtrack_enable')),
			array('config.remove', array('ppkbb_trclear_files')),
			array('config.remove', array('ppkbb_trclear_snatched')),
			array('config.remove', array('ppkbb_trclear_torrents')),
			array('config.remove', array('ppkbb_ipreg_countrestrict')),
			array('config.remove', array('ppkbb_torrent_downlink')),
			array('config.remove', array('ppkbb_poll_options')),
			array('config.remove', array('ppkbb_tcdef_statuses')),
			array('config.remove', array('ppkbb_tcauthor_candown')),
			array('config.remove', array('ppkbb_tcguest_cantdown')),
			array('config.remove', array('ppkbb_feed_enable')),
			array('config.remove', array('ppkbb_feed_overall_trackers')),
			array('config.remove', array('ppkbb_feed_torrents')),
			array('config.remove', array('ppkbb_feed_comments')),
			array('config.remove', array('ppkbb_feed_overall')),
			array('config.remove', array('ppkbb_feed_forum')),
			array('config.remove', array('ppkbb_feed_topic')),
			array('config.remove', array('ppkbb_feed_torrents_new')),
			array('config.remove', array('ppkbb_feed_torrents_active')),
			array('config.remove', array('ppkbb_feed_enblist')),
			array('config.remove', array('ppkbb_feed_trueenblist')),
			array('config.remove', array('ppkbb_feed_torrents_limit')),
			array('config.remove', array('ppkbb_feed_comments_limit')),
			array('config.remove', array('ppkbb_feed_downloads')),

			array('config.remove', array('ppkbb_tracker_top')),
			array('config.remove', array('ppkbb_tfile_annreplace')),
			array('config.remove', array('ppkbb_cron_options')),
			array('config.remove', array('ppkbb_tccron_jobs')),
			array('config.remove', array('ppkbb_fixu_list')),
			array('config.remove', array('ppkbb_topdown_torrents')),
			array('config.remove', array('ppkbb_torr_blocks')),
			array('config.remove', array('ppkbb_trclear_rannounces')),
			array('config.remove', array('ppkbb_trclear_trtrack')),
			array('config.remove', array('ppkbb_trclear_urtrack')),
			array('config.remove', array('ppkbb_torrent_statml')),
			array('config.remove', array('ppkbb_search_astracker')),
			array('config.remove', array('ppkbb_noticedisclaimer_blocks')),
			array('config.remove', array('ppkbb_disable_fpquote')),
			array('config.remove', array('ppkbb_trclear_cronjobs')),
			array('config.remove', array('ppkbb_max_extposters')),
			array('config.remove', array('ppkbb_max_extscreenshots')),
			array('config.remove', array('ppkbb_topdown_torrents_exclude')),
			array('config.remove', array('ppkbb_topdown_torrents_trueexclude')),
			array('config.remove', array('ppkbb_extposters_exclude')),
			array('config.remove', array('ppkbb_extposters_trueexclude')),
			array('config.remove', array('ppkbb_extscreenshots_exclude')),
			array('config.remove', array('ppkbb_extscreenshots_trueexclude')),
			array('config.remove', array('ppkbb_cssjs_cache')),
			array('config.remove', array('ppkbb_removeit_options')),
			array('config.remove', array('ppkbb_mua_countlist')),
			array('config.remove', array('ppkbb_postscr_opentype')),
			array('config.remove', array('num_torrents')),
			array('config.remove', array('num_comments')),
			array('config.remove', array('ppkbb_cron_last_cleanup')),
			array('config.remove', array('ppkbb_display_trstat')),
			array('config.remove', array('ppkbb_total_up_down')),
			array('config.remove', array('ppkbb_total_sup_sdown')),
			array('config.remove', array('ppkbb_total_seed_leech')),
			array('config.remove', array('ppkbb_total_tdown_tup')),
			array('config.remove', array('ppkbb_total_udown_uup')),
			array('config.remove', array('ppkbb_total_peers_size')),
			array('config.remove', array('ppkbb_total_remseed_remleech', '')),
			array('config.remove', array('ppkbb_total_complet_remcomplet', '')),
			array('config.remove', array('ppkbb_last_stattime')),
			array('config.remove', array('ppkbb_tstatus_notify')),
			array('config.remove', array('ppkbb_tstatus_salt')),
			array('config.remove', array('ppkbb_trestricts_options')),
			array('config.remove', array('ppkbb_tracker_bookmarks')),

			array('config.remove', array('xbtbb3cker_version')),

			array('permission.remove', array('u_attachfonly')),
			array('permission.remove', array('u_candowntorr')),
			array('permission.remove', array('u_candownpostscr')),
			array('permission.remove', array('u_cansearchtr')),
			array('permission.remove', array('u_canskiptcheck')),
			array('permission.remove', array('u_cansetstatus')),
			array('permission.remove', array('u_canviewtrtop')),
			array('permission.remove', array('u_canviewtopdowntorrents')),
			array('permission.remove', array('u_canviewvtstats')),
			array('permission.remove', array('u_canviewmuastatr')),
			array('permission.remove', array('u_canviewmuastatorr')),
			array('permission.remove', array('u_canviewtrstat')),
			array('permission.remove', array('u_canskiprequpratio')),
			array('permission.remove', array('u_canskiptrestricts')),
			array('permission.remove', array('u_cansetrequpratio')),

			array('permission.remove', array('f_attachfonly')),
			array('permission.remove', array('f_candowntorr')),
			array('permission.remove', array('f_candownpostscr')),
			array('permission.remove', array('f_canskiptcheck')),
			array('permission.remove', array('f_cansetstatus')),
			array('permission.remove', array('f_canviewvtstats')),
			array('permission.remove', array('f_canskiprequpratio')),
			array('permission.remove', array('f_cansetrequpratio')),

			array('permission.remove', array('a_xbtbb3cker')),

			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_xbtconfig_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_config_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_rtracker_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_rtrackurl_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_announcelog_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_scrapelog_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_downloadlog_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_candc_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_set_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_rss_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_imgset_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_trestricts_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_losttorrents_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_userdata_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_statuses_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
			array('module.remove', array('acp', 'ACP_CAT_DOT_MODS', 'ACP_XBTBB3CKER',)),

			array('module.remove', array('ucp', 'UCP_MAIN', 'UCP_XBTBB3CKER_TRACKER_DATA')),
			array('module.remove', array('ucp', 'UCP_MAIN', 'UCP_XBTBB3CKER_TORRENT_DATA')),
			array('module.remove', array('ucp', 'UCP_PREFS', 'UCP_XBTBB3CKER_TRACKER_SETTINGS')),
			array('module.remove', array('ucp', 'UCP_PREFS', 'UCP_XBTBB3CKER_TRACKER_RTRACKURL')),

		);
	}
}