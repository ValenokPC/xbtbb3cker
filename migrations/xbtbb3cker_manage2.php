<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2016 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\migrations;

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

class xbtbb3cker_manage2 extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['xbtbb3cker_version']) && version_compare($this->config['xbtbb3cker_version'], '1.7.5', '>=');
	}

	static public function depends_on()
	{
		return array('\ppk\xbtbb3cker\migrations\xbtbb3cker_manage');
	}

	public function update_schema()
	{
		return array(
			'add_tables' => array(
				$this->table_prefix . 'tracker_peers' => array(
					'COLUMNS' => array(
						'`id`' => array('INT:11', null, 'auto_increment'),
						'tid' => array('UINT:11', '0'),
						'torrent' => array('UINT:11', '0'),
						'peer_id' => array('VARBINARY', ''),
						'ip' => array('UINT:11', '0'),
						'`port`' => array('USINT', '0'),
						'uploaded' => array('BINT', '0'),
						'downloaded' => array('BINT', '0'),
						'to_go' => array('BINT', '0'),
						'startdat' => array('UINT:11', '0'),
						'last_action' => array('UINT:11', '0'),
						'userid' => array('UINT:10', '0'),
						'guests' => array('TINT:1', '0'),
						'seeder' => array('TINT:1', '0'),
						'connectable' => array('TINT:1', '0'),
						'agent' => array('VCHAR_UNI:64', ''),
					),
					'PRIMARY_KEY' => array('`id`'),
					'KEYS' => array(
						'tid' => array('INDEX', array('tid')),
						'torrent' => array('INDEX', array('torrent')),
						'userid' => array('INDEX', array('userid')),
					),
				),
			),

		);
	}

	public function update_data()
	{
		return array(

			array('config.remove', array('ppkbb_clear_peers')),
			array('config.remove', array('ppkbb_tracker_last_cleanup')),
			array('config.remove', array('ppkbb_cron_last_cleanup')),

			// Add new config vars
			array('config.add', array('ppkbb_clear_logs', '0')),
			array('config.add', array('ppkbb_phpannounce_enabled', '0')),
			array('config.add', array('ppkbb_phpannounce_url', '/tracker/announce.php')),
			array('config.add', array('ppkbb_tcgz_rewrite', '0')),
			array('config.add', array('ppkbb_tciptype', '0')),
			array('config.add', array('ppkbb_tccheck_ban', '0')),
			array('config.add', array('ppkbb_tcignore_connectable', '1')),
			array('config.add', array('ppkbb_tcmaxpeers_limit', '0')),
			array('config.add', array('ppkbb_tcmaxpeers_rewrite', '0')),
			array('config.add', array('ppkbb_tcignored_upload', '0')),
			array('config.add', array('ppkbb_logs_cleanup', '0 0')),
			array('config.add', array('ppkbb_logs_last_cleanup', '0', true)),
			array('config.add', array('ppkbb_peers_last_cleanup', '0', true)),
			array('config.add', array('ppkbb_peerstable_memory', '0')),
			array('config.add', array('ppkbb_trclear_unregtorr', '0')),
			array('config.add', array('ppkbb_phpclear_peers', '0')),
			array('config.add', array('ppkbb_cron_last_cleanup', '0', true)),

			array('config.update', array('xbtbb3cker_version', '1.8.4')),

			array('module.add', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename'	=> '\ppk\xbtbb3cker\acp\xbtbb3cker_phpannounce_module',
					'module_langname'	=> 'ACP_XBTBB3CKER_PHPANNOUNCE',
					'module_mode'	=> 'xbtbb3cker',
					'module_auth'		=> 'ext_ppk/xbtbb3cker && acl_a_xbtbb3cker && acl_a_user',

				),
			)),

			array('custom', array(array($this, 'create_table_keys'))),
		);
	}

	public function revert_schema()
	{
		return array(
			'drop_tables' => array(
				$this->table_prefix . 'tracker_peers',
			)
		);
	}

	public function revert_data()
	{
		return array(
			array('config.remove', array('ppkbb_clear_logs')),
			array('config.remove', array('ppkbb_phpannounce_enabled')),
			array('config.remove', array('ppkbb_phpannounce_url')),
			array('config.remove', array('ppkbb_tcgz_rewrite')),
			array('config.remove', array('ppkbb_tciptype')),
			array('config.remove', array('ppkbb_tccheck_ban')),
			array('config.remove', array('ppkbb_tcignore_connectable')),
			array('config.remove', array('ppkbb_tcmaxpeers_limit')),
			array('config.remove', array('ppkbb_tcmaxpeers_rewrite')),
			array('config.remove', array('ppkbb_tcignored_upload')),
			array('config.remove', array('ppkbb_logs_cleanup', '0 0')),
			array('config.remove', array('ppkbb_logs_last_cleanup')),
			array('config.remove', array('ppkbb_peers_last_cleanup')),
			array('config.remove', array('ppkbb_peerstable_memory')),
			array('config.remove', array('ppkbb_trclear_unregtorr')),
			array('config.remove', array('ppkbb_phpclear_peers')),

			array('config.remove', array('ppkbb_clear_peers')),
			array('config.remove', array('ppkbb_tracker_last_cleanup')),

			array('module.remove', array('acp', 'ACP_XBTBB3CKER', array(
					'module_basename' => '\ppk\xbtbb3cker\acp\xbtbb3cker_phpannounce_module',
					'module_mode' => 'xbtbb3cker',
				),
			)),
		);

	}

	public function create_table_keys()
	{
		$this->db->sql_query('ALTER TABLE `xbt_files` DROP PRIMARY KEY');
		$this->db->sql_query('ALTER TABLE `xbt_files` ADD `tid` INT( 11 ) NOT NULL AUTO_INCREMENT FIRST, ADD PRIMARY KEY (  `tid` )');
		$this->db->sql_query('ALTER TABLE `xbt_files` ADD UNIQUE (`fid`)');
		$this->db->sql_query('ALTER TABLE `xbt_users` ADD UNIQUE (`torrent_pass`)');

	}

}
