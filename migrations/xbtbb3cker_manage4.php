<?php

/**
*
* @package xbtBB3cker
* @copyright (c) 2016 PPK
* @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
*
*
*/

namespace ppk\xbtbb3cker\migrations;

/**
* @ignore
*/
if (!defined('IN_PHPBB'))
{
    exit;
}

class xbtbb3cker_manage4 extends \phpbb\db\migration\migration
{
	public function effectively_installed()
	{
		return isset($this->config['xbtbb3cker_version']) && version_compare($this->config['xbtbb3cker_version'], '1.9.7', '>=');
	}

	static public function depends_on()
	{
		return array('\ppk\xbtbb3cker\migrations\xbtbb3cker_manage3');
	}

	public function update_schema()
	{
		return array(
			'add_columns' => array(
				$this->table_prefix . 'topics' => array(
					'topic_torrent' => array('TINT:1', '0'),
				),
			),
			'add_index' => array(
				$this->table_prefix . 'topics' => array(
					'topic_torrent' => array('topic_torrent'),
					'topic_poster' => array('topic_poster'),
				),
				$this->table_prefix . 'posts' => array(
					'post_torrent' => array('post_torrent'),
				),
			),
		);
	}

	public function update_data()
	{
		return array(

			array('config.update', array('xbtbb3cker_version', '1.9.7')),

			array('custom', array(array($this, 'update_tables'))),

		);
	}

	public function revert_schema()
	{
		return array(
			'drop_columns' => array(
				$this->table_prefix . 'topics' => array(
					'topic_torrent',
				),
			),
			'drop_index' => array(
				$this->table_prefix . 'topics' => array(
					'topic_torrent',
					'topic_poster',
				),
				$this->table_prefix . 'posts' => array(
					'post_torrent',
				),
			),
		);
	}

	public function update_tables()
	{

		$sql="UPDATE {$this->table_prefix}topics, {$this->table_prefix}posts SET {$this->table_prefix}topics.topic_torrent = {$this->table_prefix}posts.post_torrent WHERE {$this->table_prefix}topics.topic_first_post_id = {$this->table_prefix}posts.post_id";
		$result=$this->db->sql_query($sql);

	}
}
